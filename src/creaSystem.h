/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/                                                                         

/*=========================================================================
                                                                                
  Program:   crea
  Module:    $RCSfile: creaSystem.h,v $
  Language:  C++
  Date:      $Date: 2012/11/15 10:43:26 $
  Version:   $Revision: 1.16 $
=========================================================================*/

/**
 *\file
 *\brief contains all the OS depending stuff
 */
#ifndef __creaSystem_h__
#define __creaSystem_h__

#include "creaConfigure.h"

// We try for Visual 6..????
//#pragma warning( disable : 4786)

//-----------------------------------------------------------------------------
//This is needed when compiling in debug mode
#ifdef _MSC_VER
// 'identifier' : class 'type' needs to have dll-interface to be used by
// clients of class 'type2'
#pragma warning ( disable : 4251 )
// non dll-interface class 'type' used as base for dll-interface class 'type2'
#pragma warning ( disable : 4275 )
// 'identifier' : identifier was truncated to 'number' characters in the
// debug information
#pragma warning ( disable : 4786 )
//'identifier' : decorated name length exceeded, name was truncated
#pragma warning ( disable : 4503 )
// C++ exception specification ignored except to indicate a 
// function is not __declspec(nothrow)
#pragma warning ( disable : 4290 )
// signed/unsigned mismatch
#pragma warning ( disable : 4018 )
// return type for 'identifier' is '' (ie; not a UDT or reference to UDT. Will
// produce errors if applied using infix notation
#pragma warning ( disable : 4284 )
// 'type' : forcing value to bool 'true' or 'false' (performance warning)
// //#pragma warning ( disable : 4800 )
#endif //_MSC_VER


#include <stdio.h>  // for printf, ...
#include <iostream> // for cout, endl, ...

#ifdef _MSC_VER
// Micro$oft related stuff
#pragma once
#include <conio.h>
#include <tchar.h>
#else
//#include  <curses.h> // for getch
#endif //_MSC_VER

//-----------------------------------------------------------------------------
// Micro$oft shared library related stuff
//
// all the classes that must be visible outside the crea library 
// should be defined as :
// class CREA_EXPORT ClassName 
// instead of :
// class ClassName


/*
// Useless ?
#if defined(_WIN32) 
//&& defined(BUILD_SHARED_LIBS)
  #ifdef CREA_EXPORTS2
    #define CREA_EXPORT2 export
  #else
    #define CREA_EXPORT2 
  #endif
#else
  #define CREA_EXPORT2
#endif // defined(_WIN32) && defined(BUILD_SHARED_LIBS)
*/
//#define TEST toto


#if defined(_WIN32) && defined(CREA_BUILD_SHARED)
  #ifdef CREA_EXPORT_SYMBOLS
    #define CREA_EXPORT __declspec( dllexport ) 
  #else
	#define CREA_EXPORT __declspec( dllimport )
  #endif
  #define CREA_CDECL __cdecl
#else
  #define CREA_EXPORT
  #define CREA_CDECL
#endif // defined(_WIN32) && defined(CREA_BUILD_SHARED)


#ifdef __BORLANDC__
#include <mem.h>
#endif //__BORLANDC__



// ----------------------------------------------------------------------------
// wx headers
// ----------------------------------------------------------------------------
//#include "creaWx.h"

/*
#ifdef _USE_WXWIDGETS_
#include "wx/wxprec.h"
#include <wx/datetime.h>
#ifndef WX_PRECOMP
#   include <wx/wx.h>
#endif //WX_PRECOMP

#include <wx/log.h>

#ifdef __WXGTK__
# include <locale.h>
#endif //__WXGTK__

#endif //_USE_WXWIDGETS_
*/
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Version
#include <string.h>

namespace crea
{
#define CREA_STRINGIFY(A) #A
#define CREA_STRINGIFY_SYMBOL(A) CREA_STRINGIFY(A)

  extern "C" 
  {
    CREA_EXPORT inline const std::string& CREA_CDECL GetVersion() 
    {
      static const std::string v(CREA_STRINGIFY_SYMBOL(CREA_VERSION));
      return v;
    }
  }

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/*
#ifdef _WIN32
typedef  signed char         int8_t;
typedef  signed short        int16_t;
typedef  signed int          int32_t;
typedef  unsigned char       uint8_t;
typedef  unsigned short      uint16_t;
typedef  unsigned int        uint32_t;
#else 
#include <stdint.h>
#include <inttypes.h> 
#endif
*/

#ifdef CMAKE_HAVE_STDINT_H
   #include <stdint.h>
#else
#ifdef CMAKE_HAVE_INTTYPES_H
   // Old system only have this
   #include <inttypes.h>   // For uint8_t uint16_t and uint32_t
#else
   //#include "XXX.h"
// Broken plateforms do not respect C99 and do not provide those typedef
// Special case for recent Borland compiler, comes with stdint.h
#if defined(_MSC_VER) || defined(__BORLANDC__) && (__BORLANDC__ < 0x0560)  \
                      || defined(__MINGW32__)
typedef  signed char         int8_t;
typedef  signed short        int16_t;
typedef  signed int          int32_t;
typedef  unsigned char       uint8_t;
typedef  unsigned short      uint16_t;
typedef  unsigned int        uint32_t;

/// \todo Find a clever way to deal with int64_t, uint64_t

#else
#error "Sorry your plateform is not supported"
#endif // defined(_MSC_VER) || defined(__BORLANDC__) && (__BORLANDC__ < 0x0560)  || defined(__MINGW32__)
#endif // CMAKE_HAVE_INTTYPES_H
#endif // CMAKE_HAVE_STDINT_H

// Basically for VS6 and bcc 5.5.1:
#ifndef UINT32_MAX
#define UINT32_MAX    (4294967295U)
#endif

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
namespace crea
{
 
  /// System related stuff
  struct System
  {
    /// returns true iff the program has a tty
    static int HasTTY(); 
    CREA_EXPORT static int GetAppPath(char *pname, size_t pathsize);
    CREA_EXPORT static std::string GetDllAppPath(std::string &nomdll);
    CREA_EXPORT static std::string GetDllAppPath(const char *nomdll);   
    CREA_EXPORT static std::string GetExecutablePath();
    CREA_EXPORT static void createDirectory(const char* directorypath);
  };

} // namespace crea
//-----------------------------------------------------------------------------

    // file separator
#if defined(_WIN32)
#define VALID_FILE_SEPARATOR "\\"
#define INVALID_FILE_SEPARATOR "/"
#else
#define INVALID_FILE_SEPARATOR "\\"
#define VALID_FILE_SEPARATOR "/"
#endif

#endif
