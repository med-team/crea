/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include "creaSystem.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#ifdef WIN32
	#include <windows.h> /* GetModuleFileName */
	#include <io.h>
	
#endif /* WIN32 */

#ifdef LINUX
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <errno.h> 
#endif 

#ifdef __APPLE__  /* assume this is OSX */
#include <sys/param.h>
#include <mach-o/dyld.h> /* _NSGetExecutablePath : must add -framework
CoreFoundation to link line */
#include <string.h>
# ifndef PATH_MAX
#  define PATH_MAX MAXPATHLEN
# endif
#endif /* APPLE */

#ifndef PATH_MAX
#  define PATH_MAX 2048
#endif

#if defined(WIN32)
  #include <direct.h>
#else
   #include <dirent.h>  
#endif

#include <stdlib.h>


namespace crea
{

#ifdef _WIN32
  int System::HasTTY() { return false; }
#else  
#include <unistd.h>
   int System::HasTTY() 
   { 
     return isatty(fileno(stdin));
   }
#endif

int System::GetAppPath(char *pname, size_t pathsize)
   {
#ifdef LINUX	
    /* Oddly, the readlink(2) man page says no NULL is appended. */
    /* So you have to do it yourself, based on the return value: */
    pathsize --; /* Preserve a space to add the trailing NULL */
    long result = readlink("/proc/self/exe", pname, pathsize);
    if (result > 0)
	{
		pname[result] = 0; /* add the #@!%ing NULL */
		
		if ((access(pname, 0) == 0))
			return 0; /* file exists, return OK */
		/*else name doesn't seem to exist, return FAIL (falls
		 through) */
	}
#endif /* LINUX */
    
#ifdef WIN32
    long result = GetModuleFileName(NULL, pname, pathsize);
    if (result > 0)
	{
		/* fix up the dir slashes... */
		int len = strlen(pname);
		int idx;
		for (idx = 0; idx < len; idx++)
		{
			if (pname[idx] == '\\') pname[idx] = '/';
		}
		
		for (idx = len-1; idx >=0 ; idx--)
		{
			if (pname[idx] == '/')
			{ 
				pname[idx+1] = '\0';
				idx = -1;
			}
		}
		
		if ((_access(pname, 0) == 0))
			return 0; /* file exists, return OK */
		/*else name doesn't seem to exist, return FAIL (falls
		 through) */
	}
#endif /* WIN32 */
    
#ifdef SOLARIS
    char *p = getexecname();
    if (p)
	{
		/* According to the Sun manpages, getexecname will
		 "normally" return an */
		/* absolute path - BUT might not... AND that IF it is not,
		 pre-pending */
		/* getcwd() will "usually" be the correct thing... Urgh!
		 */
		
		/* check pathname is absolute (begins with a / ???) */
		if (p[0] == '/') /* assume this means we have an
		 absolute path */
		{
			strncpy(pname, p, pathsize);
			if ((access(pname, 0) == 0))
				return 0; /* file exists, return OK */
		}
		else /* if not, prepend getcwd() then check if file
		 exists */
		{
			getcwd(pname, pathsize);
			long result = strlen(pname);
			strncat(pname, "/", (pathsize - result));
			result ++;
			strncat(pname, p, (pathsize - result));
			
			if ((access(pname, 0) == 0))
				return 0; /* file exists, return OK */
			/*else name doesn't seem to exist, return FAIL
			 (falls through) */
		}
	}
#endif /* SOLARIS */
    
#ifdef MACOSX /* assume this is OSX */
    /*
	 from http://www.hmug.org/man/3/NSModule.html
	 
	 extern int _NSGetExecutablePath(char *buf, unsigned long
	 *bufsize);
	 
	 _NSGetExecutablePath  copies  the  path  of the executable
	 into the buffer and returns 0 if the path was successfully
	 copied  in the provided buffer. If the buffer is not large
	 enough, -1 is returned and the  expected  buffer  size  is
	 copied  in  *bufsize.  Note that _NSGetExecutablePath will
	 return "a path" to the executable not a "real path" to the
	 executable.  That  is  the path may be a symbolic link and
	 not the real file. And with  deep  directories  the  total
	 bufsize needed could be more than MAXPATHLEN.
	 */
	
    int status = -1;
    char *given_path = (char*)malloc(MAXPATHLEN * 2);
    if (!given_path) return status;
    
    uint32_t npathsize = MAXPATHLEN * 2;
    long result = _NSGetExecutablePath(given_path, &npathsize);
    if (result == 0)
	{ /* OK, we got something - now try and resolve the real path...
	 */
		if (realpath(given_path, pname) != NULL)
		{
			if ((access(pname, 0) == 0))
				status = 0; /* file exists, return OK */
		}
	}
    free (given_path);
    return status;
#endif /* MACOSX */
    
    return -1; /* Path Lookup Failed */
}

std::string System::GetDllAppPath(std::string &nomdll){
	std::string path = ".";
#ifdef WIN32
	char currentPath[_MAX_PATH];
	HMODULE hand = GetModuleHandle(nomdll.c_str());
	GetModuleFileName(hand, currentPath, _MAX_PATH);

	path = currentPath;

	path = path.substr(0,path.find_last_of("\\"));
#endif
	return path;
}


std::string System::GetDllAppPath(const char *nomdll){
	std::string path = ".";
#ifdef WIN32
	char currentPath[_MAX_PATH];
	HMODULE hand = GetModuleHandle(nomdll);
	GetModuleFileName(hand, currentPath, _MAX_PATH);

	path = currentPath;

	path = path.substr(0,path.find_last_of("\\"));
#endif
	return path;
}


#if defined(_WIN32)
#define CREACONTOUR_VALID_FILE_SEPARATOR_CHAR '\\'
#else
#define CREACONTOUR_VALID_FILE_SEPARATOR_CHAR '/'
#endif	
	
	//=========================================================================
std::string System::GetExecutablePath(){
		char name[PATH_MAX];
		//EED    int err = get_app_path(name, PATH_MAX);
		int err = System::GetAppPath(name,PATH_MAX);
		if (err) 
		{
			printf("Could not determine current executable path ?  ");  
		}    
		// remove the exe name
		char *slash;
		slash = strrchr(name, CREACONTOUR_VALID_FILE_SEPARATOR_CHAR);
		if (slash)
		{
			*slash = 0;
		}
		return name;
	}
	
	void System::createDirectory(const char* directorypath){
		#ifdef WIN32
			if (CreateDirectory(directorypath, NULL) == ERROR_ALREADY_EXISTS) 
			{ 
				std::cout<<"directory already exists "<<directorypath<<std::endl;
			}else if(CreateDirectory(directorypath, NULL) == ERROR_PATH_NOT_FOUND){
				std::string error = "Directory could not be created ";
				error.append(directorypath);
				throw error.c_str();
			}
		#endif
		#ifdef LINUX
			//! include sys/types.h
			//! include sys/stat.h
			//! int mkdir(const char *path, mode_t mode);
			//! read/write/search permissions for owner and group, and with read/search permissions for others S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH
			int returnval = mkdir(directorypath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			
			if(returnval != 0){
				if(errno == EEXIST){
					std::cout<<"directory already exists "<<directorypath<<std::endl;
				}else{
					std::string error = "Directory could not be created ";
					error.append(directorypath);
					throw error.c_str();
				}
			}
		#endif
		#ifdef MACOSX
		//TODO
		#endif
	}

} // namespace crea
