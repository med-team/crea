/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/                                                                         



#include <vtkDataArrayTemplate.h>

#include <vtkCharArray.h>
#include <vtkSignedCharArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkShortArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkIntArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkLongArray.h>
#include <vtkUnsignedLongArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>

#include <vtkPointData.h>
#include <vtkTypeTraits.h>
#include <creaMessageManager.h>
namespace crea
{  
  template <class T>
  /*CREA_EXPORT*/ vtkImageData* NewVtkImageDataFromRaw( T* data, 
						    int nx, 
						    int ny,
						    int nz,
						    bool do_not_desalloc)
  {
    //    std::cout << "NV "<<nx<<" " <<ny<<" " << nz<<std::endl;
    //    std::cout <<  vtkTypeTraits<T>::SizedName() << std::endl;
    vtkImageData *image = vtkImageData::New();

    // Shouldn't we pass NumberOfScalarComponents to deal with RGB, RGBA images as well? // JPR

    image->SetNumberOfScalarComponents(1);

    int vtktype = vtkTypeTraits<T>::VTKTypeID();
    image->SetScalarType(vtktype);
    image->SetDimensions(nx, ny ,nz);
    image->SetSpacing(1, 1, 1);
    image->AllocateScalars();
    vtkDataArray* array = 0;
    switch (vtktype)
      {
      case VTK_CHAR:
	array = vtkCharArray::New(); break;
      case VTK_SIGNED_CHAR:
	array = vtkSignedCharArray::New(); break;
      case VTK_UNSIGNED_CHAR:
	array = vtkUnsignedCharArray::New(); break;
      case VTK_SHORT:
	array = vtkShortArray::New(); break;
      case VTK_UNSIGNED_SHORT:
	array = vtkUnsignedShortArray::New(); break;
      case VTK_INT:
	array = vtkIntArray::New(); break;      
      case VTK_UNSIGNED_INT:
	array = vtkUnsignedIntArray::New(); break;
      case VTK_LONG:
	array = vtkLongArray::New(); break;
      case VTK_UNSIGNED_LONG:
	array = vtkUnsignedLongArray::New(); break;
      case VTK_FLOAT:
	array = vtkFloatArray::New(); break;
      case VTK_DOUBLE:
	array = vtkDoubleArray::New(); break;
      default:
	creaGlobalError("NewVtkImageDataFromRaw : type "
		  <<vtkTypeTraits<T>::SizedName()
		  <<" non implemented");
      }
    vtkDataArrayTemplate<T>* tarray 
      		= dynamic_cast<vtkDataArrayTemplate<T>*>(array);
    array->SetNumberOfComponents( 1 );
    size_t size = (long)nx*(long)ny*(long)nz;
    // The last param of SetArray is set to 1 to keep the class from deleting the array 
    // when it cleans up or reallocates memory.
    int dndesa = 0;
    if (do_not_desalloc)
    	dndesa = 1;
    tarray->SetArray( data, size, dndesa );
    image->GetPointData( )->SetScalars( tarray );
    array->Delete( );
    return image;
  }
  
}

