/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

/*=========================================================================
                                                                                
  Program:   crea
  Module:    $RCSfile: creaException.h,v $
  Language:  C++
  Date:      $Date: 2012/11/15 10:43:26 $
  Version:   $Revision: 1.4 $
                                                                                
                                                                                 
     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.
                                                                                
=========================================================================*/


/**
 *  \file 
 *  \brief  class Exception:generic class for throwing any exception (header) 
 *
 *    Long description:
 */

/**
 *  \class crea::Exception 
 *  \brief  class Exception : generic class for throwing any exception 
 *
 *    Long description:
 */
 
#ifndef __creaException_h__
#define __creaException_h__

#include "creaSystem.h"
#include "creaMessageManager.h"
#include <exception>

namespace crea
{
  inline std::string bbGetObjectDescription() { return(""); }

  class CREA_EXPORT Exception : public std::exception
  {
  public:
    Exception(const std::string& object,
	      const std::string& source_file,
	      const std::string& message) throw()
      : mObject(object),
	mSourceFile(source_file),
	mMessage(message)
    {
       mWhatMessage = " * ERROR  : " + mMessage 
                    + " * OBJECT : " + mObject
                    + " * FILE "     + mSourceFile;
    }
    ~Exception() throw() {}
   virtual const char* what() const throw() 
   {
      return mWhatMessage.c_str();
   }

    void Print() throw()
    {
      std::cerr << "* ERROR  : " << mMessage <<std::endl; 
      int lev = crea::MessageManager::GetMessageLevel("Error");
      if (lev > 0) {
	std::cerr << "* OBJECT : " <<mObject<<std::endl;
	std::cerr << "* FILE   : " <<mSourceFile<<std::endl;
      }
    }
    const std::string& GetObject()     const { return mObject;     }
    const std::string& GetSourceFile() const { return mSourceFile; }
    const std::string& GetMessage()    const { return mMessage;    }
  private:
    std::string mObject;
    std::string mSourceFile;
    std::string mMessage;
    std::string mWhatMessage;
  };

}//namespace

#endif
