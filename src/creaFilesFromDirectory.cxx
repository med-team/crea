/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

/*=========================================================================
                                                                                
  Program:   crea
  Module:    $RCSfile: creaFilesFromDirectory.cxx,v $
  Language:  C++
  Date:      $Date: 2012/11/15 10:43:26 $
  Version:   $Revision: 1.7 $
                                                                                
   
     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.
                                                                                
=========================================================================*/


/**
 *  \file 
 *  \brief  class Exception:generic class for throwing any exception (header) 
 *
 *    Long description:
 */
#ifdef _MSC_VER
   #include <windows.h>
   #include <direct.h>
#else
   #include <dirent.h>   
   #include <sys/types.h>
#endif

#include <sys/stat.h>  //stat function

 #include "creaFilesFromDirectory.h"
 #include "creaMessageManager.h"

namespace crea
{ 

/**
 * \brief   Add a SEPARATOR to the end of the name if necessary
 * @param   pathname file/directory name to normalize 
 */
std::string DirList::NormalizePath(std::string const &pathname)
{


#ifdef _WIN32
   const char FILESEPARATOR = '\\';
#else
   const char FILESEPARATOR = '/';
#endif

	
   std::string name = pathname;
   int size = name.size();

//   if ( name[size-1] != SEPARATOR_X && name[size-1] != SEPARATOR_WIN )
   if ( name[size-1] != FILESEPARATOR )
   {
      name += FILESEPARATOR;
   }
   return name;
}  
/**
 * \brief   Explore a directory with possibility of recursion
 *          return number of files read
 * @param  dirpath   directory to explore
 * @param  recursive whether we want recursion or not
 */
int DirList::Explore(std::string const &dirpath, bool recursive)
{
   int numberOfFiles = 0;
   std::string fileName;
   std::string dirName = NormalizePath(dirpath);
#ifdef _MSC_VER
   WIN32_FIND_DATA fileData;
   //assert( dirName[dirName.size()-1] == '' );
   HANDLE hFile = FindFirstFile((dirName+"*").c_str(), &fileData);

   for(BOOL b = (hFile != INVALID_HANDLE_VALUE); b;
       b = FindNextFile(hFile, &fileData))
   {
      fileName = fileData.cFileName;
      if ( fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
      {
         // Need to check for . and .. to avoid infinite loop
         if ( fileName != "." && fileName != ".." && recursive )
         {
            numberOfFiles += Explore(dirName+fileName,recursive);
         }
      }
      else
      {
         Filenames.push_back(dirName+fileName);
         numberOfFiles++;
      }
   }
   DWORD dwError = GetLastError();
   if (hFile != INVALID_HANDLE_VALUE) 
      FindClose(hFile);
   if (dwError != ERROR_NO_MORE_FILES) 
   {
      LPVOID lpMsgBuf;
      FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|
                    FORMAT_MESSAGE_FROM_SYSTEM|
                    FORMAT_MESSAGE_IGNORE_INSERTS,
                    NULL,GetLastError(),
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                    (LPTSTR) &lpMsgBuf,0,NULL);

      creaError("FindNextFile error. Error is " << (char *)lpMsgBuf
                   <<" for the directory : "<<dirName);
      return -1;
   }

#else
  // Real POSIX implementation: scandir is a BSD extension only, and doesn't 
  // work on debian for example

   DIR* dir = opendir(dirName.c_str());
   if (!dir)
   {
      return 0;
   }

   // According to POSIX, the dirent structure contains a field char d_name[]
   // of unspecified size, with at most NAME_MAX characters preceeding the
   // terminating null character. Use of other fields will harm the  porta-
   // bility of your programs.

   struct stat buf;
   dirent *d;
   for (d = readdir(dir); d; d = readdir(dir))
   {
	   
      fileName = dirName + d->d_name;
      if( stat(fileName.c_str(), &buf) != 0 )
      {
         //gdcmErrorMacro( strerror(errno) );
      }
      if ( S_ISREG(buf.st_mode) )    //is it a regular file?
      {
		  //printf("EED DirList::Explore [%s]\n" , d->d_name );
		  if ( d->d_name[0]!='.')
		  {	
   	         Filenames.push_back( fileName );
  	         numberOfFiles++;
		  }
      }
      else if ( S_ISDIR(buf.st_mode) ) //directory?
      {
         if ( d->d_name[0] != '.' && recursive ) //we also skip hidden files
         {
            numberOfFiles += Explore( fileName, recursive);
         }
      }
      else
      {
         //gdcmErrorMacro( "Unexpected error" );
         return -1;
      }
   }
   if( closedir(dir) != 0 )
   {
      //gdcmErrorMacro( strerror(errno) );
   }
#endif

  return numberOfFiles;
}

}
