/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

/*=========================================================================
                                                                                
  Program:   crea
  Module:    $RCSfile: creaRTTI.h,v $
  Language:  C++
  Date:      $Date: 2012/11/15 10:43:26 $
  Version:   $Revision: 1.4 $
    
=========================================================================*/
/**
 *\file
 *\brief RTTI tools (system dependent).
 */

#ifndef __CREARTTI_H_INCLUDED__
#define __CREARTTI_H_INCLUDED__

#include "creaSystem.h"
#include <vector>

//-----------------------------------------------------------------------------
// RRTI type_info.name() demangling
// For type_info.name() demangling (gcc >= 3.1, see http://gcc.gnu.org/onlinedocs/libstdc++/latest-doxygen/namespaceabi.html)
// Test for GCC > 3.1.0 //
#if __GNUC__ > 3 ||					\
  (__GNUC__ == 3 && (__GNUC_MINOR__ > 1 ||		\
		     (__GNUC_MINOR__ == 1 &&		\
		      __GNUC_PATCHLEVEL__ > 0)))
#include <cxxabi.h>
#include <stdlib.h>
namespace crea
{
  inline std::string demangle_type_name(const char* name) 
  {
    int  status;
    char* dem = abi::__cxa_demangle(name, 0, 0, &status);
    std::string demangled(dem);
    free(dem);
    if (!status) return demangled;
    return name;
  }

}

#define CREA_DEMANGLE_TYPE_NAME(NAME) crea::demangle_type_name(NAME)

//==========================================================================
#elif defined(_WIN32)
// WIN32
//#include "Windows.h"
//#include "stdafx.h"
#include <windows.h>
#include <imagehlp.h>
// include the right library in the linker stage
#pragma comment( lib, "imagehlp.lib" )

namespace crea
{

  
  
  
  inline std::string demangle_type_name(const char* name) 
  {
    char demangled[513]; 
    if (UnDecorateSymbolName(name, demangled, 
			     sizeof(name), UNDNAME_COMPLETE))
      {
		  return name; //demangled;
      }
    else 
      {
	return name;
      }
  }
}
#define CREA_DEMANGLE_TYPE_NAME(NAME) crea::demangle_type_name(NAME)

#else 
// OTHER
#define CREA_DEMANGLE_TYPE_NAME(NAME) NAME
#endif 

#define CREA_GET_CURRENT_OBJECT_NAME \
  CREA_DEMANGLE_TYPE_NAME(typeid(*this).name())

#define CREA_GET_TYPE_NAME(A)		\
  CREA_DEMANGLE_TYPE_NAME(typeid(A).name())
//-----------------------------------------------------------------------------


namespace crea 
{
  /// Template method which returns the name of a type
  template <class T>
  inline std::string TypeName() 
  { return CREA_DEMANGLE_TYPE_NAME(typeid(T).name()); }
  /// Template method which returns the name of the type of a variable
  template <class T>
  inline std::string TypeName(const T& t) 
  { return CREA_DEMANGLE_TYPE_NAME(typeid(t).name()); }
  /// Specialisation of TypeName when the type passed is already a type_info :
  /// The user does not want to know the type of the type_info class but of 
  /// the class whose type_info is passed !
  template <> 
  inline std::string TypeName<std::type_info>(const std::type_info& t)
  { return CREA_DEMANGLE_TYPE_NAME(t.name()); }

  /// Template method which returns the human readable name of a type
  template <class T>
  inline std::string HumanTypeName() 
  { return TypeName<T>(); }
  /// Template method which returns the human readable name of the type of a variable
  template <class T>
  inline std::string HumanTypeName(const T& t) 
  { return TypeName(t); }
  /// Specialisation of TypeName when the type passed is already a type_info :
  /// The user does not want to know the type of the type_info class but of 
  /// the class whose type_info is passed !
  template <> 
  inline std::string HumanTypeName<std::type_info>(const std::type_info& t)
  { return TypeName<std::type_info>(t); }
  /// Macro to specialise the template function TypeName for certain types 
  /// (typically highly template types, such as STL types) 
  /// in order to return a **really** human readable string
#define CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(TYPE,NAME)			\
  template <> inline std::string HumanTypeName< TYPE >()		\
  { return NAME; }							\
    template <> inline std::string HumanTypeName< TYPE >(const TYPE&)	\
    { return NAME; }	
  
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(std::string,"String");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(signed char,"Char");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(signed short,"Short");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(signed int,"Int");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(unsigned char,"UChar");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(unsigned short,"UShort");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(unsigned int,"UInt");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(float,"Float");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(double,"Double");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(bool,"Bool");
  CREA_DEFINE_HUMAN_READABLE_TYPE_NAME(long,"Long");

  // Human readable strings for std::vector
#define CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(TYPE)		\
  template <> inline std::string HumanTypeName< std::vector<TYPE> >()	\
  { return "Vector"+HumanTypeName<TYPE>(); }				\
    template <> inline std::string HumanTypeName< std::vector<TYPE> >	\
    (const std::vector<TYPE>&) { return "Vector"+HumanTypeName<TYPE>(); }	

  /*
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(int8_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(uint8_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(int16_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(uint16_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(int32_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(uint32_t);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(long);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(float);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(double);
  CREA_DEFINE_HUMAN_READABLE_VECTOR_TYPE_NAME(std::string);
*/
/// The crea::TypeInfo type is a const ref on std::type_info (which can only be manipulated as such (because typeid returns const std::type_info& and type_info has all constructors private)) 
  typedef const std::type_info& TypeInfo;


  CREA_EXPORT void*  run_time_up_or_down_cast( const std::type_info& target_type,
				  const std::type_info& source_type,
				  void*  source_pointer
				  );
 CREA_EXPORT void*  run_time_up_or_down_cast( const std::type_info& target_type,
				  const std::type_info& source_type,
				  const void*  source_pointer
				  );


}

#endif

