/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include "creaRTTI.h"
#include "creaMessageManager.h"

 
namespace crea
{
  // Adapted from https://savannah.cern.ch/support/download.php?file_id=1972
  
  // Something to which we can cast the void*s, to allow finding the
  // dynamic typeid.
  struct Dummy {
    virtual ~Dummy() {}
  };
  

 void*  run_time_up_or_down_cast( const std::type_info& target_type,
			       const std::type_info& source_type,
			       const void*  source_pointer
			       )
 {
   return run_time_up_or_down_cast(target_type,
				   source_type,
				   const_cast<void*>(source_pointer));
 }

  void*   run_time_up_or_down_cast( const std::type_info& target_type,
			       const std::type_info& source_type,
			       void*  source_pointer
			       )
  {
    creaDebugMessage("info",5,
		     "run_time_up_or_down_cast : Casting pointer to '" 
		     << TypeName(typeid(*(Dummy*)source_pointer)) 
		     << "' from " << TypeName(source_type) 
		     << " to " << TypeName(target_type) << std::endl);

    void* target_pointer = 0;
#if __GNUC__ > 3 ||					\
  (__GNUC__ == 3 && (__GNUC_MINOR__ > 1 ||		\
		     (__GNUC_MINOR__ == 1 &&		\
		      __GNUC_PATCHLEVEL__ > 0)))
    
    const abi::__class_type_info* targetTI = 
      (const abi::__class_type_info *)( &(target_type));
    
    creaDebugMessage("info",7," * source   = "<<source_pointer<<std::endl);

    void* tmp = source_pointer;
    if (source_type.__do_upcast(targetTI,&tmp))
      {
	target_pointer = tmp;
      }
    else 
      {
	creaDebugMessage("info",7,
			 " * upcast failed : trying dynamic down cast"
			 <<std::endl);
	const abi::__class_type_info* sourceTI = 
	  (const abi::__class_type_info *)( &(source_type));
	
	
	target_pointer = abi::__dynamic_cast(source_pointer, 
					     sourceTI, 
					     targetTI, 
					     -1);   
      }
    
    creaDebugMessage("info",7," * target   = "<<target_pointer<<std::endl);
    
#else
    creaWarning("run_time_up_or_down_cast not impl. on Win : to do");
    // target_pointer = __RTDynamicCast(source_pointer, 0, source_type, target_type, 0);
#endif
    return target_pointer;
    
  }

}

