/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/                                                                         


#ifndef __creaWx_h__INCLUDED__
#define __creaWx_h__INCLUDED__

//===========================================================================
// Wx headers
#ifdef USE_WXWIDGETS
// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"
#include <wx/datetime.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#ifdef __WXGTK__
# include <locale.h>
#endif //__WXGTK__
// EO Wx headers
//===========================================================================
#else // USE_WXWIDGETS
// define wxWindow as void hence wxWindow* are void*
typedef void wxWindow;
#endif // EO USE_WXWIDGETS
//===========================================================================

//===========================================================================
#ifdef USE_WXWIDGETS

#if defined(_WIN32) 

//  How to have a Console and wxWidgets
//  http://www.wxwidgets.org/wiki/index.php/MSVC_Setup_Guide
//   In Visual C++ 6 (7 should be similar), to create an application that is both a console application 
//  (cout's to the console are visible) and has a wxWidgets GUI, 
//  you need to use the linker option "/subsystem:console" and the following code:

#define CREA_WXMAIN_WITH_CONSOLE					\
  int main(int argc, char* argv[])					\
  {									\
    return WinMain(::GetModuleHandle(NULL), NULL,			\
		   ::GetCommandLine(), SW_SHOWNORMAL);			\
  }									

#else // defined(_WIN32) 

#define CREA_WXMAIN_WITH_CONSOLE

#endif // defined(_WIN32) 

#include <string>

namespace crea
{
  //==================================================================
  /// Conversion std::string to wxString 
  inline wxString std2wx(const std::string& s) {
    wxString wx;
    const char* my_string=s.c_str();
    wxMBConvUTF8 *wxconv= new wxMBConvUTF8();
    wx=wxString(wxconv->cMB2WC(my_string),wxConvUTF8);
    delete wxconv;
    // test if conversion works of not. In case it fails convert from Ascii
    if(wx.length()==0)
      wx=wxString(wxString::FromAscii(s.c_str()));
    return wx;
  }
  //==================================================================

  //==================================================================
  /// Conversion wxString to std::string
  inline std::string wx2std(const wxString& s){
    std::string s2;
    if(s.wxString::IsAscii()) {
      s2=s.wxString::ToAscii();
    } else {
      const wxWX2MBbuf tmp_buf = wxConvCurrent->cWX2MB(s);
      const char *tmp_str = (const char*) tmp_buf;
      s2=std::string(tmp_str, strlen(tmp_str));
    }
    return s2;
  }
  //==================================================================
}

#endif // EO USE_WXWIDGETS

#endif
