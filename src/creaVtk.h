/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/                                                                         


#ifndef __creaVtk_h_INCLUDED__
#define __creaVtk_h_INCLUDED__

#ifdef USE_VTK

#include "creaSystem.h"
#include <vtkImageData.h>

namespace crea
{
  template <class T>

  CREA_EXPORT vtkImageData* NewVtkImageDataFromRaw( T* data,
                                                    int nx, 
                                                    int ny,
                                                    int nz,
                                                    bool do_not_desalloc = true);
  /*
    // Already provided by vtkTypeTraits<T>::VTKTypeID()
  template <class T>
  int GetVtkType(T&) { return VTK_VOID; }

#define SPECIALIZE_GETVTKIDTYPE(T,R) \
  template <> int GetVtkType<T>(T&) { return R; }  

  SPECIALIZE_GETVTKIDTYPE(char,VTK_CHAR)
  SPECIALIZE_GETVTKIDTYPE(signed char,VTK_SIGNED_CHAR)
  SPECIALIZE_GETVTKIDTYPE(unsigned char,VTK_UNSIGNED_CHAR)
  SPECIALIZE_GETVTKIDTYPE(short,VTK_SHORT)
  SPECIALIZE_GETVTKIDTYPE(unsigned short,VTK_UNSIGNED_SHORT)
  SPECIALIZE_GETVTKIDTYPE(int,VTK_INT)
  SPECIALIZE_GETVTKIDTYPE(unsigned int,VTK_UNSIGNED_INT)
  SPECIALIZE_GETVTKIDTYPE(long,VTK_LONG)
  SPECIALIZE_GETVTKIDTYPE(unsigned long,VTK_UNSIGNED_LONG)
  SPECIALIZE_GETVTKIDTYPE(float,VTK_FLOAT)
  SPECIALIZE_GETVTKIDTYPE(double,VTK_DOUBLE)

#undef SPECIALIZE_GETVTKIDTYPE
  */
}

#include "creaVtk.txx"


#endif // USE_VTK
#endif // __creaVtk_h_INCLUDED__
