/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#ifndef _crea_DIRLIST_H_
#define _crea_DIRLIST_H_
   


#include <string>
#include <vector>
#include <iostream>

namespace crea
{
class  DirList
{
public :
   DirList(std::string const &dirName, bool recursive);
   ~DirList();


   std::string NormalizePath(std::string const &pathname); 

   /// Return the name of the directory
   std::string const &GetDirName() const { return DirName; }

   /// Return the file names
   std::vector<std::string> const &GetFilenames() const { return Filenames; }

    /// Return the number of Files
   int GetSize() const { return Filenames.size(); }  

   static bool IsDirectory(std::string const &dirName);

   std::string GetFirst();
   std::string GetNext();

private :
   int Explore(std::string const &dirName, bool recursive=false);

   /// List of file names
   std::vector<std::string> Filenames;
   /// name of the root directory to explore
   std::string DirName;

};

}

#endif
