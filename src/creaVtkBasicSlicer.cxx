/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/                                                                         

#ifdef USE_VTK

#include <creaVtkBasicSlicer.h>

#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"

#include "vtkOutlineFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkImagePlaneWidget.h"
#include "vtkCellPicker.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"

#include "vtkInteractorStyleTrackballCamera.h"

namespace crea
{
  
  void VtkBasicSlicer( vtkImageData* I )
  {

    int xMin, xMax, yMin, yMax, zMin, zMax;
  I->GetExtent(xMin, xMax, yMin, yMax, zMin, zMax);

  double sx, sy, sz;
  I->GetSpacing(sx, sy, sz);

  double ox, oy, oz;
  I->GetOrigin(ox,oy,oz);

  
  // An outline is shown for context.
  
  vtkOutlineFilter* outline = vtkOutlineFilter::New();
  outline->SetInput(I);
  
  vtkPolyDataMapper* outlineMapper = vtkPolyDataMapper::New();
  outlineMapper->SetInputConnection(outline->GetOutputPort());
  
  vtkActor* outlineActor = vtkActor::New();
  outlineActor->SetMapper(outlineMapper);
  

  // The shared picker enables us to use 3 planes at one time
  // and gets the picking order right
  vtkCellPicker* picker = vtkCellPicker::New();
  picker->SetTolerance(0.005);
  
  // The 3 image plane widgets are used to probe the dataset.
  vtkImagePlaneWidget* planeWidgetX = vtkImagePlaneWidget::New();
  planeWidgetX->DisplayTextOn();
  planeWidgetX->SetInput(I);
  planeWidgetX->SetPlaneOrientationToXAxes();
  planeWidgetX->SetSlicePosition((xMax+xMin)/2.);
  planeWidgetX->SetPicker(picker);
  planeWidgetX->SetKeyPressActivationValue('x');
  vtkProperty* prop1 = planeWidgetX->GetPlaneProperty();
  prop1->SetColor(1, 0, 0);

  vtkImagePlaneWidget* planeWidgetY = vtkImagePlaneWidget::New();
  planeWidgetY->DisplayTextOn();
  planeWidgetY->SetInput(I);
  planeWidgetY->SetPlaneOrientationToYAxes();
  planeWidgetY->SetSlicePosition((yMax+yMin)/2.);
  planeWidgetY->SetPicker(picker);
  planeWidgetY->SetKeyPressActivationValue('y');
  vtkProperty* prop2 = planeWidgetY->GetPlaneProperty();
  prop2->SetColor(1, 1, 0);
  planeWidgetY->SetLookupTable(planeWidgetX->GetLookupTable());

  // for the z-slice, turn off texture interpolation:
  // interpolation is now nearest neighbour, to demonstrate
  // cross-hair cursor snapping to pixel centers
  vtkImagePlaneWidget* planeWidgetZ = vtkImagePlaneWidget::New();
  planeWidgetZ->DisplayTextOn();
  planeWidgetZ->SetInput(I);
  planeWidgetZ->SetPlaneOrientationToZAxes();
  planeWidgetZ->SetSlicePosition((zMax+zMin)/2.);
  planeWidgetZ->SetPicker(picker);
  planeWidgetZ->SetKeyPressActivationValue('z');
  vtkProperty* prop3 = planeWidgetZ->GetPlaneProperty();
  prop3->SetColor(0, 0, 1);
  planeWidgetZ->SetLookupTable(planeWidgetX->GetLookupTable());

  planeWidgetZ->SetWindowLevel(512,256);

  // Create the RenderWindow and Renderer
  vtkRenderer* ren = vtkRenderer::New();
  vtkRenderWindow* renWin = vtkRenderWindow::New();
  renWin->AddRenderer(ren);
  
  // Add the outline actor to the renderer, set the background color and size
  ren->AddActor(outlineActor);
  renWin->SetSize(600, 600);
  ren->SetBackground(0.1, 0.1, 0.2);
  
  // an interactor
  vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::New();
  iren->SetInteractorStyle( vtkInteractorStyleTrackballCamera::New() );
  iren->SetRenderWindow(renWin);

  // Set the interactor for the widgets
  planeWidgetX->SetInteractor(iren);
  planeWidgetX->On();
  planeWidgetY->SetInteractor(iren);
  planeWidgetY->On();
  planeWidgetZ->SetInteractor(iren);
  planeWidgetZ->On();

  // Create an initial interesting view
  vtkCamera* cam1 = ren->GetActiveCamera();
  cam1->SetFocalPoint(0, 0, 0);
  cam1->SetPosition(0, 0, -1500);
  //cam1->OrthogonalizeViewUp();
  cam1->Elevation(110);
  cam1->SetViewUp(0, 0, -1);
  cam1->Azimuth(45);
  ren->ResetCameraClippingRange();

  // Render it
  //  render_widget->Render();

  iren->Initialize();
  renWin->Render();
  iren->Start();

  ren->Delete();  
  renWin->Delete();
  iren->Delete();
  planeWidgetX->Delete();
  planeWidgetY->Delete();
  planeWidgetZ->Delete();
  outline->Delete();
  outlineMapper->Delete();
  outlineActor->Delete();
  picker->Delete();
  }

}
#endif // USE_VTK
