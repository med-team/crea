/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

/*=========================================================================
                                                                                
  Program:   crea
  Module:    $RCSfile: creaMessageManager.cxx,v $
  Language:  C++
  Date:      $Date: 2012/11/15 10:43:26 $
  Version:   $Revision: 1.4 $
                                                                                
     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.
                                                                                
=========================================================================*/
/**
 * \file
 * \brief class MessageManager : Manages the messages displayed by crea (code)
 */
#include "creaMessageManager.h"

namespace crea 
{

  //===========================================================================
  MessageManager::MessageManager() 
    : mMaxMessageLength(8),
      mSendToCout(true)
  {
    std::string key;

    key ="all";
    mMessageMap[key] = new MessageType(0,"Minimum level for all kind of messages");
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();

    key ="max";
    mMessageMap[key] = new MessageType(9,"Maximum level for all kind of messages");
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();

    key ="info";
    mMessageMap[key] = new MessageType(1,"Information messages");
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();

    key ="warning";
    mMessageMap[key] = new MessageType(1,"Warning messages");
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();

   /*
    key ="max";
    mMessageLevel[key] = 9;
    mMessageHelp[key] = "Maximum level for all kind of messages";
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();
    key = "error";
    mMessageLevel[key] = 0;
    mMessageHelp[key] = "Error messages";
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();
    key = "warning";
    mMessageLevel[key] = 0;
    mMessageHelp[key] = "Warning messages";
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();
    key = "debug";
    mMessageLevel[key] = 0;
    mMessageHelp[key] = "Debug messages";
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();
    key = "info";
    mMessageLevel[key] = 1;
    mMessageHelp[key] = "Information messages";
    if (mMaxMessageLength<key.length()) mMaxMessageLength = key.length();
    */


   }
  //===========================================================================


  //===========================================================================
  MessageManager::~MessageManager() 
  {
    //      std::cout << "~MessageManager"<<std::endl;
    MessageMapType::iterator i;
    for (i=mMessageMap.begin(); i!=mMessageMap.end(); ++i)
      {
	delete i->second;
      }
  }
  //===========================================================================


  //===========================================================================
  MessageManager* MessageManager::GetInstance() 
  { 
    static MessageManager* m = 0;
    if (!m) m = new MessageManager();
    return m; 
  }
  //===========================================================================

  //===========================================================================
  void MessageManager::RegisterMessageType(const std::string& key, 
					   const std::string& help,
					   unsigned char default_level) 
  {
    GetInstance()->mMessageMap[key] = new MessageType(default_level,help);

    if (GetInstance()->mMaxMessageLength<key.length()) 
      GetInstance()->mMaxMessageLength = key.length();
  }
  //===========================================================================

  //===========================================================================
  void MessageManager::SetMessageLevel(const std::string& key, 
				       unsigned char level) 
  {
    MessageMapType::iterator i;
    i = GetInstance()->mMessageMap.find(key);
    if (i!=GetInstance()->mMessageMap.end()) 
      {
	(*i).second->Level = level;
      }
    else 
      {
	creaWarning("MessageManager::SetMessageLevel : message type=<"
		    <<key<<"> unregistered");
      }
    
  }
  //===========================================================================  
  
  //===========================================================================
  int MessageManager::GetMessageLevel(const std::string& key) 
  {
    int l = GetInstance()->mMessageMap["all"]->Level;
    MessageMapType::iterator i = GetInstance()->mMessageMap.find(key);
    if (i!=GetInstance()->mMessageMap.end()) {
      if ( (*i).second->Level > l ) l = (*i).second->Level;      
    }
    int m = GetInstance()->mMessageMap["max"]->Level;
    if (l>m) l=m;
    return l;
  }
  //===========================================================================

  //===========================================================================
  void MessageManager::SendMessagesToCout(bool v)
  {
    GetInstance()->mSendToCout = v;
  }
  //===========================================================================

  //===========================================================================
  void MessageManager::SendMessage(const std::string& key, const std::string& mess)
  {
    if (GetInstance()->mSendToCout)
      {
	std::cout << mess;
      }
    GetInstance()->mMessageMap[key]->Signal(mess);
  }
  //===========================================================================

  //===========================================================================
  void MessageManager::AddMessageObserver( const std::string& key, MessageCallbackType callback )
  {
    GetInstance()->mMessageMap[key]->Signal.connect(callback);
  }
  //===========================================================================
 
  //===========================================================================
  void MessageManager::PrintInfo() 
  {
    creaMessage("info",1,"================ Messages =================" 
		<< creaendl);
    creaMessage("info",1, "Kind");
    for (int k=0;
	 k<(int)(GetInstance()->mMaxMessageLength-2);
	 k++) 
      {
	creaMessageCont("info",1," "); 
      }
    creaMessageCont("info",1,"Level  Nature" << creaendl);
    MessageMapType::iterator i;
    for (i=GetInstance()->mMessageMap.begin();
	 i!=GetInstance()->mMessageMap.end();
	 ++i) 
      {
	creaMessage("info",1, (*i).first);
	for (int k=0;
	     k<(int)(GetInstance()->mMaxMessageLength+2-(*i).first.length());
	     k++) {
	  creaMessageCont("info",1," ");
	}
	creaMessageCont("info",1, (*i).second->Level << "\t" 
			<< (*i).second->Help << creaendl);
    }
    creaMessage("info",1,"===========================================" 
		<< creaendl);
  }
    //===========================================================================

}
