# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#===========================================================================
# DOES THE USER WANT TO BUILD THE PACKAGE ?
OPTION(BUILD_BBTK_PACKAGE_${BBTK_PACKAGE_NAME} 
  "Build the bbtk package ${BBTK_PACKAGE_NAME} ?" OFF)
#===========================================================================

IF(BUILD_BBTK_PACKAGE_${BBTK_PACKAGE_NAME})
  #===========================================================================
  # THE USER HAS CHOSEN TO BUILD THE PACKAGE  
  #===========================================================================

  #===========================================================================
  # If the package is not a "core" bbtk package (included into the toolkit)
  # then we have to find and use bbtk 
  IF(NOT BBTK_CORE_PACKAGE)
    FIND_PACKAGE(BBTK REQUIRED)
    INCLUDE(${BBTK_USE_FILE})
  ENDIF(NOT BBTK_CORE_PACKAGE)
  #===========================================================================
  
  #===========================================================================
  # Include package configuration cmake script from bbtk cmake dir
  INCLUDE(${BBTK_CMAKE_DIR}/BBTKConfigurePackage.cmake)
  #=========================================================================== 
  
ELSE(BUILD_BBTK_PACKAGE_${BBTK_PACKAGE_NAME})

  #===========================================================================
  # Else if it is a core package we have to reset some things...
  IF(BBTK_CORE_PACKAGE)
    INCLUDE(${BBTK_CMAKE_DIR}/BBTKConfigurePackage_BuildOff.cmake)   
  ENDIF(BBTK_CORE_PACKAGE)
  #===========================================================================

ENDIF(BUILD_BBTK_PACKAGE_${BBTK_PACKAGE_NAME})
