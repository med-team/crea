/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


This is a void bbtk package structure.

You should have this file tree :

.
|-- CMakeLists.txt
|-- Configure.cmake
|-- PackageConfig.cmake.in
|-- README.txt
|-- UsePackage.cmake.in
|-- bbs
|   |-- CMakeLists.txt
|   |-- appli
|   `-- boxes
|-- data
|   `-- CMakeLists.txt
|-- doc
|   |-- CMakeLists.txt
|   |-- bbdoc
|   |   |-- CMakeLists.txt
|   |   `-- header.html.in
|   `-- doxygen
|       |-- CMakeLists.txt
|       |-- DoxyMainPage.txt.in
|       `-- Doxyfile.txt.in
`-- src
    `-- CMakeLists.txt

To use it :

* Edit the root CMakeLists.txt file to customize your package build settings
  (instructions inside the file)

* Put your c++/xml boxes sources in 'src' 
  Use the convention : If the name of your package is PACK and the name of your box is BOX then name the source files bbPACKBOX.{h|cxx|xml}

* Put your script-defined boxes in 'bbs/boxes'. 
  Use the convention : If the name of your box is 'Box' then call the file 'bbBox.bbs'

* Put your script-defined applications in 'bbs/appli'. 

* Put your data in 'data'

* You can customize the header of your package html doc by editing the file 'doc/bbdoc/header.html.in'. You must put html code in this file (or edit it with an html editor). You can include images or links to other html pages. The images and pages must be put in the folder 'doc/bbdoc' and will be properly installed. The same way, you can link to these images or pages in your boxes descriptions without giving any path. If you create subdirs for your material then you have to install the materials yourself by editing the CMakeLists.txt and links must use path with are relative to 'doc/bbdoc'.

* You can customize the main page of your doxygen doc by editing the file 'doc/doxygen/DoxyMainPage.txt.in'.

* Advanced settings : you can add custom cmake settings for your package installation process by CMakeCreateFindPackage in the files PackageConfig.cmake.in and UsePackage.cmake.in (see CMakeCreateFindPackage doc).

THAT'S ALL FOLKS !


