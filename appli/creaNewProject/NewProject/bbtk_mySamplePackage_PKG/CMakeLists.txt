# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

#===========================================================================
# CMAKE SETTINGS FOR BUILDING A BBTK PACKAGE
#===========================================================================

#===========================================================================
# THE NAME OF THE BBTK PACKAGE
SET(BBTK_PACKAGE_NAME mySamplePackage)
#===========================================================================



#===========================================================================
# IF IT IS A STANDALONE PROJECT UNCOMMENT NEXT LINE TO DECLARE YOUR PROJECT
# PROJECT(bb${BBTK_PACKAGE_NAME})
#===========================================================================

#===========================================================================
# PACKAGE AUTHOR
# !!! NO COMMA ALLOWED !!!
SET(${BBTK_PACKAGE_NAME}_AUTHOR "info-dev")
#===========================================================================

#===========================================================================
# PACKAGE DESCRIPTION
SET(${BBTK_PACKAGE_NAME}_DESCRIPTION "Sample Package")
#===========================================================================

#===========================================================================
# PACKAGE VERSION NUMBER 
SET(${BBTK_PACKAGE_NAME}_MAJOR_VERSION 1)
SET(${BBTK_PACKAGE_NAME}_MINOR_VERSION 0)
SET(${BBTK_PACKAGE_NAME}_BUILD_VERSION 0)
#===========================================================================

SET(USE_BOOST     ON)
#===========================================================================
# UNCOMMENT EACH LIBRARY NEEDED (WILL BE FOUND AND USED AUTOMATICALLY)
# SET(${BBTK_PACKAGE_NAME}_USE_VTK  ON)
SET(${BBTK_PACKAGE_NAME}_USE_ITK  ON)
# SET(${BBTK_PACKAGE_NAME}_USE_GDCM ON)
# SET(${BBTK_PACKAGE_NAME}_USE_GDCM_VTK ON)
# SET(${BBTK_PACKAGE_NAME}_USE_GSMIS ON)
# SET(${BBTK_PACKAGE_NAME}_USE_WXWIDGETS ON)
# SET(${BBTK_PACKAGE_NAME}_USE_KWWIDGETS ON)
#===========================================================================

#===========================================================================
# LIST HERE THE OTHER bbtk PACKAGES NEEDED
# (WILL BE FOUND AND USED AUTOMATICALLY)
SET(${BBTK_PACKAGE_NAME}_USE_PACKAGES 
  # std
  # wx
  # itk
  # vtk
  # ...
  )
#===========================================================================

#===========================================================================
# THE SOURCES OF THE PACKAGE
# EITHER UNCOMMENT NEXT LINE TO COMPILE ALL .cxx OF THE src DIRECTORY :
SET(${BBTK_PACKAGE_NAME}_COMPILE_ALL_CXX ON)
# ... OR LIST THE FILES TO COMPILE MANUALLY :
#SET(${BBTK_PACKAGE_NAME}_SOURCES
# LIST HERE THE FILES TO COMPILE TO BUILD THE LIB
# E.G. TO COMPILE "toto.cxx" ADD "toto" (NO EXTENSION)
# THE PATH MUST BE RELATIVE TO THE src FOLDER
#    )
#===========================================================================

#===========================================================================
# THE xml SOURCES OF THE PACKAGE
# EITHER UNCOMMENT NEXT LINE TO bbfy ALL .xml OF THE src DIRECTORY :
SET(${BBTK_PACKAGE_NAME}_COMPILE_ALL_XML ON)
# ... OR LIST THE FILES TO COMPILE MANUALLY :
#SET(${BBTK_PACKAGE_NAME}_XML_SOURCES
# LIST HERE THE FILES TO bbfy TO BUILD THE LIB
# E.G. TO bbfy "toto.xml" ADD "toto" (NO EXTENSION)
# THE PATH MUST BE RELATIVE TO THE src FOLDER
#    )
#===========================================================================

#===========================================================================
# THE SCRIPT-DEFINED BOXES OF THE PACKAGE (bbs)
# EITHER UNCOMMENT NEXT LINE TO INCLUDE ALL .bbs OF THE bbs/boxes DIRECTORY :
SET(${BBTK_PACKAGE_NAME}_INCLUDE_ALL_BBS_BOXES ON)
# ... OR LIST THE FILES TO INCLUDE MANUALLY :
# SET(${BBTK_PACKAGE_NAME}_BBS_BOXES
# LIST HERE THE bbs FILES TO INCLUDE 
# E.G. TO INCLUDE "boxes/bbtoto.bbs" ADD "boxes/bbtoto" (NO EXTENSION)
# !! THE PATH MUST BE RELATIVE TO THE bbs FOLDER !!
#)
#===========================================================================

#===========================================================================
# THE SCRIPT-DEFINED APPLICATIONS OF THE PACKAGE (bbs)
# EITHER UNCOMMENT NEXT LINE TO INCLUDE ALL .bbs OF THE bbs/appli DIRECTORY :
SET(${BBTK_PACKAGE_NAME}_INCLUDE_ALL_BBS_APPLI ON)
# ... OR LIST THE FILES TO INCLUDE MANUALLY :
# SET(${BBTK_PACKAGE_NAME}_BBS_APPLI
# LIST HERE THE bbs FILES TO INCLUDE 
# E.G. TO INCLUDE "appli/testToto.bbs" ADD "appli/testToto" (NO EXTENSION)
# !! THE PATH MUST BE RELATIVE TO THE bbs FOLDER !!
#)
#===========================================================================

#===========================================================================
SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS
  # LIST HERE YOUR ADDITIONAL INCLUDE DIRECTORIES 
  # EXCEPT :
  #  - src
  #  - bbtk dirs
  #  - automatically handled libraries or packages : wx, vtk... (see above)
  #  - the dirs automatically set by other libraries found by FIND_PACKAGE

  ${BBTK_DIR}/../../../include/bbitk  				## linux installed version
  ${BBTK_DIR}/../../creatools_source/bbtk/packages/itk/src      ## linux creatools_bin version
  ${BBTK_DIR}/include/bbitk           				## windows creatools_install version
  

  ../lib/mySampleLib
  )
#===========================================================================

#===========================================================================
SET(${BBTK_PACKAGE_NAME}_LIBS 
  # LIST HERE THE ADDITIONAL LIBS TO LINK AGAINST
  # EXCEPT : the same libs than for INCLUDE_DIRS 
  mySampleLib
  )
#===========================================================================


#===========================================================================
# IF NEEDED : UNCOMMENT NEXT LINE 
# AND LIST ADDITIONNAL DIRECTORIES 
# IN WHICH TO LOOK FOR LIBRARIES TO LINK AGAINST
# LINK_DIRECTORIES()
#===========================================================================

#===========================================================================
# SET TO TRUE TO HAVE INFORMATION ON LIBRARIES FOUND DURING CMAKE CONFIGURE
SET(FIND_PACKAGE_VERBOSE TRUE)
#===========================================================================

#===========================================================================
# END OF USER SECTION
#===========================================================================





#===========================================================================
# Include configuration script
INCLUDE(Configure.cmake)
#===========================================================================

#===========================================================================
# EOF
#===========================================================================

