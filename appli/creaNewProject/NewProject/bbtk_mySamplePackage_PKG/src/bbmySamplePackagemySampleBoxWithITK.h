/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#ifndef __bbmySamplePackagemySampleBoxWithITK_h_INCLUDED__
#define __bbmySamplePackagemySampleBoxWithITK_h_INCLUDED__

//
//
// To create a new Package with boxes that use itk don't forget 3 steps:
//
// 1. Modify de CMakeList.txt file of the whole project to include cmake itk mechanism.
//		1.1 Put ON the option          SET(USE_ITK       ON)
//
// 2. Modify de CMakeList.txt file of this package.
//		2.1. Uncomment the line  SET(${BBTK_PACKAGE_NAME}_USE_ITK  ON)
//		2.2. Add the line:       
//				${BBTK_DIR}/../../../include/bbitk
//		     In the SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS   section
//
// 3. In the .h file of your box, don't forget:
//		3.1. Define the DIM and the TYPE of the images
//                   (Comment out the BBTK_ITK_IMAGE_DIM_xxx
//                    and the BBTK_ITK_IMAGE_TYPE_xxx_t you don't need)
//		3.2. Define the ProcessTemplate method with the 
//		   BBTK_TEMPLATE_ITK_IMAGE_SWITCH mechanism.
//
//	Note: We use a generic bbtk_itk_pointer to transfere the itkImage..
//
//



// Definition of the DIM and TYPE of the itkImage ..
#define BBTK_ITK_IMAGE_DIM_2
#define BBTK_ITK_IMAGE_DIM_3
#define BBTK_ITK_IMAGE_DIM_4
#define BBTK_ITK_IMAGE_TYPE_int8_t
#define BBTK_ITK_IMAGE_TYPE_uint8_t
#define BBTK_ITK_IMAGE_TYPE_int16_t
#define BBTK_ITK_IMAGE_TYPE_uint16_t
#define BBTK_ITK_IMAGE_TYPE_int32_t
#define BBTK_ITK_IMAGE_TYPE_uint32_t
#define BBTK_ITK_IMAGE_TYPE_float
#define BBTK_ITK_IMAGE_TYPE_double



#include "bbmySamplePackage_EXPORT.h"
#include "bbtkAtomicBlackBox.h"
#include "iostream"

#include "bbitkImage.h"

#include <itkBinaryThresholdImageFilter.h>

#include "itkResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"

namespace bbmySamplePackage
{

class bbmySamplePackage_EXPORT mySampleBoxWithITK
 : 
   public bbtk::AtomicBlackBox
{
  BBTK_BLACK_BOX_INTERFACE(mySampleBoxWithITK,bbtk::AtomicBlackBox);

  BBTK_DECLARE_INPUT(In,bbitk::anyImagePointer);
  BBTK_DECLARE_INPUT(Lower,double);
  BBTK_DECLARE_INPUT(Upper,double);
  BBTK_DECLARE_OUTPUT(Out,bbitk::anyImagePointer);
	
  BBTK_PROCESS(Process);
private:
  inline void Process();
  template <class T> void ProcessTemplated();
  itk::Object* mOutput;
};

BBTK_BEGIN_DESCRIBE_BLACK_BOX(mySampleBoxWithITK,bbtk::AtomicBlackBox);
	BBTK_NAME("mySampleBoxWithITK");
	BBTK_AUTHOR("Info-Dev");
	BBTK_DESCRIPTION("Example ITK box - CreaNewProject");
	BBTK_CATEGORY("filter");
	BBTK_INPUT(mySampleBoxWithITK,In,"Input image. Can be any itk::Image<T,D>*",bbitk::anyImagePointer,"");
	BBTK_INPUT(mySampleBoxWithITK,Lower,"(200 default) Lower value for the threshold*",double,"");
	BBTK_INPUT(mySampleBoxWithITK,Upper,"(1200 default) Upper value for the threshold*",double,"");
	BBTK_OUTPUT(mySampleBoxWithITK,Out,"Output Image",bbitk::anyImagePointer,"");
BBTK_END_DESCRIBE_BLACK_BOX(mySampleBoxWithITK);

	
	
//===================================================
void mySampleBoxWithITK::Process()
{
	bbtk::TypeInfo t = bbGetInputIn().type();
	BBTK_TEMPLATE_ITK_IMAGE_SWITCH(t, this->ProcessTemplated);
}
//===================================================
	

//===================================================
template <class ImageType> 
void mySampleBoxWithITK::ProcessTemplated()
{
	bbtkDebugMessageInc("Core",9,"bbitk::BinaryThresholdImageFilter::Process<"<<bbtk::TypeName<ImageType>()<<">()"<<std::endl);

	typedef itk::BinaryThresholdImageFilter<ImageType,ImageType> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	
	// Input
	ImageType* in = this->bbGetInputIn().get<ImageType*>();
	filter->SetInput( in );
	filter->SetInsideValue (255);
	filter->SetOutsideValue (0);
	filter->SetLowerThreshold( bbGetInputLower() );
	filter->SetUpperThreshold( bbGetInputUpper() );

	filter->Update();
	filter->GetOutput()->Register();
	if (mOutput) mOutput->UnRegister();
	mOutput = filter->GetOutput();
	this->bbSetOutputOut( filter->GetOutput() );
	
	bbtkDebugDecTab("Core",9);
}
//===================================================
	
}
// EO namespace bbmySamplePackage

#endif // __bbmySamplePackagemySampleBoxWithITK_h_INCLUDED__

