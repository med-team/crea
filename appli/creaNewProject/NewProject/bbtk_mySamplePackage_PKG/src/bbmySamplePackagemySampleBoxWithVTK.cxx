/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 



#include "bbmySamplePackagemySampleBoxWithVTK.h"
#include "bbmySamplePackagePackage.h"
namespace bbmySamplePackage
{

BBTK_ADD_BLACK_BOX_TO_PACKAGE(mySamplePackage,mySampleBoxWithVTK)
BBTK_BLACK_BOX_IMPLEMENTATION(mySampleBoxWithVTK,bbtk::AtomicBlackBox);

void mySampleBoxWithVTK::Process()
{

// THE MAIN PROCESSING METHOD BODY
//   Here we simply set the input 'In' value to the output 'Out'
//   And print out the output value
// INPUT/OUTPUT ACCESSORS ARE OF THE FORM :
//    void bbSet{Input|Output}NAME(const TYPE&)
//    const TYPE& bbGet{Input|Output}NAME() const 
//    Where :
//    * NAME is the name of the input/output
//      (the one provided in the attribute 'name' of the tag 'input')
//    * TYPE is the C++ type of the input/output
//      (the one provided in the attribute 'type' of the tag 'input')

    myVtkPipeline.SetImage( bbGetInputIn() );
    myVtkPipeline.SetThresholdBetween(  bbGetInputLower(), bbGetInputUpper() );
    myVtkPipeline.Execute();
    bbSetOutputOut( myVtkPipeline.GetResult() );
  
}

void mySampleBoxWithVTK::bbUserSetDefaultValues()
{

//  SET HERE THE DEFAULT INPUT/OUTPUT VALUES OF YOUR BOX 
//    Here we initialize the input 'In' to 0
   bbSetInputIn(0);
  
}

void mySampleBoxWithVTK::bbUserInitializeProcessing()
{

//  THE INITIALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should allocate the internal/output pointers 
//    if any 

  
}

void mySampleBoxWithVTK::bbUserFinalizeProcessing()
{

//  THE FINALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should desallocate the internal/output pointers 
//    if any
  
}
}
// EO namespace bbmySamplePackage


