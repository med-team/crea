/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#ifndef __bbmySamplePackagemySampleBox1_h_INCLUDED__
#define __bbmySamplePackagemySampleBox1_h_INCLUDED__
#include "bbmySamplePackage_EXPORT.h"
#include "bbtkAtomicBlackBox.h"
#include "iostream"
#include "myFierceClass2.h"
 
namespace bbmySamplePackage
{

class bbmySamplePackage_EXPORT mySampleBox1
 : 
   public bbtk::AtomicBlackBox
{
  BBTK_BLACK_BOX_INTERFACE(mySampleBox1,bbtk::AtomicBlackBox);

  BBTK_DECLARE_INPUT(In1,double);
  BBTK_DECLARE_INPUT(In2,double);  
  BBTK_DECLARE_OUTPUT(Out,double);
  BBTK_PROCESS(Process);
  void Process();
  
  myFierceClass2 c;

};

BBTK_BEGIN_DESCRIBE_BLACK_BOX(mySampleBox1,bbtk::AtomicBlackBox);
BBTK_NAME("mySampleBox1");
BBTK_AUTHOR("info-dev");
BBTK_DESCRIPTION("sample box that uses a class of mySampleLib");
BBTK_CATEGORY("");
BBTK_INPUT(mySampleBox1,In1,"First input",double,"");
BBTK_INPUT(mySampleBox1,In2,"First input",double,"");
BBTK_OUTPUT(mySampleBox1,Out,"First output",double,"");
BBTK_END_DESCRIBE_BLACK_BOX(mySampleBox1);

}
// EO namespace bbmySamplePackage

#endif // __bbmySamplePackagemySampleBox1_h_INCLUDED__

