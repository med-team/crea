/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#include "bbmySamplePackagemySampleBoxWithITK.h"
#include "bbmySamplePackagePackage.h"
namespace bbmySamplePackage
{

BBTK_ADD_BLACK_BOX_TO_PACKAGE(mySamplePackage,mySampleBoxWithITK)
BBTK_BLACK_BOX_IMPLEMENTATION(mySampleBoxWithITK,bbtk::AtomicBlackBox);

//
// The method mySampleBoxWithITK::Process()
// has a Template mechanism and is implemented in the .h file
//
	
	
void mySampleBoxWithITK::bbUserSetDefaultValues()
{

//  SET HERE THE DEFAULT INPUT/OUTPUT VALUES OF YOUR BOX 
//    Here we initialize the inputs 'Lower' and 'Upper'
	
	mOutput = 0;
	bbSetInputLower(200);
	bbSetInputUpper(1200);
}

void mySampleBoxWithITK::bbUserInitializeProcessing()
{
//  THE INITIALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should allocate the internal/output pointers 
//    if any 
}

void mySampleBoxWithITK::bbUserFinalizeProcessing()
{
//  THE FINALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should desallocate the internal/output pointers 
//    if any
  
}

}
// EO namespace bbmySamplePackage

