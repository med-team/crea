/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# # Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



#ifndef __bbmySamplePackagemySampleBoxWithVTK_h_INCLUDED__
#define __bbmySamplePackagemySampleBoxWithVTK_h_INCLUDED__
#include "bbmySamplePackage_EXPORT.h"
#include "bbtkAtomicBlackBox.h"
#include "iostream"

#include <vtkImageData.h>
#include "myFierceClass3vtkPipeline.h"

namespace bbmySamplePackage
{

class bbmySamplePackage_EXPORT mySampleBoxWithVTK
 : 
   public bbtk::AtomicBlackBox
{
  BBTK_BLACK_BOX_INTERFACE(mySampleBoxWithVTK,bbtk::AtomicBlackBox);

  BBTK_DECLARE_INPUT(In,vtkImageData*);
  BBTK_DECLARE_INPUT(Lower,double);
  BBTK_DECLARE_INPUT(Upper,double);
  BBTK_DECLARE_OUTPUT(Out,vtkImageData*);
  BBTK_PROCESS(Process);
  void Process();

  myFierceClass3vtkPipeline myVtkPipeline;
};

BBTK_BEGIN_DESCRIBE_BLACK_BOX(mySampleBoxWithVTK,bbtk::AtomicBlackBox);
BBTK_NAME("mySampleBoxWithVTK");
BBTK_AUTHOR("Info-Dev");
BBTK_DESCRIPTION("vtk pipeline example threshold + gaussian smooth");
BBTK_CATEGORY("filter");
BBTK_INPUT(mySampleBoxWithVTK,In,"Input image",vtkImageData*,"");
BBTK_INPUT(mySampleBoxWithVTK,Lower,"lower threshold",double,"");
BBTK_INPUT(mySampleBoxWithVTK,Upper,"upper threshold",double,"");
BBTK_OUTPUT(mySampleBoxWithVTK,Out,"Output image",vtkImageData*,"");
BBTK_END_DESCRIBE_BLACK_BOX(mySampleBoxWithVTK);

}
// EO namespace bbmySamplePackage

#endif // __bbmySamplePackagemySampleBoxWithVTK_h_INCLUDED__

