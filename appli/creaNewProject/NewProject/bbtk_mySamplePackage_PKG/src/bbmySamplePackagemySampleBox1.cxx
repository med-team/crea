/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#include "bbmySamplePackagemySampleBox1.h"
#include "bbmySamplePackagePackage.h"
namespace bbmySamplePackage
{

BBTK_ADD_BLACK_BOX_TO_PACKAGE(mySamplePackage,mySampleBox1)
BBTK_BLACK_BOX_IMPLEMENTATION(mySampleBox1,bbtk::AtomicBlackBox);

void mySampleBox1::Process()
{

// THE MAIN PROCESSING METHOD BODY
//   Here we simply set the input 'In' value to the output 'Out'
//   And print out the output value
// INPUT/OUTPUT ACCESSORS ARE OF THE FORM :
//    void bbSet{Input|Output}NAME(const TYPE&)
//    const TYPE& bbGet{Input|Output}NAME() const 
//    Where :
//    * NAME is the name of the input/output
//      (the one provided in the attribute 'name' of the tag 'input')
//    * TYPE is the C++ type of the input/output
//      (the one provided in the attribute 'type' of the tag 'input')

    c.SetX(bbGetInputIn1()); 
    c.SetY(bbGetInputIn2());
    c.Execute();
    bbSetOutputOut(c.GetResult());
    
    std::cout << "Output value = " <<bbGetOutputOut() << std::endl;
  
}

void mySampleBox1::bbUserSetDefaultValues()
{

//  SET HERE THE DEFAULT INPUT/OUTPUT VALUES OF YOUR BOX 
   bbSetInputIn1(0);
   bbSetInputIn2(0);  
}

void mySampleBox1::bbUserInitializeProcessing()
{

//  THE INITIALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should allocate the internal/output pointers 
//    if any 

  
}
 
void mySampleBox1::bbUserFinalizeProcessing()
{

//  THE FINALIZATION METHOD BODY :
//    Here does nothing 
//    but this is where you should desallocate the internal/output pointers 
//    if any
  
}
}
// EO namespace bbmySamplePackage


