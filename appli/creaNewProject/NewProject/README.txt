/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 

USER! Put here your module name.
<your Module>
(c) CREATIS 2012

lib will contains as many directories as you need
---

lib/mySampleLib is just ... a sample (example) library
---------------
feel free to remove it, when you are sure you understood how to fill a library.
(Don't forget to modity the relevant CMakeLists.txt)

lib/template_lib is a template you may copy/paste to create your own library
----------------
(you may feel like using lib/mySampleLib for doing that...)

appli contains the source codes for your executables.
-----

bbtk_mySamplePackage_PKG is just ... a sample (example) package
------------------------
feel free to remove it, when you are sure you understood how to fill a Package.
(Don't forget to modity the relevant CMakeLists.txt)

bbtk_YourProjectName_PKG is the default package for your project.
------------------------
feel free to use it.

doc : everything you need to generate a full Documentation (with Doxygen,
---
with a user designed website, as a lAtEx document ...) 
Don't remove, feel free to modify...

cmake : a set of usefull macro.
----
Don't remove, don't modify!

install : everything needed for making an installer
-------
Don't remove, don't modify!

