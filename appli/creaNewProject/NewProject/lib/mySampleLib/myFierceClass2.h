/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#ifndef _myFierceClass2_H_
#define _myFierceClass2_H_

//---------------------------------------------
// dummy class 
// with Setters and Getters
// the job is done by the function Execute

//---------------------------------------------
class /*CREA_EXPORT*/ myFierceClass2
{
   public :
      myFierceClass2() : X(0.0), Y(0.0), Result(0.0) {};
      myFierceClass2(int a, int b) : X (a), Y(b) {};
      ~myFierceClass2(){};
      void SetX (double x);
      void SetY (double y);
      void Execute();
      double GetResult();
      
   private:
      float X;
      float Y;
      float Result;
};

//-----------------------------------------------------------------------------
#endif
