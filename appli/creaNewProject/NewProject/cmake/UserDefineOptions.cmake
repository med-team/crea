# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



#-----------------------------------------------------------------------------
# messages compilation options 
OPTION ( BUILD_${PROJECT_NAME}_DOC "Build documentation for ${PROJECT_NAME}." OFF)
SWITCH_ON_IF_BUILD_ALL(BUILD_${PROJECT_NAME}_DOC)
#-----------------------------------------------------------------------------
IF(BUILD_${PROJECT_NAME}_DOC)

  OPTION(BUILD_${PROJECT_NAME}_DOC_PACKAGE 
    "Build ${PROJECT_NAME} packages documentation (bbi+dot)."       ON)
  OPTION(BUILD_${PROJECT_NAME}_DOC_DOXYGEN 
    "Build doxygen documentation (doxygen)."             ON)
  OPTION(BUILD_${PROJECT_NAME}_DOC_GUIDES_PDF 
    "Build ${PROJECT_NAME} Guides', PDF format (latex)."            ON)
  OPTION(BUILD_${PROJECT_NAME}_DOC_GUIDES_HTML 
    "Build ${PROJECT_NAME} Guides', HTML format (latex+tth)."       ON)

ELSE(BUILD_${PROJECT_NAME}_DOC)
  
  IF(BUILD_${PROJECT_NAME}_DOC_PACKAGE)
    SET(BUILD_${PROJECT_NAME}_DOC_PACKAGE OFF CACHE BOOL 
      "Build ${PROJECT_NAME} packages documentation (bbdoc+dot)."	FORCE)
  ENDIF(BUILD_${PROJECT_NAME}_DOC_PACKAGE)
  IF(BUILD_${PROJECT_NAME}_DOC_DOXYGEN)
    SET(BUILD_${PROJECT_NAME}_DOC_DOXYGEN OFF CACHE BOOL 
      "Build doxygen documentation. (doxygen)"		FORCE)
  ENDIF(BUILD_${PROJECT_NAME}_DOC_DOXYGEN)
  
  IF(BUILD_${PROJECT_NAME}_DOC_GUIDES_PDF)
    SET(BUILD_${PROJECT_NAME}_DOC_GUIDES_PDF OFF CACHE BOOL 
      "Build ${PROJECT_NAME} Guides', PDF format (latex)."	FORCE)
  ENDIF(BUILD_${PROJECT_NAME}_DOC_GUIDES_PDF)
  
  IF(BUILD_${PROJECT_NAME}_DOC_GUIDES_HTML)
    SET(BUILD_${PROJECT_NAME}_DOC_GUIDES_HTML OFF CACHE BOOL 
      "Build ${PROJECT_NAME} Guides', HTML format (latex tth)."		FORCE)
  ENDIF(BUILD_${PROJECT_NAME}_DOC_GUIDES_HTML)
  
ENDIF(BUILD_${PROJECT_NAME}_DOC)
#-----------------------------------------------------------------------------
