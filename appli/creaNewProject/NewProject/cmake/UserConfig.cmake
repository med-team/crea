# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



#-----------------------------------------------------------------------------
# store documentation and scripts **RELATIVE** paths  
# from build tree root or install tree root
# (different on win/lin)
IF(WIN32)
  # A trick to install in root install dir (problem when empty path given)
  SET(${PROJECT_NAME}_SHARE_REL_PATH "bin/..")
  SET(${PROJECT_NAME}_DOC_REL_PATH doc) 
ELSE(WIN32)
  SET(${PROJECT_NAME}_SHARE_REL_PATH share)
  SET(${PROJECT_NAME}_DOC_REL_PATH doc)  
ENDIF(WIN32)   

MESSAGE(STATUS "* SHARE_REL_PATH   =${${PROJECT_NAME}_SHARE_REL_PATH}")
MESSAGE(STATUS "* DOC_REL_PATH     =${${PROJECT_NAME}_DOC_REL_PATH}")
MESSAGE(STATUS "* BBS_REL_PATH     =${${PROJECT_NAME}_BBS_REL_PATH}")
MESSAGE(STATUS "* DATA_REL_PATH    =${${PROJECT_NAME}_DATA_REL_PATH}")
MESSAGE(STATUS "===============================================")

#-----------------------------------------------------------------------------
