# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



MACRO(USER_BUILD_DOXYGEN_DOC NAME INPUT DOC_RELATIVE_INSTALL_PATH PREDEFINED)

  #--------------------------------------------------------------------------
  SET(USE_DOXYGEN ON CACHE BOOL "" FORCE)
  
  # Name
  SET(DOXYGEN_PROJECT_NAME "${NAME}")

  # Inputs
  STRING(REGEX REPLACE ";" " " DOXYGEN_INPUT "${INPUT}")

  # Output dirs
  SET(DOXYGEN_HTML_OUTPUT ".")
  SET(DOXYGEN_OUTPUT "${${PROJECT_NAME}_DOXYGEN_BUILD_PATH}") 
  STRING(REGEX REPLACE " " "~" DOXYGEN_OUTPUT "${DOXYGEN_OUTPUT}")
  
 
  IF(NOT IS_DIRECTORY ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT})
    FILE(MAKE_DIRECTORY ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT})
  ENDIF(NOT IS_DIRECTORY ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT})

  # Doc exclude
  SET(DOXYGEN_EXCLUDE "")
  STRING(REGEX REPLACE ";" " " DOXYGEN_EXCLUDE "${DOXYGEN_EXCLUDE}")

  # Log file name 
  SET(DOXYGEN_LOGFILE "${CMAKE_CURRENT_BINARY_DIR}/doxygen.log")

  # Predefined symbols
  STRING(REGEX REPLACE ";" " " DOXYGEN_DOC_PREDEFINED "${PREDEFINED}")
  
  #---------------------------------------------------------------------------
  # DOT verification
  IF(DOT)
    GET_FILENAME_COMPONENT(DOXYGEN_DOT_PATH ${DOT} PATH)
    SET(DOXYGEN_HAVE_DOT "YES")
  ELSE(DOT)
    SET(DOXYGEN_DOT_PATH "")
    SET(DOXYGEN_HAVE_DOT "NO")
  ENDIF(DOT)
  
  #---------------------------------------------------------------------------
  # Create file and project
  CONFIGURE_FILE(
    ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.txt.in
    ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile.txt
    @ONLY IMMEDIATE
    )
 
  
  ADD_CUSTOM_COMMAND(
    OUTPUT ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT}/index.html
    COMMAND 
    ${DOXYGEN}
    ARGS
    ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile.txt
    #  DEPENDS bbtk bbi 
    )
  
  ADD_CUSTOM_TARGET(doxygen_${NAME} ALL
    DEPENDS  ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT}/index.html
    )
  
  INSTALL(
    DIRECTORY
    ${DOXYGEN_OUTPUT}/${DOXYGEN_HTML_OUTPUT}
    DESTINATION
    ${${PROJECT_NAME}_DOXYGEN_INSTALL_PATH}/${DOC_RELATIVE_INSTALL_PATH}
    )
  #--------------------------------------------------------------------------
#message("user build doxygen , doxygen output = ${DOXYGEN_OUTPUT}")
ENDMACRO(USER_BUILD_DOXYGEN_DOC)
