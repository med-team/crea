# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#========================================================================
# MACRO WHICH COPIES ALL IMAGES (png, jpg, gif) 
# FROM SOURCE TREE TO DOC BUILD TREE
# AND INSTALL THEM IN BBTK DOC TREE
MACRO(USER_DOC_INSTALL_IMAGES DOC_RELATIVE_INSTALL_PATH)
  MESSAGE(STATUS "* Copying png,jpg,tif images in ${BBTK_DOC_BUILD_PATH}/${DOC_RELATIVE_INSTALL_PATH}")
  FILE(GLOB PNG_IMAGES "*.png")
  FILE(GLOB JPG_IMAGES "*.jpg")
  FILE(GLOB TIF_IMAGES "*.tif")
  SET(IMAGES
    ${PNG_IMAGES}
    ${JPG_IMAGES}
    ${TIF_IMAGES}
    )
#    MESSAGE(ERROR ${IMAGES} )
  FOREACH(image ${IMAGES})  
    GET_FILENAME_COMPONENT(filename "${image}" NAME)
    CONFIGURE_FILE(
      ${image}
       ${${PROJECT_NAME}_DOC_BUILD_PATH}/${DOC_RELATIVE_INSTALL_PATH}/${filename}
      COPYONLY
      ) 
      
    #  IF (WIN32)
#	CONFIGURE_FILE(
#          ${image}
#          ${CMAKE_CURRENT_BINARY_DIR}/${filename}
#          COPYONLY
#	  )
#      ENDIF(WIN32)
    # MESSAGE(ERROR "${image} (${filename}) in :  ${PROJECT_BINARY_DIR}/${BBTK_DOC_INSTALL_PATH}/${DOC_RELATIVE_INSTALL_PATH}/${filename}")
  ENDFOREACH(image)
  INSTALL( 
      FILES ${IMAGES} 
      DESTINATION ${${PROJECT_NAME}_DOC_INSTALL_PATH}/${DOC_RELATIVE_INSTALL_PATH}
    )  
ENDMACRO(USER_DOC_INSTALL_IMAGES)