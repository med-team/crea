# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# # Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



MACRO(USER_BUILD_HTML_DOC_FROM_LATEX CONFERENCEFOLDER TEX_FILE)
  # Need tth
  SET(USE_TTH ON CACHE BOOL "" FORCE)
  # Construct absolute build path
  SET(BUILD_PATH ${${PROJECT_NAME}_DOC_BUILD_PATH}/${OUTPUT_REL_PATH})
  #------------------------------------------------------------------------- 
  #IF(WIN32)
	#	SET(TTH_EXECUTABLE "${TTH_EXECUTABLE}/tth.exe")
	#ENDIF(WIN32)
	
	#message("MACRO(USER_BUILD_HTML_DOC_FROM_LATEX TEX_FILE OUTPUT_REL_PATH) TTH_EXECUTABLE ${TTH_EXECUTABLE}")
  ADD_CUSTOM_COMMAND(
    OUTPUT ${BUILD_PATH}/${TEX_FILE}.html
    COMMAND 
    #    tth 
    ${TTH_EXECUTABLE}
    ARGS
    -e2 ${BUILD_PATH}/${TEX_FILE}.tex 2>tth.log
 #   DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${TEX_FILE}.pdf
    )
  ADD_CUSTOM_TARGET(${TEX_FILE}.html ALL
    DEPENDS ${BUILD_PATH}/${TEX_FILE}.html
    )
  # Install
  INSTALL( 
    FILES ${BUILD_PATH}/${TEX_FILE}.html
    DESTINATION ${${PROJECT_NAME}_DOC_INSTALL_PATH}/${OUTPUT_REL_PATH}
    )   
  #-------------------------------------------------------------------------
  
ENDMACRO(USER_BUILD_HTML_DOC_FROM_LATEX)
