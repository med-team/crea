# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



MESSAGE(STATUS "===============================================")
MESSAGE(STATUS "Setting up deduced pathes:")
# Black box docs relative path
SET(${PROJECT_NAME}_BBDOC_REL_PATH ${${PROJECT_NAME}_DOC_REL_PATH}/Userbbdoc)
# Doxygen docs relative path
SET(${PROJECT_NAME}_DOXYGEN_REL_PATH ${${PROJECT_NAME}_DOC_REL_PATH}/UserDoxygen)

MESSAGE(STATUS "* BBTK_BBDOC_REL_PATH   =${BBTK_BBDOC_REL_PATH}")
MESSAGE(STATUS "* BBTK_DOXYGEN_REL_PATH =${BBTK_DOXYGEN_REL_PATH}")

# Now compute BUILD TREE **ABSOLUTE PATHS**
SET(${PROJECT_NAME}_DOC_BUILD_PATH ${PROJECT_BINARY_DIR}/${${PROJECT_NAME}_DOC_REL_PATH})
SET(${PROJECT_NAME}_BBDOC_BUILD_PATH ${PROJECT_BINARY_DIR}/${${PROJECT_NAME}_BBDOC_REL_PATH})
SET(${PROJECT_NAME}_DOXYGEN_BUILD_PATH ${PROJECT_BINARY_DIR}/${${PROJECT_NAME}_DOXYGEN_REL_PATH})
SET(${PROJECT_NAME}_BBS_BUILD_PATH ${PROJECT_BINARY_DIR}/${${PROJECT_NAME}_BBS_REL_PATH})
SET(${PROJECT_NAME}_DATA_BUILD_PATH ${PROJECT_BINARY_DIR}/${${PROJECT_NAME}_DATA_REL_PATH})

#SET(BBTK_DOC_INSTALL_PATH ${BBTK_INSTALL_PREFIX}/${BBTK_DOC_REL_PATH})
#SET(BBTK_BBDOC_INSTALL_PATH ${BBTK_INSTALL_PREFIX}/${BBTK_BBDOC_REL_PATH})
#SET(BBTK_DOXYGEN_INSTALL_PATH ${BBTK_INSTALL_PREFIX}/${BBTK_DOXYGEN_REL_PATH})
#SET(BBTK_BBS_INSTALL_PATH ${BBTK_INSTALL_PREFIX}/${BBTK_BBS_REL_PATH})
#SET(BBTK_DATA_INSTALL_PATH ${BBTK_INSTALL_PREFIX}/${BBTK_DATA_REL_PATH})

# LG 22/10/08 : REMOVED THE **ABSOLUTE INSTALL PATH**
# was buggy when packaging
# consequences : 
#  1) generated packages are NOT relocatables 
#  2) they must be installed at the same prefix than bbtk
SET(${PROJECT_NAME}_DOC_INSTALL_PATH     ${${PROJECT_NAME}_DOC_REL_PATH})
SET(${PROJECT_NAME}_BBDOC_INSTALL_PATH   ${${PROJECT_NAME}_BBDOC_REL_PATH})
SET(${PROJECT_NAME}_DOXYGEN_INSTALL_PATH ${${PROJECT_NAME}_DOXYGEN_REL_PATH})
SET(${PROJECT_NAME}_BBS_INSTALL_PATH     ${${PROJECT_NAME}_BBS_REL_PATH})
SET(${PROJECT_NAME}_DATA_INSTALL_PATH    ${${PROJECT_NAME}_DATA_REL_PATH})




# Create build directories if necessary
CREA_MKDIR(${${PROJECT_NAME}_DOC_BUILD_PATH})
CREA_MKDIR(${${PROJECT_NAME}_BBDOC_BUILD_PATH})
CREA_MKDIR(${${PROJECT_NAME}_DOXYGEN_BUILD_PATH})
CREA_MKDIR(${${PROJECT_NAME}_BBS_BUILD_PATH})
CREA_MKDIR(${${PROJECT_NAME}_DATA_BUILD_PATH})

#-----------------------------------------------------------------------------
SET (EXECUTABLE_OUTPUT_PATH 
  ${PROJECT_BINARY_DIR}/${EXECUTABLE_OUTPUT_REL_PATH})
SET (LIBRARY_OUTPUT_PATH    
  ${PROJECT_BINARY_DIR}/${LIBRARY_OUTPUT_REL_PATH})
#-----------------------------------------------------------------------------
MESSAGE(STATUS "* EXECUTABLE_OUTPUT_PATH=${EXECUTABLE_OUTPUT_PATH}")
MESSAGE(STATUS "* LIBRARY_OUTPUT_PATH   =${LIBRARY_OUTPUT_PATH}")

#-----------------------------------------------------------------------------
# Today CMakeCreateFindPackage is copied in BBTK_CMAKE_DIR
# When installed as a separate project will have to find it with FIND_PACKAGE
SET(CMakeCreateFindPackage_DIR ${${PROJECT_NAME}_CMAKE_DIR})
#-----------------------------------------------------------------------------


MESSAGE(STATUS "===============================================")

