# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 



MACRO(USER_BUILD_LATEX_DOC NUM CONFERENCEFOLDER TEX_FILE)
  # Need LaTeX
  SET(USE_LATEX ON CACHE BOOL "" FORCE)
  # Construct absolute build path
  SET(BUILD_PATH ${CMAKE_CURRENT_BINARY_DIR}/${CONFERENCEFOLDER})
  #MESSAGE(STATUS "Tex file : ${TEX_FILE}")
  #---------------------------------------------------------------------------

  CREA_CPDIR(${CMAKE_CURRENT_SOURCE_DIR}/${CONFERENCEFOLDER} ${BUILD_PATH})

  # Have to run latex twice to get references, toc, etc.
 #message("ADD_CUSTOM_COMMAND(OUTPUT ${BUILD_PATH}/${TEX_FILE}.aux   MACRO(USER_BUILD_LATEX_DOC")
ADD_CUSTOM_COMMAND(OUTPUT 
    ${BUILD_PATH}/${TEX_FILE}.pdf    

    COMMAND 
    # latex
    cd ${BUILD_PATH} && ${LATEX_COMPILER}
    ARGS
    ${BUILD_PATH}/${TEX_FILE}   

    COMMAND 
    # bibtex
    cd ${BUILD_PATH} && ${BIBTEX_COMPILER}
    ARGS
    ${BUILD_PATH}/${TEX_FILE}

    COMMAND 
    # latex
    cd ${BUILD_PATH} && ${LATEX_COMPILER}
    ARGS
    ${BUILD_PATH}/${TEX_FILE}   


    COMMAND 
    # latex
    cd ${BUILD_PATH} && ${LATEX_COMPILER}
    ARGS
    ${BUILD_PATH}/${TEX_FILE}

    COMMAND 
    # latex
    cd ${BUILD_PATH} && ${DVIPDF_CONVERTER}
    ARGS
    ${BUILD_PATH}/${TEX_FILE}
    )    


add_custom_target(${CONFERENCEFOLDER}${TEX_FILE}.pdf ALL
		  DEPENDS ${BUILD_PATH}/${TEX_FILE}.pdf)


ADD_CUSTOM_COMMAND(OUTPUT 
    ${BUILD_PATH}/${TEX_FILE}.html

    COMMAND 
    # latex
    cd ${BUILD_PATH} && ${LATEX2HTML_CONVERTER}
    ARGS
    -antialias -white -notransparent ${BUILD_PATH}/${TEX_FILE}
)

add_custom_target(${CONFERENCEFOLDER}${TEX_FILE}.html ALL
		  DEPENDS ${BUILD_PATH}/${TEX_FILE}.html)
  
#message(status "++++++++++++++++++++++++++ ${${PROJECT_NAME}_DOC_INSTALL_PATH}/${OUTPUT_REL_PATH}")

SET(ARTICLE_LINKS
      "${ARTICLE_LINKS}
       <br>${NUM}. <a href=\"../UserDocumentation/${CONFERENCEFOLDER}/${TEX_FILE}/index.html\">${TEX_FILE}</a>
       <a href=\"../UserDocumentation/${CONFERENCEFOLDER}/${TEX_FILE}.pdf\">PDF File</a><br>"
   )

ENDMACRO(USER_BUILD_LATEX_DOC)
