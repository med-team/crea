/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include <creaWx.h>
#include <wx/dirdlg.h>
#include <stdlib.h> // for getenv

class myApp : public wxApp
{
public:
  bool OnInit( );
  int  OnExit() { return true; }
};

IMPLEMENT_APP(myApp);
CREA_WXMAIN_WITH_CONSOLE

bool myApp::OnInit( )
{
  wxApp::OnInit();
#ifdef __WXGTK__
  //See http://www.wxwindows.org/faqgtk.htm#locale
  setlocale(LC_NUMERIC, "C");
#endif
   wxInitAllImageHandlers();

   wxString dir = wxDirSelector(_T("Select directory in which to create the project"), _T(""), wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST );

   if (dir.IsEmpty()) return false;

   wxString name = wxGetTextFromUser(_T("Enter project name"),
				     _T("creaNewProject"),
				     _T("New"));
   if (name.IsEmpty()) return false;
   
   wxString description = wxGetTextFromUser(_T("Enter Package Description (html format)"),
				     _T("create New Package"),
				     _T("NO_DESCRIPTION"));
   if (description.IsEmpty()) return false;

   wxString authorWX = wxGetTextFromUser(_T("Author (One word)"),
				     _T("Author"),
				     _T("Author_Name"));
   if (description.IsEmpty()) return false;



#if(_WIN32)
     
	std::string command("creaNewProject.bat ");
	std::string command1("creaSed.exe ");
	std::string command2("del ");

	command  += "\"" + crea::wx2std(dir) + "\" \"" + crea::wx2std(name) + "\"";
	command1 += "\"" + crea::wx2std(dir)+"\\"+crea::wx2std(name)+"\\CMakeLists.txt.in\" " + "NameOfTheProject " + crea::wx2std(name) + "> \"" + crea::wx2std(dir)+"\\"+crea::wx2std(name)+"\\CMakeLists.txt\"";
	command2 += "\"" + crea::wx2std(dir)+"\\"+crea::wx2std(name)+"\\CMakeLists.txt.in\"";
	if ( ! system ( command.c_str() ) )
	{
		system ( command1.c_str() );
		system ( command2.c_str() );

        // Create a Package at the same time.   JPR
		char *author = (char*) (authorWX.mb_str()) ;
		std::string nomDirectory = crea::wx2std(dir) + "\\" + crea::wx2std(name);
		std::string nomPackageDirectory = nomDirectory + "\\" + "bbtk_" + crea::wx2std(name) + "_PKG";
		std::string bbCreatePackage("bbCreatePackage ");
		bbCreatePackage += nomDirectory + " " + crea::wx2std(name) + " " + author + " " + crea::wx2std(description);
		system (bbCreatePackage.c_str());		
		std::string add;
		add = "echo ADD_SUBDIRECTORY(bbtk_" + crea::wx2std(name)  + "_PKG) >> " + nomDirectory + "/CMakeLists.txt";
		system(add.c_str());

		wxMessageBox(_T("New Project created !"),_T("creaNewProject"),
				wxOK | wxICON_INFORMATION);
	}	else 	{
		wxString err(_T("An error occured while running '"));
		err +=  crea::std2wx(command) + _T("'");
		wxMessageBox(err,_T("creaNewProject"),wxOK | wxICON_ERROR);
	}

// ------ LINUX / MacOS

#else
   	std::string command("creaNewProject.sh ");
	command += "\"" + crea::wx2std(dir) + "\"" +" " + crea::wx2std(name);
	
	if ( ! system ( command.c_str() ) )
	{
		wxMessageBox(_T("New Project created !"),_T("creaNewProject"),
					wxOK | wxICON_INFORMATION);

                // Create a Package at the same time.   JPR
		
                //execGUICreateNewPackage += "/../../../share/bbtk/bbs/toolsbbtk/appli/GUICreatePackage.bbs";
	        //system(execGUICreateNewPackage.c_str());
		
		// or, better, new trick :
		// bbCreatePackage nomDirectory nomPackage author Description
   
//EED           char *author = getenv("USER");
// JPR          char *author = (char*) (authorWX.mb_str());
                std::string author(authorWX.mb_str());
		std::string nomDirectory = crea::wx2std(dir) + "/" + crea::wx2std(name);
		std::string nomPackageDirectory = nomDirectory + "/" + "bbtk_" + crea::wx2std(name) + "_PKG";

		std::string bbCreatePackage("bbCreatePackage ");
		bbCreatePackage += nomDirectory + " " + crea::wx2std(name) + " " + author + " " + crea::wx2std(description);
		system (bbCreatePackage.c_str());
		
		std::string add;
		add = "echo 'ADD_SUBDIRECTORY(bbtk_" + crea::wx2std(name)  + "_PKG)' >> " + nomDirectory + "/CMakeLists.txt";
		//std::cout << add << std::endl;
		system(add.c_str());
		
	}
	else
	{	
		wxString err(_T("An error occured while running '"));
		err +=  crea::std2wx(command) + _T("'");
		wxMessageBox(err,_T("creaNewProject"),wxOK | wxICON_ERROR);
	}

#endif

   return false;
}

