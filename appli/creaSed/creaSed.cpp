/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


//==========================================================================
void replace(std::string& str,
			 const std::string& from, 
             const std::string& to )
{
	std::string::size_type pos = str.find( from );
	while ( pos != std::string::npos )
	{
		str.replace( pos, from.size(), to );
		pos = str.find( from, pos+from.size()-1 );
	} 
}
//==========================================================================


//==========================================================================
int main(int argc, char **argv)
{
	  
  if (argc!=4) 
    {
		for(int i = 1; i < argc; i++){
			std::cerr << "||||	"<<argv[i]<<std::endl;
		}
      std::cerr << "usage : "<< argv[0] <<" -fileIn  \"-FindString\" \"-ReplaceString\"" << std::endl;
      return 1;
    }

  FILE *ffIn;
  std::string fileIn;
  std::string fileOut;
  std::string findstring;
  std::string replacestring;
  std::string lineStr;
  char strTmp[255];

  fileIn 	= argv[1];
  findstring 	= argv[2];
  replacestring = argv[3];

  errno_t errorOpen = fopen_s(&ffIn, fileIn.c_str(),"r");
  if (!errorOpen && ffIn){
        while(!feof(ffIn)){
	    fgets( strTmp , 255, ffIn );
            lineStr=strTmp;
	    if( feof(ffIn) && (lineStr.length()==1) ) { 
	    } else {
	      replace( lineStr,findstring, replacestring );
	      std::cout << lineStr;
            }
        }
	fclose(ffIn);	
  } else {
      std::cerr << "ERROR. File : "<< argv[1] <<" does not exists." << std::endl;
      return 1;
  }


  return 0;
}
//==========================================================================


