/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * creaDevManager.h
 *
 *  Created on: 19/10/2012
 *      Author: daniel
 */

#ifndef CREADEVMANAGER_H_
#define CREADEVMANAGER_H_

#include "wxCDMMainFrame.h"

/**
 * Crea Development Manager
 * Developed by Daniel Felipe González Obando for CREATIS.
 * This software aims to help the creaTools developer on the process of building projects.
 * It allows to work with existing projects as well as it allows to create a project from zero.
 * It brings the user help on each step of the development, as well as it always shows the structure of a project.
 * It can check the compilation structure to help the developer to compile faster.
 */
class wxCreaDevManagerApp:public wxApp
{
public:
  /**
   * Default constructor.
   */
  wxCreaDevManagerApp();

  /**
   * Executed when the application starts, this method initializes the application creating a main frame and showing it to the user.
   */
  virtual bool OnInit();

  /**
   * Executed when the application is closing, this method destroys the frames created and shuts the program down.
   */
  virtual int OnExit();

private:
  wxCDMMainFrame* mainWindow;

};
DECLARE_APP(wxCreaDevManagerApp)

#endif /* CREADEVMANAGER_H_ */
