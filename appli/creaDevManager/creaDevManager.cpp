/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# # Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#include <creaWx.h>
#include <wx/tooltip.h>
#include <wx/config.h>
#include <stdlib.h>
#include <iostream> // for std::cout
#include "creaDevManager.h"

IMPLEMENT_APP(wxCreaDevManagerApp)
CREA_WXMAIN_WITH_CONSOLE

wxCreaDevManagerApp::wxCreaDevManagerApp():wxApp()
{
  mainWindow = NULL;
}

bool wxCreaDevManagerApp::OnInit()
{
  wxApp::OnInit();
  this->SetVendorName(wxT("Creatis"));
  this->SetAppName(wxT("creaDevManager"));

  wxConfigBase *pConfig = wxConfigBase::Get();

  mainWindow = new wxCDMMainFrame(NULL);
  SetTopWindow(mainWindow);
  mainWindow->SetSize(850, 700);
  wxToolTip::SetDelay(700);
  mainWindow->Show(true);
  std::cout << "Crea DevManager opened." << std::endl;
  
  return true;
}

int wxCreaDevManagerApp::OnExit()
{
  delete wxConfigBase::Set((wxConfigBase *) NULL);
  wxApp::OnExit();
  std::cout << "Crea DevManager closed." << std::endl;
  return 0;
}
