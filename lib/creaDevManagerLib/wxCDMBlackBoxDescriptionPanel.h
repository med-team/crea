/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMBlackBoxDescriptionPanel.h
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMBLACKBOXDESCRIPTIONPANEL_H_
#define WXCDMBLACKBOXDESCRIPTIONPANEL_H_

#include <creaWx.h>
#include <wx/hyperlink.h>

#include "modelCDMBlackBox.h"

/**
 * Black box description panel. Shows the properties and available actions for the described black box.
 */
class wxCDMBlackBoxDescriptionPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Black box description panel Constructor.
   * @param parent Parent window reference.
   * @param blackBox Black box class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMBlackBoxDescriptionPanel(
      wxWindow* parent,
      modelCDMBlackBox* blackBox,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMBlackBoxDescriptionPanel();

  /**
   * Black box description panel Creator.
   * @param parent Parent window reference.
   * @param blackBox Black box class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if the creatios was successful.
   */
  bool Create(
      wxWindow* parent,
      modelCDMBlackBox* blackBox,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

private:
  /**
   * Black box described.
   */
  modelCDMBlackBox* blackBox;
  /**
   * Black box author control.
   */
  wxStaticText* authortc;
  /**
   * Black box description control
   */
  wxStaticText* descriptiontc;
  /**
   * Black box categories control.
   */
  wxStaticText* categoriestc;

  //handlers
protected:
  /**
   * Handles when a return link is pressed.
   * @param event Has the link reference to know where to return
   */
  void OnBtnReturn(wxHyperlinkEvent& event);
  /**
   * Handles when the set author button is pressed.
   */
  void OnBtnSetAuthor(wxCommandEvent& event);
  /**
   * Handles when the set description button is pressed.
   */
  void OnBtnSetDescription(wxCommandEvent& event);
  /**
   * Handles when the set categories button is pressed.
   */
  void OnBtnSetCategories(wxCommandEvent& event);
  /**
   * Handles when the open Cxx button is pressed.
   */
  void OnBtnOpenCxx(wxCommandEvent& event);
  /**
   * Handles when the open Hxx button is pressed.
   */
  void OnBtnOpenHxx(wxCommandEvent& event);
  /**
   * Handles when the open Folder button is pressed.
   */
  void OnBtnOpenFolder(wxCommandEvent& event);

  /**
   * Handles when the open Cxx button is hovered.
   */
  void OnCxxMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the open Cxx button finishes hover.
   */
  void OnCxxMouseExit(wxMouseEvent& event);
  /**
   * Handles when the open Hxx button is hovered.
   */
  void OnHxxMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the open Hxx button finishes hover.
   */
  void OnHxxMouseExit(wxMouseEvent& event);


};

#endif /* WXCDMBLACKBOXDESCRIPTIONPANEL_H_ */
