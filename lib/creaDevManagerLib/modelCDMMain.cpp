/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */

/*
 * modelCDMMain.cpp
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMMain.h"


#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <fstream>

#include "CDMUtilities.h"
#include "modelCDMProject.h"

modelCDMMain::modelCDMMain()
{
  this->project = NULL;
}

modelCDMMain::~modelCDMMain()
{
  if(project != NULL)
    {
      delete this->project;
      this->project = NULL;
    }
}

modelCDMProject* modelCDMMain::GetProject() const
{
  return this->project;
}

bool modelCDMMain::CreateProject(
    const std::string& name,
    const std::string& location,
    std::string*& result,
    const std::string& author,
    const std::string& description)
{
  std::cout << "Open selection path: "<< location << std::endl;
  //get fixed location
  std::string locationFixed = CDMUtilities::fixPath(location);
  std::cout << "Opening path: "<< locationFixed << std::endl;

  //creates project in disk
#if(_WIN32)

  std::string command("creaNewProject.bat ");
  std::string command1("creaSed.exe ");
  std::string command2("del ");

  command  += "\"" + locationFixed + "\" \"" + name + "\"";
  command1 += "\"" + locationFixed + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.in\" " + "PROJECT_NAME " + name + "> \"" + locationFixed + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt\"";
  command2 += "\"" + locationFixed + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.in\"";

  if (system (command.c_str()))
    {
      result = new std::string("An error occurred while running '" + command + "'.");
      return false;
    }

  if (system (command1.c_str()))
    {
      result = new std::string("An error occurred while running '" + command1 + "'.");
      return false;
    }
  if (system (command2.c_str()))
    {
      result = new std::string("An error occurred while running '" + command2 + "'.");
      return false;
    }

  std::string nomDirectory = locationFixed + CDMUtilities::SLASH + name;
  std::string nomPackageDirectory = nomDirectory + CDMUtilities::SLASH + "bbtk_" + name + "_PKG";
  std::string bbCreatePackage("bbCreatePackage ");
  bbCreatePackage += "\"" + nomDirectory + "\" \"" + name + "\" \"" + author + "\" \"" + description + "\"";
  if (!system (bbCreatePackage.c_str()))
    {
      result = new std::string("An error occurred while running '" + bbCreatePackage + "'.");
	  return false;
    }
  std::string add;
  add = "echo ADD_SUBDIRECTORY(bbtk_" + name  + "_PKG) >> \"" + nomDirectory + CDMUtilities::SLASH + "CMakeLists.txt\"";
  if (system (add.c_str()))
    {
      result = new std::string("An error occurred while running '" + add + "'.");
	  return false;
    }

  this->project = new modelCDMProject(NULL, nomDirectory, name);

#else
  // ------ LINUX / MacOS
  std::string command("creaNewProject.sh ");
  command += "\"" + locationFixed + "\"" +" \"" + name + "\"";
  //std::cout << "executing " << command << std::endl;
  if (system ( command.c_str() ) )
    {
      result = new std::string("An error occured while running '" + command + "'.");
      return false;
    }

  std::string nomDirectory = locationFixed + CDMUtilities::SLASH + name;
  std::string nomPackageDirectory = nomDirectory + CDMUtilities::SLASH + "bbtk_" + name + "_PKG";

  std::string bbCreatePackage("bbCreatePackage ");
  bbCreatePackage += "\"" + nomDirectory + "\" \"" + name + "\" \"" + author + "\" \"" + description + "\"";
  //std::cout << "executing " << bbCreatePackage << std::endl;
  system (bbCreatePackage.c_str());

  std::string add;
  add = "echo 'ADD_SUBDIRECTORY(bbtk_" + name  + "_PKG)' >> \"" + nomDirectory + CDMUtilities::SLASH + "CMakeLists.txt\"";

  //std::cout << "executing " << add << std::endl;
  system(add.c_str());

  if(this->project != NULL)
    {
      if (!CloseProject(result))
        return false;
    }

  this->project = new modelCDMProject(NULL, nomDirectory, name);

#endif

  return true;
}

bool modelCDMMain::OpenProject(
    const std::string& path,
    std::string*& result
)
{
  //std::cout << "Open selection path: "<< path << std::endl;
  //get fixed path
  std::string pathFixed = CDMUtilities::fixPath(path);
  std::cout << "Opening path: "<< pathFixed << std::endl;

  //check if its binaries' folder
  bool isBinary = false;
  std::string pathBuild = "";

  //check if Makefile file exists
#ifdef _WIN32
  std::string pathMakefile = pathFixed + CDMUtilities::SLASH + "CMakeCache.txt";
  FILE* pFile;
  errno_t errorOpen = fopen_s(&pFile, pathMakefile.c_str(), "r");
#else
  std::string pathMakefile = pathFixed + CDMUtilities::SLASH + "Makefile";
  FILE* pFile;
  pFile = fopen(pathMakefile.c_str(), "r");
  bool errorOpen = (pFile == NULL);
#endif
  //is the binary folder
  if (!errorOpen && pFile)
    {
      fclose(pFile);
      std::ifstream readFile;
      readFile.open(pathMakefile.c_str());
      std::string word;

      
#ifdef _WIN32
	  while(!isBinary && !readFile.eof())
        {
            getline(readFile, word, '\n');
			std::vector<std::string> words;
			CDMUtilities::splitter::split(words,word,"=",CDMUtilities::splitter::no_empties);
			if(words.size() && words[0] == "CMAKE_HOME_DIRECTORY:INTERNAL")
            {
			  pathBuild = pathFixed;
              pathFixed = CDMUtilities::fixPath(words[1]);
			  for (int i = 0; i < (int)(pathFixed.size()); i++)
                {
                  if (pathFixed[i]=='/')
					  pathFixed[i]='\\';
                }
              isBinary = true;
            }
        }
#else
      while(!isBinary && readFile >> word)
        {
            if(word == "CMAKE_SOURCE_DIR")
            {
              readFile >> word;
              readFile.ignore();
              getline(readFile, word, '\n');
              pathBuild = pathFixed;
              pathFixed = CDMUtilities::fixPath(word);
              isBinary = true;
            }
	    }
#endif
      readFile.close();
    }

  //check if its source's folder
  bool isSource = false;
  std::string pathSource = "";
  //check if CMakeLists file exists
  std::string pathCMakeLists = pathFixed + CDMUtilities::SLASH + "CMakeLists.txt";
#ifdef _WIN32
  errorOpen = fopen_s(&pFile, pathCMakeLists.c_str(), "r");
#else
  pFile = fopen(pathCMakeLists.c_str(), "r");
  errorOpen = (pFile == NULL);
#endif

  //is the source folder
  if (!errorOpen && pFile)
    {
      fclose(pFile);
      std::ifstream readFile;
      readFile.open(pathCMakeLists.c_str());
      std::string word;


      while(!isSource && !readFile.eof())
        {
          std::getline(readFile,word,'(');
          std::vector<std::string> wordBits;
          CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

          if(wordBits[wordBits.size()-1] == "PROJECT")
            {
              isSource = true;
              pathSource = pathFixed;
            }
        }
      readFile.close();
    }

  //if is source folder
  if(isSource)
    {
      if(this->project != NULL)
        {
          if (!CloseProject(result))
            return false;
        }
      std::vector<std::string> words;
      CDMUtilities::splitter::split(words, pathSource, CDMUtilities::SLASH, CDMUtilities::splitter::no_empties);

      std::cout << "Project sources at: " << pathSource << std::endl;
      if(isBinary)
        {
          std::cout << ", and built in: " << pathBuild << std::endl;

          this->project = new modelCDMProject(NULL, pathSource, words[words.size()-1], pathBuild);
        }
      else
        {
          this->project = new modelCDMProject(NULL, pathSource, words[words.size()-1]);
        }
    }
  else
    {
      result = new std::string("No source folder found. Please make sure to select either the project's build or source folder. " + pathBuild + pathFixed);
      return false;
    }

  return true;
}

bool modelCDMMain::RefreshProject(
    std::string*& result
)
{
  //recreate the project using the project's path
  if (this->project != NULL)
    {
      return this->project->Refresh(result);
    }
  else
    {
      result = new std::string("There is no open project.");
      return false;
    }
}

std::map<wxCDMTreeItemId, modelCDMIProjectTreeNode*>& modelCDMMain::GetModelElements()
{
  return this->modelElements;
}

bool modelCDMMain::CloseProject(
    std::string*& result
)
{
  //delete the project tree and leave it as NULL
  if (this->project != NULL)
    {
      delete this->project;
      this->project = NULL;
      return true;
    }
  else
    {
      result = new std::string("There is no open project.");
      return false;
    }
}
