/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */

/*
 * modelCDMProject.cpp
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMProject.h"

#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>
#include <boost/regex.hpp>

#include "CDMUtilities.h"
#include "creaWx.h"
#include "wx/dir.h"
#include <wx/config.h>

modelCDMProject::modelCDMProject()
{
  std::cout << "in constructor1" << std::endl;
  this->appli = NULL;
  this->lib = NULL;
  this->CMakeLists = NULL;
}

modelCDMProject::modelCDMProject(
    modelCDMIProjectTreeNode* parent,
    const std::string& path,
    const std::string& name,
    const std::string& buildPath
)
{
  std::cout << "creating project: " + name + " in " + path + "\n";
  this->parent = parent;
  this->path = CDMUtilities::fixPath(path);
  //open makelists file
  std::string pathFixed(CDMUtilities::fixPath(path));

  std::string pathMakeLists = pathFixed + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //std::cout << "leyendo " << word << std::endl;
      //get project name
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "PROJECT")
        {
          std::getline(confFile,word,')');
          std::vector<std::string> nameBits;
          CDMUtilities::splitter::split(nameBits, word, " ", CDMUtilities::splitter::no_empties);

          this->name = this->nameProject = "";
          for (int i = 0; i < (int)(nameBits.size()); i++)
            {
              if(i != 0)
                this->name += " ";
              this->name += nameBits[i];
            }
          this->nameProject = this->name;

        }


      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get project version
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "PROJECT_MAJOR_VERSION")
            {
              version = wordBits[1];
            }
          if(wordBits[0] == "PROJECT_MINOR_VERSION")
            {
              version += "." + wordBits[1];
            }
          if(wordBits[0] == "PROJECT_BUILD_VERSION")
            {
              version += "." + wordBits[1];
            }

          //get project versionDate
          if(wordBits[0] == "PROJECT_VERSION_DATE")
            {
              std::vector<std::string> versionBits;
              CDMUtilities::splitter::split(versionBits, wordBits[1], "\"", CDMUtilities::splitter::no_empties);
              versionDate = versionBits[0];
            }
          //get project buildPath

          if (buildPath != "")
            {
              this->buildPath = buildPath;
            }
          else
            {
              this->buildPath = this->path + "Bin";
            }
        }
    }
  confFile.close();

  this->type = wxDIR_DIRS;
  this->level = 0;

  this->children.clear();
  this->appli = NULL;
  this->lib = NULL;
  this->packages.clear();


  //check all folders
  wxDir dir(crea::std2wx((pathFixed).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          //if appli, create appli
          if(stdfileName == "appli")
            {
              this->appli = new modelCDMAppli(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->appli);
            }
          //if lib, create lib
          else if(stdfileName == "lib")
            {
              this->lib = new modelCDMLib(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->lib);
            }
          else
            {
              bool isPackage = false;

              // check if cmakelists file exist in folder.
              std::string tmppath = this->GetPath() + CDMUtilities::SLASH + stdfileName + CDMUtilities::SLASH + "CMakeLists.txt";
              CDMUtilities::CMLFile CMfile = CDMUtilities::readCMLFile(tmppath.c_str());
              if (CMfile.size() > 0)
                {
                  std::cout << "-->" << stdfileName << " ::has cmakelists" << std::endl;
                  // check if cmakelists file has the SET(BBTK_PACKAGE_NAME [pkgname]) instruction.
                  for (int i = 0; !isPackage && i < CMfile.size(); ++i) {
                    if (CMfile[i].first == "command" && CMfile[i].second.size() > 1)
                      {
                        std::string cm = CMfile[i].second[0];

                        int pos = 1;
                        while(pos < CMfile[i].second.size()-1 && !isalpha(CMfile[i].second[pos][0]))
                          pos++;

                        std::string pm1 = CMfile[i].second[pos];

                        std::cout << cm << " " << pm1 << std::endl;
                        std::transform(cm.begin(), cm.end(), cm.begin(), ::toupper);
                        std::transform(pm1.begin(), pm1.end(), pm1.begin(), ::toupper);
                        if(cm == "SET" && pm1 == "BBTK_PACKAGE_NAME")
                          {
                            isPackage = true;
                          }
                      }
                  }
                }

              // if package, create package
              if(isPackage)
                {
                  std::cout << "is Package\n";
                  modelCDMPackage* package = new modelCDMPackage(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  std::cout << "Package created\n";
                  this->packages.push_back(package);
                  this->children.push_back(package);
                }
              // if is an unknown folder, create folder
              else
                {
                  this->children.push_back(new modelCDMFolder(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                }

            }

          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              this->CMakeLists = new modelCDMCMakeListsFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->CMakeLists);
            }
          //if is a code file, create modelCDMCodeFile
          else if(
              fileType == ".c" ||
              fileType == ".cxx" ||
              fileType == ".h" ||
              fileType == ".cpp" ||
              fileType == ".txx" ||
              fileType == ".cmake" )
            {
              this->children.push_back(new modelCDMCodeFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }
          //if is an unknown file, create file
          else
            {
              this->children.push_back(new modelCDMFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }
    }

  this->SortChildren();
  std::sort(this->packages.begin(), this->packages.end(), CompareNodeItem);

}

modelCDMProject::~modelCDMProject()
{
}

const std::string& modelCDMProject::GetNameProject() const
{
  return this->nameProject;
}

const std::string& modelCDMProject::GetVersion() const
{
  return this->version;
}

const std::string& modelCDMProject::GetVersionDate() const
{
  return this->versionDate;
}

const std::string& modelCDMProject::GetBuildPath() const
{
  return this->buildPath;
}

const std::vector<modelCDMPackage*>& modelCDMProject::GetPackages() const
{
  return this->packages;
}

modelCDMAppli* modelCDMProject::GetAppli() const
{
  return this->appli;
}

modelCDMLib* modelCDMProject::GetLib() const
{
  return this->lib;
}

std::string modelCDMProject::GetBuildInstruction() const
{
  wxConfigBase* pConfig =  wxConfigBase::Get();
  std::string commandline = crea::wx2std(pConfig->Read(wxT("BUILD_COMMAND"), crea::std2wx(CDMUtilities::BUILD_COMMAND)));


  std::string makeComm = "cd \"" + this->buildPath + "\";" + commandline;// -C \"" + this->buildPath + "\""; /*> \"" + this->buildPath + CDMUtilities::SLASH + "building.log\" 2>&1";*/
  return makeComm;
}

bool modelCDMProject::SetVersion(const std::string& version, std::string*& result)
{

  std::vector<std::string> vers;
  CDMUtilities::splitter::split(vers, version, " .", CDMUtilities::splitter::no_empties);

  time_t now = time(0);

  tm ltm;
#ifdef _WIN32
  localtime_s(&ltm, &now);
#else
  ltm = *(localtime(&now));
#endif

  std::stringstream date;
  date << ltm.tm_mday << "/" << 1 + ltm.tm_mon << "/" << 1900 + ltm.tm_year;

  //set name of library in CMakeLists inside copied folder
  std::string line;
  std::ifstream in((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
  if( !in.is_open())
    {
      result = new std::string("CMakeLists.txt file failed to open.");
      return false;
    }
  std::ofstream out((this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string("CMakeLists.txt.tmp file failed to open.");
      return false;
    }
  while (getline(in, line))
    {
      if(line.find("SET(PROJECT_MAJOR_VERSION") != std::string::npos)
        line = "SET(PROJECT_MAJOR_VERSION " + vers[0] + ")";
      else if(line.find("SET(PROJECT_MINOR_VERSION") != std::string::npos)
        line = "SET(PROJECT_MINOR_VERSION " + vers[1] + ")";
      else if(line.find("SET(PROJECT_BUILD_VERSION") != std::string::npos)
        line = "SET(PROJECT_BUILD_VERSION " + vers[2] + ")";
      else if(line.find("SET(PROJECT_VERSION_DATE") != std::string::npos)
        line = "SET(PROJECT_VERSION_DATE \"" + date.str() + "\")";
      out << line << std::endl;
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
  std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->version = vers[0] + "." + vers[1] + "." + vers[2];
  this->versionDate = date.str();
  return true;
}

bool modelCDMProject::SetBuildPath(const std::string& path, std::string*& result)
{
  if(path == "")
    {
      result = new std::string("The path cannot be empty");
      return false;
    }
  if(path == this->path)
    {
      result = new std::string("The path cannot be same as the project sources");
      return false;
    }
  this->buildPath = path;
  return true;
}

modelCDMIProjectTreeNode* modelCDMProject::CreatePackage(
    const std::string& name,
    std::string*& result,
    const std::string& authors,
    const std::string& authorsEmail,
    const std::string& description,
    const std::string& version
)
{
  //fixing input parameters
  std::vector<std::string> words;

  CDMUtilities::splitter::split(words,name," '/\"\\,.",CDMUtilities::splitter::no_empties);
  std::string nameFixed = "";
  for (int i = 0; i < (int)(words.size()); i++)
    {
      nameFixed += words[i];
    }

  words.clear();
  CDMUtilities::splitter::split(words,authors," '/\"\\,.",CDMUtilities::splitter::no_empties);
  std::string authorFixed;
  for (int i = 0; i < (int)(words.size()); i++)
    {
      authorFixed += words[i];
    }

  words.clear();
  std::string descriptionFixed;
  CDMUtilities::splitter::split(words,authorsEmail," '/\"\\,",CDMUtilities::splitter::no_empties);
  for (int i = 0; i < (int)(words.size()); i++)
    {
      descriptionFixed += words[i] + "/";
    }
  words.clear();
  CDMUtilities::splitter::split(words,description," '\"",CDMUtilities::splitter::no_empties);
  for (int i = 0; i < (int)(words.size()); i++)
    {
      descriptionFixed += "_" + words[i];
    }

  //call project to create package : use bbCreatePackage <path> <name> [author] [description]
  std::string creationCommand = "bbCreatePackage \"" + this->path + "\" \"" + nameFixed + "\" \"" + authorFixed + "\" \"" + descriptionFixed + "\"";
  //TODO: bbCreatePackage script always returning 0. It should return 1 or greater if any error
  bool resultCommand = 0 != system(creationCommand.c_str());
#ifdef _WIN32
  resultCommand = false;
#endif
  if(resultCommand)
    {
      result = new std::string("An error occurred while running '" + creationCommand + "'.");
      return NULL;
    }

  //add library to project CMakeLists
  std::fstream out1((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
  if (out1.is_open())
    {
      out1 << "ADD_SUBDIRECTORY(bbtk_" << nameFixed << "_PKG)" << std::endl;
      out1.close();
    }

  //add library to model
  modelCDMPackage* package = new modelCDMPackage(this, this->path + CDMUtilities::SLASH + "bbtk_" + nameFixed + "_PKG", "bbtk_" + nameFixed + "_PKG", this->level + 1);
  this->packages.push_back(package);
  this->children.push_back(package);

  //TODO: set package version

  this->SortChildren();

  result = new std::string(this->path + CDMUtilities::SLASH + name);
  return package;
}

modelCDMIProjectTreeNode* modelCDMProject::CreateLibrary(
    const std::string& name,
    std::string*& result,
    const std::string& path
)
{
  if(this->lib != NULL)
    {
      return this->lib->CreateLibrary(name, result);
    }
  result = new std::string("there is no lib folder in this project.");
  return NULL;
}

modelCDMIProjectTreeNode* modelCDMProject::CreateApplication(
    const std::string& name,
    const int& type,
    std::string*& result,
    const std::string& path
)
{
  if(this->appli != NULL)
    {
      return this->appli->CreateApplication(name, type, result);
    }
  result = new std::string("there is no appli folder in this project.");
  return NULL;
}

modelCDMIProjectTreeNode* modelCDMProject::CreateBlackBox(
    const std::string& name,
    const std::string& package,
    const std::string& authors,
    const std::string& authorsEmail,
    const std::string& categories,
    const std::string& description
)
{
  //TODO: implement method
  return NULL;
}

bool modelCDMProject::OpenCMakeListsFile(std::string*& result)
{
  if (!CDMUtilities::openTextEditor(this->CMakeLists->GetPath()))
    return true;
  else
    {
      result = new std::string("Couldn't open CMakeLists file.");
      return false;
    }
}

const bool modelCDMProject::Refresh(std::string*& result)
{
  std::cout << "refreshing project" << std::endl;
  //open makelists file
  std::string pathMakeLists = this->path + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //std::cout << "leyendo " << word << std::endl;
      //get project name
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "PROJECT")
        {
          std::getline(confFile,word,')');
          std::vector<std::string> nameBits;
          CDMUtilities::splitter::split(nameBits, word, " ", CDMUtilities::splitter::no_empties);

          this->name = this->nameProject = "";
          for (int i = 0; i < (int)(nameBits.size()); i++)
            {
              if(i != 0)
                this->name += " ";
              this->name += nameBits[i];
            }
          this->nameProject = this->name;

        }


      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get project version
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "PROJECT_MAJOR_VERSION")
            {
              version = wordBits[1];
            }
          if(wordBits[0] == "PROJECT_MINOR_VERSION")
            {
              version += "." + wordBits[1];
            }
          if(wordBits[0] == "PROJECT_BUILD_VERSION")
            {
              version += "." + wordBits[1];
            }

          //get project versionDate
          if(wordBits[0] == "PROJECT_VERSION_DATE")
            {
              std::vector<std::string> versionBits;
              CDMUtilities::splitter::split(versionBits, wordBits[1], "\"", CDMUtilities::splitter::no_empties);
              versionDate = versionBits[0];
            }
        }
    }
  confFile.close();

  this->type = wxDIR_DIRS;
  this->level = 0;

  std::vector<bool> checked(this->children.size(), false);
  std::vector<bool> checkedPackages(this->packages.size(), false);

  //check all folders
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          //if appli, create appli
          if(stdfileName == "appli")
            {
              if (this->appli == NULL)
                {
                  this->appli = new modelCDMAppli(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->appli);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->appli) - this->children.begin();
                  checked[pos] = true;
                  if(!this->appli->Refresh(result))
                    return false;
                }
            }
          //if lib, create lib
          else if(stdfileName == "lib")
            {
              if (this->lib == NULL)
                {
                  this->lib = new modelCDMLib(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->lib);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->lib) - this->children.begin();
                  checked[pos] = true;
                  if(!this->lib->Refresh(result))
                    return false;
                }

            }
          //if package , create package
          /*else if(stdfileName.size() > 9 && stdfileName.substr(0,5) == "bbtk_" && stdfileName.substr(stdfileName.size()-4,4) == "_PKG")
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->packages.size()); i++)
                {
                  if (this->packages[i]->GetName() == stdfileName)
                    {
                      found = true;
                      int pos = std::find(this->children.begin(), this->children.end(), this->packages[i]) - this->children.begin();
                      checked[pos] = true;
                      checkedPackages[i] = true;
                      if(!this->packages[i]->Refresh(result))
                        return false;
                    }
                }
              if(!found)
                {
                  modelCDMPackage* package = new modelCDMPackage(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->packages.push_back(package);
                  this->children.push_back(package);
                }

            }*/
          // package or folder
          else
            {
              bool isPackage = false;

              // check if cmakelists file exist in folder.
              std::string tmppath = this->GetPath() + CDMUtilities::SLASH + stdfileName + CDMUtilities::SLASH + "CMakeLists.txt";
              CDMUtilities::CMLFile CMfile = CDMUtilities::readCMLFile(tmppath.c_str());
              if (CMfile.size() > 0)

                {
                  // check if cmakelists file has the SET(BBTK_PACKAGE_NAME [pkgname]) instruction.
                  for (int i = 0; !isPackage && i < CMfile.size(); ++i) {
                    if (CMfile[i].first == "command" && CMfile[i].second.size() > 1)
                      {
                        std::string cm = CMfile[i].second[0];

                        int pos = 1;
                        while(pos < CMfile[i].second.size()-1 && !isalpha(CMfile[i].second[pos][0]))
                          pos++;

                        std::string pm1 = CMfile[i].second[pos];

                        std::cout << cm << " " << pm1 << std::endl;
                        std::transform(cm.begin(), cm.end(), cm.begin(), ::toupper);
                        std::transform(pm1.begin(), pm1.end(), pm1.begin(), ::toupper);
                        if(cm == "SET" && pm1 == "BBTK_PACKAGE_NAME")
                          {
                            isPackage = true;
                          }
                      }
                  }
                }

              // if package, create package
              if (isPackage)
                {
                  bool found = false;
                  for (int i = 0; !found && i < (int)(this->packages.size()); i++)
                    {
                      if (this->packages[i]->GetName() == stdfileName)
                        {
                          found = true;
                          int pos = std::find(this->children.begin(), this->children.end(), this->packages[i]) - this->children.begin();
                          checked[pos] = true;
                          checkedPackages[i] = true;
                          if(!this->packages[i]->Refresh(result))
                            return false;
                        }
                    }

                  if (!found)
                    {
                      modelCDMPackage* package = new modelCDMPackage(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->packages.push_back(package);
                      this->children.push_back(package);
                    }
                }
              //if is an unknown folder, create folder
              else
                {
                  bool found = false;
                  for (int i = 0; !found && i < (int)(this->children.size()); i++)
                    {
                      if (this->children[i]->GetName() == stdfileName)
                        {
                          found = true;
                          checked[i] = true;
                          if(!this->children[i]->Refresh(result))
                            return false;
                        }
                    }

                  if(!found)
                    {
                      modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(folder);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }
          //if is a code file, create modelCDMCodeFile
          //if is an unknown file, create file
          else
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  if(
                    fileType == ".c" ||
                    fileType == ".cxx" ||
                    fileType == ".h" ||
                    fileType == ".cpp" ||
                    fileType == ".txx" ||
                    fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }
    }

  for (int i = 0; i < (int)(checkedPackages.size()); i++)
    {
      if(!checkedPackages[i])
        {
          this->packages.erase(this->packages.begin()+i);
          checkedPackages.erase(checkedPackages.begin()+i);
          i--;
        }
    }
  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }

  this->SortChildren();
  std::sort(this->packages.begin(), this->packages.end(), CompareNodeItem);
  return true;
}

bool modelCDMProject::ConfigureBuild(std::string*& result)
{
  //TODO: adjust for windows and mac
#ifdef _WIN32
  // ------ Windows
  if(0 == system("cmake-gui"))
    return true;
  else
    {
	  result = new std::string("There was an error opening cmake-gui. Please make sure it's installed and that cmake's bin folder is in the system path.");
      return false;
    }
#elif __APPLE__
  // ------ Apple
#else
  // ------ Linux
  //open binary folder
  wxDir dir(crea::std2wx((this->buildPath).c_str()));

  //if folder doesn't exist then create it
  if (!dir.IsOpened())
    {
      //create command line to create folder
      std::string createComm = "mkdir \"" + this->buildPath + "\"";
      //execute creation command
      if (system(createComm.c_str()))
        {
          //if there was an error then report it
          result = new std::string("There was an error creating the build path: \"" + this->buildPath + "\"");
          return false;
        }
    }
  //create command line to execute ccmake
  //TODO:: adjust for different Linux distributions
  std::string confComm = "gnome-terminal --tab --working-directory=\"" + this->buildPath + "\" -e \"ccmake '" + this->path + "'\"";
  //execute command
  if(CDMUtilities::openTerminal(confComm))
    {
      //if there was an error then report it
      result = new std::string("There was an error opening the configuration tool in the desired place.");
      return false;
    }
#endif
  return true;
}

bool modelCDMProject::Build(std::string*& result, const std::string& line)
{
  //TODO: adjust for windows and mac
#ifdef _WIN32
  // ------ Windows
	std::string command = "start \"\" \"" + this->buildPath + CDMUtilities::SLASH + this->nameProject + ".sln\"";
  //wxMessageBox(crea::std2wx(command), wxT("Project Compilation - Check!"));
	if(0 == system(command.c_str()))
    return true;
  else
    {
      result = new std::string("An error has happened running: \"" + command + "\". Please make sure to have visual c++ installed.");
	  return false;
    }
#elif __APPLE__
  // ------ Apple
#else
  // ------ Linux
  //open binary folder
  wxDir dir(crea::std2wx((this->buildPath).c_str()));

  //if binary folder can't be opened then return false
  if (!dir.IsOpened())
    {
      result = new std::string("The build path could not be opened. Make sure to configure the project before compiling it");
      return false;
    }
  //create make command
  std::string makeComm = "gnome-terminal -e \"bash -c \\\"";
  for (int i = 0; i < line.size(); i++)
    {
      if(line[i] == '"')
        {
          makeComm+="\\\\\\\"";
        }
      else if(line[i] == '\\')
        {
          makeComm+="\\\\\\\\";
        }
      else
        {
          makeComm.push_back(line[i]);
        }
    }
  makeComm += "; echo -e '\\a'; bash";
  makeComm += "\\\"\"";

  std::cout << "executing '" << makeComm << "'" << std::endl;
  //execute make command
  if(system(makeComm.c_str()))
    {
      //if there was an error then report it
      result = new std::string("There was an error compiling the project, please check the \"building.log\" file located in the build folder to read more about the problem.");
      return false;
    }
#endif
  return true;
}

bool modelCDMProject::Connect(std::string*& result, const std::string& folder)
{
  //TODO: adjust for mac
#ifdef _WIN32
  // ------ Windows
  //open binary folder
  wxDir dir(crea::std2wx(folder));

  //if binary folder can't be opened then return false
  if (!dir.IsOpened())
    {
      result = new std::string("The path could not be opened. Make sure the folder exists and contains a bbtkPackage file.");
      return false;
    }
  //create plug command
  std::string plugComm = "bbPlugPackage \"" + folder + "\"";
  std::cout << "executing '" << plugComm << "'" << std::endl;
  //execute plug command
  if(system(std::string("start cmd.exe /k \"" + plugComm + "\"").c_str()))
    {
      //if there was an error then report it
      result = new std::string("There was an error plugging the packages of the project, please check the console to read more about the problem.");
      return false;
    }
#elif __APPLE__
  // ------ Apple
#else
  // ------ Linux
  //open binary folder
  wxDir dir(crea::std2wx(folder));

  //if binary folder can't be opened then return false
  if (!dir.IsOpened())
    {
      result = new std::string("The path could not be opened. Make sure the folder exists and contains a bbtkPackage file.");
      return false;
    }
  //create plug command
  std::string plugComm = "bbPlugPackage \"" + this->buildPath + "\"";

  std::string Comm = "gnome-terminal -e \"bash -c \\\"";
    for (int i = 0; i < plugComm.size(); i++)
      {
        if(plugComm[i] == '"')
          {
            Comm+="\\\\\\\"";
          }
        else if(plugComm[i] == '\\')
          {
            Comm+="\\\\\\\\";
          }
        else
          {
            Comm.push_back(plugComm[i]);
          }
      }
    Comm += "; echo -e '\\a'; bash";
    Comm += "\\\"\"";


  std::cout << "executing '" << Comm << "'" << std::endl;
  //execute plug command
  if(system(Comm.c_str()))
    {
      //if there was an error then report it
      result = new std::string("There was an error plugging the packages of the project, please check the console to read more about the problem.");
      return false;
    }
#endif
  return true;
}

void modelCDMProject::CheckStructure(std::map<std::string, bool>& properties)
{
  //check cmake exist
  if(this->CMakeLists != NULL)
    {
      //set properties parameters based on model
      properties["project add appli"] = this->appli != NULL;
      properties["project add lib"] = this->lib != NULL;
      for (int i = 0; i < (int)(this->packages.size()); i++)
        properties["project add " + packages[i]->GetName()] = false;

      //open cmakelists
      std::ifstream confFile;
      confFile.open((this->CMakeLists->GetPath()).c_str());

      //take everything that is not commented
      std::string fileContent;

      std::string word;
      std::vector<std::string> words;
      while(confFile.is_open() && !confFile.eof())
        {
          std::getline(confFile,word, '\n');
          if(word[0] != '#')
            {
              CDMUtilities::splitter::split(words, word, "#", CDMUtilities::splitter::empties_ok);
              if (words.size() > 0)
                {
                  word = words[0];
                  CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::empties_ok);
                  for (int i = 0; i < (int)(words.size()); i++)
                    {
                      if(words[i].substr(0,2) == "//")
                        break;
                      fileContent += words[i] + " ";
                    }
                }

            }
        }

      //check every instruction
      std::stringstream ss(fileContent);
      while(!ss.eof())
        {
          std::getline(ss,word, '(');

          //check instruction name
          CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

          //set instructions
          if (words.size() > 0 && words[words.size()-1] == "SET")
            {
              std::getline(ss,word, ')');

              CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);
              if (words.size() > 1)
                {
                  if (words[0] == "USE_CREA" && words[1] == "ON")
                    {
                      properties["project set USE_CREA"] = true;
                    }
                  else if (words[0] == "USE_GDCM" && words[1] == "ON")
                    {
                      properties["project set USE_GDCM"] = true;
                    }
                  else if (words[0] == "USE_GDCM_VTK" && words[1] == "ON")
                    {
                      properties["project set USE_GDCM_VTK"] = true;
                    }
                  else if (words[0] == "USE_GDCM2" && words[1] == "ON")
                    {
                      properties["project set USE_GDCM2"] = true;
                    }
                  else if (words[0] == "USE_WXWIDGETS" && words[1] == "ON")
                    {
                      properties["project set USE_WXWIDGETS"] = true;
                    }
                  else if (words[0] == "USE_KWWIDGETS" && words[1] == "ON")
                    {
                      properties["project set USE_KWWIDGETS"] = true;
                    }
                  else if (words[0] == "USE_VTK" && words[1] == "ON")
                    {
                      properties["project set USE_VTK"] = true;
                    }
                  else if (words[0] == "USE_ITK" && words[1] == "ON")
                    {
                      properties["project set USE_ITK"] = true;
                    }
                  else if (words[0] == "USE_BOOST" && words[1] == "ON")
                    {
                      properties["project set USE_BOOST"] = true;
                    }
                }
            }
          //add instructions
          else if (words.size() > 0 && words[words.size()-1] == "ADD_SUBDIRECTORY")
            {
              std::getline(ss,word, ')');
              //std::cout << word << std::endl;
              CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

              if (words.size() > 0)
                {
                  if(words[0] == "appli")
                    {
                      properties["project add "+words[0]] = this->appli != NULL;
                    }
                  else if(words[0] == "lib")
                    {
                      properties["project add "+words[0]] = this->lib != NULL;
                    }
                  else
                    {
                      properties["project add "+words[0]] = true;
                    }
                }
            }
        }

    }

  //check appli's structure
  this->appli->CheckStructure(properties);
  //check lib's structure
  this->lib->CheckStructure(properties);
  //check packages' structure
  for (int i = 0; i < (int)(this->packages.size()); i++)
    {
      properties["package " + this->packages[i]->GetName()] = true;
      this->packages[i]->CheckStructure(properties);
    }
}

bool modelCDMProject::IsPackageIncluded(const std::string& package_name)
{
  if (this->HasCMakeLists())
    {
      CDMUtilities::CMLFile cmlFile = CDMUtilities::readCMLFile(this->CMakeLists->GetPath().c_str());
      for (int i = 0; i < cmlFile.size(); ++i)
        {
          if (cmlFile[i].first=="command" && cmlFile[i].second[0] == "ADD_SUBDIRECTORY")
            {
              int pos = 1;
              while (pos < cmlFile[i].second.size())
                {
                  if (!isspace(cmlFile[i].second[pos][0]) && cmlFile[i].second[pos][0] != '(' && cmlFile[i].second[pos][0] != '#')
                    {
                      if (package_name == cmlFile[i].second[pos])
                        return true;
                      break;
                    }
                  pos++;
                }
            }
        }
    }
  return false;

}

bool modelCDMProject::SetPackageInclude(const std::string& package_name, const bool& toInclude)
{
  if (this->HasCMakeLists())
    {
      CDMUtilities::CMLFile cmlFile = CDMUtilities::readCMLFile(this->CMakeLists->GetPath().c_str());

      bool found = false;

      for (int i = 0; i < cmlFile.size(); ++i)
        {
          if(toInclude && cmlFile[i].first == "comment")
            {
              std::vector<std::string> segments;
              std::string line = cmlFile[i].second[0];
              while(line[0] == '#')
                line.erase(0,1);

              CDMUtilities::splitter::split(segments, line, " ()", CDMUtilities::splitter::no_empties);
              if (segments.size() > 1 && segments[0] == "ADD_SUBDIRECTORY" && segments[1] == package_name)
                {
                  found = true;
                  while(cmlFile[i].second[0][0] == '#')
                    cmlFile[i].second[0].erase(0,1);
                }
            }
          else if(cmlFile[i].first == "command" && cmlFile[i].second[0] == "ADD_SUBDIRECTORY")
            {
              int pos = 1;
              while (pos < cmlFile[i].second.size())
                {
                  if (!isspace(cmlFile[i].second[pos][0]) && cmlFile[i].second[pos][0] != '(' && cmlFile[i].second[pos][0] != '#')
                    {
                      if (package_name == cmlFile[i].second[pos])
                        {
                          found = true;
                          if (!toInclude)
                            {
                              cmlFile[i].first = "comment";
                              cmlFile[i].second[0] = "#" + cmlFile[i].second[0];
                              while (cmlFile[i].second.size() > 1)
                                {
                                  cmlFile[i].second[0] += cmlFile[i].second[1];
                                  cmlFile[i].second.erase(cmlFile[i].second.begin()+1);
                                }

                            }
                        }
                      break;
                    }
                  pos++;
                }
            }
        }
      if (!found && toInclude)
        {
          CDMUtilities::syntaxElement element;
          element.first = "command";
          element.second.push_back("ADD_SUBDIRECTORY(" + package_name + ")");
          cmlFile.push_back(element);
        }

      return CDMUtilities::writeCMLFile(this->CMakeLists->GetPath().c_str(),cmlFile);
    }
  return false;
}

std::map<std::string, bool> modelCDMProject::Get3rdPartyLibraries()
{
  std::map<std::string, std::string> correspondence;
  correspondence["USE_CREA"] = "Crea";
  correspondence["USE_GDCM"] = "GDCM";
  correspondence["USE_GDCM_VTK"] = "GDCM_VTK";
  correspondence["USE_GDCM2"] = "GDCM2";
  correspondence["USE_WXWIDGETS"] = "WxWidgets";
  correspondence["USE_KWWIDGETS"] = "KWWidgets";
  correspondence["USE_VTK"] = "VTK";
  correspondence["USE_ITK"] = "ITK";
  correspondence["USE_BOOST"] = "Boost";

  std::map<std::string, bool> res;
  res["Crea"] = false;
  res["GDCM"] = false;
  res["GDCM_VTK"] = false;
  res["GDCM2"] = false;
  res["WxWidgets"] = false;
  res["KWWidgets"] = false;
  res["VTK"] = false;
  res["ITK"] = false;
  res["Boost"] = false;

  if (this->HasCMakeLists())
    {
      std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());

      boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*USE_\\w+\\s+ON");
      std::string::const_iterator start, end;
      start = CMfile.begin();
      end = CMfile.end();
      boost::match_results<std::string::const_iterator> what;
      boost::match_flag_type flags = boost::match_default;
      while(boost::regex_search(start, end, what, expression, flags))
        {
          //std::cout << what[0].str() << std::endl;
          boost::regex expression1 = boost::regex("USE_\\w+");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression1, flags))
            {
              std::string dete = what1.str();
              CDMUtilities::normalizeStr(dete);
              //std::cout << dete << std::endl;
              if(correspondence.find(dete) != correspondence.end())
                res[correspondence[dete]] = true;
            }
          start = what[0].second;
        }
    }

  return res;
}

bool modelCDMProject::Set3rdPartyLibrary(const std::string& library_name,
    const bool& toInclude)
{
  std::map<std::string, std::string> correspondence;
  correspondence["Crea"] = "USE_CREA";
  correspondence["GDCM"] = "USE_GDCM";
  correspondence["GDCM_VTK"] = "USE_GDCM_VTK";
  correspondence["GDCM2"] = "USE_GDCM2";
  correspondence["WxWidgets"] = "USE_WXWIDGETS";
  correspondence["KWWidgets"] = "USE_KWWIDGETS";
  correspondence["VTK"] = "USE_VTK";
  correspondence["ITK"] = "USE_ITK";
  correspondence["Boost"] = "USE_BOOST";

  if (correspondence.find(library_name) != correspondence.end())
    {
      std::string library_command = correspondence[library_name];

      if (this->HasCMakeLists())
        {
          std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());
          std::string resCMfile = "";

          std::string::const_iterator start, end;
          boost::match_results<std::string::const_iterator> what, tmp;
          boost::match_flag_type flags = boost::match_default;
          bool found = false;

          start = CMfile.begin();
          end = CMfile.end();

          //search for existing inclusions
          boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*" + library_command + "\\s+ON\\s*\\)");
          while(boost::regex_search(start, end, what, expression, flags))
            {
              found = true;
              resCMfile += what.prefix().str();
              if(!toInclude)
                {
                  std::string dete = what.str();
                  int pos = dete.find("ON",0);
                  dete.replace(pos, 2, "OFF");
                  resCMfile += dete;
                }
              else
                resCMfile += what.str();
              tmp = what;
              start = what[0].second;
            }

          if (found)
            resCMfile += tmp.suffix().str();
          else
            {
              start = CMfile.begin();
              end = CMfile.end();

              //search for existing exclusions
              boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*" + library_command + "\\s+OFF\\s*\\)");
              while(boost::regex_search(start, end, what, expression, flags))
                {
                  found = true;
                  resCMfile += what.prefix().str();
                  if(toInclude)
                    {
                      std::string dete = what.str();
                      int pos = dete.find("OFF",0);
                      dete.replace(pos, 3, "ON");
                      resCMfile += dete;
                    }
                  else
                    resCMfile += what.str();
                  tmp = what;
                  start = what[0].second;
                }

              if (found)
                resCMfile += tmp.suffix().str();
              else
                {
                  start = CMfile.begin();
                  end = CMfile.end();

                  //search for existing commented inclusions
                  expression = boost::regex("^\\h*#+\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*" + library_command + "\\s+ON\\s*\\)");
                  while(boost::regex_search(start, end, what, expression, flags))
                    {
                      found = true;
                      resCMfile += what.prefix().str();
                      if(toInclude)
                        {
                          std::string dete = what.str();
                          for (int i = 0; i < dete.size(); ++i) {
                            if(dete[i] == '#')
                              {
                                dete.erase(i,1);
                                i--;
                              }
                          }
                          resCMfile += dete;
                        }
                      else
                        resCMfile += what.str();

                      tmp = what;
                      start = what[0].second;
                    }

                  if (found)
                    resCMfile += tmp.suffix().str();
                  else
                    {
                      if(toInclude)
                        {
                          start = CMfile.begin();
                          end = CMfile.end();

                          //search for position to insert
                          expression = boost::regex("^\\h*#\\h*Libraries\\/tools used\\h*$");
                          if(boost::regex_search(start, end, what, expression, flags))
                            {
                              found = true;
                              resCMfile += what.prefix().str();
                              resCMfile += what.str();
                              resCMfile += "\nSET(" + library_command + " ON)";
                              resCMfile += what.suffix().str();
                            }
                        }
                      else
                        {
                          found = true;
                        }
                    }
                }
            }
          if (!found) {
            return false;
          }
          else
            return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
        }
    }

  return false;
}
