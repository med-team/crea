/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCreaDevManagerTreeCtrl.cpp
 *
 *  Created on: 19/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectsTreeCtrl.h"

#include <vector>
#include <sstream>

#include <wx/imaglist.h>

#include "creaDevManagerIds.h"
#include "images/AIcon20.xpm"
#include "images/ApIcon20.xpm"
#include "images/BBIcon20.xpm"
#include "images/BBSIcon20.xpm"
#include "images/BBGIcon20.xpm"
#include "images/CIcon20.xpm"
#include "images/CFIcon20.xpm"
#include "images/CMIcon20.xpm"
#include "images/FdIcon20.xpm"
#include "images/FlIcon20.xpm"
#include "images/LbIcon20.xpm"
#include "images/LIcon20.xpm"
#include "images/PrIcon20.xpm"
#include "images/PkIcon20.xpm"

wxCDMProjectsTreeCtrl::wxCDMProjectsTreeCtrl(
    wxWindow *parent,
    wxWindowID id,
    const wxPoint &pos,
    const wxSize &size,
    long style,
    const wxValidator &validator,
    const wxString &name
)
{
  wxCDMProjectsTreeCtrl::Create(parent, id, pos, size, style, validator, name);
}

wxCDMProjectsTreeCtrl::~wxCDMProjectsTreeCtrl()
{
}

bool wxCDMProjectsTreeCtrl::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxPoint &pos,
    const wxSize &size,
    long style,
    const wxValidator &validator,
    const wxString &name
)
{
  wxTreeCtrl::Create (parent, id, pos, size, style, validator, name);

  this->DeleteAllItems();

  wxImageList* images = new wxImageList(20, 20, true);
  this->ID_AIcon = images->Add(wxIcon(AIcon20));
  this->ID_ApIcon = images->Add(wxIcon(ApIcon20));
  this->ID_BBIcon = images->Add(wxIcon(BBIcon20));
  this->ID_BBGIcon = images->Add(wxIcon(BBGIcon20));
  this->ID_BBSIcon = images->Add(wxIcon(BBSIcon20));
  this->ID_Cicon = images->Add(wxIcon(CIcon20));
  this->ID_CMIcon = images->Add(wxIcon(CMIcon20));
  this->ID_FdIcon = images->Add(wxIcon(FdIcon20));
  this->ID_FlIcon = images->Add(wxIcon(FlIcon20));
  this->ID_CFIcon = images->Add(wxIcon(CFIcon20));
  this->ID_LbIcon = images->Add(wxIcon(LbIcon20));
  this->ID_LIcon = images->Add(wxIcon(LIcon20));
  this->ID_PrIcon = images->Add(wxIcon(PrIcon20));
  this->ID_PkIcon = images->Add(wxIcon(PkIcon20));
  this->AssignImageList(images);

  wxTreeItemId rootIndex = this->AddRoot(wxT("No Open Project"), this->ID_Cicon, this->ID_Cicon);

  this->Update();
  return TRUE;
}

void wxCDMProjectsTreeCtrl::BuildTree(std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* >& modelElements, modelCDMProject* projectTree)
{
  this->Unselect();
  std::cout << "building tree" << std::endl;
  this->DeleteAllItems();
  modelElements.clear();
  if(projectTree != NULL)
    {
      projectTree->SetId(this->AddRoot(crea::std2wx(projectTree->GetName()), this->ID_PrIcon));
	  
	  modelElements[projectTree->GetId()] = projectTree;

      std::cout << "Building TreeCtrl for " << projectTree->GetName() << std::endl;
      this->BuildTree(projectTree->GetChildren(), modelElements, projectTree->GetId());

      this->Expand(projectTree->GetId().GetWxId());

      this->Update();
    }
  else
    {
      wxTreeItemId rootIndex = this-> AddRoot(_("No Open Project"), this->ID_Cicon);
    }
}

void wxCDMProjectsTreeCtrl::BuildTree(const std::vector<modelCDMIProjectTreeNode*>& treeNodes, std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* >& modelElements, const wxCDMTreeItemId& parent)
{
  for (int i = 0; i < (int)(treeNodes.size()); i++)
    {
      //cout << projectsTree[i].GetName() << endl;
      if(treeNodes[i] != NULL)
        {
          int idIcon = GetIconId(treeNodes[i]);
          wxString nodeName((treeNodes[i]->GetName()).c_str(), wxConvUTF8);
          treeNodes[i]->SetId(wxCDMTreeItemId(this->AppendItem(parent.GetWxId(), nodeName, idIcon)));
	  
		  modelElements[treeNodes[i]->GetId()] = treeNodes[i];

          std::vector<modelCDMIProjectTreeNode*> innerChildren = treeNodes[i]->GetChildren();
          if(innerChildren.size() > 0)
            {
              this->BuildTree(innerChildren, modelElements, treeNodes[i]->GetId());
            }
        }

    }
}

int wxCDMProjectsTreeCtrl::GetIconId(modelCDMIProjectTreeNode* node)
{
  modelCDMIProjectTreeNode* element = dynamic_cast<modelCDMProject*>(node);
  if(element != NULL)
    {
      return this->ID_PrIcon;
    }
  else
    {
      element = dynamic_cast<modelCDMAppli*>(node);
      if(element != NULL)
        {
          return this->ID_ApIcon;
        }
      else
        {
          element = dynamic_cast<modelCDMApplication*>(node);
          if(element != NULL)
            {
              return this->ID_AIcon;
            }
          else
            {
              element = dynamic_cast<modelCDMLib*>(node);
              if(element != NULL)
                {
                  return this->ID_LbIcon;
                }
              else
                {
                  element = dynamic_cast<modelCDMLibrary*>(node);
                  if(element != NULL)
                    {
                      return this->ID_LIcon;
                    }
                  else
                    {
                      element = dynamic_cast<modelCDMPackage*>(node);
                      if(element != NULL)
                        {
                          return this->ID_PkIcon;
                        }
                      else
                        {
                          element = dynamic_cast<modelCDMBlackBox*>(node);
                          if(element != NULL)
                            {
                              return this->ID_BBIcon;
                            }
                          else
                            {
                              element = dynamic_cast<modelCDMCMakeListsFile*>(node);
                              if(element != NULL)
                                {
                                  return this->ID_CMIcon;
                                }
                              else
                                {
                                  element = dynamic_cast<modelCDMCodeFile*>(node);
                                  if(element != NULL)
                                    {
                                      return this->ID_CFIcon;
                                    }
                                  else
                                    {
                                      element = dynamic_cast<modelCDMBBSFile*>(node);
                                      if(element != NULL)
                                        {
                                          return this->ID_BBSIcon;
                                        }
                                      else
                                        {
                                          element = dynamic_cast<modelCDMBBGFile*>(node);
                                          if(element != NULL)
                                            {
                                              return this->ID_BBGIcon;
                                            }
                                          else
                                            {
                                              element = dynamic_cast<modelCDMFolder*>(node);
                                              if(element != NULL)
                                                {
                                                  return this->ID_FdIcon;
                                                }
                                              else
                                                {
                                                  element = dynamic_cast<modelCDMFile*>(node);
                                                  if(element != NULL)
                                                    {
                                                      return this->ID_FlIcon;
                                                    }
                                                  else
                                                    {
                                                      return this->ID_Cicon;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
