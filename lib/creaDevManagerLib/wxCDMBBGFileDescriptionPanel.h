/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMBBGFileDescriptionPanel.h
 *
 *  Created on: Jun 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMBBGFILEDESCRIPTIONPANEL_H_
#define WXCDMBBGFILEDESCRIPTIONPANEL_H_

#include <creaWx.h>
#include <wx/hyperlink.h>

#include "modelCDMBBGFile.h"

/**
 * BBG File description panel. Shows the file properties and the available actions for it.
 */
class wxCDMBBGFileDescriptionPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * BBG File description panel Constructor.
   * @param parent Parent window reference.
   * @param bbgfile BBGFile class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMBBGFileDescriptionPanel(
      wxWindow* parent,
      modelCDMBBGFile* bbgfile,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMBBGFileDescriptionPanel();

  /**
   * BBG File description panel Constructor.
   * @param parent Parent window reference.
   * @param bbgfile BBGFile class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      modelCDMBBGFile* bbgfile,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

private:
  /**
   * BBG file described.
   */
  modelCDMBBGFile* bbgFile;

  //handlers
protected:
  /**
   * Handles when a return link is pressed.
   * @param event Has the link reference to know where to return
   */
  void OnBtnReturn(wxHyperlinkEvent& event);
  /**
   * Handles when the open file in text editor button is pressed.
   */
  void OnBtnOpenInEditor(wxCommandEvent& event);
  /**
   * Handles when the open file in bbi button is pressed.
   */
  void OnBtnOpenInBBEditor(wxCommandEvent& event);
  /**
   * Handles when the open containing folder button is pressed.
   */
  void OnBtnOpenFolder(wxCommandEvent& event);
};

#endif /* WXCDMBBGFILEDESCRIPTIONPANEL_H_ */
