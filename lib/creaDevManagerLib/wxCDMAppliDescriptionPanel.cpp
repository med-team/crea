/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMAppliDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMAppliDescriptionPanel.h"



#include "wxCDMMainFrame.h"

#include "wxCDMAppliHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/ApIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMAppliDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMAppliDescriptionPanel::OnBtnReturn)
EVT_HYPERLINK(ID_LINK_SELECT_APPLICATION, wxCDMAppliDescriptionPanel::OnLnkApplicationSelect)
EVT_BUTTON(ID_BUTTON_CREATE_APPLICATION, wxCDMAppliDescriptionPanel::OnBtnCreateApplication)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMAppliDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMAppliDescriptionPanel::OnBtnOpenFolder)
EVT_CHECKBOX(ID_CHECK_INCLUDE_APPLICATION, wxCDMAppliDescriptionPanel::OnChBApplicationChange)
END_EVENT_TABLE()

wxCDMAppliDescriptionPanel::wxCDMAppliDescriptionPanel(
    wxWindow* parent,
    modelCDMAppli* appli,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMAppliDescriptionPanel::Create(parent, appli, id, caption, pos, size, style);
}

wxCDMAppliDescriptionPanel::~wxCDMAppliDescriptionPanel()
{
}

bool wxCDMAppliDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMAppli* appli,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->appli = appli;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMAppliDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->appli->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
      returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }
  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->appli->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(ApIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Application Management")),0, wxALIGN_LEFT, 0);

    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Applications
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("A&vailable Applications"));
  propertiesBox->GetStaticBox()->SetToolTip(wxT("Select any of the available applications to see its details or the modify them."));
  wxPanel* propertiesPanel = new wxPanel(this);

  std::vector<modelCDMApplication*> applications = this->appli->GetApplications();
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(applications.size()+1, 3, 9, 5);

  wxStaticText* ChBTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Include in\nCMake"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* LkTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Application Name"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* HlpTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Help"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

  propertiesGridSizer -> Add(ChBTitle, 0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(LkTitle,  0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(HlpTitle, 0, wxEXPAND | wxALL, 5);

  for (int i = 0; i < (int)(applications.size()); i++)
    {
      //checkbox for cmake inclusion
      wxCheckBox* pApplicationChB = new wxCheckBox(propertiesPanel, ID_CHECK_INCLUDE_APPLICATION, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
      pApplicationChB->SetName(crea::std2wx(applications[i]->GetName()));
      std::string tt = "if this box is checked the the " + applications[i]->GetName() + " application is included in the project compilation.";
      pApplicationChB->SetToolTip(crea::std2wx(tt));
      pApplicationChB->SetValue(this->appli->IsApplicationIncluded(applications[i]->GetName()));
      propertiesGridSizer -> Add(pApplicationChB, 0, wxEXPAND | wxALIGN_CENTER);

      //link to library with description
      wxHyperlinkCtrl* pApplicationlk = new wxHyperlinkCtrl(propertiesPanel, ID_LINK_SELECT_APPLICATION, crea::std2wx(applications[i]->GetName().c_str()), crea::std2wx(applications[i]->GetName().c_str()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      pApplicationlk->SetWindowStyle(wxALIGN_LEFT | wxNO_BORDER);
      tt = "Name: " + applications[i]->GetName() + "\n";
      tt += "Location: " + applications[i]->GetPath();
      pApplicationlk->SetToolTip(crea::std2wx(tt));
      pApplicationlk->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliDescriptionPanel::OnMouseEnter,NULL,this);
      pApplicationlk->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliDescriptionPanel::OnMouseExit,NULL,this);
      propertiesGridSizer -> Add(pApplicationlk, 0, wxEXPAND);

      //help icon
      wxButton* pApplicationHlp = new wxButton(propertiesPanel, wxID_ANY, wxT("?"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
      pApplicationHlp->Enable(false);
      tt = "When this application is included in the CMakeLists file, the\nfollowing line is included in the CMakeList.txt file in the\nappli folder:\n"
          "ADD_SUBDIRECTORY(" + applications[i]->GetName() + ")";
      pApplicationHlp->SetToolTip(crea::std2wx(tt));

      propertiesGridSizer -> Add(pApplicationHlp, 0, wxEXPAND | wxALIGN_CENTER);
    }

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanel->SetSizer(propertiesGridSizer);
  propertiesGridSizer->Fit(propertiesPanel);

  propertiesBox->Add(propertiesPanel, 1, wxEXPAND | wxALL, 5);
  sizer -> Add(propertiesBox, 0, wxEXPAND | wxALL, 10);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Actions"));
  actionsBox->GetStaticBox()->SetToolTip(wxT("Create a new application or make any change to the appli's CMakeLists.txt file by selecting the desired action."));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* createApplicationbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_APPLICATION, _T("A. Create Application"));
  createApplicationbt->SetToolTip(wxT("Create a new application for this project."));
  actionsGridSizer->Add(createApplicationbt, 1, wxALL | wxEXPAND, 5);
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Open the system default text editor to edit the CMakeLists.txt file."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliDescriptionPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliDescriptionPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Applications Folder"));
  openFolderbt->SetToolTip(wxT("Open the appli folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 0, wxALL | wxEXPAND, 5);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMAppliHelpDialog* helpDialog = new wxCDMAppliHelpDialog(this->GetParent(), this->appli, wxID_ANY);
      helpDialog->Show(true);
    }
}

void wxCDMAppliDescriptionPanel::OnBtnCreateApplication(wxCommandEvent& event)
{
  //get name
  wxTextEntryDialog* appDlg = new wxTextEntryDialog(
        this,
        wxT("Enter the new application name (NO white spaces)"),
        wxT("New Application - creaDevManager"),
        wxT(""),
        wxOK | wxCANCEL
    );

    if (appDlg->ShowModal() == wxID_OK)
      {
        std::string applicationName = crea::wx2std(appDlg->GetValue());
        //check name
        if(applicationName.size() > 0)
          {
            wxArrayString types;
            types.Add(wxT("Console Application"));
            types.Add(wxT("GUI Application (wxWidgets)"));
            int applicationType = wxGetSingleChoiceIndex(
                wxT("Select the application type"),
                wxT("New Application - creaDevManager"),
                types
            );

            if (applicationType != -1)
              {
                std::string* result;
                //create library
                modelCDMIProjectTreeNode* application = this->appli->CreateApplication(applicationName, applicationType ,result);
                //check library created
                if(application == NULL)
                  {
                    wxMessageBox(crea::std2wx(*result),_T("New Application - Error!"),wxOK | wxICON_ERROR);
                    event.Skip();
                    return;
                  }
                wxMessageBox(crea::std2wx("Application successfully created."),_T("New Application - Success!"),wxOK | wxICON_INFORMATION);

                //refreshing tree and description
                //send event instead of calling parent to avoid crashing

                ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

                wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
                newEvent->SetClientData(application);
                wxPostEvent(this->GetParent(), *newEvent);
                event.Skip();
              }
          }
        else
          {
            wxMessageBox(crea::std2wx("Invalid application name, please try again with a valid name."),_T("New Application - Error!"),wxOK | wxICON_INFORMATION);
          }
      }
}

void wxCDMAppliDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->appli->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMAppliDescriptionPanel::OnChBApplicationChange(wxCommandEvent& event)
{
  this->appli->SetApplicationInclude(
      crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()),
      ((wxCheckBox*)event.GetEventObject())->GetValue()
    );
}

void wxCDMAppliDescriptionPanel::OnLnkApplicationSelect(wxHyperlinkEvent& event)
{
  modelCDMApplication* applicationFound = NULL;
  std::vector<modelCDMApplication*> applications = this->appli->GetApplications();
  for (int i = 0; i < (int)(applications.size()); i++)
    {
      if(applications[i]->GetName() == crea::wx2std(event.GetURL()))
        {
          applicationFound = applications[i];
          break;
        }
    }
  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  newEvent1->SetClientData(applicationFound);
  newEvent1->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent1);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetClientData(applicationFound);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
}

void wxCDMAppliDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->appli->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMAppliDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->appli->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMAppliDescriptionPanel::OnMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);
  std::string AppName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMApplication* theApp = NULL;
  std::vector<modelCDMApplication*> applications = this->appli->GetApplications();
  for (int i = 0; i < (int)(applications.size()); i++)
    {
      if(applications[i]->GetName() == AppName)
        {
          theApp = applications[i];
          break;
        }
    }
  newEvent->SetClientData(theApp);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMAppliDescriptionPanel::OnMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  std::string AppName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMApplication* theApp = NULL;
  std::vector<modelCDMApplication*> applications = this->appli->GetApplications();
  for (int i = 0; i < (int)(applications.size()); i++)
    {
      if(applications[i]->GetName() == AppName)
        {
          theApp = applications[i];
          break;
        }
    }
  newEvent->SetClientData(theApp);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMAppliDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMAppliDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
