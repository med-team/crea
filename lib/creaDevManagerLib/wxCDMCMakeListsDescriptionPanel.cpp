/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMCMakeListsDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMCMakeListsDescriptionPanel.h"

#include "wxCDMMainFrame.h"

#include "creaDevManagerIds.h"
#include "images/CMIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMCMakeListsDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMCMakeListsDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMCMakeListsDescriptionPanel::OnBtnOpenInEditor)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMCMakeListsDescriptionPanel::OnBtnOpenFolder)
END_EVENT_TABLE()

wxCDMCMakeListsDescriptionPanel::wxCDMCMakeListsDescriptionPanel(
    wxWindow* parent,
    modelCDMCMakeListsFile* makefile,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMCMakeListsDescriptionPanel::Create(parent, makefile, id, caption, pos, size, style);
}

wxCDMCMakeListsDescriptionPanel::~wxCDMCMakeListsDescriptionPanel()
{
}

bool wxCDMCMakeListsDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMCMakeListsFile* makefile,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->cMakeLists = makefile;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMCMakeListsDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->cMakeLists->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
      returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->cMakeLists->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(CMIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("CMake Configuration File")),0, wxALIGN_LEFT, 0);
    //File Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx("CMakeLists.txt")),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(1, 2, 9, 15);

  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Open File"));
  editCMakebt->SetToolTip(wxT("Open the CMakeLists file in the default text editor."));
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Containing Folder"));
  openFolderbt->SetToolTip(wxT("Open the folder where the CMakeLists file is located in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);
}

void wxCDMCMakeListsDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->cMakeLists->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMCMakeListsDescriptionPanel::OnBtnOpenInEditor(wxCommandEvent& event)
{
  std::string* result;
  if(!this->cMakeLists->OpenFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMCMakeListsDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->cMakeLists->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}
