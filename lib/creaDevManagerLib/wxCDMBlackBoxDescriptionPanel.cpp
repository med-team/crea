/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMBlackBoxDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMBlackBoxDescriptionPanel.h"

#include "wxCDMMainFrame.h"

#include "wxCDMBlackBoxHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/BBIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMBlackBoxDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMBlackBoxDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_SET_AUTHOR, wxCDMBlackBoxDescriptionPanel::OnBtnSetAuthor)
EVT_BUTTON(ID_BUTTON_SET_DESCRIPTION, wxCDMBlackBoxDescriptionPanel::OnBtnSetDescription)
EVT_BUTTON(ID_BUTTON_SET_CATEGORY, wxCDMBlackBoxDescriptionPanel::OnBtnSetCategories)
EVT_BUTTON(ID_BUTTON_OPEN_CXX, wxCDMBlackBoxDescriptionPanel::OnBtnOpenCxx)
EVT_BUTTON(ID_BUTTON_OPEN_HXX, wxCDMBlackBoxDescriptionPanel::OnBtnOpenHxx)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMBlackBoxDescriptionPanel::OnBtnOpenFolder)
END_EVENT_TABLE()

wxCDMBlackBoxDescriptionPanel::wxCDMBlackBoxDescriptionPanel(
    wxWindow* parent,
    modelCDMBlackBox* blackBox,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMBlackBoxDescriptionPanel::Create(parent, blackBox, id, caption, pos, size, style);
}

wxCDMBlackBoxDescriptionPanel::~wxCDMBlackBoxDescriptionPanel()
{
}

bool wxCDMBlackBoxDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMBlackBox* blackBox,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  this->blackBox = blackBox;
  wxPanel::Create(parent, id, pos, size, style);
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMBlackBoxDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->blackBox->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
      returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->blackBox->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(BBIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Black Box")),0, wxALIGN_LEFT, 0);
    //Black Box Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->blackBox->GetNameBlackBox())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //BlackBox Properties
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Properties"));
  wxPanel* propertiesPanel = new wxPanel(this);
  wxBoxSizer* propertiesPanelSizer = new wxBoxSizer(wxVERTICAL);
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(4, 2, 9, 15);

  wxStaticText *pAuthor = new wxStaticText(propertiesPanel, -1, wxT("Author"));
  wxStaticText *pDescription = new wxStaticText(propertiesPanel, -1, wxT("Description"));
  wxStaticText *pCategories = new wxStaticText(propertiesPanel, -1, wxT("Categories"));

  // author
  wxBoxSizer* pAuthorsz = new wxBoxSizer(wxHORIZONTAL);
  this->authortc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->blackBox->GetAuthors()));
  wxButton* pAuthorbt = new wxButton(propertiesPanel, ID_BUTTON_SET_AUTHOR, wxT("Change"));
  pAuthorbt->SetToolTip(wxT("Update the author/s of the package."));
  pAuthorsz->Add(this->authortc, 1, wxALIGN_CENTER, 0);
  pAuthorsz->Add(pAuthorbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);

  // description
  wxBoxSizer* pDescriptionsz = new wxBoxSizer(wxHORIZONTAL);
  this->descriptiontc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->blackBox->GetDescription()));
  wxButton* pDescriptionbt = new wxButton(propertiesPanel, ID_BUTTON_SET_DESCRIPTION, wxT("Change"));
  pDescriptionbt->SetToolTip(wxT("Update the description of the project."));
  pDescriptionsz->Add(this->descriptiontc, 1, wxALIGN_CENTER, 1);
  pDescriptionsz->Add(pDescriptionbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);

  // categories
  wxBoxSizer* pCategoriessz = new wxBoxSizer(wxHORIZONTAL);
  this->categoriestc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->blackBox->GetCategories()));
  wxButton* pCategoriesbt = new wxButton(propertiesPanel, ID_BUTTON_SET_CATEGORY, wxT("Change"));
  pCategoriesbt->SetToolTip(wxT("Update the categories of the project."));
  pCategoriessz->Add(this->categoriestc, 1, wxALIGN_CENTER, 0);
  pCategoriessz->Add(pCategoriesbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);

  propertiesGridSizer->Add(pAuthor, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pAuthorsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pDescription, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pDescriptionsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pCategories, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pCategoriessz, 1, wxEXPAND);

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanelSizer -> Add(propertiesGridSizer, 1, wxEXPAND | wxALL, 5);
  propertiesPanel->SetSizer(propertiesPanelSizer);
  propertiesPanelSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 1, wxEXPAND, 5);

  sizer->Add(propertiesBox, 0, wxEXPAND | wxALL, 10);


  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* openHxxbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_HXX, _T("A. Open .h"));
  openHxxbt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxDescriptionPanel::OnHxxMouseEnter,NULL,this);
  openHxxbt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxDescriptionPanel::OnHxxMouseExit,NULL,this);
  openHxxbt->SetToolTip(wxT("Open the .h file in the default text editor."));
  actionsGridSizer->Add(openHxxbt, 1, wxALL | wxEXPAND, 5);
  wxButton* openCxxbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_CXX, _T("B. Open .cxx"));
  openCxxbt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxDescriptionPanel::OnCxxMouseEnter,NULL,this);
  openCxxbt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxDescriptionPanel::OnCxxMouseExit,NULL,this);
  openCxxbt->SetToolTip(wxT("Open the .cxx file in the default text editor."));
  actionsGridSizer->Add(openCxxbt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Source Folder"));
  openFolderbt->SetToolTip(wxT("Open the source folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMBlackBoxHelpDialog* helpDialog = new wxCDMBlackBoxHelpDialog(this->GetParent(), this->blackBox, wxID_ANY);
      helpDialog->Show(true);
    }
}

void wxCDMBlackBoxDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->blackBox->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }

}

void wxCDMBlackBoxDescriptionPanel::OnBtnSetAuthor(wxCommandEvent& event)
{
  //get author from user
  wxTextEntryDialog* authDlg = new wxTextEntryDialog(
      this,
      wxT("Enter the new authors name. Separate each author with a '/'."),
      wxT("Change Black Box Author - creaDevManager"),
      crea::std2wx(this->blackBox->GetAuthors()),
      wxOK | wxCANCEL
  );

  if (authDlg->ShowModal() == wxID_OK)
    {
      std::string authorsStr = crea::wx2std(authDlg->GetValue());
      //check name
      if(authorsStr.size() > 0)
        {
          std::string* result;
          if(!this->blackBox->SetAuthors(authorsStr, result))
            wxMessageBox(crea::std2wx(*result),_T("Change Black Box Author - Error!"),wxOK | wxICON_ERROR);

        }

      this->authortc->SetLabel(crea::std2wx(this->blackBox->GetAuthors()));
      this->authortc->GetParent()->GetSizer()->RecalcSizes();
    }

}

void wxCDMBlackBoxDescriptionPanel::OnBtnSetDescription(wxCommandEvent& event)
{
  //get author from user
  wxTextEntryDialog* descDlg = new wxTextEntryDialog(
      this,
      wxT("Edit the black box description."),
      wxT("Change Black Box Description - creaDevManager"),
      crea::std2wx(this->blackBox->GetDescription()),
      wxTE_MULTILINE | wxOK | wxCANCEL
  );

  if (descDlg->ShowModal() == wxID_OK)
    {
      std::string descriptionStr = crea::wx2std(descDlg->GetValue());
      //check name
      if(descriptionStr.size() > 0)
        {
          std::string* result;
          if(!this->blackBox->SetDescription(descriptionStr, result))
            wxMessageBox(crea::std2wx(*result),_T("Change Black Box Description - Error!"),wxOK | wxICON_ERROR);
        }
      this->descriptiontc->SetLabel(crea::std2wx(this->blackBox->GetDescription()));
      this->descriptiontc->GetParent()->GetSizer()->RecalcSizes();
    }
}

void wxCDMBlackBoxDescriptionPanel::OnBtnSetCategories(wxCommandEvent& event)
{
  //get author from user
  wxTextEntryDialog* catsDlg = new wxTextEntryDialog(
      this,
      wxT("Edit the black box categories separated by '/'."),
      wxT("Change Black Box Categories - creaDevManager"),
      crea::std2wx(this->blackBox->GetCategories()),
      wxTE_MULTILINE | wxOK | wxCANCEL
  );

  if (catsDlg->ShowModal() == wxID_OK)
    {
      std::string categoriesStr = crea::wx2std(catsDlg->GetValue());
      //check name
      if(categoriesStr.size() > 0)
        {
          std::string* result;
          if(!this->blackBox->SetCategories(categoriesStr, result))
            wxMessageBox(crea::std2wx(*result),_T("Change Black Box Categories - Error!"),wxOK | wxICON_ERROR);
        }
      this->categoriestc->SetLabel(crea::std2wx(this->blackBox->GetCategories()));
      this->categoriestc->GetParent()->GetSizer()->RecalcSizes();
    }
}

void wxCDMBlackBoxDescriptionPanel::OnBtnOpenCxx(wxCommandEvent& event)
{
  std::string* result;
  if(!this->blackBox->OpenCxx(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Source File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->blackBox->GetSourceFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetSourceFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }

  event.Skip();
}

void wxCDMBlackBoxDescriptionPanel::OnBtnOpenHxx(wxCommandEvent& event)
{
  std::string* result;
  if(!this->blackBox->OpenHxx(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Header File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->blackBox->GetHeaderFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetHeaderFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }

  event.Skip();
}

void wxCDMBlackBoxDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->blackBox->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMBlackBoxDescriptionPanel::OnCxxMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->blackBox->GetSourceFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetSourceFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMBlackBoxDescriptionPanel::OnCxxMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->blackBox->GetSourceFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetSourceFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMBlackBoxDescriptionPanel::OnHxxMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->blackBox->GetHeaderFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetHeaderFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMBlackBoxDescriptionPanel::OnHxxMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->blackBox->GetHeaderFile() != NULL)
    {
      newEvent->SetClientData(this->blackBox->GetHeaderFile());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
