/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */

/*
 * modelCDMIProjectTreeNode.h
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMIPROJECTTREENODE_H_
#define MODELCDMIPROJECTTREENODE_H_

#include <iostream>
#include <vector>
#include <creaWx.h>
#include "wx/treectrl.h"
#include "wxCDMTreeItemId.h"

/**
 * Class that represents an element of a Crea Project.
 */
class modelCDMIProjectTreeNode
{
public:
  /**
   * Destructor that doesn't do anything.
   */
  virtual ~modelCDMIProjectTreeNode() {};

  /**
   * Compares the precedence of an modelCDMIProjectTreeNode object with another by the node's name and its type (first folder then file).
   * @return Either if the node x goes before (true) or after (false) y.
   */
  static bool CompareNodeItem(const modelCDMIProjectTreeNode* x, const modelCDMIProjectTreeNode* y);

  /**
   * Returns the id of the node in the tree of the project.
   * @return Id of the node in the tree project.
   */
  const wxCDMTreeItemId& GetId() const;
  /**
   * Returns the full path to the node, including the name of the node.
   * @return Node's full path.
   */
  const std::string& GetPath() const;
  /**
   * Return the name of the node, either the file name or the folder name.
   * @return Name of the node.
   */
  const std::string& GetName() const;
  /**
   * Returns the type of node.
   * @return Either wxDIR_FILES for files or wxDIR_DIRS for folders.
   */
  const unsigned char& GetType() const;
  /**
   * Returns the level of the node in the project tree.
   * @return Level of the node in the project tree.
   */
  const int& GetLevel() const;
  /**
   * Returns a reference to the parent node of the actual node.
   * @return The reference of the parent node or NULL.
   */
  modelCDMIProjectTreeNode* GetParent() const;
  /**
   * Returns an array with the hierarchy route between the node and the project root
   * @return Hierarchy array ordered from higher to lower node level down to the project root.
   */
  std::vector<modelCDMIProjectTreeNode*> GetParents() const;
  /**
   * Returns the children nodes of the actual node.
   * @return An array with the children nodes.
   */
  const std::vector<modelCDMIProjectTreeNode*>& GetChildren() const;
  /**
   * Returns the file size of the node.
   * @return File size.
   */
  const int& GetLength();
  /**
   * Sets the id of the node in the project tree.
   * @param id Id of the node.
   */
  void SetId(const wxCDMTreeItemId& id);
  /**
   * Sorts the children using the compareNodeItem function.
   */
  void SortChildren();
  /**
   * Sets the children array of the node. Warning: it discards the older children.
   * @param children Array of children nodes.
   */
  void SetChildren(const std::vector<modelCDMIProjectTreeNode*>& children);

  /**
   * Refreshes the structure of the node and its children.
   * @param result Result of the procedure.
   * @return True if the procedure was successful.
   */
  virtual const bool Refresh(std::string*& result);
  /**
   * Opens the file explorer in the containing folder of the file or in the folder itself.
   * @param result Result of the procedure.
   * @return True if the procedure was successful.
   */
  const bool OpenInFileExplorer(std::string*& result) const;

  protected:
  /**
   * Id of the node in the project tree.
   */
  wxCDMTreeItemId id;
  /**
   * path of the node in the computer.
   */
  std::string path;
  /**
   * Name of the folder or file.
   */
  std::string name;
  /**
   * Type of the node. Either wxDIR_FILES for files or wxDIR_DIRS for folders.
   */
  unsigned char type;
  /**
   * Level of the node in the file hierarchy.
   */
  int level;
  /**
   * Length of the file in the system.
   */
  int length;
  /**
   * Reference to the parent node.
   */
  modelCDMIProjectTreeNode* parent;
  /**
   * Array of references to the children nodes.
   */
  std::vector<modelCDMIProjectTreeNode*> children;

};


#endif /* MODELCDMIPROJECTTREENODE_H_ */
