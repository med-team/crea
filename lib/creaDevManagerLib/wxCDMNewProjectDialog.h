/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMNewProjectDialog.h
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMNEWPROJECTDIALOG_H_
#define WXCDMNEWPROJECTDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>

/**
 * Dialog to create a new Crea Project.
 */
class wxCDMNewProjectDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * New Project Dialog Constructor.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Project".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 700, 400.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMNewProjectDialog(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Project"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(700,400),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMNewProjectDialog();
  /**
   * New Project Dialog Creator.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Project".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 700, 400.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return True if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Project"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(700,400),
      long style = wxDEFAULT_DIALOG_STYLE
  );

  /**
   * Returns the project location chosen by the user.
   * @return Project creation path.
   */
  const wxString GetProjectLocation();
  /**
   * Returns the project name chosen by the user.
   * @return Project name.
   */
  const wxString GetProjectName();
  /**
   * Returns the default package authors chosen by the user.
   * @return Default Package authors.
   */
  const wxString GetPackageAuthor();
  /**
   * Returns the default package description chosen by the user.
   * @return Default Package description.
   */
  const wxString GetPackageDescription();

protected:
  /**
   * Creates the visual controls of the Dialog.
   */
  void CreateControls();

private:
  /**
   * Project creation path.
   */
  wxStaticText* projectLocation;
  /**
   * Project Name.
   */
  wxTextCtrl* projectName;
  /**
   * Default Package Author.
   */
  wxTextCtrl* packageAuthor;
  /**
   * Default Package Description.
   */
  wxTextCtrl* packageDescription;

  //handlers
protected:
  /**
   * Handles when the create project button is pressed. If no path or no author was specified, the dialog shows an error.
   * @param event Unused event.
   */
  void OnCreateProject(wxCommandEvent& event);
  /**
   * Handles when the cancel button is pressed.
   * @param event Unused event.
   */
  void OnCancel(wxCommandEvent& event);
  /**
   * Handles when the choose location is pressed. a Directory dialog is shown and the new project location is chosen.
   * @param event Unused event.
   */
  void OnChooseLocation(wxCommandEvent& event);
};

#endif /* WXCDMNEWPROJECTDIALOG_H_ */
