/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sante)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
*/


/*
 * wxCDMTreeItemId.cpp
 *
 *  Created on: 29/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMTreeItemId.h"

wxCDMTreeItemId::wxCDMTreeItemId(){
  this->_id = this->_idWx.m_pItem;
}

wxCDMTreeItemId::wxCDMTreeItemId(const wxTreeItemId& id)
{
  this->_idWx = id;
  this->_id = id.m_pItem;
}

wxCDMTreeItemId::~wxCDMTreeItemId(){}

const wxTreeItemId& wxCDMTreeItemId::GetWxId() const
{
  return this->_idWx;
}

const wxTreeItemIdValue& wxCDMTreeItemId::GetId() const
{
  return this->_id;
}

bool wxCDMTreeItemId::operator == (const wxCDMTreeItemId& id) const
{
  return this->_id == id._id;
}

bool wxCDMTreeItemId::operator != (const wxCDMTreeItemId& id) const
{
  return !(*this == id);
}

bool wxCDMTreeItemId::operator < (const wxCDMTreeItemId& id) const
{
  return this->_id < id._id;
}

std::ostream& operator << (std::ostream& stream, const wxCDMTreeItemId& id)
{
  stream << id._id;
  return stream;
}
