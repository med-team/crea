/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMProjectActionsPanel.h
 *
 *  Created on: 25/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPROJECTACTIONSPANEL_H_
#define WXCDMPROJECTACTIONSPANEL_H_

#include <creaWx.h>

#include "modelCDMProject.h"

/**
 * Panel with common actions for an open Crea project.
 */
class wxCDMProjectActionsPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Project actions panel constructor.
   * @param parent Parent window reference.
   * @param project Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMProjectActionsPanel(
      wxWindow* parent,
      modelCDMProject* project,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );
  /**
   * Destructor.
   */
  ~wxCDMProjectActionsPanel();
  /**
   * Project actions panel constructor.
   * @param parent Parent window reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

protected:
  /**
   * Creates the Panel controls (buttons)
   */
  void CreateControls();

  /**
   * Handles when the Check Project Structure button is pressed.
   */
  void OnBtnCheckProjectStructure(wxCommandEvent& event);
  /**
   * Handles when the Configure Compilation button is pressed.
   */
  void OnBtnConfigureBuild(wxCommandEvent& event);
  /**
   * Handles when the Compile Project button is pressed.
   */
  void OnBtnBuildProject(wxCommandEvent& event);
  /**
   * Handles when the Plug packages button is pressed.
   */
  void OnBtnConnectProject(wxCommandEvent& event);

private:
  /**
   * Project class reference
   */
  modelCDMProject* project;
};

#endif /* WXCDMPROJECTACTIONSPANEL_H_ */
