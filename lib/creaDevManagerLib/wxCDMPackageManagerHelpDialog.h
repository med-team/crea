/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMPackageManagerHelpDialog.h
 *
 *  Created on: 7/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPACKAGEMANAGERHELPDIALOG_H_
#define WXCDMPACKAGEMANAGERHELPDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>

#include "wxCDMPackageManagerPanel.h"

/**
 * Package Manager Panel Help Dialog
 */
class wxCDMPackageManagerHelpDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Package Manager Panel Help Dialog Constructor.
   * @param parent Parent window.
   * @param packageManager Package Manager panel reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Managing Packages".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 450.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMPackageManagerHelpDialog(
      wxWindow* parent,
      wxCDMPackageManagerPanel* packageManager,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Managing Packages"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,450),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMPackageManagerHelpDialog();
  /**
   * Package Manager Panel Help Dialog Creator.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Managing Packages".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 450.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Managing Packages"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,450),
      long style = wxDEFAULT_DIALOG_STYLE
  );

protected:
  /**
   * Creates the help controls (text and buttons).
   */
  void CreateControls();

//attributes
private:
  /**
   * Package Manager panel reference.
   */
  wxCDMPackageManagerPanel* packageManager;

//handlers
protected:
  /**
   * Handler to close help dialog.
   * @param event Unused.
   */
  void OnFinish(wxCommandEvent& event);

  /**
   * Handler when the create package button is pressed.
   * @param event Unused.
   */
  void OnCreatePackage(wxCommandEvent& event);
  /**
   * Handler when a cmakelists button is pressed.
   * @param event cmakelists event.
   */
  void OnEditCMake(wxCommandEvent& event);

  /**
   * Handler when a cmakelists button is hovered.
   * @param event cmakelists event.
   */
  void OnEditCMakeMouseEnter(wxMouseEvent& event);
  /**
   * Handler when a cmakelists button hover finishes.
   * @param event cmakelists event.
   */
  void OnEditCMakeMouseExit(wxMouseEvent& event);

  /**
   * Handler when the disable help checkbox is changed.
   * @param event check box event.
   */
  void OnDisableHelp(wxCommandEvent& event);
};

#endif /* WXCDMPACKAGEMANAGERHELPDIALOG_H_ */
