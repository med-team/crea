/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMAppli.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMAPPLI_H_
#define MODELCDMAPPLI_H_

#include<iostream>
#include<vector>
#include<map>

#include"modelCDMFolder.h"
#include"modelCDMApplication.h"

/**
 * Represents the appli folder of Crea project. The appli folder holds the applications of a project.
 */
class modelCDMAppli : public modelCDMFolder
{
public:
  /**
   * Default constructor.
   */
  modelCDMAppli();
  /**
   * Constructor of the appli folder node.
   * @param parent Parent node of the appli node.
   * @param path Full path of the appli node.
   * @param name Folder name of the appli node. By default "appli"
   * @param level Folder Level in the project hierarchy. By default 1
   */
  modelCDMAppli(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name = "appli", const int& level = 1);
  /**
   * Destructor.
   */
  ~modelCDMAppli();

  /**
   * Retrieves the applications inside the appli folder node.
   * @return Reference array of the applications in the appli node.
   */
  const std::vector<modelCDMApplication*>& GetApplications() const;

  /**
   * Creates a new application in the system and creates an application node. This node is stored in the applications attribute and returned. The created application is included in the appli's CMakeLists file.
   * @param name Name of the new application.
   * @param type 0=console application, 1=GUI Application (wxWidgets).
   * @param result Result message of the operation.
   * @return Reference to the created application or NULL.
   */
  modelCDMApplication* CreateApplication(
      const std::string& name,
      const int& type,
      std::string*& result
  );
  /**
   * Refreshes the file structure of the appli node. Deletes deleted folders and files and creates created files and folders since lasts refresh.
   * @param result Result message of the operation.
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Checks the CMakeLists structure and the applications in order to look for compilation errors before compiling.
   * @param properties Properties found in the structure.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks if the given application is included in the CMakeLists file.
   * @param application_name Name of the library to check.
   * @return True if the library is included, otherwise returns False.
   */
  bool IsApplicationIncluded(const std::string& application_name);

  /**
   * Sets the inclusion of the application in the lib's CMakeLists file. If the application inclusion already exist in file, then the line is uncommented/commented depending on the requested action. If the application inclusion doesn't exist yet, then it is included if the request is an inclusion.
   * @param application_name Name of the application to include/exclude.
   * @param toInclude True if the request is an inclusion, False otherwise.
   * @return True if the request was processed successfully.
   */
  bool SetApplicationInclude(const std::string& application_name, const bool& toInclude);

private:
  /**
   * application in the appli folder node.
   */
  std::vector<modelCDMApplication*> applications;
};

#endif /* MODELCDMAPPLI_H_ */
