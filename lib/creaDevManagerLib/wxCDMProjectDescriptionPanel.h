/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMProjectDescriptionPanel.h
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPROJECTDESCRIPTIONPANEL_H_
#define WXCDMPROJECTDESCRIPTIONPANEL_H_

#include <creaWx.h>

#include "modelCDMProject.h"

/**
 * Project Description Panel. Shows the project properties and the principal actions for the project.
 */
class wxCDMProjectDescriptionPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:

  /**
   * Project Description panel Constructor.
   * @param parent Parent window reference.
   * @param project Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMProjectDescriptionPanel(
      wxWindow* parent,
      modelCDMProject* project,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMProjectDescriptionPanel();

  /**
   * Project Description panel Creator.
   * @param parent Parent window reference.
   * @param project Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if creation was successful.
   */
  bool Create(
      wxWindow* parent,
      modelCDMProject* project,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

  /**
   * Retrieves the described project class reference.
   * @return Project reference.
   */
  modelCDMProject* GetProject() const;

private:
  /**
   * Project described
   */
  modelCDMProject* project;
  /**
   * Text control showing the project version.
   */
  wxStaticText* versiontc;
  /**
   * Text control showing the project version date.
   */
  wxStaticText* versionDatetc;
  /**
   * Text control showing the project build path.
   */
  wxStaticText* buildPathtc;

  //handlers
protected:
  /**
   * Handles when the manage packages button is pressed.
   */
  void OnBtnManagePackages(wxCommandEvent& event);
  /**
   * Handles when the manage libraries button is pressed.
   */
  void OnBtnManageLibraries(wxCommandEvent& event);
  /**
   * Handles when the manage applications button is pressed.
   */
  void OnBtnManageApplications(wxCommandEvent& event);
  /**
   * Handles when the 3rd party libraries manager button is pressed.
   */
  void OnBtnConfigProject(wxCommandEvent& event);
  /**
   * Handles when the open cmakelists file button is pressed.
   */
  void OnBtnEditCMakeLists(wxCommandEvent& event);
  /**
   * Handles when the choose build path button is pressed.
   */
  void OnBtnSetBuildPath(wxCommandEvent& event);
  /**
   * Handles when the open build folder button is pressed.
   */
  void OnBtnOpenBuild(wxCommandEvent& event);
  /**
   * Handles when the open containing folder button is pressed.
   */
  void OnBtnOpenFolder(wxCommandEvent& event);
  /**
   * Handles when the set version button is pressed.
   */
  void OnBtnSetVersion(wxCommandEvent& event);

  /**
   * Handles when the open cmakelists file button is hovered.
   */
  void OnCMakeMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the open cmakelists file button finishes hover.
   */
  void OnCMakeMouseExit(wxMouseEvent& event);
  /**
   * Handles when the manage applications button is hovered.
   */
  void OnAppliMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the manage applications file button finishes hover.
   */
  void OnAppliMouseExit(wxMouseEvent& event);
  /**
   * Handles when the manage libraries button is hovered.
   */
  void OnLibMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the manage libraries button finishes hover.
   */
  void OnLibMouseExit(wxMouseEvent& event);
};

#endif /* WXCDMPROJECTDESCRIPTIONPANEL_H_ */
