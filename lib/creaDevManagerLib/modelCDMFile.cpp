/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMFile.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMFile.h"

#include <fstream>
#include <creaWx.h>
#include <wx/dir.h>

#include "CDMUtilities.h"

modelCDMFile::modelCDMFile()
{
}

modelCDMFile::modelCDMFile(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating file: " + path + "\n";
  this->parent = parent;
  this->children.clear();
  this->level = level;
  this->name = name;
  this->path = path;
  this->type = wxDIR_FILES;

  std::ifstream in(path.c_str(), std::ifstream::in | std::ifstream::binary);
  in.seekg(0, std::ifstream::end);
  this->length = in.tellg();
  in.close();

}

modelCDMFile::~modelCDMFile()
{
}

bool modelCDMFile::OpenFile(std::string*& result, const std::string& command)
{
  if (!CDMUtilities::openFileWithCommand(path, command))
    return true;
  else
    {
      result = new std::string("Couldn't open file with command " + command + ".");
      return false;
    }
}

const bool modelCDMFile::Refresh(std::string*& result)
{
  //std::cout << "refreshing file " << this->name << std::endl;
  std::ifstream in((this->path).c_str());
  if(!in.is_open())
    {
      in.close();
      return false;
    }
  std::ifstream in2(path.c_str(), std::ifstream::in | std::ifstream::binary);
  in2.seekg(0, std::ifstream::end);
  this->length = in2.tellg();
  in2.close();
  return true;
}

const bool modelCDMFile::OpenInFileExplorer(std::string*& result) const
{
  std::string pth = this->path.substr(0, path.size() - name.size() - 1);
  if (!CDMUtilities::openFileExplorer(pth))
    return true;
  else
    {
      result = new std::string("Couldn't open file.");
      return false;
    }
}
