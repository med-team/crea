/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMProjectMapDialog.cpp
 *
 *  Created on: 25/4/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectMapDialog.h"

#include "creaDevManagerIds.h"
#include "../../src/creaSystem.h"
#include "CDMUtilities.h"

BEGIN_EVENT_TABLE(wxCDMProjectMapDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMProjectMapDialog::OnFinish)
EVT_PAINT(wxCDMProjectMapDialog::PaintEvent)
END_EVENT_TABLE()

wxCDMProjectMapDialog::wxCDMProjectMapDialog(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMProjectMapDialog::Create(parent, id, caption, position, size, style);
}

wxCDMProjectMapDialog::~wxCDMProjectMapDialog()
{
}

bool wxCDMProjectMapDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);
  this->SetMaxSize(wxSize(650,-1));

  this->CreateControls();

  return TRUE;
}

void wxCDMProjectMapDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "This is the map of the possible routes for project development in CreaTools.\n"
          "It's a simple image guide. For more information please read the user manual.\n"
          " - Each bubble represents a step in the project development.\n"
          " - The arrows are the possible routes of the development.\n"
          " - The icons represent the different software used in each step."
          ),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxScrolledWindow* imageWindow = new wxScrolledWindow(this);
  imageWindow->FitInside();
  imageWindow->SetScrollRate(5,5);
  imagePanel = new wxPanel(imageWindow);
  imagePanel->SetMinSize(wxSize(600,1641));
  imageWindow->SetSizer(new wxBoxSizer(wxVERTICAL));
  imageWindow->GetSizer()->Add(imagePanel,1,wxEXPAND);

  wxImage::AddHandler(new wxPNGHandler);
  image = new wxImage();

  std::string pathToImage = crea::System::GetExecutablePath() + CDMUtilities::SLASH + ".." + CDMUtilities::SLASH + "share" + CDMUtilities::SLASH + "creaDevManager" + CDMUtilities::SLASH +  "data" + CDMUtilities::SLASH + "projectMap.png";
  image->LoadFile(crea::std2wx(pathToImage), wxBITMAP_TYPE_PNG);


  wxPaintDC* canvas = new wxPaintDC(imageWindow);



  v_sizer1->Add(imageWindow, 1, wxEXPAND);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
}

void
wxCDMProjectMapDialog::PaintEvent(wxPaintEvent& evt)
{
  wxPaintDC* canvas = new wxPaintDC(imagePanel);
  canvas->DrawBitmap(wxBitmap(image->Scale(600,1641,wxIMAGE_QUALITY_HIGH)),0,0, false);
}

void wxCDMProjectMapDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}


