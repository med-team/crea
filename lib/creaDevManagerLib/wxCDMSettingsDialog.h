/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMSettingsDialog.h
 *
 *  Created on: 14/2/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMSETTINGSDIALOG_H_
#define WXCDMSETTINGSDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>
#include <wx/dialog.h>

/**
 * Settings Dialog
 */
class wxCDMSettingsDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Settings Dialog Constructor.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Settings - CreaDevManager  CREATIS".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMSettingsDialog(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Settings - CreaDevManager  CREATIS"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,370),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMSettingsDialog();
  /**
   * Settings Dialog Creator.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Settings - CreaDevManager  CREATIS".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Settings - CreaDevManager  CREATIS"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,370),
      long style = wxDEFAULT_DIALOG_STYLE
  );

  /**
   * Retrieves if the user has help enabled.
   * @return True if the user has the help enabled.
   */
  bool IsHelpEnabled();

  /**
   * Sets the help enabled checkbox value.
   * @param isHelp value of the help enabled checkbox.
   */
  void SetHelpEnabled(bool isHelp);

protected:
  /**
   * Creates the help controls (text and buttons).
   */
  void CreateControls();

  //attributes
private:
  /**
   * Text Editor Command.
   */
  wxTextCtrl* textEditor;
  /**
   * Terminal Command.
   */
  wxTextCtrl* terminal;
  /**
   * File Explorer Command.
   */
  wxTextCtrl* fileExplorer;
  /**
   * Help Enabled.
   */
  wxCheckBox* helpEnabled;
  /**
   * Build Command.
   */
  wxTextCtrl* buildCommand;

  //handlers
protected:
  /**
   * Handler to close help dialog saving changes.
   * @param event Unused.
   */
  void OnFinish(wxCommandEvent& event);

  /**
   * Handler to close help dialog discarding changes.
   * @param event Unused.
   */
  void OnCancel(wxCommandEvent& event);

  /**
   * Handler to return to default settings.
   * @param event Unused.
   */
  void OnDefaults(wxCommandEvent& event);
};

#endif /* WXCDMSETTINGSDIALOG_H_ */
