/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMPackageSrc.h
 *
 *  Created on: Dic 19, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMPACKAGESRC_H_
#define MODELCDMPACKAGESRC_H_

#include<iostream>
#include<vector>

#include "modelCDMFolder.h"
#include "modelCDMBlackBox.h"

/**
 * Class representing the source folder of a package from a Crea project.
 */
class modelCDMPackageSrc : public modelCDMFolder
{
public:
  /**
   * Default constructor.
   */
  modelCDMPackageSrc();
  /**
   * Package source folder node constructor.
   * @param parent Parent node of the package source folder.
   * @param path Full path to the package source folder.
   * @param name Name of the package source folder.
   * @param level Project hierarchy level of the package source folder node.
   */
  modelCDMPackageSrc(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name = "src", const int& level = 3);
  /**
   * Destructor.
   */
  ~modelCDMPackageSrc();

  /**
   * Retrieves the black boxes inside the package source folder node.
   * @return Array of black box references.
   */
  const std::vector<modelCDMBlackBox*>& GetBlackBoxes() const;

  /**
   * Creates a new black box and returns a reference to it if the creation is successful. This operation affects the project model as well as the system files.
   * @param result Result message
   * @param name New black box name.
   * @param package Black box package name.
   * @param type Black box type.
   * @param format Black box format.
   * @param authors Black box authors' name.
   * @param authorsEmail Black box authors' email.
   * @param categories Categories associated to this black box.
   * @param description Black box description.
   * @return True if the operation was successful.
   */
  modelCDMBlackBox* CreateBlackBox(
      std::string*& result,
      const std::string& name,
      const std::string& package,
      const std::string& type = "std",
      const std::string& format = "C++",
      const std::string& authors = "unknown",
      const std::string& authorsEmail = "",
      const std::string& categories = "empty",
      const std::string& description = "no description"
  );

  /**
   * Refreshes the structure of the package source folder node. This method updates the properties of the package source folder as well as it refreshes its children.
   * @param result Result message
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

private:
  /**
   * Black box references of the package.
   */
  std::vector<modelCDMBlackBox*> blackBoxes;

};

#endif /* MODELCDMPACKAGESRC_H_ */
