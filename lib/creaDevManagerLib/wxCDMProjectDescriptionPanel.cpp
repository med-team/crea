/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMProjectDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectDescriptionPanel.h"

#include "wxCDMMainFrame.h"
#include "wxCDMNewPackageDialog.h"
#include "wxCDMProjectConfigurationDialog.h"

#include "wxCDMProjectHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/PrIcon64.xpm"
#include "CDMUtilities.h"

BEGIN_EVENT_TABLE(wxCDMProjectDescriptionPanel, wxPanel)
EVT_BUTTON(ID_BUTTON_GOTO_PACKAGE_MANAGER, wxCDMProjectDescriptionPanel::OnBtnManagePackages)
EVT_BUTTON(ID_BUTTON_GOTO_LIB_MANAGER, wxCDMProjectDescriptionPanel::OnBtnManageLibraries)
EVT_BUTTON(ID_BUTTON_GOTO_APPLI_MANAGER, wxCDMProjectDescriptionPanel::OnBtnManageApplications)
EVT_BUTTON(ID_BUTTON_CHOOSE, wxCDMProjectDescriptionPanel::OnBtnConfigProject)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMProjectDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_SET_BUILD_PATH, wxCDMProjectDescriptionPanel::OnBtnSetBuildPath)
EVT_BUTTON(ID_BUTTON_OPEN_BUILD_PATH, wxCDMProjectDescriptionPanel::OnBtnOpenBuild)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMProjectDescriptionPanel::OnBtnOpenFolder)
EVT_BUTTON(ID_BUTTON_SET_VERSION, wxCDMProjectDescriptionPanel::OnBtnSetVersion)
END_EVENT_TABLE()

wxCDMProjectDescriptionPanel::wxCDMProjectDescriptionPanel(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMProjectDescriptionPanel::Create(parent, project, id, caption, pos, size, style);
}

wxCDMProjectDescriptionPanel::~wxCDMProjectDescriptionPanel()
{
}

bool wxCDMProjectDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->project = project;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMProjectDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(PrIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Project")),0, wxALIGN_LEFT, 0);
    //Project Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->project->GetName())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);




  //----------------------------------Project Properties-----------------------------------
  //properties StaticBoxSizer
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, _T("&Properties"));
  //properties Panel
  wxPanel* propertiesPanel = new wxPanel(this);
  //properties Sizer
  wxBoxSizer* propertiesPanelSizer = new wxBoxSizer(wxVERTICAL);
  //propertiesGrid Sizer
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(4, 2, 9, 15);
  //properties elements
  //labels
  wxStaticText* pVersion = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Version"));
  wxStaticText* pVersionDate = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Version Date"));
  wxStaticText* pSourceLocation = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Source Location"));
  wxStaticText* pBuildLocation = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Build Location"));
  //values
  // version
  wxBoxSizer* pVersionsz = new wxBoxSizer(wxHORIZONTAL);
  this->versiontc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->project->GetVersion()));
  wxButton* pVersionbt = new wxButton(propertiesPanel, ID_BUTTON_SET_VERSION, wxT("Set"));
  pVersionbt->SetToolTip(wxT("Update the version of the project."));
  pVersionsz->Add(this->versiontc, 1, wxALIGN_CENTER, 1);
  pVersionsz->Add(pVersionbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);
  // versionDate
  this->versionDatetc = new wxStaticText(propertiesPanel, -1, crea::std2wx(this->project->GetVersionDate()));
  // sourceLocation
  wxBoxSizer* pSourceLocationsz = new wxBoxSizer(wxHORIZONTAL);
  wxStaticText* pSourceLocationtc = new wxStaticText(propertiesPanel, -1, crea::std2wx(this->project->GetPath()));
  pSourceLocationtc->SetMaxSize(wxSize(200,-1));
  wxButton* pSourceLocationbt = new wxButton(propertiesPanel, ID_BUTTON_OPEN_FOLDER, wxT("Open"));
  pSourceLocationbt->SetToolTip(wxT("Open the source folder in the file explorer."));
  pSourceLocationsz->Add(pSourceLocationtc, 1, wxALIGN_CENTER, 1);
  pSourceLocationsz->Add(pSourceLocationbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);
  // buildLocation
  wxBoxSizer* pBuildLocationsz = new wxBoxSizer(wxHORIZONTAL);
  this->buildPathtc = new wxStaticText(propertiesPanel, -1, crea::std2wx(this->project->GetBuildPath()));
  this->buildPathtc->SetMaxSize(wxSize(200,-1));

  wxButton* pBuildLocationbt = new wxButton(propertiesPanel, ID_BUTTON_SET_BUILD_PATH, wxT("Choose..."));
  pBuildLocationbt->SetToolTip(wxT("Select a new location for compiling the project."));

  wxButton* pBuildOpenbt = new wxButton(propertiesPanel, ID_BUTTON_OPEN_BUILD_PATH, wxT("Open"));
  pBuildOpenbt->SetToolTip(wxT("Open the binaries folder in the file explorer."));

  pBuildLocationsz->Add(this->buildPathtc, 1, wxALIGN_CENTER, 1);
  pBuildLocationsz->Add(pBuildLocationbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);
  pBuildLocationsz->Add(pBuildOpenbt, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, 10);

  propertiesGridSizer->Add(pVersion, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pVersionsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pVersionDate, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(this->versionDatetc, 1, wxEXPAND);
  propertiesGridSizer->Add(pSourceLocation, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pSourceLocationsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pBuildLocation, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pBuildLocationsz, 1, wxEXPAND);

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanelSizer-> Add(propertiesGridSizer, 1, wxALL | wxEXPAND, 5);

  //SetPanel sizer and box
  propertiesPanel->SetSizer(propertiesPanelSizer);
  propertiesPanelSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 1, wxEXPAND);
  sizer->Add(propertiesBox, 0, wxEXPAND | wxLEFT | wxRIGHT, 10);

  //----------------------------------Project Actions--------------------------------------
  //actions StaticBoxSizer
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL,this, _T("&Actions"));
  actionsBox->GetStaticBox()->SetToolTip(wxT("Go to any of the content managers of the project or edit the project's CMakeLists.txt file by selecting any of the available actions."));
  //actions Panel
  wxPanel* actionsPanel = new wxPanel(this);
  //actions Sizer
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);

  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(3, 2, 9, 15);
  //buttons
  // lib manager
  //   show only if there is a lib folder
  if(this->project->GetLib() != NULL)
    {
      wxButton* libMgrbt = new wxButton(actionsPanel, ID_BUTTON_GOTO_LIB_MANAGER, _T("A. Library Manager"));
      libMgrbt->SetToolTip(wxT("Find the current available libraries into this project. You can also create new Libraries in this section as well as edit the lib folder's CMakeLists.txt file."));
      libMgrbt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnLibMouseEnter,NULL,this);
      libMgrbt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnLibMouseExit,NULL,this);
      actionsGridSizer->Add(libMgrbt, 1, wxALL | wxEXPAND, 5);
    }
  // package manager
  wxButton* packageMgrbt = new wxButton(actionsPanel, ID_BUTTON_GOTO_PACKAGE_MANAGER, _T("B. Package Manager"));
  packageMgrbt->SetToolTip(wxT("Find the current available packages into this project. You can also create new Packages in this section."));
  actionsGridSizer->Add(packageMgrbt, 1, wxALL | wxEXPAND, 5);

  // appli manager
  //   show only if there is a appli folder
  if(this->project->GetAppli() != NULL)
    {
      wxButton* appliMgrbt = new wxButton(actionsPanel, ID_BUTTON_GOTO_APPLI_MANAGER, _T("C. Application Manager"));
      appliMgrbt->SetToolTip(wxT("Find the current available applications into this project. You can also create new Applications in this section as well as edit the appli folder's CMakeLists.txt file."));
      appliMgrbt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnAppliMouseEnter,NULL,this);
      appliMgrbt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnAppliMouseExit,NULL,this);
      actionsGridSizer->Add(appliMgrbt, 1, wxALL | wxEXPAND, 5);
    }
  // edit 3rd Party libraries
  wxButton* configPrjbt = new wxButton(actionsPanel, ID_BUTTON_CHOOSE, _T("D. 3rd Party Libraries Manager"));
  configPrjbt->SetToolTip(wxT("Select which third party libraries will be used in this project."));
  actionsGridSizer->Add(configPrjbt, 1, wxALL | wxEXPAND, 5);

  // edit CMakeLists file
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Edit the CMakeLists.txt file of this project."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMProjectDescriptionPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  //SetPanel sizer and box
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxALL | wxEXPAND, 5);
  sizer->Add(actionsBox, 0, wxEXPAND | wxALL, 10);


  //Assign sizer
  this->SetSizer(sizer);
  sizer->SetSizeHints(this);

  if(((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMProjectHelpDialog* helpDialog = new wxCDMProjectHelpDialog(this->GetParent(), this->project, wxID_ANY);
      helpDialog->Show(true);
    }
}

modelCDMProject* wxCDMProjectDescriptionPanel::GetProject() const
{
  return this->project;
}

void wxCDMProjectDescriptionPanel::OnBtnManagePackages(wxCommandEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetId(1);
  newEvent->SetString(wxT("manage_packages"));
  newEvent->SetClientData(this->project);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnBtnManageLibraries(wxCommandEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetId(1);
  newEvent->SetString(wxT("manage_libraries"));
  wxPostEvent(this->GetParent(), *newEvent);

  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetLib() != NULL)
    {
      newEvent1->SetClientData(this->project->GetLib());
      newEvent1->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent1);
    }

  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnBtnManageApplications(wxCommandEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetId(1);
  newEvent->SetString(wxT("manage_applications"));
  wxPostEvent(this->GetParent(), *newEvent);

  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetAppli() != NULL)
    {
      newEvent1->SetClientData(this->project->GetAppli());
      newEvent1->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent1);
    }

  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnBtnConfigProject(wxCommandEvent& event)
{
  wxCDMProjectConfigurationDialog* dialog = new wxCDMProjectConfigurationDialog(this,this->project);
  long userResponse;
  userResponse = dialog->ShowModal();
}

void wxCDMProjectDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->project->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMProjectDescriptionPanel::OnBtnSetBuildPath(wxCommandEvent& event)
{
  wxDirDialog* dialog = new wxDirDialog(this, wxT("Choose the new build location for the project"));
  if(dialog->ShowModal())
    {
      std::string* result;
      if(!this->project->SetBuildPath(crea::wx2std(dialog->GetPath()), result))
        wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
    }
  this->buildPathtc->SetLabel(crea::std2wx(this->project->GetBuildPath()));
}

void wxCDMProjectDescriptionPanel::OnBtnOpenBuild(wxCommandEvent& event)
{
  if(CDMUtilities::openFileExplorer(this->project->GetBuildPath()))
    wxMessageBox(crea::std2wx("The folder doesn't exist or hasn't yet been created."),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMProjectDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->project->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMProjectDescriptionPanel::OnBtnSetVersion(wxCommandEvent& event)
{
  //get version
  wxString libraryName = wxGetTextFromUser(
      wxT("Enter the new version name"),
      wxT("Change Project Version - creaDevManager"),
      crea::std2wx(this->project->GetVersion())
  );
  //check name
  std::vector<std::string> parts;
  CDMUtilities::splitter::split(parts, crea::wx2std(libraryName), " .", CDMUtilities::splitter::no_empties);
  if(parts.size() == 3)
    {
      std::string* result;
      if(!this->project->SetVersion(crea::wx2std(libraryName), result))
        wxMessageBox(crea::std2wx(*result),_T("Open File - Error!"),wxOK | wxICON_ERROR);
    }
  else
    {
      wxMessageBox(crea::std2wx("The version format is incorrect, please follow the following format:\nX.Y.Z\nX: Major Version\nY: Minor Version\nZ: Build Version"),_T("Set Project Version - Error!"),wxOK | wxICON_ERROR);
    }
  this->versiontc->SetLabel(crea::std2wx(this->project->GetVersion()));
  this->versionDatetc->SetLabel(crea::std2wx(this->project->GetVersionDate()));

}

void wxCDMProjectDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnAppliMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);
  std::cout << "entra appli " << this->project->GetAppli()->GetCMakeLists();
  if(this->project->GetAppli() != NULL)
    {
      newEvent->SetClientData(this->project->GetAppli());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnAppliMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetAppli() != NULL)
    {
      newEvent->SetClientData(this->project->GetAppli());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnLibMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->project->GetLib() != NULL)
    {
      newEvent->SetClientData(this->project->GetLib());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMProjectDescriptionPanel::OnLibMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetLib() != NULL)
    {
      newEvent->SetClientData(this->project->GetLib());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
