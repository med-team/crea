/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMProjectReadyDialog.cpp
 *
 *  Created on: 3/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectHelpDialog.h"

#include "wxCDMProjectDescriptionPanel.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMProjectHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMProjectHelpDialog::OnFinish)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMProjectHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMProjectHelpDialog::wxCDMProjectHelpDialog(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMProjectHelpDialog::Create(parent, id, caption, position, size, style);
  this->project = project;
}

wxCDMProjectHelpDialog::~wxCDMProjectHelpDialog()
{
}

bool wxCDMProjectHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMProjectHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Your project is ready!"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "A project has four main elements:\n"
          "- Packages: Host the black boxes you make.\n"
          "- Libraries: Contain the core functions of your programs, they are called by the black boxes and applications you make.\n"
          "- Applications: Stand alone programs that use the functions available on your libraries.\n"
          "- Configuration file: Contains the information of what should or shouldn't be compiled from this project.\n"
          "\n"
          "The Panel on the left is called \"Description Panel\" and show the details of the project item you are currently working on.\n"
          "To the right of the description panel you will find a tree with the project structure and it's actual content.\n"
          "Below the description panel you will find a panel with the project's main actions when you're ready to compile "
          "the project. you can hover on this buttons to see more information about what they do. They must be executed in the displayed "
          "order.\n"
          "When you create a project it comes with a default package. If you need to work on it or if you want to create more "
          "packages you can do it by clicking the \"Package Manager\" button below. You can also work with Libraries and "
          "Applications. Just click in the \"Library Manager\" button or \"Application manager\" button to start working "
          "with them.\n"),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMProjectHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMProjectHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
