/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMFolder.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMPackageSrc.h"

#include <fstream>
#include <algorithm>

#include <creaWx.h>
#include <wx/dir.h>

#include "CDMUtilities.h"

modelCDMPackageSrc::modelCDMPackageSrc()
{
  this->CMakeLists = NULL;
}

modelCDMPackageSrc::modelCDMPackageSrc(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating package src: " + path + "\n";
  this->parent = parent;
  //set attributes
  this->children.clear();
  this->level = level;
  this->CMakeLists = NULL;
  this->length = 0;
  this->name = name;
  this->path = CDMUtilities::fixPath(path);
  this->type = wxDIR_DIRS;

  //check all folders
  wxDir dir(crea::std2wx(path));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          //if is an unknown folder, create folder
          this->children.push_back(new modelCDMFolder(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));

          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxT("CMakeLists.txt"), wxDIR_FILES);
      if (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          this->CMakeLists = new modelCDMCMakeListsFile(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
          this->children.push_back(this->CMakeLists);
        }

      cont = dir.GetFirst(&fileName, wxT("*.h"), wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          modelCDMCodeFile* file;

          if(stdfileName.substr(0,2) == "bb")
            {
              file = new modelCDMCodeFile(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
              modelCDMBlackBox* blackBox = new modelCDMBlackBox(this, path, stdfileName.substr(2,stdfileName.size()-4), level + 1);
              blackBox->SetHeaderFile(file);
              wxDir dir2(crea::std2wx(path));
              cont = dir2.GetFirst(&fileName, crea::std2wx(stdfileName.substr(0,stdfileName.size()-2) + ".cxx"), wxDIR_FILES);
              if (cont)
                {
                  file = new modelCDMCodeFile(this, path + CDMUtilities::SLASH + crea::wx2std(fileName), crea::wx2std(fileName), this->level + 1);
                  this->children.push_back(file);
                  blackBox->SetSourceFile(file);
                }
              this->blackBoxes.push_back(blackBox);
            }
          cont = dir.GetNext(&fileName);
        }
    }

  this->SortChildren();
  std::sort(this->blackBoxes.begin(), this->blackBoxes.end(), CompareNodeItem);
}

modelCDMPackageSrc::~modelCDMPackageSrc()
{
  for (int i = 0; i < (int)(this->blackBoxes.size()); i++)
    {
      if(this->blackBoxes[i] != NULL)
        {
          delete this->blackBoxes[i];
          this->blackBoxes[i] = NULL;
        }
    }
  this->blackBoxes.clear();
}

const std::vector<modelCDMBlackBox*>& modelCDMPackageSrc::GetBlackBoxes() const
{
  return this->blackBoxes;
}

modelCDMBlackBox* modelCDMPackageSrc::CreateBlackBox(
    std::string*& result,
    const std::string& name,
    const std::string& package,
    const std::string& type,
    const std::string& format,
    const std::string& categories,
    const std::string& authors,
    const std::string& authorsEmail,
    const std::string& description)
{
  //parse name
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, name, " \n\",/\\'", CDMUtilities::splitter::no_empties);
  std::string bbName;
  for (int i = 0; i < (int)(words.size()); i++)
    {
      bbName += words[i];
    }

  //parse categories
  CDMUtilities::splitter::split(words, categories, " \n\",/\\'", CDMUtilities::splitter::no_empties);
  std::string bbCategories;
  if(words.size() > 0)
    {
      bbCategories = words[0];
      for (int i = 1; i < (int)(words.size()); i++)
        {
          bbCategories += "," + words[i];
        }
    }
  if(bbCategories == "")
    bbCategories = "empty";

  //parse authors
  CDMUtilities::splitter::split(words, authors, "\n\",/\\'", CDMUtilities::splitter::no_empties);
  std::string bbAuthors;
  if(words.size() > 0)
    {
      bbAuthors = words[0];
      for (int i = 1; i < (int)(words.size()); i++)
        {
          bbAuthors += "," + words[i];
        }
    }
  if(bbAuthors == "")
    bbAuthors = "Unknown";

  //parse description
  CDMUtilities::splitter::split(words, authorsEmail, " \n\"/\\'", CDMUtilities::splitter::no_empties);
  std::string bbDescription;
  if(words.size() > 0)
    {
      bbDescription = words[0];
      for (int i = 1; i < (int)(words.size()); i++)
        {
          bbDescription += "," + words[i];
        }
      bbDescription += " - ";
    }
  CDMUtilities::splitter::split(words, description, "\n\"/\\'", CDMUtilities::splitter::no_empties);
  if(words.size() > 0)
    {
      bbDescription += words[0];
      for (int i = 1; i < (int)(words.size()); i++)
        {
          bbDescription += words[i];
        }
    }

  if(bbDescription == "")
    bbDescription = "No Description.";


  //create command
  std::string command = "bbCreateBlackBox";
  command += " \"" + this->path + "\"";
#ifdef _WIN32
  command += " " + package;
  command += " " + bbName;
  command += " " + type;
  command += " " + format;
#else
  command += " \"" + package + "\"";
  command += " \"" + bbName + "\"";
  command += " \"" + type + "\"";
  command += " \"" + format + "\"";
#endif
  command += " \"" + bbAuthors + "\"";
  command += " \"" + bbDescription + "\"";
  command += " \"" + bbCategories + "\"";

  //excecute command
  //wxMessageBox(crea::std2wx("Command: ->" + command + "<-"),_T("Creating Black Box"),wxOK | wxICON_INFORMATION);
  if(system(command.c_str()))
    {
      result = new std::string("Error executing command '" + command + "'");
      return NULL;
    }

  //if command succeed

  //create header
  //create source
  modelCDMFile* header = NULL;
  modelCDMFile* source = NULL;
  wxDir dir(crea::std2wx(path));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, crea::std2wx("bb"+package+bbName+".h"), wxDIR_FILES);
      if (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          header = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level+1);
        }
      cont = dir.GetFirst(&fileName, crea::std2wx("bb"+package+bbName+".cxx"), wxDIR_FILES);
      if (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          source = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level+1);
        }
    }
  //if source and header exist
  if (header != NULL && source != NULL)
    {
      //create black box
      modelCDMBlackBox* blackBox = new modelCDMBlackBox(this, this->path, package+bbName);

      //associate header and source
      blackBox->SetHeaderFile(header);
      blackBox->SetSourceFile(source);

      this->children.push_back(header);
      this->children.push_back(source);

      this->blackBoxes.push_back(blackBox);

      //sort children
      std::sort(this->blackBoxes.begin(), this->blackBoxes.end(), CompareNodeItem);
      this->SortChildren();

      return blackBox;
    }
  else
    {
      result = new std::string("The header and source files were not found. Black box not created.");
      return NULL;
    }
}

const bool modelCDMPackageSrc::Refresh(std::string*& result)
{
  std::cout << "refreshing package src" << std::endl;
  //set attributes
  this->type = wxDIR_DIRS;

  //check children
  std::vector<bool> checked(this->children.size(), false);
  std::vector<bool> checkedBoxes(this->blackBoxes.size(), false);

  //check all boxes
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::string folderName = stdfileName;
          //check if they already exist
          bool found = false;
          for (int i = 0; !found && i < (int)(this->children.size()); i++)
            {
              if (this->children[i]->GetName() == folderName)
                {
                  found = true;
                  checked[i] = true;
                  if(!this->children[i]->Refresh(result))
                    return false;
                }
            }
          if(!found)
            {
              modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(folder);
            }
          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }
          //if is an unknown file, create file
          else
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  //if is a code file, create modelCDMCodeFile
                  if(
                      fileType == ".c" ||
                      fileType == ".cxx" ||
                      fileType == ".h" ||
                      fileType == ".cpp" ||
                      fileType == ".txx" ||
                      fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          //if is a Black Box header, check in black boxes
          if(stdfileName.substr(stdfileName.size() - 2, 2) == ".h" && stdfileName.substr(0,2) == "bb")
            {
              bool found = false;
              for (int i = 0; i < (int)(this->blackBoxes.size()); i++)
                {
                  if(this->blackBoxes[i]->GetHeaderFile()->GetName() == stdfileName)
                    {
                      checkedBoxes[i] = true;
                      found = true;
                      if(!this->blackBoxes[i]->Refresh(result))
                        return false;
                      break;
                    }
                }

              if (!found)
                {
                  modelCDMBlackBox* blackBox = new modelCDMBlackBox(this, path, stdfileName.substr(2,stdfileName.size()-4), level + 1);
                  this->blackBoxes.push_back(blackBox);
                }

            }

          cont = dir.GetNext(&fileName);
        }
    }

  for (int i = 0; i < (int)(checkedBoxes.size()); i++)
    {
      if(!checkedBoxes[i])
        {
          delete this->blackBoxes[i];
          this->blackBoxes.erase(this->blackBoxes.begin()+i);
          checkedBoxes.erase(checkedBoxes.begin()+i);
          i--;
        }
    }

  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }
  this->SortChildren();
  std::sort(this->blackBoxes.begin(), this->blackBoxes.end(), CompareNodeItem);
  return true;
}
