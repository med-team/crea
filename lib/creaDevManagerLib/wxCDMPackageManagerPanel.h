/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMPackageManagerPanel.h
 *
 *  Created on: Dec 10, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPACKAGEMANAGERPANEL_H_
#define WXCDMPACKAGEMANAGERPANEL_H_

#include <creaWx.h>
#include <wx/hyperlink.h>
#include <wx/event.h>

#include "modelCDMProject.h"

/**
 * Package manager description panel. Shows the available packages in the project and the actions corresponding to package management.
 */
class wxCDMPackageManagerPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:

  /**
   * Package manager description panel Constructor.
   * @param parent Parent window reference.
   * @param project Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMPackageManagerPanel(
      wxWindow* parent,
      modelCDMProject* project,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMPackageManagerPanel();

  /**
   * Package manager description panel Creator.
   * @param parent Parent window reference.
   * @param project Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  bool Create(
      wxWindow* parent,
      modelCDMProject* project,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

  /**
   * Retrieves the described project class reference.
   * @return Project reference.
   */
  modelCDMProject* GetProject() const;

private:
  /**
   * Project described.
   */
  modelCDMProject* project;

  //handlers
protected:
  /**
   * Handles when a return link is pressed.
   * @param event Has the link reference to know where to return
   */
  void OnBtnReturn(wxHyperlinkEvent& event);
  /**
   * Handles when a package checkbox is (un)checked.
   * @param event Has the link reference to know which package was selected.
   */
  void OnChBPackageChange(wxCommandEvent& event);
  /**
   * Handles when a packages link is pressed.
   * @param event Has the link reference to know which package was selected.
   */
  void OnLnkPackageSelect(wxHyperlinkEvent& event);
  /**
   * Handles when the create package button is pressed.
   */
  void OnBtnCreatePackage(wxCommandEvent& event);
  /**
   * Handles when the open package cmakelists file button is pressed.
   */
  void OnBtnEditCMakeLists(wxCommandEvent& event);

  /**
   * Handles when a package link is hovered.
   * @param event Has the link reference to know which package was selected.
   */
  void OnMouseEnter(wxMouseEvent& event);
  /**
   * Handles when a package link button finishes hover.
   * @param event Has the link reference to know which package was selected.
   */
  void OnMouseExit(wxMouseEvent& event);
  /**
   * Handles when the open cmakelists file button is hovered.
   */
  void OnCMakeMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the open cmakelists file button finishes hover.
   */
  void OnCMakeMouseExit(wxMouseEvent& event);
};

#endif /* WXCDMPACKAGEMANAGERPANEL_H_ */
