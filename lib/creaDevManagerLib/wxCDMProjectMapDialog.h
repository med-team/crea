/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMProjectMapDialog.h
 *
 *  Created on: 25/4/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPROJECTMAPDIALOG_H_
#define WXCDMPROJECTMAPDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>


/**
 * Project Map Dialog
 */
class wxCDMProjectMapDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Project Map Dialog Constructor.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Project Routes Map".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMProjectMapDialog(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Project Routes Map"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(630,500),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMProjectMapDialog();
  /**
   * Project Map Dialog Creator.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Project Routes Map".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Project Routes Map"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(630,500),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );

protected:
  /**
   * Creates the help controls (text and buttons).
   */
  void CreateControls();

  void PaintEvent(wxPaintEvent& evt);

//attributes
private:

  wxImage* image;
  wxPanel* imagePanel;

//handlers
protected:
  /**
   * Handler to close help dialog.
   * @param event Unused.
   */
  void OnFinish(wxCommandEvent& event);
};

#endif /* WXCDMPROJECTMAPDIALOG_H_ */
