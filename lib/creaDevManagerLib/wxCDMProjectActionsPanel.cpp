/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# # Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMProjectActionsPanel.cpp
 *
 *  Created on: 25/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectActionsPanel.h"

#include <wx/progdlg.h>

#include "creaDevManagerIds.h"

#include <map>
#include "wxCDMProjectStructureReportDialog.h"

BEGIN_EVENT_TABLE(wxCDMProjectActionsPanel, wxPanel)
EVT_BUTTON(ID_BUTTON_CHECK_PROJECT, wxCDMProjectActionsPanel::OnBtnCheckProjectStructure)
EVT_BUTTON(ID_BUTTON_BUILD_PROJECT, wxCDMProjectActionsPanel::OnBtnBuildProject)
EVT_BUTTON(ID_BUTTON_CONFIGURE_BUILD, wxCDMProjectActionsPanel::OnBtnConfigureBuild)
EVT_BUTTON(ID_BUTTON_CONNECT_PROJECT, wxCDMProjectActionsPanel::OnBtnConnectProject)
END_EVENT_TABLE()

wxCDMProjectActionsPanel::wxCDMProjectActionsPanel(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMProjectActionsPanel::Create(parent,id,caption,pos,size,style);
  this->project = project;
}

wxCDMProjectActionsPanel::~wxCDMProjectActionsPanel()
{
}

bool wxCDMProjectActionsPanel::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent,id,pos,size,style);
  wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
  this->SetSizer(sizer);
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);

  return TRUE;
}

void wxCDMProjectActionsPanel::CreateControls()
{
  wxButton* checkStructbt = new wxButton(this, ID_BUTTON_CHECK_PROJECT, _T("1. Check Project Structure"));
  checkStructbt->SetToolTip(wxT("This step checks the project structure and tells what is going to be compiled."));
  wxButton* configurebt = new wxButton(this, ID_BUTTON_CONFIGURE_BUILD, _T("2. Configure Project (CMake)"));
  configurebt->SetToolTip(wxT("This is the second step in order to execute the project. Make sure you have selected the desired Build location."));
  wxButton* compilebt = new wxButton(this, ID_BUTTON_BUILD_PROJECT, _T("3. Compile Project"));
  compilebt->SetToolTip(wxT("This step should be done after configuring the project. This will create the executables."));
  wxButton* plugbt = new wxButton(this, ID_BUTTON_CONNECT_PROJECT, _T("4. Plug Packages (BBTK)"));
  plugbt->SetToolTip(wxT("This step should be done after compiling the project. This will allow to use the boxes in this project to be available in the bbEditor."));
  this->GetSizer()->Add(checkStructbt, 0, wxALL, 5);
  this->GetSizer()->Add(configurebt, 0, wxALL, 5);
  this->GetSizer()->Add(compilebt, 0, wxALL, 5);
  this->GetSizer()->Add(plugbt, 0, wxALL, 5);
}

//check project structure
void wxCDMProjectActionsPanel::OnBtnCheckProjectStructure(wxCommandEvent& event)
{
  std::map<std::string, bool> prjStruct;
  this->project->CheckStructure(prjStruct);
  std::cout << prjStruct.size() << std::endl;
  wxCDMProjectStructureReportDialog* structure = new wxCDMProjectStructureReportDialog(this->GetParent(), prjStruct, wxID_ANY);
  structure->Show(true);
}

//configure project
void wxCDMProjectActionsPanel::OnBtnConfigureBuild(wxCommandEvent& event)
{
  std::string* result;
  if(!this->project->ConfigureBuild(result))
    {
      wxMessageBox(crea::std2wx(result->c_str()), wxT("Project Configuration - Error!"));
      return;
    }
  wxMessageBox(crea::std2wx("The configuration was executed successfully."), wxT("Project Configuration"));
}

//compile project
void wxCDMProjectActionsPanel::OnBtnBuildProject(wxCommandEvent& event)
{
#ifdef _WIN32
  std::string* result;
  if(!this->project->Build(result, ""))
    {
      wxMessageBox(crea::std2wx(result->c_str()), wxT("Project Compilation - Error!"));
      return;
    }
#else

  std::string* result;
  int isDir = wxMessageBox(crea::std2wx("Is this the path of the project compilation?:\n"+this->project->GetBuildPath()), wxT("Project Compilation - CreaDevManager"), wxYES_NO|wxCANCEL);
  if(isDir != wxCANCEL)
    {
      wxString file = crea::std2wx(this->project->GetBuildPath());
      if(isDir == wxNO)
        {
          file = wxDirSelector(
              wxT("Please select the folder where your project is to be built."),
              crea::std2wx(this->project->GetBuildPath())
          );
        }

      if(!file.IsEmpty())
        {
          this->project->SetBuildPath(crea::wx2std(file), result);

          wxTextEntryDialog* buildDlg = new wxTextEntryDialog(
                this,
                wxT("Check the compilation instruction:"),
                wxT("Project Compilation - CreaDevManager"),
                crea::std2wx(this->project->GetBuildInstruction()),
                wxTE_MULTILINE | wxOK | wxCANCEL
            );

          if (buildDlg->ShowModal() == wxID_OK)
            {
              std::string buildDlgStr = crea::wx2std(buildDlg->GetValue());
              //check line
              if (buildDlgStr != "")
                {
                  std::string* result;
                  if(!this->project->Build(result, buildDlgStr))
                    {
                      wxMessageBox(crea::std2wx(result->c_str()), wxT("Project Compilation - Error!"));
                      return;
                    }
                }
            }
        }
    }
#endif
}
//plug packages
void wxCDMProjectActionsPanel::OnBtnConnectProject(wxCommandEvent& event)
{
  std::string* result;
  int isDir = wxMessageBox(crea::std2wx("Is this the path of the project compilation to plug?:\n"+this->project->GetBuildPath()), wxT("Plug BBTK Packages"), wxYES_NO|wxCANCEL);
  if(isDir != wxCANCEL)
    {
      wxString file = crea::std2wx(this->project->GetBuildPath());
      if(isDir == wxNO)
        {
          file = wxDirSelector(
              wxT("Please select the folder containing the bbtkPackage file you want to use. Usually it is where you built your project."),
              crea::std2wx(this->project->GetBuildPath())
          );
        }

      if(!file.IsEmpty())
        {
          if(!this->project->Connect(result, crea::wx2std(file)))
            {
              wxMessageBox(crea::std2wx(result->c_str()), wxT("Plug BBTK Packages - Error!"), wxICON_ERROR);
              return;
            }
          else
            {
              wxMessageBox(crea::std2wx("The connection was executed successfully. Please check the console to see the compilation result.\n Also, don't forget to restart the BBTK Graphical Editor (if already opened) to see the plugged packages."), wxT("Plug Package"));
            }

        }

    }
}
