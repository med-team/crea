/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMPackageDescriptionPanel.h
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPACKAGEDESCRIPTIONPANEL_H_
#define WXCDMPACKAGEDESCRIPTIONPANEL_H_

#include <creaWx.h>
#include <wx/hyperlink.h>

#include "modelCDMPackage.h"

/**
 * Package description panel. Shows the package's properties and the available actions for a package.
 */
class wxCDMPackageDescriptionPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Package description panel Constructor.
   * @param parent Parent window reference.
   * @param package Package class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMPackageDescriptionPanel(
      wxWindow* parent,
      modelCDMPackage* package,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor
   */
  ~wxCDMPackageDescriptionPanel();

  /**
   * Package description panel Creator.
   * @param parent Parent window reference.
   * @param package Package class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if creation was successful.
   */
  bool Create(
      wxWindow* parent,
      modelCDMPackage* package,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

private:
  /**
   * Package described.
   */
  modelCDMPackage* package;
  /**
   * Control with the package author.
   */
  wxStaticText* authortc;
  /**
   * Control with the package version.
   */
  wxStaticText* versiontc;
  /**
   * Control with the package description.
   */
  wxStaticText* descriptiontc;

  //handlers
protected:
  /**
   * Handles when a return link is pressed.
   */
  void OnBtnReturn(wxHyperlinkEvent& event);

  /**
   * Handles when the Set author button is pressed.
   */
  void OnBtnSetAuthor(wxCommandEvent& event);
  /**
   * Handles when the Set version button is pressed.
   */
  void OnBtnSetVersion(wxCommandEvent& event);
  /**
   * Handles when the Change description button is pressed.
   */
  void OnBtnSetDescription(wxCommandEvent& event);

  /**
   * Handles when a black box link is pressed.
   */
  void OnLnkBlackBoxSelect(wxHyperlinkEvent& event);

  /**
   * Handles when the create black box button is pressed.
   */
  void OnBtnCreateBlackBox(wxCommandEvent& event);
  /**
   * Handles when the configure package button is pressed.
   */
  void OnBtnConfigurePackage(wxCommandEvent& event);
  /**
   * Handles when the edit cmakelists file button is pressed.
   */
  void OnBtnEditCMakeLists(wxCommandEvent& event);
  /**
   * Handles when the open folder button is pressed.
   */
  void OnBtnOpenFolder(wxCommandEvent& event);
  /**
   * Handles when a black box link is hovered.
   */
  void OnMouseEnter(wxMouseEvent& event);
  /**
   * Handles when a black box link exits hover.
   */
  void OnMouseExit(wxMouseEvent& event);
  /**
   * Handles when the edit cmakelists file button is hovered.
   */
  void OnCMakeMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the edit cmakelists file button exits hover.
   */
  void OnCMakeMouseExit(wxMouseEvent& event);

};

#endif /* WXCDMPACKAGEDESCRIPTIONPANEL_H_ */
