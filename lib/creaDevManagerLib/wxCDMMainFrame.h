/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#ifndef WXCDMMAINFRAME_H_INCLUDED
#define WXCDMMAINFRAME_H_INCLUDED

#include <creaWx.h>
#include <wx/aui/aui.h>
#include <wx/treectrl.h>
#include "wxCDMProjectsTreeCtrl.h"
#include "modelCDMMain.h"

/**
 * Main Frame Class.
 * This class is the main class of the application. It starts the other classes and windows as well as it holds a reference to the application model.
 */
class wxCDMMainFrame:public wxFrame
{
  DECLARE_EVENT_TABLE()

public:
  /**
   * Constructor receiving common parameter for frame construction.
   * @param parent The parent window of the wxCDMMainFrame object.
   * @param id The id of the Frame, by default wxID_ANY.
   * @param caption Frame caption. Usually shown on the top of the window. It's by default "CREATIS CreaDevManager".
   * @param pos Position of the application, by default wxDefaultPosition.
   * @param size Size of the application, by default wxDefaultSize.
   * @param style Style of the application, by default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMMainFrame(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("CREATIS CreaDevManager"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMMainFrame();

  /**
   * Create Method.
   * Actually creates the frame and creates the controls inside the application.
   * @param parent The parent window of the wxCDMMainFrame object.
   * @param id The id of the Frame, by default wxID_ANY.
   * @param caption Frame caption. Usually shown on the top of the window. It's by default "CREATIS CreaDevManager".
   * @param pos Position of the application, by default wxDefaultPosition.
   * @param size Size of the application, by default wxDefaultSize.
   * @param style Style of the application, by default wxDEFAULT_FRAME_STYLE.
   * @return True if the creation process went well.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("CREATIS CreaDevManager"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Retreives the application model.
   * @return Model of the application.
   */
  modelCDMMain* GetModel() const;

  /**
   * Returns the properties panel. where the selection description is shown.
   * @return the description panel of the project component chosen by the user.
   */
  wxPanel* GetPropertiesPanel() const;

  /**
   * Checks if the help is enabled.
   * @return true if the help is enabled.
   */
  bool isHelp() const;

  /**
   * Refresh the project structure by comparing the existing model with the corresponding files in the hard drive.
   */
  void RefreshProject();

protected:

  /**
   * Creates the menu bar and binds the corresponding event handler to each menu.
   */
  void CreateMenus();

  /**
   * Create the user interface containing a wxCDMMainDescriptionPanel and a wxCDMProjectsTreeCtrl.
   */
  void CreateControls();

private:

  //Menus
  /**
   * Recently opened projects menu
   */
  wxMenu* menu_Recent;
  /**
   * File menu
   */
  wxMenu* menu_File;
  /**
   * Edit menu
   */
  wxMenu* menu_Edit;
  /**
   * Tools menu
   */
  wxMenu* menu_Tools;
  /**
   * Help menu
   */
  wxMenu* menu_Help;

  //Controls
  /**
   * Floating panel manager
   */
  wxAuiManager auiManager;
  /**
   * Tree control for an open project
   */
  wxCDMProjectsTreeCtrl* tree_Projects;
  /**
   * Tree item for the selected item in the tree.
   */
  wxTreeItemId actualTreeItem;
  /**
   * Description panel for a selected project item
   */
  wxPanel* panel_Properties;
  /**
   * Main actions for an open project
   */
  wxPanel* panel_ProjectActions;

  //Model
  /**
   * Application model. It holds the open project model.
   */
  modelCDMMain* model;

  /**
   * Help enabled
   */
  bool help;

  //events
protected:

  /**
   * Starts when the menu bar is opened.
   * @param event The event object that triggers the handler.
   */
  void OnMenuBarOpen(wxMenuEvent& event);

  //File
  /**
   * New project handler. Launches a new project dialog and creates a project model if the project is correctly created.
   * @param event The event object that triggers the handler.
   */
  void OnMenuNewProject(wxCommandEvent& event);
  /**
   * Open project handler. Launches a directory dialog and creates a project model if the project is correctly opened.
   * @param event The event object that triggers the handler.
   */
  void OnMenuOpenProject(wxCommandEvent& event);
  /**
   * Open recent project handler. Creates a project model if the project is correctly opened given its path.
   * @param event The event object that triggers the handler.
   */
  void OnMenuOpenRecent(wxCommandEvent& event);
  /**
   * Close project handler. Remove the project from the model and restarts the user interface.
   * @param event The event object that triggers the handler.
   */
  void OnMenuCloseProject(wxCommandEvent& event);
  /**
   * Unimplemented optional method handler. It should export the project structure in XML format.
   * @param event The event object that triggers the handler.
   */
  void OnMenuExportHierarchy(wxCommandEvent& event);
  /**
   * Exit handler. It closes any open project and quits the application.
   * @param event The event object that triggers the handler.
   */
  void OnMenuExit(wxCommandEvent& event);

  //Edit
  /**
   * Refresh project handler. Refreshes the project structure.
   * @param event The event object that triggers the handler.
   */
  void OnMenuRefreshProject(wxCommandEvent& event);
  /**
   * Open the settings dialog.
   * @param event The event object that triggers the handler.
   */
  void OnMenuSettings(wxCommandEvent& event);

  //Tools
  /**
   * Launches the BBTK Graphical Editor, also known as BBEditor.
   * @param event The event object that triggers the handler.
   */
  void OnMenuBBTKGraphicalEditor(wxCommandEvent& event);
  /**
   * Launches the Minitools application alse known as creaTools
   * @param event The event object that triggers the handler.
   */
  void OnMenuMiniTools(wxCommandEvent& event);
  /**
   * Launches the system default code editor.
   * Linux: gedit
   * Mac:
   * Windows:
   * @param event The event object that triggers the handler.
   */
  void OnMenuCodeEditor(wxCommandEvent& event);
  /**
   * Launches the system command line interpreter (CLI).
   * Linux: gnome-terminal
   * Mac:
   * Windows:
   * @param event The event object that triggers the handler.
   */
  void OnMenuCommandLine(wxCommandEvent& event);

  //Help
  /**
   * Enables/Disables the help option.
   * @param event The event object that triggers the handler.
   */
  void OnMenuToggleHelp(wxCommandEvent& event);
  /**
   * Open the road map for crea projects.
   * @param event The event object that triggers the handler.
   */
  void OnMenuShowProjectMap(wxCommandEvent& event);
  /**
   * Open the default web browser and redirects to the CreaTools Documentation page.
   * @param event The event object that triggers the handler.
   */
  void OnMenuHelp(wxCommandEvent& event);
  /**
   * Open the default web browser and redirects to the Crea Bug Tracking page.
   * @param event The event object that triggers the handler.
   */
  void OnMenuReportBug(wxCommandEvent& event);
  /**
   * Shows the about dialog of creaDevManager
   * @param event The event object that triggers the handler.
   */
  void OnMenuAboutCreaDevManager(wxCommandEvent& event);
  /**
   * Open the default web browser and redirects to the Creatis page.
   * @param event The event object that triggers the handler.
   */
  void OnMenuAboutCreatis(wxCommandEvent& event);

  //Tree
  /**
   * Handles the propertiesPanel change when there is a change in the selection on the Project Tree.
   * @param event The event object that triggers the handler.
   */
  void OnTreeSelectionChanged(wxTreeEvent& event);

  //PropertiesPanel
  /**
   *Handles the propertiesPanel change when the event wxEVT_DISPLAY_CHANGED is triggered.
   * @param event The event object that triggers the handler.
   */
  void OnChangeView(wxCommandEvent& event);

  //Element higlighted
  /**
   * Handles the change of the style of an element in the tree when buttons are hovered.
   * @param event The event object that triggers the handler.
   */
  void OnElementSelected(wxCommandEvent& event);
  /**
   * Handles the change of the style of an element in the tree when buttons finish hover.
   * @param event The event object that triggers the handler.
   */
  void OnElementDeselected(wxCommandEvent& event);

  //Enable/Disable help
  /**
   * Handles the change of the state of the help option when it's triggered by another class.
   * @param event The event object that triggers the handler.
   */
  void OnDisableHelp(wxCommandEvent& event);
};

#endif
