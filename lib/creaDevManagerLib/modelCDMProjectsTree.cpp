/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * ModelCreaDevManagerTree.cpp
 *
 *  Created on: 22/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMProjectsTree.h"

#include <creaWx.h>
#include <wx/dir.h>

#include <sstream>
#include <algorithm>

modelCDMProjectsTree::modelCDMProjectsTree()
{
}

modelCDMProjectsTree::~modelCDMProjectsTree()
{
}

bool modelCDMProjectsTree::CompareNodeItem(modelCDMProjectsTreeNode x, modelCDMProjectsTreeNode y)
{
  bool returnValue;
  bool noWinner = true;
  unsigned int i = 0;
  std::string xName = x.GetName();
  std::string yName = y.GetName();
  unsigned char xType = x.GetType();
  unsigned char yType = y.GetType();

  while ((i < xName.length()) && (i < yName.length()))
  {
    if (tolower (xName[i]) < tolower (yName[i]))
    {
      noWinner = false;
      returnValue = true;
      break;
    }
    else if (tolower (xName[i]) > tolower (yName[i]))
    {
      noWinner = false;
      returnValue = false;
      break;
    }
    i++;
  }

  if(noWinner)
  {
    if (xName.length() < yName.length())
      returnValue = true;
    else
      returnValue = false;
  }

  if(xType != yType)
  {
    if(xType == wxDIR_DIRS)
      returnValue = true;
    else
      returnValue = false;
  }

  return returnValue;
}

void modelCDMProjectsTree::SetRoot(std::string path)
{
  std::stringstream p(path);
  std::vector<std::string> breadcrumbs;
  std::string name;

  while(!p.eof())
  {
    getline(p,name,'/');
    if(name != "")
      breadcrumbs.push_back(name);
  }

  path = "/";
  for (int i = 0; i < (int)(breadcrumbs.size())-1; i++)
  {
    path += breadcrumbs[i] + "/";
  }
  name = breadcrumbs[breadcrumbs.size()-1];

  bool projectFound = false;
  if(projectRoot.GetName() == name)
    projectFound = true;

  if(!projectFound)
  {
    this->projectRoot = modelCDMProjectsTreeNode(path,name,wxDIR_DIRS ,0);
  }else{
    std::cout << "already existing ";
  }

  std::cout << "project root set: " << name << " in " << path << std::endl;
}

void modelCDMProjectsTree::populateNode(modelCDMProjectsTreeNode& node)
{
  //std::cout << "populating " << node.GetName() << " path " << node.GetPath() << "..." << std::endl;
  std::vector <modelCDMProjectsTreeNode>*  nodes = new std::vector <modelCDMProjectsTreeNode>;

  std::string path = node.GetPath()+node.GetName()+"/";

  wxDir dir(crea::std2wx(path.c_str()));
  if (!dir.IsOpened())
  {
    std::cerr << "Couldn't open the directory" << std::endl;
    return;
  }

  wxString fileName;
  bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
  while (cont)
  {
      modelCDMProjectsTreeNode innerNode = modelCDMProjectsTreeNode(path, crea::wx2std(fileName), wxDIR_DIRS, node.GetLevel()+1);
    this->populateNode(innerNode);
    nodes->push_back(innerNode);
    cont = dir.GetNext(&fileName);
  }

  cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
  while (cont)
  {
    modelCDMProjectsTreeNode innerNode = modelCDMProjectsTreeNode(path, crea::wx2std(fileName), wxDIR_FILES, node.GetLevel()+1);
    nodes->push_back(innerNode);
    cont = dir.GetNext(&fileName);
  }

  sort (nodes->begin(), nodes->end(), CompareNodeItem);
  node.SetChildren(*nodes);

  /*

  DIR *dp;
  struct dirent *ep;

  std::string path = node.GetPath()+node.GetName()+"/";

  dp = opendir(path.c_str());
  if (dp != NULL)
  {
    while ((ep = readdir (dp)) != NULL)
    {
      //std::cout << ep->d_name << std::endl;
      if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 )
      {
        ModelCreaDevManagerTreeNode innerNode = ModelCreaDevManagerTreeNode(path, ep->d_name, ep->d_type, node.GetLevel()+1);
        if (ep->d_type == DT_DIR)
        {
          this->populateNode(innerNode);
        }

        nodes->push_back(innerNode);
      }

    }

    (void) closedir (dp);

    sort (nodes->begin(), nodes->end(), CompareNodeItem);

    node.SetChildren(*nodes);
  }
  else
  {
    std::cerr << "Couldn't open the directory" << std::endl;
  }*/
}

void modelCDMProjectsTree::populateNode(std::string path)
{
  if(path[path.size()-1] != '/')
    path+="/";
  std::cout << "searching " << path << std::endl;
  if(this->projectRoot.GetPath()+this->projectRoot.GetName()+"/" == path)
  {
      std::cout << "Populating Project: " << path << "..." << std::endl;
    this->populateNode(this->projectRoot);
  }
}
