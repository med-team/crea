/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMLib.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMLIB_H_
#define MODELCDMLIB_H_

#include<iostream>
#include<vector>
#include<map>

#include "modelCDMFolder.h"
#include "modelCDMLibrary.h"

/**
 * Class representing the lib folder of a Crea project.
 */
class modelCDMLib : public modelCDMFolder
{
public:
  /**
   * Default Constructor.
   */
  modelCDMLib();
  /**
   * Lib folder node constructor.
   * @param parent Parent node of the lib folder node.
   * @param path Full path to the lib folder node.
   * @param name Name of the lib folder node. By default "lib".
   * @param level Project hierarchy level of the lib folder node.
   */
  modelCDMLib(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name = "lib", const int& level = 1);
  /**
   * Destructor.
   */
  ~modelCDMLib();

  /**
   * Returns the libraries registered in the lib folder.
   * @return Array of library references.
   */
  const std::vector<modelCDMLibrary*>& GetLibraries() const;

  /**
   * Creates a new library node for the actual project and registers it. It modifies the project model as well as the system. The created library is included in the lib's CMakeLists file.
   * @param name Name of the new library.
   * @param result Result message.
   * @return New library reference.
   */
  modelCDMLibrary* CreateLibrary(
      const std::string& name,
      std::string*& result
  );

  /**
   * Refreshes the structure of the lib folder. Deletes folders and files deleted since the las refresh and Adds folders and files created since the las refresh.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Checks the file structure and the CMakeLists file to find structure definition errors before compiling the project.
   * @param properties Properties of the project.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks if the given library is included in the CMakeLists file.
   * @param library_name Name of the library to check.
   * @return True if the library is included, otherwise returns False.
   */
  bool IsLibraryIncluded(const std::string& library_name);

  /**
   * Sets the inclusion of the library in the lib's CMakeLists file. If the library inclusion already exist in file, then the line is uncommented/commented depending on the requested action. If the library inclusion doesn't exist yet, then it is included if the request is an inclusion.
   * @param library_name Name of the library to include/exclude.
   * @param toInclude True if the request is an inclusion, False otherwise.
   * @return True if the request was processed successfully.
   */
  bool SetLibraryInclude(const std::string& library_name, const bool& toInclude);

private:
  /**
   * Libraries references.
   */
  std::vector<modelCDMLibrary*> libraries;
};

#endif /* MODELCDMLIB_H_ */
