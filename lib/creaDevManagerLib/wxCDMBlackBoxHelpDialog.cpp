/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMBlackBoxHelpDialog.cpp
 *
 *  Created on: 11/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMBlackBoxHelpDialog.h"

#include "creaDevManagerIds.h"

#include "modelCDMProject.h"
#include "modelCDMPackage.h"

BEGIN_EVENT_TABLE(wxCDMBlackBoxHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMBlackBoxHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMBlackBoxHelpDialog::OnCMakeLists)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMBlackBoxHelpDialog::OnCMakeLists)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMBlackBoxHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMBlackBoxHelpDialog::wxCDMBlackBoxHelpDialog(
    wxWindow* parent,
    modelCDMBlackBox* blackBox,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMBlackBoxHelpDialog::Create(parent, id, caption, position, size, style);
  this->blackBox = blackBox;
}

wxCDMBlackBoxHelpDialog::~wxCDMBlackBoxHelpDialog()
{
}

bool wxCDMBlackBoxHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMBlackBoxHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Working with BlackBoxes"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Black boxes are made to work in a modular fashion. They have programmer defined inputs and outputs. Their "
          "purpose is to use the functions available in the libraries and expose them to be used as boxes in the crea "
          "environment.\n"
          "Black boxes are stored in the src folder of a package and they are composed of two files, the header(.h) "
          "and the implementation(.cxx) files.\n"
          "To start developing black boxes, go ahead and open the header file with the button \"Open .h\" and define "
          "the inputs and outputs of the black box. Then, you will be able to use them in the implementation file, which "
          "you can open using the \"Open .cxx\" button.\n"
          "If you don't understand how this inputs and outputs are used, try looking at the sample black boxes available "
          "in the sample package, which is shipped with every new project.\n"
          "Also, don't forget to include the libraries your boxes use in the header and implementation files. They should "
          "also be pointed and included in the package's directory CMakeLists.txt file by uncommenting the \"SET\" commands "
          "for third party libraries or by including the library name inside the \"SET(${BBTK_PACKAGE_NAME}_LIBS\"command "
          "and its path inside the \"SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS\" command for custom libraries. You must also "
          "include the package your black box is in by including the command \"ADD_SUBDIRECTORY([packageName])\" in the "
          "project's directory CMakeLists.txt file. Again, please take a look at the sample package and its boxes to see "
          "how to include libraries in order to use them in the boxes.\n"
          "\n"
          "You can easily edit the CMakeLists files previously mentioned by clicking on the following buttons."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakePkgBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Open Package's directory CMakeLists file"));
  editCMakePkgBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakePkgBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxHelpDialog::OnCMakeListsExit,NULL,this);
  wxButton* editCMakePrjBtn= new wxButton(this, ID_BUTTON_OPENPROJECT, wxT("Open Project's directory CMakeLists file"));
  editCMakePrjBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakePrjBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMBlackBoxHelpDialog::OnCMakeListsExit,NULL,this);

  v_sizer1->Add(editCMakePkgBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);
  v_sizer1->Add(editCMakePrjBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMBlackBoxHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMBlackBoxHelpDialog::OnCMakeLists(wxCommandEvent& event)
{
  std::string* result;

  if((int)((wxButton*)event.GetEventObject())->GetId() == (int)ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      modelCDMIProjectTreeNode* node = this->blackBox;
      while (node != NULL && dynamic_cast<modelCDMPackage*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          if(!((modelCDMPackage*)node)->OpenCMakeListsFile(result))
            wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMPackage*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMPackage*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
      else
        {
          wxMessageBox(crea::std2wx("No project CMakeLists file was found."),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->blackBox;
      while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          if(!((modelCDMProject*)node)->OpenCMakeListsFile(result))
            wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
      else
        {
          wxMessageBox(crea::std2wx("No project CMakeLists file was found."),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMBlackBoxHelpDialog::OnCMakeListsEnter(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      modelCDMIProjectTreeNode* node = this->blackBox;
      while (node != NULL && dynamic_cast<modelCDMPackage*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

          if(((modelCDMPackage*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMPackage*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->blackBox;
      while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

          if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMBlackBoxHelpDialog::OnCMakeListsExit(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
      {
        modelCDMIProjectTreeNode* node = this->blackBox;
        while (node != NULL && dynamic_cast<modelCDMPackage*>(node) == NULL)
          {
            node = node->GetParent();
          }
        if (node != NULL)
          {
            wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

            if(((modelCDMPackage*)node)->GetCMakeLists() != NULL)
              {
                newEvent->SetClientData(((modelCDMPackage*)node)->GetCMakeLists());
                newEvent->SetId(0);
                wxPostEvent(this->GetParent(), *newEvent);
              }
          }
      }
    else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
      {
        modelCDMIProjectTreeNode* node = this->blackBox;
        while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
          {
            node = node->GetParent();
          }
        if (node != NULL)
          {
            wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

            if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
              {
                newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
                newEvent->SetId(0);
                wxPostEvent(this->GetParent(), *newEvent);
              }
          }
      }
    event.Skip();
}

void wxCDMBlackBoxHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
