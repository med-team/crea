/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMMainHelpDialog.cpp
 *
 *  Created on: 14/2/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMSettingsDialog.h"

#include "creaDevManagerIds.h"
#include "CDMUtilities.h"

#include <wx/config.h>

BEGIN_EVENT_TABLE(wxCDMSettingsDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_NEXT, wxCDMSettingsDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_PREV, wxCDMSettingsDialog::OnDefaults)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMSettingsDialog::OnCancel)
END_EVENT_TABLE()

wxCDMSettingsDialog::wxCDMSettingsDialog(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMSettingsDialog::Create(parent, id, caption, position, size, style);
}

wxCDMSettingsDialog::~wxCDMSettingsDialog()
{
}

bool wxCDMSettingsDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

bool wxCDMSettingsDialog::IsHelpEnabled()
{
  return this->helpEnabled->GetValue();
}

void wxCDMSettingsDialog::SetHelpEnabled(bool isHelp)
{
  this->helpEnabled->SetValue(isHelp);
}

void wxCDMSettingsDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Crea Development Manager Settings"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(this, wxID_ANY, wxT("Change the values to modify the default behavior of the program."), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(instruction, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL | wxEXPAND, 5);

  wxFlexGridSizer* formItems = new wxFlexGridSizer(5,2,9,15);

  wxStaticText *stxtTextEditor = new wxStaticText(this, -1, wxT("Text Editor Command"));
  wxStaticText *stxtFileExplorer = new wxStaticText(this, -1, wxT("File Explorer Command"));
  wxStaticText *stxtTerminal = new wxStaticText(this, -1, wxT("Terminal Command"));
  wxStaticText *stxtHelpEnabled = new wxStaticText(this, -1, wxT("Help Enabled"));
#ifndef _WIN32
  wxStaticText* stxtBuildCommand = new wxStaticText(this, -1, wxT("Build Command"));
#endif
  wxConfigBase* pConfig = wxConfigBase::Get();

  this->textEditor = new wxTextCtrl(this, -1, pConfig->Read(wxT("TEXT_EDITOR"), crea::std2wx(CDMUtilities::TEXT_EDITOR)));
  this->fileExplorer = new wxTextCtrl(this, -1, pConfig->Read(wxT("FILE_EXPLORER"), crea::std2wx(CDMUtilities::FILE_EXPLORER)));
  this->terminal = new wxTextCtrl(this, -1, pConfig->Read(wxT("TERMINAL"), crea::std2wx(CDMUtilities::TERMINAL)));
  this->helpEnabled = new wxCheckBox(this, -1, wxT(""));
  this->helpEnabled->SetValue(pConfig->Read(wxT("HELP"), true) != 0);
#ifndef _WIN32
  this->buildCommand = new wxTextCtrl(this, -1, pConfig->Read(wxT("BUILD_COMMAND"), crea::std2wx(CDMUtilities::BUILD_COMMAND)));
#endif

  formItems->Add(stxtTextEditor, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->textEditor, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtFileExplorer, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->fileExplorer, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtTerminal, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->terminal, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
#ifndef _WIN32
  formItems->Add(stxtBuildCommand, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->buildCommand, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
#endif
  formItems->Add(stxtHelpEnabled, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->helpEnabled, 1, wxEXPAND);

  formItems->AddGrowableCol(1,1);

  v_sizer1->Add(formItems, 1, wxEXPAND | wxALL, 15);

  wxBoxSizer* h_sizer2 = new wxBoxSizer(wxHORIZONTAL);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_PREV, wxT("Restore Defaults")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_NEXT, wxT("OK")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Cancel")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);


  v_sizer1->Add(h_sizer2, 0, wxALIGN_CENTER | wxALL | wxEXPAND, 10);

  SetSizer(v_sizer1);
}

void wxCDMSettingsDialog::OnFinish(wxCommandEvent& event)
{
  wxConfigBase* pConfig = wxConfigBase::Get();

  pConfig->Write(wxT("TEXT_EDITOR"), this->textEditor->GetValue());
  pConfig->Write(wxT("TERMINAL"), this->terminal->GetValue());
  pConfig->Write(wxT("FILE_EXPLORER"), this->fileExplorer->GetValue());
  pConfig->Write(wxT("HELP"), this->helpEnabled->GetValue());
#ifndef _WIN32
  pConfig->Write(wxT("BUILD_COMMAND"), this->buildCommand->GetValue());
#endif

  pConfig->Flush();

  this->EndModal(wxID_OK);
}

void wxCDMSettingsDialog::OnCancel(wxCommandEvent& event)
{
  this->EndModal(wxID_CANCEL);
}

void wxCDMSettingsDialog::OnDefaults(wxCommandEvent& event)
{
  this->textEditor->SetValue(crea::std2wx(CDMUtilities::TEXT_EDITOR));
  this->terminal->SetValue(crea::std2wx(CDMUtilities::TERMINAL));
  this->fileExplorer->SetValue(crea::std2wx(CDMUtilities::FILE_EXPLORER));
#ifndef _WIN32
  this->buildCommand->SetValue(crea::std2wx(CDMUtilities::BUILD_COMMAND));
#endif
  this->helpEnabled->SetValue(true);
}
