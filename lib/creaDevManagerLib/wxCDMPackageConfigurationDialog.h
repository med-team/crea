/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMPackageConfigurationDialog.h
 *
 *  Created on: 6/4/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPACKAGECONFIGURATIONDIALOG_H_
#define WXCDMPACKAGECONFIGURATIONDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>

#include "modelCDMPackage.h"

/**
 * Package Configuration Dialog
 */
class wxCDMPackageConfigurationDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Package Configuration Dialog Constructor.
   * @param parent Parent window.
   * @param package Package Description reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Package Library Configuration".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE.
   */
  wxCDMPackageConfigurationDialog(
      wxWindow* parent,
      modelCDMPackage * package,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Package Library Configuration"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,370),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMPackageConfigurationDialog();
  /**
   * Package Configuration Dialog Creator.
   * @param parent Parent window.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "Package Library Configuration".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 350, 370.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE.
   * @return if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Package Library Configuration"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(350,370),
      long style = wxDEFAULT_DIALOG_STYLE
  );

protected:
  /**
   * Creates the help controls (text and buttons).
   */
  void CreateControls();

//attributes
private:
  /**
   * Main panel reference.
   */
  modelCDMPackage* package;

//handlers
protected:
  /**
   * Handler to close configuration dialog.
   * @param event Unused.
   */
  void OnFinish(wxCommandEvent& event);

  /**
   * Handler when a third party library include is pressed.
   * @param event checkbox event.
   */
  void On3rdLibraryIncludeChange(wxCommandEvent& event);

  /**
   * Handler when a custom library include is pressed.
   * @param event checkbox event.
   */
  void OnCustomLibraryIncludeChange(wxCommandEvent& event);
};

#endif /* WXCDMPACKAGECONFIGURATIONDIALOG_H_ */
