/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMNewBlackBoxDialog.cpp
 *
 *  Created on: 26/12/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMNewBlackBoxDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMNewBlackBoxDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_NEXT, wxCDMNewBlackBoxDialog::OnCreateBlackBox)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMNewBlackBoxDialog::OnCancel)
END_EVENT_TABLE()

wxCDMNewBlackBoxDialog::wxCDMNewBlackBoxDialog(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMNewBlackBoxDialog::Create(parent, id, caption, position, size, style);
}

wxCDMNewBlackBoxDialog::~wxCDMNewBlackBoxDialog()
{
}

bool wxCDMNewBlackBoxDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxName() const
{
  return this->blackBoxName->GetValue();
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxAuthor() const
{
  return this->blackBoxAuthor->GetValue();
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxAuthorEmail() const
{
  return this->blackBoxAuthorEmail->GetValue();
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxDescription() const
{
  return this->blackBoxDescription->GetValue();
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxCategories() const
{
  return this->blackBoxCategories->GetValue();
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxType() const
{
  wxString res;
  switch(this->blackBoxType->GetCurrentSelection())
  {
  case 0:
    res = wxT("std");
    break;
  case 1:
    res = wxT("widget");
    break;
  case 2:
#ifdef _WIN32
	res = wxT("VTK_ImageAlgorithm");
#else
    res = wxT("VTK-ImageAlgorithm");
#endif
    break;
  case 3:
#ifdef _WIN32
	res = wxT("VTK_PolyDataAlgorithm");
#else
    res = wxT("VTK-PolyAlgorithm");
#endif
    break;
  default:
    res = wxT("std");
    break;
  }
  return res;
}

const wxString wxCDMNewBlackBoxDialog::GetBlackBoxFormat() const
{
  wxString res;
  switch(this->blackBoxFormat->GetCurrentSelection())
  {
  case 0:
    res = wxT("C++");
    break;
  case 1:
    res = wxT("XML");
    break;
  default:
    res = wxT("C++");
    break;
  }
  return res;
}

void wxCDMNewBlackBoxDialog::CreateControls()
{
  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Create a new black box"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(this, wxID_ANY, wxT("Please fill the following details."), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(instruction, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxFlexGridSizer* formItems = new wxFlexGridSizer(4,2,9,15);

  wxStaticText *stxtPrjName = new wxStaticText(this, -1, wxT("Black Box Name"));
  wxStaticText *stxtPrjAuth = new wxStaticText(this, -1, wxT("Black Box Authors (separated by ',')"));
  wxStaticText *stxtPrjAuthEmail = new wxStaticText(this, -1, wxT("Black Box Authors' Email"));
  wxStaticText *stxtPrjDsc = new wxStaticText(this, -1, wxT("Black Box Description"));
  wxStaticText *stxtPrjCat = new wxStaticText(this, -1, wxT("Black Box Categories (separated by ',')"));
  wxStaticText *stxtPrjTyp = new wxStaticText(this, -1, wxT("Black Box Type"));
  wxStaticText *stxtPrjFmt = new wxStaticText(this, -1, wxT("Black Box Format"));

  this->blackBoxName = new wxTextCtrl(this, -1);
  this->blackBoxAuthor = new wxTextCtrl(this, -1);
  this->blackBoxAuthorEmail = new wxTextCtrl(this, -1);
  this->blackBoxDescription = new wxTextCtrl(this, -1, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
  this->blackBoxCategories = new wxTextCtrl(this, -1);
  wxString BBTypes[] =
      {
          wxT("Basic - AtomicBlackBox"),
		  wxT("Widget - WxBlackBox"),
          wxT("VTK ImageAlgorithm - Basic and vtkImageAlgorithm (standard vtk I/O)"),
          wxT("VTK PolyDataAlgorithm - Basic and vtkPolyDataAlgorithm (standard vtk I/O)")
      };
  this->blackBoxType = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 4, BBTypes);

  wxString BBFormats[] =
      {
          wxT("C++"),
          wxT("XML")
      };
  this->blackBoxFormat = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 2, BBFormats);

  formItems->Add(stxtPrjName, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxName, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjAuth, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxAuthor, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjAuthEmail, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxAuthorEmail, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjDsc, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxDescription, 1, wxEXPAND);
  formItems->Add(stxtPrjCat,0 , wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxCategories, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjTyp, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxType, 0, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjFmt, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->blackBoxFormat, 0, wxEXPAND | wxALIGN_CENTER_VERTICAL);

  formItems->AddGrowableCol(1,1);
  formItems->AddGrowableRow(3,1);

  v_sizer1->Add(formItems, 1, wxEXPAND | wxALL, 15);

  wxBoxSizer* h_sizer2 = new wxBoxSizer(wxHORIZONTAL);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_NEXT, wxT("Create Black Box")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Cancel")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

  v_sizer1->Add(h_sizer2, 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
}

void wxCDMNewBlackBoxDialog::OnCreateBlackBox(wxCommandEvent& event)
{
  bool ready = true;

  if(ready && this->blackBoxName->GetValue() == wxT(""))
    {
      wxMessageBox(wxT("The black box name cannot be empty"),_T("Error"),wxOK | wxICON_ERROR);
      ready = false;
    }
  if(ready && this->blackBoxAuthor->GetValue() == wxT(""))
    {
      wxMessageBox(wxT("The black box author has to be specified"),_T("Error"),wxOK | wxICON_ERROR);
      ready = false;
    }

  if(ready)
    {
      this->EndModal(wxID_FORWARD);
    }
  event.Skip();
}

void wxCDMNewBlackBoxDialog::OnCancel(wxCommandEvent& event)
{
  this->EndModal(wxID_CANCEL);
  event.Skip();
}
