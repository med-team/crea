/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMPackageDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMPackageDescriptionPanel.h"

#include "wxCDMMainFrame.h"

#include "creaDevManagerIds.h"
#include "images/PkIcon64.xpm"

#include "wxCDMNewBlackBoxDialog.h"
#include "wxCDMPackageConfigurationDialog.h"
#include <wx/textdlg.h>
#include "CDMUtilities.h"

#include <vector>
#include "modelCDMCMakeListsFile.h"
#include "wxCDMPackageHelpDialog.h"


BEGIN_EVENT_TABLE(wxCDMPackageDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMPackageDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_SET_AUTHOR, wxCDMPackageDescriptionPanel::OnBtnSetAuthor)
EVT_BUTTON(ID_BUTTON_SET_VERSION, wxCDMPackageDescriptionPanel::OnBtnSetVersion)
EVT_BUTTON(ID_BUTTON_SET_DESCRIPTION, wxCDMPackageDescriptionPanel::OnBtnSetDescription)
EVT_HYPERLINK(ID_LINK_SELECT_BLACKBOX, wxCDMPackageDescriptionPanel::OnLnkBlackBoxSelect)
EVT_BUTTON(ID_BUTTON_CREATE_BLACKBOX, wxCDMPackageDescriptionPanel::OnBtnCreateBlackBox)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMPackageDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMPackageDescriptionPanel::OnBtnOpenFolder)
EVT_BUTTON(ID_BUTTON_CHOOSE, wxCDMPackageDescriptionPanel::OnBtnConfigurePackage)
END_EVENT_TABLE()

wxCDMPackageDescriptionPanel::wxCDMPackageDescriptionPanel(
    wxWindow* parent,
    modelCDMPackage* package,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMPackageDescriptionPanel::Create(parent, package, id, caption, pos, size, style);
}

wxCDMPackageDescriptionPanel::~wxCDMPackageDescriptionPanel()
{
}

bool wxCDMPackageDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMPackage* package,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  this->package = package;
  wxPanel::Create(parent, id, pos, size, style);
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMPackageDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->package->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
	  returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->package->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(PkIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Package")),0, wxALIGN_LEFT, 0);
    //Package Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->package->GetNamePackage())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Package Properties
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Properties"));
  wxPanel* propertiesPanel = new wxPanel(this);
  wxBoxSizer* propertiesPanelSizer = new wxBoxSizer(wxVERTICAL);
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(4, 2, 9, 15);

  wxStaticText *pAuthor = new wxStaticText(propertiesPanel, -1, wxT("Author"));
  wxStaticText *pVersion = new wxStaticText(propertiesPanel, -1, wxT("Version"));
  wxStaticText *pDescription = new wxStaticText(propertiesPanel, -1, wxT("Description"));

  // author
  wxBoxSizer* pAuthorsz = new wxBoxSizer(wxHORIZONTAL);
  this->authortc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->package->GetAuthors()));
  wxButton* pAuthorbt = new wxButton(propertiesPanel, ID_BUTTON_SET_AUTHOR, wxT("Change"));
  pAuthorbt->SetToolTip(wxT("Update the author/s of the package."));
  pAuthorsz->Add(this->authortc, 1, wxALIGN_CENTER_VERTICAL);
  pAuthorsz->Add(pAuthorbt, 0, wxALIGN_CENTER | wxLEFT, 10);

  // version
  wxBoxSizer* pVersionsz = new wxBoxSizer(wxHORIZONTAL);
  this->versiontc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->package->GetVersion()));
  wxButton* pVersionbt = new wxButton(propertiesPanel, ID_BUTTON_SET_VERSION, wxT("Set"));
  pVersionbt->SetToolTip(wxT("Update the version of the package."));
  pVersionsz->Add(this->versiontc, 1, wxALIGN_CENTER_VERTICAL);
  pVersionsz->Add(pVersionbt, 0, wxALIGN_CENTER | wxLEFT, 10);

  // description
  wxBoxSizer* pDescriptionsz = new wxBoxSizer(wxHORIZONTAL);
  this->descriptiontc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->package->GetDescription()));
  wxButton* pDescriptionbt = new wxButton(propertiesPanel, ID_BUTTON_SET_DESCRIPTION, wxT("Change"));
  pDescriptionbt->SetToolTip(wxT("Update the description of the project."));
  pDescriptionsz->Add(this->descriptiontc, 1, wxALIGN_CENTER_VERTICAL);
  pDescriptionsz->Add(pDescriptionbt, 0, wxALIGN_CENTER | wxLEFT, 10);

  propertiesGridSizer->Add(pAuthor, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pAuthorsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pVersion, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pVersionsz, 1, wxEXPAND);
  propertiesGridSizer->Add(pDescription, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pDescriptionsz, 1, wxEXPAND);

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanelSizer -> Add(propertiesGridSizer, 1, wxEXPAND);
  propertiesPanel->SetSizer(propertiesPanelSizer);
  propertiesPanelSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 0, wxEXPAND | wxALL, 5);

  sizer->Add(propertiesBox, 0, wxEXPAND | wxALL, 10);

  //Black Boxes
  wxStaticBoxSizer* BBBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("A&vailable Black Boxes"));
  BBBox->GetStaticBox()->SetToolTip(wxT("Select any of the available black boxes to see its details or modify them."));
  wxPanel* BBPanel = new wxPanel(this);
  wxBoxSizer* BBPanelSizer = new wxBoxSizer(wxVERTICAL);


  std::vector<modelCDMBlackBox*> blackBoxes = this->package->GetSrc()->GetBlackBoxes();
  for (int i = 0; i < (int)(blackBoxes.size()); i++)
    {

      if(blackBoxes[i] != NULL)
        {

			wxHyperlinkCtrl* pBBlk = new wxHyperlinkCtrl(BBPanel,ID_LINK_SELECT_BLACKBOX, crea::std2wx(blackBoxes[i]->GetName().c_str()), crea::std2wx(blackBoxes[i]->GetName().c_str()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
          pBBlk->SetWindowStyle(wxALIGN_LEFT | wxNO_BORDER);
          std::string tt = "Author: " + blackBoxes[i]->GetAuthors() + "\nDescription: " + blackBoxes[i]->GetDescription() + "\nCategories: " + blackBoxes[i]->GetCategories();
          pBBlk->SetToolTip(crea::std2wx(tt));
          pBBlk->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageDescriptionPanel::OnMouseEnter,NULL,this);
          pBBlk->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageDescriptionPanel::OnMouseExit,NULL,this);
          BBPanelSizer -> Add(pBBlk, 0, wxEXPAND | wxALL, 5);
        }
    }

  BBPanel->SetSizer(BBPanelSizer);
  BBPanelSizer->Fit(BBPanel);
  BBBox->Add(BBPanel, 1, wxEXPAND | wxALL, 5);
  sizer -> Add(BBBox, 0, wxEXPAND | wxALL, 10);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* createBBbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_BLACKBOX, _T("A. Create Black Box"));
  createBBbt->SetToolTip(wxT("Create a new black box for the active project inside this package."));
  actionsGridSizer->Add(createBBbt, 1, wxALL | wxEXPAND, 5);
  wxButton* editConfbt = new wxButton(actionsPanel, ID_BUTTON_CHOOSE, _T("B. Configure Package"));
  editConfbt->SetToolTip(wxT("Select the libraries your package is going to use. This procedure changes the package's CMakeLists file."));
  actionsGridSizer->Add(editConfbt, 1, wxALL | wxEXPAND, 5);
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Open the system default text editor to edit the package's CMakeLists.txt file."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageDescriptionPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageDescriptionPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Package Folder"));
  openFolderbt->SetToolTip(wxT("Open the package folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMPackageHelpDialog* helpDialog = new wxCDMPackageHelpDialog(this->GetParent(), this->package, wxID_ANY);
      helpDialog->Show(true);
    }
}

void wxCDMPackageDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->package->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMPackageDescriptionPanel::OnBtnSetAuthor(wxCommandEvent& event)
{
  //get author from user
  wxTextEntryDialog* authDlg = new wxTextEntryDialog(
      this,
      wxT("Enter the new authors name. Separate each author with a '/'."),
      wxT("Change Package Author - creaDevManager"),
      crea::std2wx(this->package->GetAuthors()),
      wxTE_MULTILINE | wxOK | wxCANCEL
  );

  if (authDlg->ShowModal() == wxID_OK)
    {
      std::string authorsStr = crea::wx2std(authDlg->GetValue());
      //check name
      if(authorsStr.size() > 0)
        {
          std::string* result;
          if(!this->package->SetAuthors(authorsStr, result))
            wxMessageBox(crea::std2wx(*result),_T("Change Package Author - Error!"),wxOK | wxICON_ERROR);
        }
      this->authortc->SetLabel(crea::std2wx(this->package->GetAuthors()));
      this->authortc->GetParent()->GetSizer()->RecalcSizes();
    }

}

void wxCDMPackageDescriptionPanel::OnBtnSetVersion(wxCommandEvent& event)
{
  //get version
  wxString versionWx = wxGetTextFromUser(
      wxT("Enter the new version name"),
      wxT("Change Package Version - creaDevManager"),
      crea::std2wx(this->package->GetVersion())
  );
  //check name
  std::vector<std::string> parts;
  CDMUtilities::splitter::split(parts, crea::wx2std(versionWx), " .", CDMUtilities::splitter::no_empties);
  if(parts.size() == 3)
    {
      std::string* result;
      if(!this->package->SetVersion(crea::wx2std(versionWx), result))
        wxMessageBox(crea::std2wx(*result),_T("Change Package Version - Error!"),wxOK | wxICON_ERROR);
    }
  else
    {
      wxMessageBox(crea::std2wx("The version format is incorrect, please follow the following format:\nX.Y.Z\nX: Major Version\nY: Minor Version\nZ: Build Version"),_T("Set Project Version - Error!"),wxOK | wxICON_ERROR);
    }
  this->versiontc->SetLabel(crea::std2wx(this->package->GetVersion()));
}

void wxCDMPackageDescriptionPanel::OnBtnSetDescription(wxCommandEvent& event)
{
  //get description from user
  wxTextEntryDialog* descDlg = new wxTextEntryDialog(
      this,
      wxT("Edit the package description."),
      wxT("Change Package Description - creaDevManager"),
      crea::std2wx(this->package->GetDescription()),
      wxTE_MULTILINE | wxOK | wxCANCEL
  );

  if (descDlg->ShowModal() == wxID_OK)
    {
      std::string descriptionStr = crea::wx2std(descDlg->GetValue());
      //check desc
      if(descriptionStr.size() > 0)
        {
          std::string* result;
          if(!this->package->SetDescription(descriptionStr, result))
            wxMessageBox(crea::std2wx(*result),_T("Change Package Description - Error!"),wxOK | wxICON_ERROR);
        }
      this->descriptiontc->SetLabel(crea::std2wx(this->package->GetDescription()));
      this->descriptiontc->GetParent()->GetSizer()->RecalcSizes();
    }
}

void wxCDMPackageDescriptionPanel::OnLnkBlackBoxSelect(wxHyperlinkEvent& event)
{
  modelCDMBlackBox* bb;
  std::vector<modelCDMBlackBox*> bbs = this->package->GetSrc()->GetBlackBoxes();
  for (int i = 0; i < (int)(bbs.size()); i++)
    {
      if(bbs[i]->GetName() == crea::wx2std(event.GetURL()))
        {
          bb = bbs[i];
          break;
        }
    }

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetClientData(bb);
  newEvent->SetId(1);
  newEvent->SetString(wxT("blackbox"));
  wxPostEvent(this->GetParent(), *newEvent);

  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  newEvent1->SetClientData(bb->GetHeaderFile());
  newEvent1->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent1);

}

void wxCDMPackageDescriptionPanel::OnBtnCreateBlackBox(wxCommandEvent& event)
{

  wxCDMNewBlackBoxDialog* dialog = new wxCDMNewBlackBoxDialog(this);
  long userResponse;
  userResponse = dialog->ShowModal();

  if(userResponse == wxID_FORWARD)
    {
      std::string* result;
      //create black box
      modelCDMIProjectTreeNode* blackBox = this->package->CreateBlackBox(
          result,
          crea::wx2std(dialog->GetBlackBoxName()),
          crea::wx2std(dialog->GetBlackBoxType()),
          crea::wx2std(dialog->GetBlackBoxFormat()),
          crea::wx2std(dialog->GetBlackBoxCategories()),
          crea::wx2std(dialog->GetBlackBoxAuthor()),
          crea::wx2std(dialog->GetBlackBoxAuthorEmail()),
          crea::wx2std(dialog->GetBlackBoxDescription())
      );
      //check black box created
      if(blackBox == NULL)
        {
          wxMessageBox(crea::std2wx(*result),_T("New Black Box - Error!"),wxOK | wxICON_ERROR);
          return;
        }
      wxMessageBox(crea::std2wx("Black box successfully created."),_T("New Black Box - Success!"),wxOK | wxICON_INFORMATION);

      //refreshing tree and description
      //send event instead of calling parent to avoid crashing

      ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
      newEvent->SetId(1);
      newEvent->SetClientData(blackBox);
      newEvent->SetString(wxT("blackbox"));
      newEvent->SetClientData(blackBox);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void
wxCDMPackageDescriptionPanel::OnBtnConfigurePackage(wxCommandEvent& event)
{
  wxCDMPackageConfigurationDialog* dialog = new wxCDMPackageConfigurationDialog(this,this->package);
  long userResponse;
  userResponse = dialog->ShowModal();
}

void wxCDMPackageDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->package->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->package->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->package->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }

  event.Skip();
}

void wxCDMPackageDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->package->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMPackageDescriptionPanel::OnMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);
  std::string BBName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());

  modelCDMFile* bbHeader = NULL;
  std::vector<modelCDMBlackBox*> boxes = this->package->GetSrc()->GetBlackBoxes();
  for (int i = 0; i < (int)(boxes.size()); i++)
    {
      if(boxes[i]->GetName() == BBName)
        {
          bbHeader = boxes[i]->GetHeaderFile();
          break;
        }
    }
  newEvent->SetClientData(bbHeader);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMPackageDescriptionPanel::OnMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  std::string BBName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMFile* bbHeader = NULL;
  std::vector<modelCDMBlackBox*> boxes = this->package->GetSrc()->GetBlackBoxes();
  for (int i = 0; i < (int)(boxes.size()); i++)
    {
      if(boxes[i]->GetName() == BBName)
        {
          bbHeader = boxes[i]->GetHeaderFile();
          break;
        }
    }
  newEvent->SetClientData(bbHeader);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMPackageDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->package->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->package->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMPackageDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->package->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->package->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}


