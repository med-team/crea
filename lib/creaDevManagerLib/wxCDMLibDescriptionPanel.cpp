/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMLibDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMLibDescriptionPanel.h"

#include "wxCDMMainFrame.h"

#include "wxCDMLibHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/LbIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMLibDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMLibDescriptionPanel::OnBtnReturn)
EVT_HYPERLINK(ID_LINK_SELECT_LIBRARY, wxCDMLibDescriptionPanel::OnLnkLibrarySelect)
EVT_BUTTON(ID_BUTTON_CREATE_LIBRARY, wxCDMLibDescriptionPanel::OnBtnCreateLibrary)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMLibDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMLibDescriptionPanel::OnBtnOpenFolder)
EVT_CHECKBOX(ID_CHECK_INCLUDE_LIBRARY, wxCDMLibDescriptionPanel::OnChBLibraryChange)
END_EVENT_TABLE()

wxCDMLibDescriptionPanel::wxCDMLibDescriptionPanel(
    wxWindow* parent,
    modelCDMLib* lib,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMLibDescriptionPanel::Create(parent, lib, id, caption, pos, size, style);
}

wxCDMLibDescriptionPanel::~wxCDMLibDescriptionPanel()
{
}

bool wxCDMLibDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMLib* lib,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->lib = lib;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMLibDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->lib->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
	  returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->lib->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(LbIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Library Management")),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Libraries
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("A&vailable Libraries"));
  propertiesBox->GetStaticBox()->SetToolTip(wxT("Select any of the available libraries to see its details or modify them."));
  wxPanel* propertiesPanel = new wxPanel(this);

  std::vector<modelCDMLibrary*> libraries = this->lib->GetLibraries();
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(libraries.size()+1, 3, 9, 5);

  wxStaticText* ChBTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Include in\nCMake"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* LkTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Library Name"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* HlpTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Help"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

  propertiesGridSizer -> Add(ChBTitle, 0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(LkTitle,  0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(HlpTitle, 0, wxEXPAND | wxALL, 5);

  for (int i = 0; i < (int)(libraries.size()); i++)
    {
      //checkbox for cmake inclusion
      wxCheckBox* pLibraryChB = new wxCheckBox(propertiesPanel, ID_CHECK_INCLUDE_LIBRARY, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
      pLibraryChB->SetName(crea::std2wx(libraries[i]->GetName()));
      std::string tt = "if this box is checked the the " + libraries[i]->GetName() + " library is included in the project compilation.";
      pLibraryChB->SetToolTip(crea::std2wx(tt));
      pLibraryChB->SetValue(this->lib->IsLibraryIncluded(libraries[i]->GetName()));
      propertiesGridSizer -> Add(pLibraryChB, 0, wxEXPAND | wxALIGN_CENTER);

      //link to library with description
      wxHyperlinkCtrl* pLibrarylk = new wxHyperlinkCtrl(propertiesPanel, ID_LINK_SELECT_LIBRARY, crea::std2wx(libraries[i]->GetName().c_str()), crea::std2wx(libraries[i]->GetName().c_str()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      pLibrarylk->SetWindowStyle(wxALIGN_LEFT | wxNO_BORDER);
      tt = "Name: " + libraries[i]->GetName() + "\n";
      tt += "Location: " + libraries[i]->GetPath();
      pLibrarylk->SetToolTip(crea::std2wx(tt));
      pLibrarylk->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibDescriptionPanel::OnMouseEnter,NULL,this);
      pLibrarylk->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibDescriptionPanel::OnMouseExit,NULL,this);
      propertiesGridSizer -> Add(pLibrarylk, 0, wxEXPAND);

      //help icon
      wxButton* pLibraryHlp = new wxButton(propertiesPanel, wxID_ANY, wxT("?"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
      pLibraryHlp->Enable(false);
      tt = "When this library is included in the CMakeLists file, the\nfollowing line is included in the CMakeList.txt file in the lib\nfolder:\n"
          "ADD_SUBDIRECTORY(" + libraries[i]->GetName() + ")";
      pLibraryHlp->SetToolTip(crea::std2wx(tt));

      propertiesGridSizer -> Add(pLibraryHlp, 0, wxEXPAND | wxALIGN_CENTER);
    }

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanel->SetSizer(propertiesGridSizer);
  propertiesGridSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 1, wxALL | wxEXPAND, 5);
  sizer -> Add(propertiesBox, 0, wxEXPAND | wxALL, 10);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Actions"));
  actionsBox->GetStaticBox()->SetToolTip(wxT("Create a new library or make any change to the lib's CMakeLists.txt file by selecting the desired action."));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);

  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* createLibrarybt = new wxButton(actionsPanel, ID_BUTTON_CREATE_LIBRARY, _T("A. Create Library"));
  createLibrarybt->SetToolTip(wxT("Create a new library for this project."));
  actionsGridSizer->Add(createLibrarybt, 1, wxALL | wxEXPAND, 5);
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Open the system default text editor to edit the Lib's CMakeLists.txt file."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibDescriptionPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibDescriptionPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Libraries Folder"));
  openFolderbt->SetToolTip(wxT("Open the lib folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);

  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 0, wxALL, 5);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMLibHelpDialog* helpDialog = new wxCDMLibHelpDialog(this->GetParent(), this->lib, wxID_ANY);
      helpDialog->Show(true);
    }
}

void wxCDMLibDescriptionPanel::OnBtnCreateLibrary(wxCommandEvent& event)
{
  //get name
  wxString libraryName = wxGetTextFromUser(
      _T("Enter the new library name"),
      _T("New Library - creaDevManager"),
      _T("")
  );
  //check name
  if(libraryName.Len() > 0)
    {
      std::string* result;
      //create library
      modelCDMIProjectTreeNode* library = this->lib->CreateLibrary(crea::wx2std(libraryName),result);
      //check library created
      if(library == NULL)
        {
          wxMessageBox(crea::std2wx(*result),_T("New Library - Error!"),wxOK | wxICON_ERROR);
          return;
        }
      wxMessageBox(crea::std2wx("Library successfully created."),_T("New Library - Success!"),wxOK | wxICON_INFORMATION);

      //refreshing tree and description
      //send event instead of calling parent to avoid crashing

      ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
      newEvent->SetClientData(library);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMLibDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->lib->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMLibDescriptionPanel::OnChBLibraryChange(wxCommandEvent& event)
{
  this->lib->SetLibraryInclude(
      crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()),
      ((wxCheckBox*)event.GetEventObject())->GetValue()
    );
}

void wxCDMLibDescriptionPanel::OnLnkLibrarySelect(wxHyperlinkEvent& event)
{
  modelCDMLibrary* theLibrary = NULL;
  std::vector<modelCDMLibrary*> libraries = this->lib->GetLibraries();
  for (int i = 0; i < (int)(libraries.size()); i++)
    {
      if(libraries[i]->GetName() == crea::wx2std(event.GetURL()))
        {
          theLibrary = libraries[i];
          break;
        }
    }

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetClientData(theLibrary);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);

  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  newEvent1->SetClientData(theLibrary);
  newEvent1->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent1);

}

void wxCDMLibDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->lib->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMLibDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->lib->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMLibDescriptionPanel::OnMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);
  std::string LibName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMLibrary* theLibrary = NULL;
  std::vector<modelCDMLibrary*> libraries = this->lib->GetLibraries();
  for (int i = 0; i < (int)(libraries.size()); i++)
    {
      if(libraries[i]->GetName() == LibName)
        {
          theLibrary = libraries[i];
          break;
        }
    }
  newEvent->SetClientData(theLibrary);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMLibDescriptionPanel::OnMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  std::string LibName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMLibrary* theLibrary = NULL;
  std::vector<modelCDMLibrary*> libraries = this->lib->GetLibraries();
  for (int i = 0; i < (int)(libraries.size()); i++)
    {
      if(libraries[i]->GetName() == LibName)
        {
          theLibrary = libraries[i];
          break;
        }
    }
  newEvent->SetClientData(theLibrary);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMLibDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMLibDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
