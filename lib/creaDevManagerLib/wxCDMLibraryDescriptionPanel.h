/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMLibraryDescriptionPanel.h
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMLIBRARYDESCRIPTIONPANEL_H_
#define WXCDMLIBRARYDESCRIPTIONPANEL_H_

#include <creaWx.h>
#include <wx/hyperlink.h>

#include "modelCDMLibrary.h"

/**
 * Library description panel. Shows the available actions on the described library.
 */
class wxCDMLibraryDescriptionPanel : public wxScrolledWindow
{
  DECLARE_EVENT_TABLE()
public:

  /**
   * Library description panel Constructor.
   * @param parent Parent window reference.
   * @param library Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   */
  wxCDMLibraryDescriptionPanel(
      wxWindow* parent,
      modelCDMLibrary* library,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Destructor.
   */
  ~wxCDMLibraryDescriptionPanel();

  /**
   * Library description panel Creator.
   * @param parent Parent window reference.
   * @param library Project class reference.
   * @param id Panel ID. By default -1.
   * @param caption Panel label. By default "Description Frame".
   * @param pos Panel position. By default wxDefaultPosition.
   * @param size Panel size. By default wxDefaultSize.
   * @param style Panel style. By default wxDEFAULT_FRAME_STYLE.
   * @return True if creation was successful.
   */
  bool Create(
      wxWindow* parent,
      modelCDMLibrary* library,
      wxWindowID id = -1,
      const wxString& caption = _("Description Frame"),
      const wxPoint& pos = wxDefaultPosition,
      const wxSize& size = wxDefaultSize,
      long style = wxDEFAULT_FRAME_STYLE
  );

  /**
   * Creates all the controls in the panel (property and action controls).
   */
  void CreateControls();

private:
  /**
   * Library described.
   */
  modelCDMLibrary* library;
  /**
   * Control with the described library name.
   */
  wxStaticText* libraryNametc;

  //handlers
protected:
  /**
   * Handles when a return link is pressed.
   * @param event Has the link reference to know where to return
   */
  void OnBtnReturn(wxHyperlinkEvent& event);
  /**
   * Handles when the set executable name button is pressed.
   * @param event Unused.
   */
  void OnBtnSetExeName(wxCommandEvent& event);
  /**
   * Handles when a 3rd Party Library checkbox state is changed. It calls to include/exclude the selected library.
   * @param event CheckBox event.
   */
  void On3rdLibraryChBChange(wxCommandEvent& event);
  /**
   * Handles when a Custom Library checkbox state is changed. It calls to include/exclude the selected library.
   * @param event CheckBox event.
   */
  void OnLibraryChBChange(wxCommandEvent& event);
  /**
   * Handles when the create class button is pressed.
   * @param event Unused.
   */
  void OnBtnCreateClass(wxCommandEvent& event);
  /**
   * Handles when the create folder buttonis pressed.
   * @param event Unused.
   */
  void OnBtnCreateFolder(wxCommandEvent& event);
  /**
   * Handles when Edit Cmakelists button is pressed.
   * @param event Unused.
   */
  void OnBtnEditCMakeLists(wxCommandEvent& event);
  /**
   * Handles when the open containing folder button is pressed.
   * @param event Unused.
   */
  void OnBtnOpenFolder(wxCommandEvent& event);
  /**
   * Handles when the edit cmakelists button is hovered.
   * @param event Unused.
   */
  void OnCMakeMouseEnter(wxMouseEvent& event);
  /**
   * Handles when the edit cmakelists button exits hover.
   * @param event Unused.
   */
  void OnCMakeMouseExit(wxMouseEvent& event);

};

#endif /* WXCDMLIBRARYDESCRIPTIONPANEL_H_ */
