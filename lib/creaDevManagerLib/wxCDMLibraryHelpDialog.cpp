/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMLibraryHelpDialog.cpp
 *
 *  Created on: 11/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMLibraryHelpDialog.h"

#include "creaDevManagerIds.h"

#include "modelCDMLib.h"

BEGIN_EVENT_TABLE(wxCDMLibraryHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMLibraryHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMLibraryHelpDialog::OnCMakeLists)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMLibraryHelpDialog::OnCMakeLists)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMLibraryHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMLibraryHelpDialog::wxCDMLibraryHelpDialog(
    wxWindow* parent,
    modelCDMLibrary* library,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMLibraryHelpDialog::Create(parent, id, caption, position, size, style);
  this->library = library;
}

wxCDMLibraryHelpDialog::~wxCDMLibraryHelpDialog()
{
}

bool wxCDMLibraryHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMLibraryHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Working with Libraries"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Libraries are made to expose the projects' main functionalities to the applications and black boxes of the project. "
          "The functions and classes available in the libraries are the core of any project, thus they should implement important "
          "tasks such as image processing algorithms or point cloud processing algorithms."
          "Libraries are stored in the project's lib folder and each library has its own dedicated folder. Inside these folders "
          "each library has its classes.\n"
          "\n"
          "To start developing a library, go ahead and create a new class with the button \"Create Class\" and implement the main "
          "functionalities of your project inside the created files.\n"
          "If you need to separate classes in folders you can do it by creating a folder with the \"Create Folder\" button.\n"
          "Then, in order to include your libraries in the project correctly you must include them in the lib's folder "
          "\"CMakeLists.txt\" file by using the \"ADD_SUBDIRECTORY([libraryName])\". Also, if you use third party libraries in your "
          "library you must include them in the library's \"CMakeLists.txt\" file by uncommenting them in the "
          "\"SET ( ${LIBRARY_NAME}_LINK_LIBRARIES\" command; if you use custom libraries in your library you must also include them "
          "in the library's \"CMakeLists.txt\" file by including them in the \"SET ( ${LIBRARY_NAME}_LINK_LIBRARIES\" command. Or, "
          "if you create additional folders in your library you must include them in the library's \"CMakeLists.txt\" file by using "
          "the \"ADD_SUBDIRECTORY([folderName])\" command.\n"
          "\n"
          "You can easily edit the CMakeLists files previously mentioned by clicking on the following buttons."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakeLibraryBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Open Library's directory CMakeLists file"));
  editCMakeLibraryBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakeLibraryBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryHelpDialog::OnCMakeListsExit,NULL,this);
  wxButton* editCMakeLibBtn= new wxButton(this, ID_BUTTON_OPENPROJECT, wxT("Open Lib's directory CMakeLists file"));
  editCMakeLibBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakeLibBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryHelpDialog::OnCMakeListsExit,NULL,this);

  v_sizer1->Add(editCMakeLibraryBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);
  v_sizer1->Add(editCMakeLibBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMLibraryHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMLibraryHelpDialog::OnCMakeLists(wxCommandEvent& event)
{
  std::string* result;

  if((int)((wxButton*)event.GetEventObject())->GetId() == (int)ID_BUTTON_EDIT_CMAKELISTSFILE)
    {

      if(!this->library->OpenCMakeListsFile(result))
        wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

      if(this->library->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->library->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->library;
      while (node != NULL && dynamic_cast<modelCDMLib*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          if(!((modelCDMLib*)node)->OpenCMakeListsFile(result))
            wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMLib*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMLib*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
      else
        {
          wxMessageBox(crea::std2wx("No project CMakeLists file was found."),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMLibraryHelpDialog::OnCMakeListsEnter(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

      if(this->library->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->library->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->library;
      while (node != NULL && dynamic_cast<modelCDMLib*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

          if(((modelCDMLib*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMLib*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMLibraryHelpDialog::OnCMakeListsExit(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
      {
        wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

        if(this->library->GetCMakeLists() != NULL)
          {
            newEvent->SetClientData(this->library->GetCMakeLists());
            newEvent->SetId(0);
            wxPostEvent(this->GetParent(), *newEvent);
          }
      }
    else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
      {
        modelCDMIProjectTreeNode* node = this->library;
        while (node != NULL && dynamic_cast<modelCDMLib*>(node) == NULL)
          {
            node = node->GetParent();
          }
        if (node != NULL)
          {
            wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

            if(((modelCDMLib*)node)->GetCMakeLists() != NULL)
              {
                newEvent->SetClientData(((modelCDMLib*)node)->GetCMakeLists());
                newEvent->SetId(0);
                wxPostEvent(this->GetParent(), *newEvent);
              }
          }
      }
    event.Skip();
}

void wxCDMLibraryHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
