/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */

/*
 * modelCDMProject.h
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMPROJECT_H_
#define MODELCDMPROJECT_H_

#include<iostream>
#include<vector>
#include <map>

#include "modelCDMFolder.h"
#include "modelCDMLib.h"
#include "modelCDMAppli.h"
#include "modelCDMPackage.h"
#include "modelCDMCMakeListsFile.h"
#include "modelCDMCodeFile.h"

/**
 * Project model class.
 * This class represents a project stored in a hard drive. It can perform some of the most relevant operations for a crea project.
 */
class modelCDMProject : public modelCDMFolder
{
public:
  /**
   * Default constructor.
   */
  modelCDMProject();

  /**
   * Constructor receiving the source path and the build path.
   * @param parent Parent node of the Project node.
   * @param path The source path.
   * @param name Name of the project folder.
   * @param buildPath The build path. By default it's an empty string.
   */
  modelCDMProject(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const std::string& buildPath = "");

  /**
   * Destructor.
   */
  ~modelCDMProject();


  //Getters
  /**
   * Retrieves the name of the project.
   * @return The name of the project.
   */
  const std::string& GetNameProject() const;

  /**
   * Retrieves the version of the project.
   * @return The version of the project in the format X.Y.Z where X is the major version of the project, Y is the minor version of the project, and Z is the build version.
   */
  const std::string& GetVersion() const;

  /**
   * Retrieves the last version modification date of the project
   * @return The date of the last version modification in the format DD/MM/YYYY
   */
  const std::string& GetVersionDate() const;

  /**
   * Retrieves the build path of the project.
   * @return The build path of the project. By default .../ProjectNameBin.
   */
  const std::string& GetBuildPath() const;

  /**
   * Retrieves the package vector containing the packages present in the project.
   * @return The package vector containing references to the packages of the project.
   */
  const std::vector<modelCDMPackage*>& GetPackages() const;

  /**
   * Retrieves the appli containing the applications present in the project.
   * @return The appli object present in the project.
   */
  modelCDMAppli* GetAppli() const;

  /**
   * Retrieves the lib containing the libraries present in the project.
   * @return The lib object present in the project.
   */
  modelCDMLib* GetLib() const;

  /**
   * Retrieves the default make instruction to compile the project.
   * @return The make instruction to compile.
   */
  std::string GetBuildInstruction() const;


  //Setters
  /**
   * Sets the version of the project. It also modifies the build date of the project.
   * @param version New version of the project.
   * @param result returns the result of the operation.
   * @return If the version configuration was successful it returns true. If not, it returns false and the error description returns in the parameter result.
   */
  bool SetVersion(const std::string& version, std::string*& result);

  /**
   * Sets the build path of the project.
   * @param path Path for builing the project.
   * @param result Result of the operation.
   * @return If the build path configuration was successful it returns true. If not, it returns false and the error description returns in the parameter result.
   */
  bool SetBuildPath(const std::string& path, std::string*& result);


  //Creations
  /**
   * Creates a package and sets it as a children of the project. This method creates the package in the hard drive and also in the model. The created package is included in the project's CMakeLists file.
   * @param name Name of the package.
   * @param result Result of the operation.
   * @param authors Authors of the operation. If any space is found, it will be replaced by '_'.
   * @param authorsEmail Authors' E-mails. This is appended to the package description.
   * @param description Package description.
   * @param version Package version in the format X.Y.Z where X is the major version, Y the minor version, and Z the build version.
   * @return The result of the creation. If everything goes well it returns true, else it returns false.
   */
  modelCDMIProjectTreeNode* CreatePackage(
      const std::string& name,
      std::string*& result,
      const std::string& authors = "info-dev",
      const std::string& authorsEmail = "info-dev@creatis.insa-lyon.fr",
      const std::string& description = "no description",
      const std::string& version = "1.0.0"
  );

  /**
   * Creates a library and sets it as a children of the lib folder in the project. This method creates the library in the hard drive and also in the model.
   * @param name Library name.
   * @param result Result of the operation.
   * @param path Path of the library if not in the lib folder. This parameter is not used (for now).
   * @return The result of the creation. If everything goes well it returns true, else it returns false.
   */
  modelCDMIProjectTreeNode* CreateLibrary(
      const std::string& name,
      std::string*& result,
      const std::string& path = "/lib"
  );

  /**
   * Creates an application and sets it as a children of the appli folder in the project. This method creates the library in the hard drive and also in the model.
   * @param name Application name.
   * @param type Application type. 0=console application, 1=GUI application (wxWidgets).
   * @param result Result of the operation.
   * @param path Path of the application if not in the application folder. This parameter is not used (for now).
   * @return The result of the creation. If everything goes well it returns true, else it returns false.
   */
  modelCDMIProjectTreeNode* CreateApplication(
      const std::string& name,
      const int& type,
      std::string*& result,
      const std::string& path = "/appli"
  );

  /**
   * Creates a black box and sets it as a children of the specified package folder in the project. This method creates the black box in the hard drive and also in the model.
   * @param name Black box name.
   * @param package The name of the package where the black box is created. The name should not contain neither the "bbtk_" , nor the "_PKG" parts of the folder name. If empty converts into "/bbtk_*projectName*_PKG"
   * @param authors The authors of the black box. This string should not contain commas or spaces, they will be replaced by '_'.
   * @param authorsEmail The authors e-mails. This string should not contain spaces, they will be replaced by '/'. This field is appended to the black box description.
   * @param categories The categories of concerning for the created black box.
   * @param description Description of the black box. It should not contain spaces, they will be replaced by '_'.
   * @return The result of the creation. If everything goes well it returns true, else it returns false.
   */
  modelCDMIProjectTreeNode* CreateBlackBox(
      const std::string& name,
      const std::string& package = "", //if empty converts into "[projectName]"
      const std::string& authors = "unknown",
      const std::string& authorsEmail = "",
      const std::string& categories = "empty",
      const std::string& description = "no description"
  );


  //Operations
  /**
   * Opens the CMakeLists.txt file in the system's default text editor.
   * @param result Result of the operation.
   * @return Success of the operation. If the file doesn't exist or can't be opened it returns false.
   */
  bool OpenCMakeListsFile(std::string*& result);

  /**
   * Refresh the model with the file hierarchy in the hard drive.
   * @param result Result of the operation.
   * @return if there's an error refreshing the project it returns false.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Launches in console the ccmake tool in the build path to configure the building of the project.
   * @param result The result message of the operation.
   * @return if the ccmake tool doesn't launches it returns false.
   */
  bool ConfigureBuild(std::string*& result);

  /**
   * Launches in console the make -clean and make commands to build the project.
   * @param result Result message for building the project.
   * @param line Line to execute the compilation.
   * @return if any of the commands cannot be executed it return false.
   */
  bool Build(std::string*& result, const std::string& line);

  /**
   * Launches in console the bbPlugPackage command to connect the project to the .bbtk folder in the hard drive.
   * @param result Result message for connecting the project.
   * @param folder Folder to make connection with. It must contain the bbtkPackage file
   * @return if the command cannot be executed it return false.
   */
  bool Connect(std::string*& result, const std::string& folder);

  /**
   * Checks the CMakeLists files to see what's going to be compiled and what's not.
   * @param properties Map containing the project compilation properties.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks if the given package is included in the CMakeLists file.
   * @param package_name Name of the package to check.
   * @return True if the package is included, otherwise returns False.
   */
  bool IsPackageIncluded(const std::string& package_name);

  /**
   * Sets the inclusion of the package in the project's CMakeLists file. If the package inclusion already exist in file, then the line is uncommented/commented depending on the requested action. If the package inclusion doesn't exist yet, then it is included if the request is an inclusion.
   * @param package_name Name of the package to include/exclude.
   * @param toInclude True if the request is an inclusion, False otherwise.
   * @return True if the request was processed successfully.
   */
  bool SetPackageInclude(const std::string& package_name, const bool& toInclude);

  /**
   * Checks the project folder's CMakeLists file to check which third party libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> Get3rdPartyLibraries();

  /**
   * Sets the 3rd party library inclusion in the CMakeLists file.
   * @return if the operation was successful.
   */
  bool Set3rdPartyLibrary(const std::string& library_name, const bool& toInclude);


private:

  /**
   * Project Name
   */
  std::string nameProject;
  /**
   * Project Version
   */
  std::string version;
  /**
   * Last Project Version Modification Date
   */
  std::string versionDate;
  /**
   * Build Path for compiling the project
   */
  std::string buildPath;

  /**
   * lib folder
   */
  modelCDMLib* lib;
  /**
   * appli folder
   */
  modelCDMAppli* appli;
  /**
   * package folders
   */
  std::vector<modelCDMPackage*> packages;

};

#endif /* MODELCDMPROJECT_H_ */
