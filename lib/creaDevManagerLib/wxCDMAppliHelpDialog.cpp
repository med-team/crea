/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMAppliHelpDialog.cpp
 *
 *  Created on: 11/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMAppliHelpDialog.h"

#include "creaDevManagerIds.h"


BEGIN_EVENT_TABLE(wxCDMAppliHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMAppliHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMAppliHelpDialog::OnEditCMake)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMAppliHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMAppliHelpDialog::wxCDMAppliHelpDialog(
    wxWindow* parent,
    modelCDMAppli* appli,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMAppliHelpDialog::Create(parent, id, caption, position, size, style);
  this->appli = appli;
}

wxCDMAppliHelpDialog::~wxCDMAppliHelpDialog()
{
}

bool wxCDMAppliHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMAppliHelpDialog::CreateControls()
{
  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Managing your applications"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Applications are stand alone programs that use the projects' core functionality (libraries' functions). These "
          "applications are useful when showing the projects' \"out of the box\" functionalities.\n"
          "\n"
          "In the application manager you can view a list of the available applications in the current project, as well as create "
          "new applications. Remember that any application you make must be included in the appli's folder CMakeLists file by using the "
          "\"ADD_SUBDIRECTORY([applicationName])\" command. You can do that by clicking on the \"Edit Appli's CMakeLists File\" button "
          "below or in the Application Manager the \"Edit CMakeLists file\" button and include the desired applications at the end of "
          "the file.\n"
          "For a better understanding of how to use the applications please check the \"myFierceAppli\" application (which is shipped by "
          "default in every new project) and take a look at how it's included in the project."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakeAppliBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Edit Appli's CMakeLists File"));
  editCMakeAppliBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakeAppliBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMAppliHelpDialog::OnCMakeListsExit,NULL,this);
  v_sizer1->Add(editCMakeAppliBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);

}

void wxCDMAppliHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMAppliHelpDialog::OnEditCMake(wxCommandEvent& event)
{
  std::string* result;
  if(!this->appli->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}


void wxCDMAppliHelpDialog::OnCMakeListsEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMAppliHelpDialog::OnCMakeListsExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->appli->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->appli->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}


void wxCDMAppliHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
