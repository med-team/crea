/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMAppli.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMAppli.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "CDMUtilities.h"
#include "creaWx.h"
#include "wx/dir.h"

modelCDMAppli::modelCDMAppli()
{
}

modelCDMAppli::modelCDMAppli(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating appli\n";
  this->parent = parent;
  this->type = wxDIR_DIRS;
  this->name = name;
  this->level = level;
  this->path = path;



  this->path = CDMUtilities::fixPath(path);
  std::string pathFixed(CDMUtilities::fixPath(path));

  this->applications.clear();
  wxDir dir(crea::std2wx((pathFixed).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          if(stdfileName != "template_appli" && stdfileName != "template_wx_appli")
            {
              modelCDMApplication* application = new modelCDMApplication(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->applications.push_back(application);
              this->children.push_back(application);
            }
          else
            {
              modelCDMFolder* folder = new modelCDMFolder(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(folder);
            }

          cont = dir.GetNext(&fileName);
        }
      //files
      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              this->CMakeLists = new modelCDMCMakeListsFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->CMakeLists);
            }
          //if is a code file, create modelCDMCodeFile
          else if(
              fileType == ".c" ||
              fileType == ".cxx" ||
              fileType == ".h" ||
              fileType == ".cpp" ||
              fileType == ".txx" ||
              fileType == ".cmake" )
            {
              this->children.push_back(new modelCDMCodeFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }
          //if is an unknown file, create file
          else
            {
              this->children.push_back(new modelCDMFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }

    }
  this->SortChildren();
  std::sort(this->applications.begin(), this->applications.end(), CompareNodeItem);
}

modelCDMAppli::~modelCDMAppli()
{
}

const std::vector<modelCDMApplication*>& modelCDMAppli::GetApplications() const
{
  return this->applications;
}

modelCDMApplication* modelCDMAppli::CreateApplication(
    const std::string& namein,
    const int& type,
    std::string*& result
)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words,namein," '/\\*\"%",CDMUtilities::splitter::no_empties);
  std::string name;
  for (int i = 0; i < (int)(words.size()); i++)
    {
      name += words[i];
    }
  if (name == "")
    {
      result = new std::string("The given name is not valid:  '/\\*\"% are forbidden.");
      return NULL;
    }

  if (type == 0)
    {
      //copy template application folder with new name
#ifdef _WIN32
      std::string copyCommand = "xcopy \"" + this->path + CDMUtilities::SLASH + "template_appli\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "\" /Y";
#else
      std::string copyCommand = "cp -r \"" + this->path + CDMUtilities::SLASH + "template_appli\" \"" + this->path + CDMUtilities::SLASH + name + "\"";
#endif

      if(system(copyCommand.c_str()))
        {
          result = new std::string("An error occurred while running '" + copyCommand + "'.");
          return NULL;
        }
      //set name of library in CMakeLists inside copied folder
      std::string line;
      std::ifstream in((this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
      if( !in.is_open())
        {
          result = new std::string("CMakeLists.txt file failed to open.");
          return NULL;
        }
      std::ofstream out((this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
      while (getline(in, line))
        {
          if(line == "SET ( EXE_NAME   MyExe  )")
            line = "SET ( EXE_NAME   " + name + "  )";
          out << line << std::endl;
        }
      in.close();
      out.close();
      //delete old file and rename new file
#ifdef _WIN32
      std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
      std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif

      if(system(renameCommand.c_str()))
        {
          result = new std::string("An error occurred while running '" + renameCommand + "'.");
          return NULL;
        }

      //add application to appli CMakeLists
      std::fstream out1((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
      if (out1.is_open())
        {
          out1 << "ADD_SUBDIRECTORY(" << name << ")" << std::endl;
          out1.close();
        }

      //add application to model
      modelCDMApplication* application = new modelCDMApplication(this, this->path + CDMUtilities::SLASH + name, name, this->level + 1);
      this->applications.push_back(application);
      this->children.push_back(application);

      this->SortChildren();

      result = new std::string(this->path + CDMUtilities::SLASH + name);
      return application;
    }
  else if(type == 1)
    {
      //copy template application folder with new name
#ifdef _WIN32
      std::string copyCommand = "xcopy \"" + this->path + CDMUtilities::SLASH + "template_wx_appli\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "\" /Y";
#else
      std::string copyCommand = "cp -r \"" + this->path + CDMUtilities::SLASH + "template_wx_appli\" \"" + this->path + CDMUtilities::SLASH + name + "\"";
#endif

      if(system(copyCommand.c_str()))
        {
          result = new std::string("An error occurred while running '" + copyCommand + "'.");
          return NULL;
        }
      //set name of library in CMakeLists inside copied folder
      std::string line;
      std::ifstream in((this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
      if( !in.is_open())
        {
          result = new std::string("CMakeLists.txt file failed to open.");
          return NULL;
        }
      std::ofstream out((this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
      while (getline(in, line))
        {
          if(line == "SET ( EXE_NAME   MyExeWx  )")
            line = "SET ( EXE_NAME   " + name + "  )";
          out << line << std::endl;
        }
      in.close();
      out.close();
      //delete old file and rename new file
#ifdef _WIN32
      std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
      std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + name + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif

      if(system(renameCommand.c_str()))
        {
          result = new std::string("An error occurred while running '" + renameCommand + "'.");
          return NULL;
        }

      //add application to appli CMakeLists
      std::fstream out1((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
      if (out1.is_open())
        {
          out1 << "ADD_SUBDIRECTORY(" << name << ")" << std::endl;
          out1.close();
        }

      //add application to model
      modelCDMApplication* application = new modelCDMApplication(this, this->path + CDMUtilities::SLASH + name, name, this->level + 1);
      this->applications.push_back(application);
      this->children.push_back(application);

      this->SortChildren();

      result = new std::string(this->path + CDMUtilities::SLASH + name);
      return application;
    }
  else
    {
      std::string res = "Invalid application type: ";
      res += type;
      res += std::string(".\n0:Console application.\n1:GUI Application (wxWidgets).");
      result = new std::string(res);

      return NULL;
    }
}

const bool modelCDMAppli::Refresh(std::string*& result)
{
  std::cout << "refreshing appli" << std::endl;
  this->type = wxDIR_DIRS;

  std::vector<bool> checked(this->children.size(), false);
  std::vector<bool> checkedApplications(this->applications.size(), false);

  //check all folders
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          if(stdfileName != "template_appli" && stdfileName != "template_wx_appli")
            {
              std::string applicationName = stdfileName;
              //check if application already exist
              bool found = false;
              for (int i = 0; !found && i < (int)(this->applications.size()); i++)
                {
                  if (this->applications[i]->GetName() == applicationName)
                    {
                      found = true;
                      int pos = std::find(this->children.begin(), this->children.end(), this->applications[i]) - this->children.begin();
                      checked[pos] = true;
                      checkedApplications[i] = true;
                      if(!this->applications[i]->Refresh(result))
                        return false;
                    }
                }
              if(!found)
                {
                  modelCDMApplication* application= new modelCDMApplication(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->applications.push_back(application);
                  this->children.push_back(application);
                }
            }
          else
            {
              //check if folder already exist
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;

                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }
              if(!found)
                {
                  modelCDMFolder* folder= new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(folder);
                }
            }
          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }
          //if is a code file, create modelCDMCodeFile
          //if is an unknown file, create file
          else
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  if(
                    fileType == ".c" ||
                    fileType == ".cxx" ||
                    fileType == ".h" ||
                    fileType == ".cpp" ||
                    fileType == ".txx" ||
                    fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }
    }

  for (int i = 0; i < (int)(checkedApplications.size()); i++)
    {
      if(!checkedApplications[i])
        {
          this->applications.erase(this->applications.begin()+i);
          checkedApplications.erase(checkedApplications.begin()+i);
          i--;
        }
    }
  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }
  this->SortChildren();
  std::sort(this->applications.begin(), this->applications.end(), CompareNodeItem);
  return true;
}

void modelCDMAppli::CheckStructure(std::map<std::string, bool>& properties)
{
  //check cmake exist
  if(this->CMakeLists != NULL)
    {
      //set properties parameters based on model
      for (int i = 0; i < (int)(this->applications.size()); i++)
        properties["appli add " + this->applications[i]->GetName()] = false;

      //open cmakelists
      std::ifstream confFile;
      confFile.open((this->CMakeLists->GetPath()).c_str());

      //take everything that is not commented
      std::string fileContent;

      std::string word;
      std::vector<std::string> words;
      while(confFile.is_open() && !confFile.eof())
        {
          std::getline(confFile,word, '\n');
          if(word[0] != '#')
            {
              CDMUtilities::splitter::split(words, word, "#", CDMUtilities::splitter::empties_ok);
              if (words.size() > 0)
                {
                  word = words[0];
                  CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::empties_ok);
                  for (int i = 0; i < (int)(words.size()); i++)
                    {
                      if(words[i].substr(0,2) == "//")
                        break;
                      fileContent += words[i] + " ";
                    }
                }
            }
        }

      //check every instruction
      std::stringstream ss(fileContent);
      while(!ss.eof())
        {
          std::getline(ss,word, '(');

          //check instruction name
          CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

          //add instructions
          if (words.size() > 0 && words[words.size()-1] == "ADD_SUBDIRECTORY")
            {
              std::getline(ss,word, ')');
              //std::cout << word << std::endl;
              CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

              if (words.size() > 0)
                {
                  {
                    properties["appli add " + words[0]] = true;
                  }
                }
            }
        }

    }

  //check libraries' structure
  for (int i = 0; i < (int)(this->applications.size()); i++)
    {
      properties["application " + this->applications[i]->GetName()] = true;
      this->applications[i]->CheckStructure(properties);
    }
}

bool modelCDMAppli::IsApplicationIncluded(const std::string& application_name)
{
  if (this->HasCMakeLists())
      {
        CDMUtilities::CMLFile cmlFile = CDMUtilities::readCMLFile(this->CMakeLists->GetPath().c_str());
        for (int i = 0; i < cmlFile.size(); ++i)
          {
            if (cmlFile[i].first=="command" && cmlFile[i].second[0] == "ADD_SUBDIRECTORY")
              {
                int pos = 1;
                while (pos < cmlFile[i].second.size())
                  {
                    if (!isspace(cmlFile[i].second[pos][0]) && cmlFile[i].second[pos][0] != '(' && cmlFile[i].second[pos][0] != '#')
                      {
                        if (application_name == cmlFile[i].second[pos])
                          return true;
                        break;
                      }
                    pos++;
                  }
              }
          }
      }
    return false;
}

bool modelCDMAppli::SetApplicationInclude(const std::string& application_name, const bool& toInclude)
{
  if (this->HasCMakeLists())
    {
      CDMUtilities::CMLFile cmlFile = CDMUtilities::readCMLFile(this->CMakeLists->GetPath().c_str());

      bool found = false;

      for (int i = 0; i < cmlFile.size(); ++i)
        {
          if(toInclude && cmlFile[i].first == "comment")
            {
              std::vector<std::string> segments;
              std::string line = cmlFile[i].second[0];
              while(line[0] == '#')
                line.erase(0,1);

              CDMUtilities::splitter::split(segments, line, " ()", CDMUtilities::splitter::no_empties);
              if (segments.size() > 1 && segments[0] == "ADD_SUBDIRECTORY" && segments[1] == application_name)
                {
                  found = true;
                  while(cmlFile[i].second[0][0] == '#')
                    cmlFile[i].second[0].erase(0,1);
                }
            }
          else if(cmlFile[i].first == "command" && cmlFile[i].second[0] == "ADD_SUBDIRECTORY")
            {
              int pos = 1;
              while (pos < cmlFile[i].second.size())
                {
                  if (!isspace(cmlFile[i].second[pos][0]) && cmlFile[i].second[pos][0] != '(' && cmlFile[i].second[pos][0] != '#')
                    {
                      if (application_name == cmlFile[i].second[pos])
                        {
                          found = true;
                          if (!toInclude)
                            {
                              cmlFile[i].first = "comment";
                              cmlFile[i].second[0] = "#" + cmlFile[i].second[0];
                              while (cmlFile[i].second.size() > 1)
                                {
                                  cmlFile[i].second[0] += cmlFile[i].second[1];
                                  cmlFile[i].second.erase(cmlFile[i].second.begin()+1);
                                }

                            }
                        }
                      break;
                    }
                  pos++;
                }
            }
        }
      if (!found && toInclude)
        {
          CDMUtilities::syntaxElement element;
          element.first = "command";
          element.second.push_back("ADD_SUBDIRECTORY(" + application_name + ")");
          cmlFile.push_back(element);
        }

      return CDMUtilities::writeCMLFile(this->CMakeLists->GetPath().c_str(),cmlFile);
    }
  return false;
}
