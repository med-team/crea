/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMApplicationHelpDialog.cpp
 *
 *  Created on: 11/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMApplicationHelpDialog.h"

#include "creaDevManagerIds.h"

#include "modelCDMAppli.h"

BEGIN_EVENT_TABLE(wxCDMApplicationHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMApplicationHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMApplicationHelpDialog::OnCMakeLists)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMApplicationHelpDialog::OnCMakeLists)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMApplicationHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMApplicationHelpDialog::wxCDMApplicationHelpDialog(
    wxWindow* parent,
    modelCDMApplication* application,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMApplicationHelpDialog::Create(parent, id, caption, position, size, style);
  this->application = application;
}

wxCDMApplicationHelpDialog::~wxCDMApplicationHelpDialog()
{
}

bool wxCDMApplicationHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMApplicationHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Working with Applications"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Applications are stand alone programs that use the projects' core functionality (libraries' functions). These "
          "applications are useful when showing the projects' \"out of the box\" functionalities.\n"
          "Applications are stored in the project's appli folder and each application has its own dedicated folder. Inside "
          "these folders each application has its main file.\n"
          "\n"
          "To start developing an application, go ahead and open the application main file with the button \"Open Main File\" "
          "and implement the application's functionality.\n"
          "If you need to create separate classes in the application folder, or inside an specific folders you can do it by "
          "creating a new class with the \"Create Class\" button or by creating a folder with the \"Create Folder\" button.\n"
          "Then, in order to include your applications in the project correctly you must include them in the appli's folder "
          "\"CMakeLists.txt\" file using the command \"ADD_SUBDIRECTORY([applicationName])\". Also, if you create additional "
          "folders in your application you should include them in the application's \"CMakeLists.txt\" file using the command "
          "\"ADD_SUBDIRECTORY([folderName])\". If you use a third party library in your application you must include it by "
          "uncommenting its inclusion in the application's \"CMakeLists.txt\" file. If you use a custom library in your "
          "application you must include it by including the library path inside the \"INCLUDE_DIRECTORIES\" command, and its "
          "name inside the \"SET ( ${EXE_NAME}_LINK_LIBRARIES...\"command.\n"
          "\n"
          "You can easily edit the CMakeLists files previously mentioned by clicking on the following buttons."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakeApplicationBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Open Application's directory CMakeLists file"));
  editCMakeApplicationBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMApplicationHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakeApplicationBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMApplicationHelpDialog::OnCMakeListsExit,NULL,this);
  wxButton* editCMakeAppliBtn= new wxButton(this, ID_BUTTON_OPENPROJECT, wxT("Open Appli's directory CMakeLists file"));
  editCMakeAppliBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMApplicationHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakeAppliBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMApplicationHelpDialog::OnCMakeListsExit,NULL,this);

  v_sizer1->Add(editCMakeApplicationBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);
  v_sizer1->Add(editCMakeAppliBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMApplicationHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMApplicationHelpDialog::OnCMakeLists(wxCommandEvent& event)
{
  std::string* result;

  if((int)((wxButton*)event.GetEventObject())->GetId() == (int)ID_BUTTON_EDIT_CMAKELISTSFILE)
    {

      if(!this->application->OpenCMakeListsFile(result))
        wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

      if(this->application->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->application->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->application;
      while (node != NULL && dynamic_cast<modelCDMAppli*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          if(!((modelCDMAppli*)node)->OpenCMakeListsFile(result))
            wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMAppli*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMAppli*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
      else
        {
          wxMessageBox(crea::std2wx("No project CMakeLists file was found."),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMApplicationHelpDialog::OnCMakeListsEnter(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

      if(this->application->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->application->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->application;
      while (node != NULL && dynamic_cast<modelCDMAppli*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

          if(((modelCDMAppli*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMAppli*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMApplicationHelpDialog::OnCMakeListsExit(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

      if(this->application->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->application->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->application;
      while (node != NULL && dynamic_cast<modelCDMAppli*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMAppli*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMAppli*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMApplicationHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
