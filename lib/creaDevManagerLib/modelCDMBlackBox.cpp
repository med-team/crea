/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMBlackBox.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMBlackBox.h"

#include<fstream>

#include "CDMUtilities.h"

#include<creaWx.h>
#include"wx/dir.h"

modelCDMBlackBox::modelCDMBlackBox()
{
  this->source = NULL;
  this->header = NULL;
}

modelCDMBlackBox::modelCDMBlackBox(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating black box: " + name + " in " + path + "\n";
  this->parent = parent;
  this->name = name;
  this->path = path;
  this->level = level;
  this->type = wxDIR_DIRS;
  this->source = NULL;
  this->header = NULL;

  std::string pathHeader = path + CDMUtilities::SLASH + "bb" + this->name + ".h";

  std::ifstream confFile;
  confFile.open((pathHeader).c_str());
  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //get BBTK's
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," \n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "BBTK_NAME")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->nameBlackBox = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_AUTHOR")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->authors = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_DESCRIPTION")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->description = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_CATEGORY")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->categories = word;
          if (this->categories == "")
            this->categories = "empty";
        }
    }
  confFile.close();

}

modelCDMBlackBox::~modelCDMBlackBox()
{
  this->header = NULL;
  this->source = NULL;
}

const std::string& modelCDMBlackBox::GetNameBlackBox() const
{
  return this->nameBlackBox;
}

const std::string& modelCDMBlackBox::GetAuthors() const
{
  return this->authors;
}

const std::string& modelCDMBlackBox::GetCategories() const
{
  return this->categories;
}

const std::string& modelCDMBlackBox::GetDescription() const
{
  return this->description;
}

bool modelCDMBlackBox::SetAuthors(const std::string& authors, std::string*& result)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, authors, "/\\\"\n", CDMUtilities::splitter::no_empties);
  std::string authorsReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      authorsReal += "," + words[i];
    }

  //opening original header
  std::string pathHeader = this->header->GetPath();
  std::ifstream in(pathHeader.c_str());
  if( !in.is_open())
    {
      result = new std::string(pathHeader + " file failed to open.");
      return false;
    }
  //opening temporal header
  std::ofstream out((pathHeader + ".tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string(pathHeader + ".tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  std::string reading;
  while (getline(in, reading, '('))
    {
      CDMUtilities::splitter::split(words, reading, "\n ", CDMUtilities::splitter::no_empties);
      if(words[words.size() - 1] == "BBTK_AUTHOR")
        {
          out << reading << "(\"" << authorsReal << "\")";
          getline(in, reading, ')');
        }
      else
        {
          out << reading;
          if (!in.eof())
            out << "(";
        }
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#else
  std::string renameCommand = "mv \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#endif
  
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->authors = authorsReal;
  return true;

}

bool modelCDMBlackBox::SetCategories(
    const std::string& categories,
    std::string*& result
)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, categories, "\"\\/", CDMUtilities::splitter::no_empties);
  std::string catsReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      catsReal += "," + words[i];
    }

  //opening original header
  std::string pathHeader = this->header->GetPath();
  std::ifstream in(pathHeader.c_str());
  if( !in.is_open())
    {
      result = new std::string(pathHeader + " file failed to open.");
      return false;
    }
  //opening temporal header
  std::ofstream out((pathHeader + ".tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string(pathHeader + ".tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  std::string reading;
  while (getline(in, reading, '('))
    {
      CDMUtilities::splitter::split(words, reading, "\n ", CDMUtilities::splitter::no_empties);
      if(words[words.size() - 1] == "BBTK_CATEGORY")
        {
          out << reading << "(\"" << catsReal << "\")";
          getline(in, reading, ')');
        }
      else
        {
          out << reading;
          if (!in.eof())
            out << "(";
        }
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#else
  std::string renameCommand = "mv \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#endif
  
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->categories = catsReal;
  return true;
}

bool modelCDMBlackBox::SetDescription(
    const std::string& description,
    std::string*& result
)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, description, "\"\n\\/", CDMUtilities::splitter::no_empties);
  std::string descReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      descReal += "-" + words[i];
    }

  //opening original header
  std::string pathHeader = this->header->GetPath();
  std::ifstream in(pathHeader.c_str());
  if( !in.is_open())
    {
      result = new std::string(pathHeader + " file failed to open.");
      return false;
    }
  //opening temporal header
  std::ofstream out((pathHeader + ".tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string(pathHeader + ".tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  std::string reading;
  while (getline(in, reading, '('))
    {
      CDMUtilities::splitter::split(words, reading, "\n ", CDMUtilities::splitter::no_empties);
      if(words[words.size() - 1] == "BBTK_DESCRIPTION")
        {
          out << reading << "(\"" << descReal << "\")";
          getline(in, reading, ')');
        }
      else
        {
          out << reading;
          if (!in.eof())
            out << "(";
        }
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#else
  std::string renameCommand = "mv \"" + pathHeader + ".tmp\" \"" + pathHeader + "\"";
#endif
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->description = descReal;
  return true;
}

void modelCDMBlackBox::SetHeaderFile(modelCDMFile* file)
{
  this->header = file;
}

void modelCDMBlackBox::SetSourceFile(modelCDMFile* file)
{
  this->source = file;
}

bool modelCDMBlackBox::OpenCxx(std::string*& result)
{
  return !CDMUtilities::openTextEditor(this->source->GetPath());
}

bool modelCDMBlackBox::OpenHxx(std::string*& result)
{
  return !CDMUtilities::openTextEditor(this->header->GetPath());
}

modelCDMFile* modelCDMBlackBox::GetHeaderFile() const
{
  return this->header;
}

modelCDMFile* modelCDMBlackBox::GetSourceFile() const
{
  return this->source;
}

const bool modelCDMBlackBox::Refresh(std::string*& result)
{
  std::cout << "refreshing black box: " << this->nameBlackBox << std::endl;
  std::string pathHeader = path + CDMUtilities::SLASH + "bb" + this->name + ".h";

  std::ifstream confFile;
  confFile.open((pathHeader).c_str());
  std::string word;

  if(!confFile.is_open())
    return false;

  while(confFile.is_open() && !confFile.eof())
    {
      //get BBTK's
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," \n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "BBTK_NAME")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->nameBlackBox = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_AUTHOR")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->authors = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_DESCRIPTION")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->description = word;
        }
      else if(wordBits[wordBits.size()-1] == "BBTK_CATEGORY")
        {
          std::getline(confFile,word,'"');
          std::getline(confFile,word,'"');
          this->categories = word;
          if (this->categories == "")
            this->categories = "empty";
        }
    }
  confFile.close();
  return true;
}
