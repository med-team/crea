/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMMainPanel.cpp
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMMainDescriptionPanel.h"

#include "wxCDMMainFrame.h"

#include "wxCDMMainHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/CIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMMainDescriptionPanel, wxPanel)
EVT_MENU(ID_MENU_NEW_PROJECT, wxCDMMainDescriptionPanel::OnBtnNewProject)
EVT_MENU(ID_MENU_OPEN_PROJECT, wxCDMMainDescriptionPanel::OnBtnOpenProject)

END_EVENT_TABLE()

wxCDMMainDescriptionPanel::wxCDMMainDescriptionPanel(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMMainDescriptionPanel::Create(parent, id, caption, pos, size, style);
}

wxCDMMainDescriptionPanel::~wxCDMMainDescriptionPanel()
{
}

bool wxCDMMainDescriptionPanel::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  CreateControls();
  //to scroll
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);

  
  return TRUE;
}

void wxCDMMainDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(CIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Welcome")),0, wxALIGN_LEFT, 0);
    //Application Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx("Crea Development Manager")),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER | wxUP, 10);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  actionsBox->GetStaticBox()->SetToolTip(wxT("Create a new crea project or open an existing one selection any of the available actions."));
  //wxPanel* actionsPanel = new wxPanel(this);
  //wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);

  wxButton* newProjectbt = new wxButton(this, ID_BUTTON_NEWPROJECT, _T("New Project"));
  newProjectbt->SetToolTip(wxT("Create a new crea project."));
  actionsBox->Add(newProjectbt, 0, wxALL, 20);
  wxButton* openProjectbt = new wxButton(this, ID_BUTTON_OPENPROJECT, _T("Open Project (source/binaries)"));
  openProjectbt->SetToolTip(wxT("Open an existing crea project from its binaries or its sources."));
  actionsBox->Add(openProjectbt, 0, wxALL, 20);

  //actionsPanel->SetSizer(actionsPanelSizer);
  //actionsPanelSizer->Fit(actionsPanel);

  //actionsBox->Add(actionsPanel, 0,wxEXPAND | wxALIGN_CENTER | wxALL, 10);
  sizer -> Add(actionsBox, 0, wxALIGN_CENTER | wxALL, 20);

  //Asign sizer
  sizer->SetSizeHints(this);
  SetSizer(sizer);
  sizer->Fit(this);
  if(((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxDialog* helpDialog = new wxCDMMainHelpDialog(this->GetParent(), this, wxID_ANY);
      helpDialog->Show(true);
    }

}

void wxCDMMainDescriptionPanel::OnBtnNewProject(wxCommandEvent& event)
{
  event.Skip();
}

void wxCDMMainDescriptionPanel::OnBtnOpenProject(wxCommandEvent& event)
{
  event.Skip();
}

