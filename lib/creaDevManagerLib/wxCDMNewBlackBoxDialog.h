/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMNewBlackBoxDialog.h
 *
 *  Created on: 26/12/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMNEWBLACKBOXDIALOG_H_
#define WXCDMNEWBLACKBOXDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>
#include <wx/choice.h>

/**
 * Dialog to create a new Black Box in a Crea Project.
 */
class wxCDMNewBlackBoxDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * New Black Box Dialog Constructor.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Black Box".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 500, 500.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMNewBlackBoxDialog(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Black Box"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(500,500),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMNewBlackBoxDialog();
  /**
   * New Black Box Dialog Constructor.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Black Box".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 500, 500.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return True if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Black Box"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(500,500),
      long style = wxDEFAULT_DIALOG_STYLE
  );

  /**
   * Returns the black box name chosen by the user.
   * @return Black box name.
   */
  const wxString GetBlackBoxName() const;
  /**
   * Returns the black box's author name chosen by the user.
   * @return Black box's author name.
   */
  const wxString GetBlackBoxAuthor() const;
  /**
   * Returns the black box's author email chosen by the user.
   * @return Black box's author email.
   */
  const wxString GetBlackBoxAuthorEmail() const;
  /**
   * Returns the black box description chosen by the user.
   * @return Black box description.
   */
  const wxString GetBlackBoxDescription() const ;
  /**
   * Returns the black box categories chosen by the user.
   * @return Black box categories.
   */
  const wxString GetBlackBoxCategories() const ;
  /**
   * Returns the black box type chosen by the user.
   * @return Black box type.
   */
  const wxString GetBlackBoxType() const;
  /**
   * Returns the black box format chosen by the user.
   * @return Black box format.
   */
  const wxString GetBlackBoxFormat() const;

protected:
  /**
   * Creates the visual controls of the Dialog.
   */
  void CreateControls();

private:
  /**
   * Black box name.
   */
  wxTextCtrl* blackBoxName;
  /**
   * Black box's author name.
   */
  wxTextCtrl* blackBoxAuthor;
  /**
   * Black box's author email.
   */
  wxTextCtrl* blackBoxAuthorEmail;
  /**
   * Black box description.
   */
  wxTextCtrl* blackBoxDescription;
  /**
   * Black box categories.
   */
  wxTextCtrl* blackBoxCategories;
  /**
   * Black box type.
   */
  wxChoice* blackBoxType;
  /**
   * Black box format.
   */
  wxChoice* blackBoxFormat;

  //handlers
protected:
  /*
   * Handles when the create black box button is pressed.
   * @param event Unused event.
   */
  void OnCreateBlackBox(wxCommandEvent& event);
  /*
   * Handles when the cancel button is pressed.
   * @param event Unused event.
   */
  void OnCancel(wxCommandEvent& event);
};

#endif /* WXCDMNEWBLACKBOXDIALOG_H_ */
