/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMBlackBox.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMBLACKBOX_H_
#define MODELCDMBLACKBOX_H_

#include<iostream>

#include"modelCDMFolder.h"

/**
 * Class representing a black box inside a Crea project. A black box is modular construct that uses the project libraries to execute its operations
 */
class modelCDMBlackBox : public modelCDMFolder
{
public:
  /**
   * Default Constructor.
   */
  modelCDMBlackBox();
  /**
   * Black Box Constructor.
   * @param parent parent node of the black box node.
   * @param path Path of the source folder of the black box node.
   * @param name Name of the black box node.
   * @param level Hierarchy level of the black box node in the project. By default 3
   */
  modelCDMBlackBox(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level = 3);
  /**
   * Destructor.
   */
  ~modelCDMBlackBox();

  /**
   * Returns the name of the black box node.
   * @return Name of the black box node.
   */
  const std::string& GetNameBlackBox() const;
  /**
   * Retrieves the black box authors' names.
   * @return Names of the black box node authors.
   */
  const std::string& GetAuthors() const;
  /**
   * Retrieves the categories of the black box.
   * @return Categories associated with the black box.
   */
  const std::string& GetCategories() const;
  /**
   * Returns the description of the black box purpose.
   * @return Description of the black box.
   */
  const std::string& GetDescription() const;

  /**
   * Returns a reference of the header file of the black box.
   * @return Header file of the black box.
   */
  modelCDMFile* GetHeaderFile() const;
  /**
   * Returns a reference of the source file of the black box.
   * @return Source file of the black box.
   */
  modelCDMFile* GetSourceFile() const;

  /**
   * Sets the name of the black box with the one provided.
   * @param name New name of the black box.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool SetNameBlackBox(const std::string& name, std::string*& result);
  /**
   * Sets the authors' names of the black box to the ones given.
   * @param authors Authors' names.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool SetAuthors(const std::string& authors, std::string*& result);
  /**
   * Sets the Categories associated with the black box to the given ones.
   * @param categories Categories associated with the black box.
   * @param result Result message
   * @return True if the operation was successful.
   */
  bool SetCategories(const std::string& categories, std::string*& result);
  /**
   * Sets the Description of the black box to the given one.
   * @param description Description of the black box.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool SetDescription(const std::string& description, std::string*& result);

  /**
   * Sets the reference of the header file.
   * @param file Reference to the Header file.
   */
  void SetHeaderFile(modelCDMFile* file);
  /**
   * Sets the reference of the source file.
   * @param file Reference to the source file.
   */
  void SetSourceFile(modelCDMFile* file);


  /**
   * Opens the source file with the system default code editor
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool OpenCxx(std::string*& result);
  /**
   * Opens the header file with the system default code editor
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool OpenHxx(std::string*& result);
  /**
   * Refreshes the structure and properties of the black box.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  const bool Refresh(std::string*& result);

private:
  /**
   * Name of the black box. It might be different from the header file name.
   */
  std::string nameBlackBox;
  /**
   * Authors of the black box.
   */
  std::string authors;
  /**
   * Categories associated to the categories.
   */
  std::string categories;
  /**
   * Description of the black box.
   */
  std::string description;

  /**
   * Reference to the header file of the black box.
   */
  modelCDMFile* header;
  /**
   * Reference to the source file of the black box.
   */
  modelCDMFile* source;
};

#endif /* MODELCDMBLACKBOX_H_ */
