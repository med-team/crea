/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMPackageHelpDialog.cpp
 *
 *  Created on: 9/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMPackageHelpDialog.h"

#include "creaDevManagerIds.h"

#include "modelCDMProject.h"

BEGIN_EVENT_TABLE(wxCDMPackageHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMPackageHelpDialog::OnCMakeLists)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMPackageHelpDialog::OnCMakeLists)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMPackageHelpDialog::OnFinish)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMPackageHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMPackageHelpDialog::wxCDMPackageHelpDialog(
    wxWindow* parent,
    modelCDMPackage* package,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMPackageHelpDialog::Create(parent, id, caption, position, size, style);
  this->package = package;
}

wxCDMPackageHelpDialog::~wxCDMPackageHelpDialog()
{
}

bool wxCDMPackageHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMPackageHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Working with Packages"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Packages contain black boxes, which allow to work modularly with other boxes. This boxes can use the functions "
          "present in your libraries and expose them to work in a BBTK-fashion.\n"
          "\n"
          "To create a black box click on the \"Create Black Box\" button.\n"
          "If you want to check the files in the file explorer click the \"Open Package Folder\" "
          "button.\n"
          "\n"
          "Don't forget to include the libraries your black boxes use in the Package directory CMakeLists.txt file by "
          "uncommenting the \"SET\" commands for third party libraries or by including the library name inside the "
          "\"SET(${BBTK_PACKAGE_NAME}_LIBS\"command and its path inside the \"SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS\" "
          "command for custom libraries.\n"
          "Also, make sure you include this package in the Project directory's CMakeLists.txt file\n using the "
          "ADD_SUBDIRECTORY([packageName]) command.\n"
          "You can open these files with the following buttons."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakePkgBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Open Package's directory CMakeLists file"));
  editCMakePkgBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakePkgBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageHelpDialog::OnCMakeListsExit,NULL,this);
  wxButton* editCMakePrjBtn= new wxButton(this, ID_BUTTON_OPENPROJECT, wxT("Open Project's directory CMakeLists file"));
  editCMakePrjBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageHelpDialog::OnCMakeListsEnter,NULL,this);
  editCMakePrjBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageHelpDialog::OnCMakeListsExit,NULL,this);

  v_sizer1->Add(editCMakePkgBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);
  v_sizer1->Add(editCMakePrjBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMPackageHelpDialog::OnCMakeLists(wxCommandEvent& event)
{
  std::string* result;

  if((int)((wxButton*)event.GetEventObject())->GetId() == (int)ID_BUTTON_EDIT_CMAKELISTSFILE)
    {

      if(!this->package->OpenCMakeListsFile(result))
        wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

      if(this->package->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->package->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->package;
      while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          if(!((modelCDMProject*)node)->OpenCMakeListsFile(result))
            wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
      else
        {
          wxMessageBox(crea::std2wx("No project CMakeLists file was found."),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMPackageHelpDialog::OnCMakeListsEnter(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

      if(this->package->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->package->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->package;
      while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

          if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMPackageHelpDialog::OnCMakeListsExit(wxMouseEvent& event)
{
  if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_EDIT_CMAKELISTSFILE)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

      if(this->package->GetCMakeLists() != NULL)
        {
          newEvent->SetClientData(this->package->GetCMakeLists());
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
  else if(((wxButton*)event.GetEventObject())->GetId() == ID_BUTTON_OPENPROJECT)
    {
      modelCDMIProjectTreeNode* node = this->package;
      while (node != NULL && dynamic_cast<modelCDMProject*>(node) == NULL)
        {
          node = node->GetParent();
        }
      if (node != NULL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

          if(((modelCDMProject*)node)->GetCMakeLists() != NULL)
            {
              newEvent->SetClientData(((modelCDMProject*)node)->GetCMakeLists());
              newEvent->SetId(0);
              wxPostEvent(this->GetParent(), *newEvent);
            }
        }
    }
  event.Skip();
}

void wxCDMPackageHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMPackageHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
