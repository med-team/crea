/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMPackageManagerPanel.cpp
 *
 *  Created on: Dec 10, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMPackageManagerPanel.h"

#include "wxCDMMainFrame.h"
#include "wxCDMNewPackageDialog.h"

#include "wxCDMPackageManagerHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/PkIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMPackageManagerPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMPackageManagerPanel::OnBtnReturn)
EVT_HYPERLINK(ID_LINK_SELECT_PACKAGE, wxCDMPackageManagerPanel::OnLnkPackageSelect)
EVT_BUTTON(ID_BUTTON_CREATE_PACKAGE, wxCDMPackageManagerPanel::OnBtnCreatePackage)
EVT_COMMAND(wxID_ANY, wxEVT_COMMAND_TOOL_CLICKED, wxCDMPackageManagerPanel::OnBtnCreatePackage)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMPackageManagerPanel::OnBtnEditCMakeLists)
EVT_COMMAND(wxID_ANY, wxEVT_COMMAND_TOOL_ENTER, wxCDMPackageManagerPanel::OnBtnEditCMakeLists)
EVT_CHECKBOX(ID_CHECK_INCLUDE_PACKAGE, wxCDMPackageManagerPanel::OnChBPackageChange)
END_EVENT_TABLE()

wxCDMPackageManagerPanel::wxCDMPackageManagerPanel(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMPackageManagerPanel::Create(parent, project, id, caption, pos, size, style);
}

wxCDMPackageManagerPanel::~wxCDMPackageManagerPanel()
{
}

bool wxCDMPackageManagerPanel::Create(
    wxWindow* parent,
    modelCDMProject* project,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->project = project;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMPackageManagerPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Link to return
  wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(this->project->GetName()), crea::std2wx(this->project->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
  returnLnk->SetWindowStyle(wxNO_BORDER);
  returnLnk->SetToolTip(wxT("Return to the active project description."));
  sizer->Add(returnLnk, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(PkIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Package Management")),0, wxALIGN_LEFT, 0);

    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Packages
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("A&vailable Packages"));
  propertiesBox->GetStaticBox()->SetToolTip(wxT("Select any of the available packages to see its details or modify them. Remember that black boxes are created inside packages, any of these packages is available."));
  wxPanel* propertiesPanel = new wxPanel(this);

  std::vector<modelCDMPackage*> packages = this->project->GetPackages();
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(packages.size()+1, 3, 9, 5);

  wxStaticText* ChBTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Include in\nCMake"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* LkTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Package Name"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
  wxStaticText* HlpTitle = new wxStaticText(propertiesPanel, wxID_ANY, wxT("Help"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

  propertiesGridSizer -> Add(ChBTitle, 0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(LkTitle,  0, wxEXPAND | wxALL, 5);
  propertiesGridSizer -> Add(HlpTitle, 0, wxEXPAND | wxALL, 5);

  for (int i = 0; i < (int)(packages.size()); i++)
    {
      //checkbox for cmake inclusion
      wxCheckBox* pPackageChB = new wxCheckBox(propertiesPanel, ID_CHECK_INCLUDE_PACKAGE, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
      pPackageChB->SetName(crea::std2wx(packages[i]->GetName()));
      std::string tt = "if this box is checked the the " + packages[i]->GetName() + " package is included in the project compilation.";
      pPackageChB->SetToolTip(crea::std2wx(tt));
      pPackageChB->SetValue(this->project->IsPackageIncluded(packages[i]->GetName()));
      propertiesGridSizer -> Add(pPackageChB, 0, wxEXPAND | wxALIGN_CENTER);

      //link to package with description
      wxHyperlinkCtrl* pPackagelk = new wxHyperlinkCtrl(propertiesPanel, ID_LINK_SELECT_PACKAGE, crea::std2wx(packages[i]->GetName().c_str()), crea::std2wx(packages[i]->GetName().c_str()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      pPackagelk->SetWindowStyle(wxALIGN_LEFT | wxNO_BORDER);
      tt = "Name: " + packages[i]->GetName() + "\n";
      tt += "Location: " + packages[i]->GetPath();
      pPackagelk->SetToolTip(crea::std2wx(tt));
      pPackagelk->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerPanel::OnMouseEnter,NULL,this);
      pPackagelk->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerPanel::OnMouseExit,NULL,this);
      propertiesGridSizer -> Add(pPackagelk, 0, wxEXPAND);

      //help icon
      wxButton* pPackageHlp = new wxButton(propertiesPanel, wxID_ANY, wxT("?"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
      pPackageHlp->Enable(false);
      tt = "When this package is included in the CMakeLists file, the\nfollowing line is included in the CMakeList.txt file in the\nproject folder:\n"
          "ADD_SUBDIRECTORY(" + packages[i]->GetName() + ")";
      pPackageHlp->SetToolTip(crea::std2wx(tt));

      propertiesGridSizer -> Add(pPackageHlp, 0, wxEXPAND | wxALIGN_CENTER);
    }

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanel->SetSizer(propertiesGridSizer);
  propertiesGridSizer->Fit(propertiesPanel);


  propertiesBox->Add(propertiesPanel, 1, wxALL | wxEXPAND, 5);

  sizer -> Add(propertiesBox, 0, wxEXPAND | wxALL, 10);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);

  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(1, 2, 9, 15);

  wxButton* createPkgbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_PACKAGE, _T("A. Create Package"));
  createPkgbt->SetToolTip(wxT("Create a new package for this project."));
  actionsGridSizer->Add(createPkgbt, 1, wxALL | wxEXPAND, 5);
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Edit the CMakeLists.txt file of this project."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMPackageManagerHelpDialog* helpDialog = new wxCDMPackageManagerHelpDialog(this->GetParent(), this, wxID_ANY);
      helpDialog->Show(true);
    }
}

modelCDMProject* wxCDMPackageManagerPanel::GetProject() const
{
  return this->project;
}

void wxCDMPackageManagerPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetClientData(project);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
}

void wxCDMPackageManagerPanel::OnChBPackageChange(wxCommandEvent& event)
{
  this->project->SetPackageInclude(
      crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()),
      ((wxCheckBox*)event.GetEventObject())->GetValue()
    );
}

void wxCDMPackageManagerPanel::OnLnkPackageSelect(wxHyperlinkEvent& event)
{
  modelCDMPackage* thePackage = NULL;
  std::vector<modelCDMPackage*> packages = this->project->GetPackages();
  for (int i = 0; i < (int)(packages.size()); i++)
    {
      if(packages[i]->GetName() == crea::wx2std(event.GetURL()))
        {
          thePackage = packages[i];
          break;
        }
    }

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
  newEvent->SetClientData(thePackage);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);

  wxCommandEvent* newEvent1 = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  newEvent1->SetClientData(thePackage);
  newEvent1->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent1);

}

void wxCDMPackageManagerPanel::OnBtnCreatePackage(wxCommandEvent& event)
{
  std::string* result;

  wxCDMNewPackageDialog* dialog = new wxCDMNewPackageDialog(this);
  long userResponse;
  userResponse = dialog->ShowModal();

  if(userResponse == wxID_FORWARD)
    {
      modelCDMIProjectTreeNode* package = this->project->CreatePackage(
          crea::wx2std(dialog->GetPackageName()),
          result,
          crea::wx2std(dialog->GetPackageAuthor()),
          crea::wx2std(dialog->GetPackageAuthorEmail()),
          crea::wx2std(dialog->GetPackageDescription())
      );
      if(package == NULL)
        {
          std::cout << "error creating package: " << *result << std::endl;
          wxMessageBox(crea::std2wx(*result),_T("New Package - Error!"),wxOK | wxICON_ERROR);
          event.Skip();
          return;
        }
      wxMessageBox(crea::std2wx("Package successfully created."),_T("New Package - Success!"),wxOK | wxICON_INFORMATION);

      //refreshing tree and description
      //send event instead of calling parent to avoid crashing

      ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
      newEvent->SetClientData(package);
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
      event.Skip();
    }

  event.Skip();
}

void wxCDMPackageManagerPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->project->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMPackageManagerPanel::OnMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);
  std::string PkgName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMPackage* thePackage = NULL;
  std::vector<modelCDMPackage*> packages = this->project->GetPackages();
  for (int i = 0; i < (int)(packages.size()); i++)
    {
      if(packages[i]->GetName() == PkgName)
        {
          thePackage = packages[i];
          break;
        }
    }
  newEvent->SetClientData(thePackage);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMPackageManagerPanel::OnMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);
  std::string PkgName = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  modelCDMPackage* thePackage = NULL;
  std::vector<modelCDMPackage*> packages = this->project->GetPackages();
  project->GetPackages();
  for (int i = 0; i < (int)(packages.size()); i++)
    {
      if(packages[i]->GetName() == PkgName)
        {
          thePackage = packages[i];
          break;
        }
    }
  newEvent->SetClientData(thePackage);
  newEvent->SetId(0);
  wxPostEvent(this->GetParent(), *newEvent);
  event.Skip();
}

void wxCDMPackageManagerPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMPackageManagerPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->project->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->project->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}


