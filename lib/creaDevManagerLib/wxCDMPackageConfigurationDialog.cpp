/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMPackageConfigurationDialog.cpp
 *
 *  Created on: 6/4/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMPackageConfigurationDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMPackageConfigurationDialog, wxDialog)
EVT_BUTTON(wxID_OK, wxCDMPackageConfigurationDialog::OnFinish)
EVT_CHECKBOX(ID_CHECK_INCLUDE_3RDLIBRARY, wxCDMPackageConfigurationDialog::On3rdLibraryIncludeChange)
EVT_CHECKBOX(ID_CHECK_INCLUDE_LIBRARY, wxCDMPackageConfigurationDialog::OnCustomLibraryIncludeChange)
END_EVENT_TABLE()

wxCDMPackageConfigurationDialog::wxCDMPackageConfigurationDialog(
    wxWindow* parent,
    modelCDMPackage* package,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  this->package = package;
  wxCDMPackageConfigurationDialog::Create(parent, id, caption, position, size, style);
}

wxCDMPackageConfigurationDialog::~wxCDMPackageConfigurationDialog()
{
}

bool wxCDMPackageConfigurationDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMPackageConfigurationDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Please select the libraries that are used in this package."), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxEXPAND | wxALIGN_LEFT | wxALL, 5);

  wxScrolledWindow* includesPanel = new wxScrolledWindow(this);
  includesPanel->FitInside();
  includesPanel->SetScrollRate(5,5);

  wxBoxSizer* includesPanelSizer = new wxBoxSizer(wxVERTICAL);

    //Third Party Libraries
    wxStaticText* Title1 = new wxStaticText(includesPanel, wxID_ANY, wxT("Third Party Libraries:"));
    wxFont font = Title1->GetFont();
    font.SetWeight(wxFONTWEIGHT_BOLD);
    Title1->SetFont(font);
    includesPanelSizer->Add(Title1, 0, wxEXPAND);

    //inclusion data
    std::map<std::string, bool> inclusions = this->package->Get3rdPartyLibraries();
    //includesGrid Sizer
    wxFlexGridSizer* includesGridSizer = new wxFlexGridSizer(inclusions.size()+1, 2, 0, 5);

    wxStaticText* ChBTitle = new wxStaticText(
        includesPanel,
        wxID_ANY,
        wxT("Included"),
        wxDefaultPosition,
        wxDefaultSize,
        wxALIGN_CENTER
      );
    wxStaticText* LNmTitle = new wxStaticText(
        includesPanel,
        wxID_ANY,
        wxT("Library Name"),
        wxDefaultPosition,
        wxDefaultSize,
        wxALIGN_LEFT
      );

    includesGridSizer->Add(ChBTitle, 1, wxEXPAND);
    includesGridSizer->Add(LNmTitle, 1, wxEXPAND);

    for (std::map<std::string, bool>::iterator it = inclusions.begin(); it != inclusions.end(); ++it) {
      wxCheckBox* ChBIncl = new wxCheckBox(
          includesPanel, ID_CHECK_INCLUDE_3RDLIBRARY, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT
        );
      ChBIncl->SetToolTip(crea::std2wx(
          "When this box is checked the " + it->first + " library\n"
          "is included in the project configuration for\n"
          "this package including the following instruction\n"
          "in the package's folder CMakeLists.txt file:\n"
          "SET(${BBTK_PACKAGE_NAME}_USE_" + it->first+ "  ON)\n"
          ));
      ChBIncl->SetName(crea::std2wx(it->first));
      ChBIncl->SetValue(it->second);
      includesGridSizer->Add(ChBIncl, 0, wxEXPAND);

      wxStaticText* LNmIncl = new wxStaticText(includesPanel, wxID_ANY, crea::std2wx(it->first));
      includesGridSizer->Add(LNmIncl, 1, wxEXPAND);
    }

    includesGridSizer->AddGrowableCol(1,1);

    includesPanelSizer->Add(includesGridSizer, 1, wxEXPAND | wxLEFT, 5);

    //Custom Libraries
    wxStaticText* Title2 = new wxStaticText(includesPanel, wxID_ANY, wxT("Custom Libraries:"));
    font = Title2->GetFont();
    font.SetWeight(wxFONTWEIGHT_BOLD);
    Title2->SetFont(font);
    includesPanelSizer->Add(Title2, 0, wxEXPAND);

    //inclusion data
    std::map<std::string, bool> inclusionsLibs = this->package->GetCustomLibraries();
    //includesGrid Sizer
    wxFlexGridSizer* includesLibGridSizer = new wxFlexGridSizer(inclusionsLibs.size()+1, 2, 0, 5);

    wxStaticText* ChBTitle1 = new wxStaticText(
        includesPanel,
        wxID_ANY,
        wxT("Included"),
        wxDefaultPosition,
        wxDefaultSize,
        wxALIGN_CENTER
      );
    wxStaticText* LNmTitle1 = new wxStaticText(
        includesPanel,
        wxID_ANY,
        wxT("Library Name"),
        wxDefaultPosition,
        wxDefaultSize,
        wxALIGN_LEFT
      );

    includesLibGridSizer->Add(ChBTitle1, 1, wxEXPAND);
    includesLibGridSizer->Add(LNmTitle1, 1, wxEXPAND);

    for (std::map<std::string, bool>::iterator it = inclusionsLibs.begin(); it != inclusionsLibs.end(); ++it) {
      wxCheckBox* ChBIncl = new wxCheckBox(
          includesPanel, ID_CHECK_INCLUDE_LIBRARY, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT
        );
      ChBIncl->SetToolTip(crea::std2wx(
          "When this box is checked the " + it->first + " custom\n"
          "library is included in the project configuration for\n"
          "this packages including the following code in the\n"
          "package's folder CMakeLists.txt file:\n"
          "SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS\n"
          "  ../lib/"+ it->first + "\n"
          ")\n"
          "SET(${BBTK_PACKAGE_NAME}_LIBS\n"
          "  " + it->first+ "\n"
          ")"));
      ChBIncl->SetName(crea::std2wx(it->first));
      ChBIncl->SetValue(it->second);
      includesLibGridSizer->Add(ChBIncl, 0, wxEXPAND);

      wxStaticText* LNmIncl = new wxStaticText(includesPanel, wxID_ANY, crea::std2wx(it->first));
      includesLibGridSizer->Add(LNmIncl, 1, wxEXPAND);
    }

    includesLibGridSizer->AddGrowableCol(1,1);

    includesPanelSizer->Add(includesLibGridSizer, 0, wxEXPAND | wxLEFT, 5);

    includesPanel->SetSizer(includesPanelSizer);

    v_sizer1->Add(includesPanel, 1, wxEXPAND | wxALL, 10);

  v_sizer1->Add(new wxButton(this, wxID_OK, wxT("Close")), 0, wxALIGN_CENTER | wxRIGHT | wxBOTTOM, 30);

  SetSizer(v_sizer1);
}

void wxCDMPackageConfigurationDialog::OnFinish(wxCommandEvent& event)
{
  this->EndModal(wxID_OK);
}

void wxCDMPackageConfigurationDialog::On3rdLibraryIncludeChange(
    wxCommandEvent& event)
{
  if(this->package->Set3rdPartyLibrary(crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()), ((wxCheckBox*)event.GetEventObject())->GetValue()))
    ((wxCheckBox*)event.GetEventObject())->SetValue(((wxCheckBox*)event.GetEventObject())->GetValue());
  else
    ((wxCheckBox*)event.GetEventObject())->SetValue(!((wxCheckBox*)event.GetEventObject())->GetValue());
}

void wxCDMPackageConfigurationDialog::OnCustomLibraryIncludeChange(
    wxCommandEvent& event)
{
  if(this->package->SetCustomLibrary(crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()), ((wxCheckBox*)event.GetEventObject())->GetValue()))
      ((wxCheckBox*)event.GetEventObject())->SetValue(((wxCheckBox*)event.GetEventObject())->GetValue());
    else
      ((wxCheckBox*)event.GetEventObject())->SetValue(!((wxCheckBox*)event.GetEventObject())->GetValue());
}
