/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMFolder.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMFolder.h"

#include <fstream>
#include <algorithm>

#include <creaWx.h>
#include <wx/dir.h>

#include "CDMUtilities.h"

modelCDMFolder::modelCDMFolder()
{
  this->CMakeLists = NULL;
}

modelCDMFolder::modelCDMFolder(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating folder: " + path + "\n";
  //set attributes
  this->parent = parent;
  this->children.clear();
  this->level = level;
  this->CMakeLists = NULL;
  this->length = 0;
  this->name = name;
  this->path = path;
  this->type = wxDIR_DIRS;

  std::string pathFixed(CDMUtilities::fixPath(path));
  //check all folders
  wxDir dir(crea::std2wx((pathFixed).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          //if is an unknown folder, create folder
          modelCDMFolder* folder = new modelCDMFolder(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
          this->children.push_back(folder);
          this->folders.push_back(folder);

          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::cout << "analyzing " << stdfileName << std::endl;
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";
          std::cout << "fileType: " << fileType <<std::endl;

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              this->CMakeLists = new modelCDMCMakeListsFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->CMakeLists);
            }
          //if is a code file, create code file
          else if(fileType == ".c" ||
              fileType == ".cxx" ||
              fileType == ".h" ||
              fileType == ".cpp" ||
              fileType == ".txx" ||
              fileType == ".cmake" )
            {
              modelCDMCodeFile* file = new modelCDMCodeFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
            }
          //if is a bbs file, create bbs file
          else if(fileType == ".bbs")
            {
              modelCDMBBSFile* file = new modelCDMBBSFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
            }
          //if is a bbg file, create bbg file
          else if(fileType == ".bbg")
            {
              modelCDMBBGFile* file = new modelCDMBBGFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
            }
          //if is an unknown file, create file
          else
            {
              this->children.push_back(new modelCDMFile(this, pathFixed + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }
    }

  this->SortChildren();
  std::sort(this->folders.begin(), this->folders.end(), CompareNodeItem);
}

modelCDMFolder::~modelCDMFolder()
{
  this->folders.clear();
  this->CMakeLists = NULL;
  for (int i = 0; i < (int)(this->children.size()); i++)
    {
      if(this->children[i] != NULL)
        {
          delete this->children[i];
          this->children[i] = NULL;
        }
    }
  this->children.clear();
}

bool modelCDMFolder::CreateClass(const std::string& name)
{
  if (!CDMUtilities::createEmptyClass(name, this->path))
      {
        return false;
      }
    else
      {
        this->children.push_back(new modelCDMFile(this, this->path + CDMUtilities::SLASH + name + ".h", name + ".h", this->level + 1));
        this->children.push_back(new modelCDMFile(this, this->path + CDMUtilities::SLASH + name + ".cpp", name + ".cpp", this->level + 1));
        this->SortChildren();
        return true;
      }
}

modelCDMFolder* modelCDMFolder::CreateFolder(const std::string& name, std::string*& result)
{
  //TODO:: mkdir depending on OS
  std::string command = "mkdir \"" + path + CDMUtilities::SLASH + name + "\"";
  if(system(command.c_str()))
    {
      result = new std::string("Error executing: " + command + ".");
      return NULL;
    }
  modelCDMFolder* folder = new modelCDMFolder(this, path + CDMUtilities::SLASH + name, name, level + 1);
  this->folders.push_back(folder);
  this->children.push_back(folder);

  return folder;
}

bool modelCDMFolder::OpenCMakeListsFile(std::string*& result)
{
  if (this->CMakeLists == NULL)
    {
      result = new std::string("There's no CMakeLists file to open.");
      return false;
    }
  if (!CDMUtilities::openTextEditor(this->CMakeLists->GetPath()))
    return true;
  else
    {
      result = new std::string("Couldn't open CMakeLists file.");
      return false;
    }
}

const bool modelCDMFolder::Refresh(std::string*& result)
{
  //std::cout << "refreshing folder " << this->name << std::endl;
  //set attributes
  this->type = wxDIR_DIRS;

  //check children
  std::vector<bool> checked(this->children.size(), false);
  std::vector<bool> checkedFolders(this->folders.size(), false);

  //check all folders
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          //check if they already exist
          bool found = false;
          for (int i = 0; !found && i < (int)(this->folders.size()); i++)
            {
              if (this->folders[i]->GetName() == stdfileName)
                {
                  found = true;
                  int pos = std::find(this->children.begin(), this->children.end(), this->folders[i]) - this->children.begin();
                  checked[pos] = true;
                  checkedFolders[i] = true;
                  if(!this->folders[i]->Refresh(result))
                    return false;
                }
            }
          if(!found)
            {
              modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->folders.push_back(folder);
              this->children.push_back(folder);
            }
          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }

          //if is an unknown file, create file
          else
            {
              bool found = false;
              for (int i = 0;!found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  //if is a code file, create modelCDMCodeFile
                  if(
                      fileType == ".c" ||
                      fileType == ".cxx" ||
                      fileType == ".h" ||
                      fileType == ".cpp" ||
                      fileType == ".txx" ||
                      fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  //if is a bbs file, create modelCDMBBSFile
                  else if(fileType == ".bbs")
                    {
                      this->children.push_back(new modelCDMBBSFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  //if is a bbg file, create modelCDMBBGFile
                  else if(fileType == ".bbg")
                    {
                      this->children.push_back(new modelCDMBBGFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  //if is an unknown file, create modelCDMFile
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }
    }

  for (int i = 0; i < (int)(checkedFolders.size()); i++)
    {
      if(!checkedFolders[i])
        {
          this->folders.erase(this->folders.begin()+i);
          checkedFolders.erase(checkedFolders.begin()+i);
          i--;
        }
    }
  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }
  this->SortChildren();
  std::sort(this->folders.begin(), this->folders.end(), CompareNodeItem);
  return true;
}

modelCDMCMakeListsFile* modelCDMFolder::GetCMakeLists() const
{
  return this->CMakeLists;
}

std::vector<modelCDMFolder*> modelCDMFolder::GetFolders() const
{
  return this->folders;
}

bool
modelCDMFolder::HasCMakeLists()
{
  return this->CMakeLists != NULL;
}
