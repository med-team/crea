/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


#ifndef _$PROJECT_NAME$SYSTEM_H_
#define _$PROJECT_NAME$SYSTEM_H_


// Windoze related troubles (as usual)

//-----------------------------------------------------------------------------

#if defined(_WIN32)
  #ifdef $PROJECT_NAME$_EXPORT_SYMBOLS
    #define $PROJECT_NAME$_EXPORT __declspec( dllexport )
#else
    #define $PROJECT_NAME$_EXPORT __declspec( dllimport )
  #endif
  #define $PROJECT_NAME$_CDECL __cdecl
#else
  #define $PROJECT_NAME$_EXPORT
  #define $PROJECT_NAME$_CDECL
#endif // defined(_WIN32)

#ifdef __BORLANDC__
  #include <mem.h>
#endif

#endif
