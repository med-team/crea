/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMBlackBoxDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMFileDescriptionPanel.h"

#include<sstream>

#include "wxCDMMainFrame.h"

#include "creaDevManagerIds.h"
#include "images/FlIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMFileDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMFileDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMFileDescriptionPanel::OnBtnOpenFolder)
EVT_BUTTON(ID_BUTTON_OPEN_COMMAND, wxCDMFileDescriptionPanel::OnBtnOpenWithCommand)
END_EVENT_TABLE()

wxCDMFileDescriptionPanel::wxCDMFileDescriptionPanel(
    wxWindow* parent,
    modelCDMFile* file,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMFileDescriptionPanel::Create(parent, file, id, caption, pos, size, style);
}

wxCDMFileDescriptionPanel::~wxCDMFileDescriptionPanel()
{
}

bool wxCDMFileDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMFile* file,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->file = file;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMFileDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->file->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
      returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->file->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(FlIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("File")),0, wxALIGN_LEFT, 0);
    //File Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->file->GetName())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //File Properties
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Properties"));
  wxPanel* propertiesPanel = new wxPanel(this);
  wxBoxSizer* propertiesPanelSizer = new wxBoxSizer(wxVERTICAL);
  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(4, 2, 9, 15);

  wxStaticText *pLocation = new wxStaticText(propertiesPanel, -1, wxT("Location"));
  wxStaticText *pLength = new wxStaticText(propertiesPanel, -1, wxT("File Size"));

  wxStaticText* pLocationtc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->file->GetPath()));
  pLocationtc->SetMaxSize(wxSize(300,-1));
  int lgth = this->file->GetLength();
  std::stringstream ss;
  ss << lgth / 1024;
  std::string lgths = ss.str() + " KB";
  wxStaticText* pLengthtc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(lgths));

  propertiesGridSizer->Add(pLocation, 0, wxALIGN_RIGHT | wxALIGN_TOP);
  propertiesGridSizer->Add(pLocationtc, 1, wxEXPAND);
  propertiesGridSizer->Add(pLength, 0, wxALIGN_RIGHT | wxALIGN_TOP);
  propertiesGridSizer->Add(pLengthtc, 1, wxEXPAND);

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanelSizer -> Add(propertiesGridSizer, 1, wxEXPAND);
  propertiesPanel->SetSizer(propertiesPanelSizer);
  propertiesPanelSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 1, wxALL|wxEXPAND, 5);

  sizer->Add(propertiesBox, 0, wxEXPAND | wxALL, 10);


  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(1, 2, 9, 15);

  wxButton* openCommandbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_COMMAND, _T("Open with Command"));
  openCommandbt->SetToolTip(wxT("Open this file executing a command in a terminal/command line."));
  actionsGridSizer->Add(openCommandbt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open Containing Folder"));
  openFolderbt->SetToolTip(wxT("Open the folder where this file is located in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);
}

void wxCDMFileDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->file->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMFileDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->file->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMFileDescriptionPanel::OnBtnOpenWithCommand(wxCommandEvent& event)
{
  //get command
  wxString commandEx = wxGetTextFromUser(
      _T("Enter the command to execute file"),
      _T("Execute File - creaDevManager"),
      _T("")
  );
  //check name
  if(commandEx.Len() > 0)
    {
      std::string* result;
      if(!this->file->OpenFile(result, crea::wx2std(commandEx)))
        wxMessageBox(crea::std2wx(*result),_T("Execute File - Error!"),wxOK | wxICON_ERROR);
    }
}
