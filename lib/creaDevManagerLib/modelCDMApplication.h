/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMApplication.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMAPPLICATION_H_
#define MODELCDMAPPLICATION_H_

#include<iostream>
#include<vector>
#include<map>

#include "modelCDMFolder.h"
#include "modelCDMFile.h"
#include "modelCDMCodeFile.h"

/**
 * Class representing an application in a Crea project. An Application is an stand alone application that uses the project libraries to show their functionalities.
 */
class modelCDMApplication : public modelCDMFolder
{
public:
  /**
   * Default Constructor.
   */
  modelCDMApplication();
  /**
   * Application Constructor
   * @param parent Parent node of the application node.
   * @param path Full path of the application node.
   * @param name Name of the applcation folder node.
   * @param level Level of the application node folder in the project.
   */
  modelCDMApplication(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level = 2);
  /**
   * Destructor.
   */
  ~modelCDMApplication();

  /**
   * Returns the executable name of the application node
   * @return
   */
  const std::string& GetExecutableName() const;
  /**
   * Returns the main source file of the application node. That is, the file that contains the main method.
   * @return File reference to main file.
   */
  modelCDMFile* GetMainFile() const;

  /**
   * Sets the executable name for the application.
   * @param fileName Name of the application executable.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool SetExecutableName(const std::string& fileName, std::string*& result);

  /**
   * Creates a folder in the application folder node. This takes effect in the system as well as in the project model.
   * @param name Name of the new folder.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  modelCDMFolder* CreateFolder(const std::string& name, std::string*& result);

  /**
   * Refreshes the structure of the application. Removes folders and files deleted since the last refresh. Also, adds folders and files created since the las refresh.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Checks the CMakeLists file and the application structure to identify registration errors before compiling the project.
   * @param properties Properties found in the structure.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks the application's CMakeLists file to check which third party libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> Get3rdPartyLibraries();

  /**
   * Sets the 3rd party library inclusion in the application's CMakeLists file.
   * @return if the operation was successful.
   */
  bool Set3rdPartyLibrary(const std::string& library_name, const bool& toInclude);

  /**
   * Checks the application's CMakeLists file to check which custom libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> GetCustomLibraries();

  /**
   * Sets the custom library inclusion in the application's CMakeLists file.
   * @return if the operation was successful.
   */
  bool SetCustomLibrary(const std::string& library_name, const bool& toInclude);

private:
  /**
   * Name of the application executable file.
   */
  std::string executableName;
  /**
   * Reference to the main file of the application.
   */
  modelCDMFile* mainFile;
  /**
   * Reference array of the children folders of the application.
   */
  std::vector<modelCDMFolder*> folders;
};

#endif /* MODELCDMAPPLICATION_H_ */
