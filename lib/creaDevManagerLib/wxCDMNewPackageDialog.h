/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMNewPackageDialog.h
 *
 *  Created on: 05/12/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMNEWPACKAGEDIALOG_H_
#define WXCDMNEWPACKAGEDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>

/**
 * Dialog to create a new Package in a Crea Project.
 */
class wxCDMNewPackageDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * New Package Dialog Constructor.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Package".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 500, 400.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMNewPackageDialog(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Package"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(500,400),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMNewPackageDialog();
  /**
   * New Package Dialog Creator.
   * @param parent Parent window reference.
   * @param id Dialog ID. By default wxID_ANY.
   * @param caption Dialog label. By default "New Package".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 500, 400.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("New Package"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(500,400),
      long style = wxDEFAULT_DIALOG_STYLE
  );

  /**
   * Returns the package name chosen by the user.
   * @return Package name.
   */
  const wxString GetPackageName();
  /**
   * Returns the package author name chosen by the user.
   * @return Package author name.
   */
  const wxString GetPackageAuthor();
  /**
   * Returns the package author's email chosen by the user.
   * @return Package author's email.
   */
  const wxString GetPackageAuthorEmail();
  /**
   * Returns the package description chosen by the user.
   * @return Package description.
   */
  const wxString GetPackageDescription();

protected:
  /**
   * Creates the visual controls of the Dialog.
   */
  void CreateControls();

private:
  /**
   * Package name.
   */
  wxTextCtrl* packageName;
  /**
   * Package author name.
   */
  wxTextCtrl* packageAuthor;
  /**
   * Package author's email.
   */
  wxTextCtrl* packageAuthorEmail;
  /**
   * Package description.
   */
  wxTextCtrl* packageDescription;

  //handlers
protected:
  /**
   * Handles when the create package button is pressed.
   * @param event Unused event.
   */
  void OnCreatePackage(wxCommandEvent& event);
  /**
   * Handles when the cancel button is pressed.
   * @param event Unused event.
   */
  void OnCancel(wxCommandEvent& event);
};

#endif /* WXCDMNEWPACKAGEDIALOG_H_ */
