/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * CDMUtilities.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef CDMUTILITIES_H_
#define CDMUTILITIES_H_

#include<iostream>
#include<vector>
#include<cstddef>

namespace CDMUtilities
{
  /**
   * Path slash
   */
#ifdef _WIN32
  // ------ Windows
  static std::string SLASH = "\\";
#elif __APPLE__
  // ------ Apple
  static std::string SLASH = "/";
#else
  static std::string SLASH = "/";
#endif

  /**
   * Text editor program
   */
#ifdef _WIN32
  // ------ Windows
  static std::string TEXT_EDITOR = "notepad";
#elif __APPLE__
  // ------ Apple
  //TODO: implementation for apple
#else
  static std::string TEXT_EDITOR = "gedit";
#endif

  /**
   * File explorer program
   */
#ifdef _WIN32
  // ------ Windows
  static std::string FILE_EXPLORER = "explorer";
#elif __APPLE__
  // ------ Apple
  //TODO: implementation for apple
#else
  static std::string FILE_EXPLORER = "nautilus";
#endif

  /**
   * Terminal program
   */
#ifdef _WIN32
  // ------ Windows
  static std::string TERMINAL = "start cmd.exe";
#elif __APPLE__
  // ------ Apple
  //TODO: implementation for apple
#else
  static std::string TERMINAL = "gnome-terminal";
#endif


//EED 10/07/2013
//#ifndef _WIN32
  /**
   * Build Command
   */
  static std::string BUILD_COMMAND = "make";
//#endif

  /**
   * Structure that handles the split method for c++
   * It calls the split method to split a string given certain delimiters.
   */
  struct splitter
  {
    /**
     * Enum to allow or not empty resulting strings after performing splits.
     */
    enum empties_t { empties_ok, no_empties };
    /**
     * Method to split a string given a set of delimiter characters.
     * @param result Resulting container.
     * @param s String to be splitted.
     * @param delimiters Delimiter characters to split the string.
     * @param empties Either allow or not empty resulting strings after performing split.
     * @return Resulting container.
     */
    template <typename Container>
    static Container& split
    (
        Container& result,
        const typename Container::value_type& s,
        const typename Container::value_type& delimiters,
        empties_t empties = empties_ok
      )
      {
        result.clear();
        size_t current;
        size_t next = -1;
        do
        {
          if (empties == no_empties)
          {
            next = s.find_first_not_of(delimiters, next + 1);
            if (next == Container::value_type::npos)
            {
              break;
            }
            next -= 1;
          }
          current = next + 1;
          next = s.find_first_of(delimiters, current);
          result.push_back(s.substr(current, next - current));
        }
        while (next != Container::value_type::npos);
        return result;
      }
  };

  /**
   * Fixes a given path to avoid double slash directories
   * @param path Unfixed path.
   * @return Fixed path.
   */
  const std::string fixPath(const std::string& path);

  /**
   * Opens the default text editor. If a file is given, then it tries to open the given file.
   * @param file Full path to the file.
   * @return True if there was an error on the execution of the operation.
   */
  int openTextEditor(const std::string& file = "");
  /**
   * Opens the system file explorer on the given file path
   * @param file Path of the desired folder to open.
   * @return True if there was an error on the execution of the operation.
   */
  int openFileExplorer(const std::string& file = "");
  /**
   * Opens a file with a given command.
   * @param file Full path of the file to open.
   * @param command Command to execute the file with.
   * @param parameters Parameters to open file.
   * @return True if there was an error on the execution of the operation.
   */
  int openFileWithCommand(const std::string& file, const std::string& command, const std::string& parameters = "");
  /**
   * Opens the BBTK Graphical Editor
   * @return True if there was an error on the execution of the operation.
   */
  int openBBEditor();
  /**
   * Opens the minitools or the creaTools
   * @return True if there was an error on the execution of the operation.
   */
  int openCreaToolsTools();
  /**
   * Open a command line interpreter and executes the given command if any.
   * @param command Command to execute.
   * @return True if there was an error on the execution of the operation.
   */
  int openTerminal(const std::string& command = "");
  /**
   * Creates a blank class(.h and .cpp files).
   * @param name Name of the new class.
   * @param path Path where the class is to be created.
   * @return True if the class was successfully created.
   */
  bool createEmptyClass(const std::string& name, const std::string& path);
  /**
   * Creates a string replacing each \ by double \ .
   * @param line String to stringify.
   * @return line stringified.
   */
  std::string stringify(const std::string& line);

  //CMakeLists file handling
  /**
   * Type definition for the value of a syntax element for CMakeLists files
   */
  typedef std::vector<std::string> cmdValue;

  /**
   * Type definition for the type of a syntax element for CMakeLists files
   */
  typedef std::string cmdType;

  /**
   * Type definition for syntax elements of a CMakeLists file
   */
  typedef std::pair<cmdType,cmdValue> syntaxElement;

  /**
   * Type definition for describing a CMakeLists file content
   */
  typedef std::vector<syntaxElement> CMLFile;

  /**
   * Reads a file as string and returns the read data.
   * @param file_path Full path of the CMakeLists file.
   * @return A string with the contents of the given file.
   */
  std::string readFile(const std::string& file_path);
  /**
   * Writes the given string into a file and returns whether the operation is successful.
   * @param file_path Full path of the CMakeLists file.
   * @param st string to write.
   * @return True if the operation was successful.
   */
  bool writeFile(const std::string& file_path, const std::string& st);

  /**
   * Reads a CMakeLists file and returns the read data.
   * @param file_path Full path of the CMakeLists file.
   * @return A CMLFile with the contents of the given file.
   */
  CMLFile readCMLFile(const std::string& file_path);

  /**
   * Writes the given data into specified CMakeLists file.
   * @param file_path Full path of the CMakeLists file.
   * @param data CMakeLists data.
   * @return True if the operation was successful.
   */
  bool writeCMLFile(const std::string& file_path, const CMLFile& data);

  /**
   * @param st Strips all space character at the beginning and at the end of the string.
   */
  void normalizeStr(std::string& st);

};

#endif /* CDMUTILITIES_H_ */
