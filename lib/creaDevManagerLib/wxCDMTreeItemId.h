/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sante)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
*/


/*
 * wxCDMTreeItemId.h
 *
 *  Created on: 29/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef _WXCDMTREEITEMID_H_
#define _WXCSMTREEITEMID_H_


#include <creaWx.h>
#include <wx/treectrl.h>
#include <wx/treebase.h>
#include <iostream>

/**
 * Class Name: wxCDMTreeItemId
 * Class Id manager for the project tree control.
 */
class wxCDMTreeItemId
{

//---------------------------------------------
//Methods and attributes exposed to other classes
//---------------------------------------------
public :
  /**
   * Default Constructor
   */
  wxCDMTreeItemId();
  /**
   * Constructor
   * @param id Original TreeItem Id.
   */
  wxCDMTreeItemId(const wxTreeItemId& id);
  /**
   * Destructor
   */
  ~wxCDMTreeItemId();

  /**
   * Retrieves the id from the wxTreeItemId object
   * @return id name
   */
  const wxTreeItemId& GetWxId() const;
  /**
   * Retrieves the private id of the tree item.
   * @return id value
   */
  const wxTreeItemIdValue& GetId() const;

  /**
   * Operator ==
   * @param id comparing tree item id
   * @return true if the id value is the same.
   */
  bool operator == (const wxCDMTreeItemId& id) const;
  /**
   * Operator !=
   * @param id comparing tree item id
   * @return true if the id value is not the same.
   */
  bool operator != (const wxCDMTreeItemId& id) const;
  /**
   * Operator <
   * @param id comparing tree item id
   * @return true if the id value is less than the one in the given id.
   */
  bool operator < (const wxCDMTreeItemId& id) const;
  /**
   * operator << allows to manage output stream printing of the object.
   * @param stream Target stream.
   * @param id Tree item id to print.
   * @return Stream modified.
   */
  friend std::ostream& operator << (std::ostream& stream, const wxCDMTreeItemId& id);

//---------------------------------------------
//Methods and attributes exposed only to classes
//that are derived from this class
//---------------------------------------------
protected:
  /**
   * Real tree item
   */
  wxTreeItemId _idWx;
  /**
   * Tree item value
   */
  wxTreeItemIdValue _id;
//---------------------------------------------
//Methods and attributes only visible by this class
//---------------------------------------------
private:

};

//-end of _WXCDMTREEITEMID_H_------------------------------------------------------
#endif
