/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMNewPackageDialog.cpp
 *
 *  Created on: 05/12/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMNewPackageDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMNewPackageDialog, wxDialog)
  EVT_BUTTON(ID_BUTTON_NEXT, wxCDMNewPackageDialog::OnCreatePackage)
  EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMNewPackageDialog::OnCancel)
END_EVENT_TABLE()

wxCDMNewPackageDialog::wxCDMNewPackageDialog(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMNewPackageDialog::Create(parent, id, caption, position, size, style);
}

wxCDMNewPackageDialog::~wxCDMNewPackageDialog()
{
}

bool wxCDMNewPackageDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

const wxString wxCDMNewPackageDialog::GetPackageName()
{
  return this->packageName->GetValue();
}
const wxString wxCDMNewPackageDialog::GetPackageAuthor()
{
  return this->packageAuthor->GetValue();
}
const wxString wxCDMNewPackageDialog::GetPackageAuthorEmail()
{
  return this->packageAuthorEmail->GetValue();
}
const wxString wxCDMNewPackageDialog::GetPackageDescription()
{
  return this->packageDescription->GetValue();
}

void wxCDMNewPackageDialog::CreateControls()
{
  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Create a new package"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(this, wxID_ANY, wxT("Please fill the following details."), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
  v_sizer1->Add(instruction, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxFlexGridSizer* formItems = new wxFlexGridSizer(4,2,9,15);

  wxStaticText *stxtPrjName = new wxStaticText(this, -1, wxT("Package Name"));
  wxStaticText *stxtPrjAuth = new wxStaticText(this, -1, wxT("Package's Author (1 word)"));
  wxStaticText *stxtPrjAuthEmail = new wxStaticText(this, -1, wxT("Package's Author Email"));
  wxStaticText *stxtPrjPkg = new wxStaticText(this, -1, wxT("Package's Description (HTML)"));

  this->packageName = new wxTextCtrl(this, -1);
  this->packageAuthor = new wxTextCtrl(this, -1);
  this->packageAuthorEmail = new wxTextCtrl(this, -1);
  this->packageDescription = new wxTextCtrl(this, -1, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);

  formItems->Add(stxtPrjName, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->packageName, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjAuth, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->packageAuthor, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjAuthEmail, 0, wxALIGN_CENTER_VERTICAL);
  formItems->Add(this->packageAuthorEmail, 1, wxEXPAND | wxALIGN_CENTER_VERTICAL);
  formItems->Add(stxtPrjPkg);
  formItems->Add(this->packageDescription, 1, wxEXPAND);

  formItems->AddGrowableCol(1,1);
  formItems->AddGrowableRow(3,1);

  v_sizer1->Add(formItems, 1, wxEXPAND | wxALL, 15);

  wxBoxSizer* h_sizer2 = new wxBoxSizer(wxHORIZONTAL);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_NEXT, wxT("Create Package")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
  h_sizer2->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Cancel")), 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

  v_sizer1->Add(h_sizer2, 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
}

void wxCDMNewPackageDialog::OnCreatePackage(wxCommandEvent& event)
{
  bool ready = true;

  if(ready && this->packageName->GetValue() == wxT(""))
    {
      wxMessageBox(wxT("The package name cannot be empty"),_T("Error"),wxOK | wxICON_ERROR);
      ready = false;
    }
  if(ready && this->packageAuthor->GetValue() == wxT(""))
    {
      wxMessageBox(wxT("The package's author cannot be empty"),_T("Error"),wxOK | wxICON_ERROR);
      ready = false;
    }

  if(ready)
    {
      this->EndModal(wxID_FORWARD);
    }

  event.Skip();
}

void wxCDMNewPackageDialog::OnCancel(wxCommandEvent& event)
{
  this->EndModal(wxID_CANCEL);
  event.Skip();
}
