/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMMainHelpDialog.cpp
 *
 *  Created on: 7/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMMainHelpDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMMainHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMMainHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_NEWPROJECT, wxCDMMainHelpDialog::OnNewProject)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMMainHelpDialog::OnOpenProject)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMMainHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMMainHelpDialog::wxCDMMainHelpDialog(
    wxWindow* parent,
    wxCDMMainDescriptionPanel* mainDescription,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMMainHelpDialog::Create(parent, id, caption, position, size, style);
  this->mainDescription = mainDescription;
}

wxCDMMainHelpDialog::~wxCDMMainHelpDialog()
{
}

bool wxCDMMainHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMMainHelpDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Welcome to the Crea Development Manager"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "This application was made to help you in the creation process of Crea projects. This help dialogs will explain what "
          "each part of the program does and will give you tips to get your project up and running in no time. You can disable "
          "them by checking the \"Disable Help\" option, or you can also enable or disable them at any time checking the \"Help "
          "Dialogs\" option in the Help menu.\n To begin working on your projects you can either create a new project or open an "
          "already existing project.\n"),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

//  wxFlexGridSizer* formItems = new wxFlexGridSizer(1,2,9,15);
//
//  wxButton* createPackageBtn = new wxButton(this, ID_BUTTON_NEWPROJECT, wxT("New Project"));
//  wxButton* editCMakeBtn= new wxButton(this, ID_BUTTON_OPENPROJECT, wxT("Open Project"));
//
//  formItems->Add(createPackageBtn, 1, wxALIGN_CENTER);
//  formItems->Add(editCMakeBtn, 1, wxALIGN_CENTER);
//
//  formItems->AddGrowableCol(0,1);
//  formItems->AddGrowableCol(1,1);
//
//  v_sizer1->Add(formItems, 1, wxEXPAND | wxALL, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMMainHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMMainHelpDialog::OnNewProject(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
  event.Skip();

  this->EndDialog(wxID_OK);
}

void wxCDMMainHelpDialog::OnOpenProject(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
    event.Skip();

  this->EndDialog(wxID_OK);
}

void wxCDMMainHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
