/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMLibrary.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMLibrary.h"

#include "modelCDMLib.h"

#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/regex.hpp>

#include "CDMUtilities.h"
#include "creaWx.h"
#include "wx/dir.h"

modelCDMLibrary::modelCDMLibrary()
{
}

modelCDMLibrary::modelCDMLibrary(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating library: " + path + "\n";
  this->parent = parent;
  //folder name
  this->name = name;
  //path
  this->path = CDMUtilities::fixPath(path);
  //type
  this->type = wxDIR_DIRS;
  //level
  this->level = level;

  //open CMakeList
  std::string pathMakeLists = path + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //get sets
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get library name
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "LIBRARY_NAME")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }

              this->nameLibrary = word;
            }
        }
    }

  confFile.close();

  //add library contents

  this->children.clear();
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      //folders
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);

          modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
          this->folders.push_back(folder);
          this->children.push_back(folder);

          cont = dir.GetNext(&fileName);
        }
      //files
      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->CMakeLists);
            }
          //if is a code file, create code file
          else if(fileType == ".c" ||
              fileType == ".cxx" ||
              fileType == ".h" ||
              fileType == ".cpp" ||
              fileType == ".txx" ||
              fileType == ".cmake" )
            {
              modelCDMCodeFile* file = new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
            }
          //if is an unknown file, create file
          else
            {
              this->children.push_back(new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }
    }
  this->SortChildren();
  std::sort(this->folders.begin(), this->folders.end(), CompareNodeItem);
}

modelCDMLibrary::~modelCDMLibrary()
{
}

const std::string& modelCDMLibrary::GetNameLibrary() const
{
  return this->nameLibrary;
}

bool modelCDMLibrary::SetNameLibrary(const std::string& fileName, std::string*& result)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, fileName, ", /\\\"", CDMUtilities::splitter::no_empties);
  std::string fileNameReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      fileNameReal += "-" + words[i];
    }

  std::string line;
  //opening original cmakelists
  std::ifstream in((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
  if( !in.is_open())
    {
      result = new std::string("CMakeLists.txt file failed to open.");
      return false;
    }
  //opening temporal cmakelists
  std::ofstream out((this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string("CMakeLists.txt.tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  while (getline(in, line))
    {
      if(line.find("SET ( LIBRARY_NAME") != std::string::npos)
        line = "SET ( LIBRARY_NAME  " + fileNameReal + "  )";
      out << line << std::endl;
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
  std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->nameLibrary = fileNameReal;
  return true;
}

modelCDMFolder* modelCDMLibrary::CreateFolder(const std::string& name, std::string*& result)
{
  //TODO:: mkdir depending on OS
  std::string command = "mkdir \"" + path + CDMUtilities::SLASH + name + "\"";
  if(system(command.c_str()))
    {
      result = new std::string("Error executing: " + command + ".");
      return NULL;
    }
  modelCDMFolder* folder = new modelCDMFolder(this, path + CDMUtilities::SLASH + name, name, level + 1);
  this->folders.push_back(folder);
  this->children.push_back(folder);

  return folder;
}

const bool modelCDMLibrary::Refresh(std::string*& result)
{
  std::cout << "refreshing library: " << this->nameLibrary << std::endl;
  //set attributes
  this->type = wxDIR_DIRS;

  //open CMakeList
  std::string pathMakeLists = path + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //get sets
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get library name
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "LIBRARY_NAME")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }

              this->nameLibrary = word;
            }
        }
    }

  confFile.close();

  //check children
  std::vector<bool> checked(this->children.size(), false);
  std::vector<bool> checkedFolders(this->folders.size(), false);

  //check all folders
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          //check if they already exist
          bool found = false;
          for (int i = 0; !found && i < (int)(this->folders.size()); i++)
            {
              if (this->folders[i]->GetName() == stdfileName)
                {
                  found = true;
                  int pos = std::find(this->children.begin(), this->children.end(), this->folders[i]) - this->children.begin();
                  checked[pos] = true;
                  checkedFolders[i] = true;
                  if(!this->folders[i]->Refresh(result))
                    return false;
                }
            }
          if(!found)
            {
              modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->folders.push_back(folder);
              this->children.push_back(folder);
            }
          cont = dir.GetNext(&fileName);
        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }
          //if is an unknown file, check if exist in children
          else
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  //if is a code file, create modelCDMCodeFile
                  if(
                      fileType == ".c" ||
                      fileType == ".cxx" ||
                      fileType == ".h" ||
                      fileType == ".cpp" ||
                      fileType == ".txx" ||
                      fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }
    }

  for (int i = 0; i < (int)(checkedFolders.size()); i++)
    {
      if(!checkedFolders[i])
        {
          this->folders.erase(this->folders.begin()+i);
          checkedFolders.erase(checkedFolders.begin()+i);
          i--;
        }
    }
  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }
  this->SortChildren();
  std::sort(this->folders.begin(), this->folders.end(), CompareNodeItem);
  return true;
}

void modelCDMLibrary::CheckStructure(std::map<std::string, bool>& properties)
{
  //check cmake exist
  if(this->CMakeLists != NULL)
    {
      //set default values
      properties["library " + this->name + " lib ${crea_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${WXWIDGETS_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${KWWidgets_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${VTK_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${ITK_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${GDCM_LIBRARIES}"] = false;
      properties["library " + this->name + " lib ${BOOST_LIBRARIES}"] = false;


      //open cmakelists
      std::ifstream confFile;
      confFile.open((this->CMakeLists->GetPath()).c_str());

      //take everything that is not commented
      std::string fileContent;

      std::string word;
      std::vector<std::string> words;
      while(confFile.is_open() && !confFile.eof())
        {
          std::getline(confFile,word, '\n');
          if(word[0] != '#')
            {
              CDMUtilities::splitter::split(words, word, "#", CDMUtilities::splitter::empties_ok);
              if (words.size() > 0)
                {
                  word = words[0];
                  CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::empties_ok);
                  for (int i = 0; i < (int)(words.size()); i++)
                    {
                      if(words[i].substr(0,2) == "//")
                        break;
                      fileContent += words[i] + " ";
                    }
                }
            }
        }

      //check every instruction
      std::stringstream ss(fileContent);
      while(!ss.eof())
        {
          std::getline(ss,word, '(');

          //check instruction name
          CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

          //set instructions
          if (words.size() > 0 && words[words.size()-1] == "SET")
            {
              std::getline(ss,word, ')');

              CDMUtilities::splitter::split(words, word, " \t", CDMUtilities::splitter::no_empties);
              if (words.size() > 1)
                {
                  if (words[0] == "${LIBRARY_NAME}_LINK_LIBRARIES")
                    {
                      for (int i = 1; i < (int)(words.size()); i++)
                        {
                          properties["library " + this->name + " lib " + words[i]] = true;
                        }
                    }
                }
            }
        }

    }
}

std::map<std::string, bool> modelCDMLibrary::Get3rdPartyLibraries()
{
  std::map<std::string, std::string> correspondence;
  correspondence["${crea_LIBRARIES}"] = "Crea";
  correspondence["${WXWIDGETS_LIBRARIES}"] = "WxWidgets";
  correspondence["${KWWidgets_LIBRARIES}"] = "KWWidgets";
  correspondence["${VTK_LIBRARIES}"] = "VTK";
  correspondence["${ITK_LIBRARIES}"] = "ITK";
  correspondence["${GDCM_LIBRARIES}"] = "GDCM";
  correspondence["${BOOST_LIBRARIES}"] = "Boost";
  std::map<std::string, bool> res;
  res["Crea"] = false;
  res["WxWidgets"] = false;
  res["KWWidgets"] = false;
  res["VTK"] = false;
  res["ITK"] = false;
  res["GDCM"] = false;
  res["Boost"] = false;

  if (this->HasCMakeLists())
    {
      std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());

      boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
      std::string::const_iterator start, end;
      start = CMfile.begin();
      end = CMfile.end();
      boost::match_results<std::string::const_iterator> what;
      boost::match_flag_type flags = boost::match_default;
      if(boost::regex_search(start, end, what, expression, flags))
        {

          expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression, flags))
            {
              expression = boost::regex("(#[^\\n]*\\n|\\s*\\$\\{\\w+\\})");
              std::string::const_iterator start2, end2;
              start2 = what1[0].second;
              end2 = what[0].second;
              boost::match_results<std::string::const_iterator> what2;
              while(boost::regex_search(start2, end2, what2, expression, flags))
                {
                  if(what2.str()[0] != '#')
                    {
                      std::string dete = what2.str();
                      CDMUtilities::normalizeStr(dete);
                      if(correspondence.find(dete) != correspondence.end())
                        res[correspondence[dete]] = true;
                    }
                  start2 = what2[0].second;
                }
            }
        }
    }
  return res;
}

bool modelCDMLibrary::Set3rdPartyLibrary(const std::string& library_name, const bool& toInclude)
{
  std::map<std::string, std::string> correspondence;

  correspondence["Crea"] = "${crea_LIBRARIES}";
  correspondence["WxWidgets"] = "${WXWIDGETS_LIBRARIES}";
  correspondence["KWWidgets"] = "${KWWidgets_LIBRARIES}";
  correspondence["VTK"] = "${VTK_LIBRARIES}";
  correspondence["ITK"] = "${ITK_LIBRARIES}";
  correspondence["GDCM"] = "${GDCM_LIBRARIES}";
  correspondence["Boost"] = "${BOOST_LIBRARIES}";

  std::map<std::string, std::string> regexCorrespondence;

  regexCorrespondence["Crea"] = "\\$\\{crea_LIBRARIES\\}";
  regexCorrespondence["WxWidgets"] = "\\$\\{WXWIDGETS_LIBRARIES\\}";
  regexCorrespondence["KWWidgets"] = "\\$\\{KWWidgets_LIBRARIES\\}";
  regexCorrespondence["VTK"] = "\\$\\{VTK_LIBRARIES\\}";
  regexCorrespondence["ITK"] = "\\$\\{ITK_LIBRARIES\\}";
  regexCorrespondence["GDCM"] = "\\$\\{GDCM_LIBRARIES\\}";
  regexCorrespondence["Boost"] = "\\$\\{BOOST_LIBRARIES\\}";

  if (correspondence.find(library_name) != correspondence.end())
    {
      std::string library_command = correspondence[library_name];
      std::string regex_command = regexCorrespondence[library_name];
      if (this->HasCMakeLists())
        {
          std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());
          std::string resCMfile = "";

          boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
          std::string::const_iterator start, end;
          start = CMfile.begin();
          end = CMfile.end();
          boost::match_results<std::string::const_iterator> what;
          boost::match_flag_type flags = boost::match_default;
          if(boost::regex_search(start, end, what, expression, flags))
            {
              resCMfile += what.prefix().str();
              bool found = false;
              if (toInclude) {
                expression = "^\\h*#+\\h*" + regex_command;
                std::string::const_iterator start1, end1;
                start1 = what[0].first;
                end1 = what[0].second;
                boost::match_results<std::string::const_iterator> what1, what2;
                while(boost::regex_search(start1, end1, what1, expression, flags))
                  {
                    found = true;
                    resCMfile += what1.prefix().str();
                    std::string dete = what1[0].str();
                    for (int i = 0; i < dete.size(); ++i) {
                      if (dete[i] != '#')
                        resCMfile.push_back(dete[i]);
                    }
                    what2 = what1;
                    start1 = what1[0].second;
                  }
                if (found)
                  resCMfile += what2.suffix().str();
                else
                  {
                    expression = "^\\h*" + regex_command;
                    if(boost::regex_search(start1, end1, what1, expression, flags))
                      found = true;

                    expression = "^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES";
                    boost::regex_search(start1, end1, what1, expression, flags);

                    resCMfile += what1.prefix().str() + what1.str();
                    if(!found)
                      resCMfile += "\n" + library_command + "\n";
                    resCMfile += what1.suffix().str();
                  }

              }else{
                expression = "^\\h*" + regex_command;
                std::string::const_iterator start1, end1;
                start1 = what[0].first;
                end1 = what[0].second;
                boost::match_results<std::string::const_iterator> what1, what2;
                while(boost::regex_search(start1, end1, what1, expression, flags))
                  {
                    found = true;
                    resCMfile += what1.prefix().str();
                    resCMfile += "#" + what1.str();
                    what2 = what1;
                    start1 = what1[0].second;
                  }
                if (found)
                  resCMfile += what2.suffix().str();
                else
                  {
                    expression = "^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES";
                    boost::regex_search(start1, end1, what1, expression, flags);

                    resCMfile += what1.prefix().str() + what1.str() + what1.suffix().str();
                  }
              }
              resCMfile += what.suffix().str();

              return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
            }
        }
    }
  return false;
}

std::map<std::string, bool> modelCDMLibrary::GetCustomLibraries()
{
  std::map<std::string, bool> res;
  std::map<std::string, bool> res1;

  std::map<std::string, std::string> correspondence;
  std::vector<modelCDMLibrary*> libraries = ((modelCDMLib*)this->parent)->GetLibraries();
  for (int i = 0; i < libraries.size(); ++i)
    {
      if(libraries[i]->GetNameLibrary() == this->nameLibrary)
        continue;
      correspondence[libraries[i]->GetName()] = libraries[i]->GetNameLibrary();
      res[libraries[i]->GetNameLibrary()] = false;
      res1[libraries[i]->GetNameLibrary()] = false;
    }

  if (this->HasCMakeLists())
    {
      std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());

      //find included libraries
      boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
      std::string::const_iterator start, end;
      start = CMfile.begin();
      end = CMfile.end();
      boost::match_results<std::string::const_iterator> what;
      boost::match_flag_type flags = boost::match_default;
      if(boost::regex_search(start, end, what, expression, flags))
        {

          expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression, flags))
            {
              expression = boost::regex("^\\h*[\\w\\d]+");
              std::string::const_iterator start2, end2;
              start2 = what1[0].second;
              end2 = what[0].second;
              boost::match_results<std::string::const_iterator> what2;
              while(boost::regex_search(start2, end2, what2, expression, flags))
                {
                  std::string dete = what2.str();
                  CDMUtilities::normalizeStr(dete);
                  //std::cout << "detectado lib: " << dete << std::endl;
                  if(res1.find(dete) != res1.end())
                    res1[dete] = true;

                  start2 = what2[0].second;
                }
            }
        }

      //find included folders
      //std::cout << "searching..." << std::endl;
      expression = boost::regex("^\\h*INCLUDE_DIRECTORIES([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*([\\./\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"){0,1}?(([\\s]|#[^\\n]*\\n)+([\\./\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
      start = CMfile.begin();
      end = CMfile.end();
      if(boost::regex_search(start, end, what, expression, flags))
        {
          //std::cout << what.str() << std::endl;
          expression = boost::regex("^\\h*INCLUDE_DIRECTORIES([\\s]|#[^\\n]*\\n)*\\(");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression, flags))
            {
              //std::cout << what1.str() << std::endl;
              expression = boost::regex("^\\h*\\.\\.\\/([\\w\\d])+");
              std::string::const_iterator start2, end2;
              start2 = what1[0].second;
              end2 = what[0].second;
              boost::match_results<std::string::const_iterator> what2;
              while(boost::regex_search(start2, end2, what2, expression, flags))
                {
                  std::string dete = what2.str();
                  CDMUtilities::normalizeStr(dete);
                  //std::cout << "detectado dir: " << dete.substr(3) << std::endl;
                  if(correspondence.find(dete.substr(3)) != correspondence.end())
                    res[correspondence[dete.substr(3)]] = res1[correspondence[dete.substr(3)]];

                  start2 = what2[0].second;
                }
            }
        }
    }

  return res;
}

bool modelCDMLibrary::SetCustomLibrary(const std::string& library_name, const bool& toInclude)
{
  std::map<std::string, std::string> correspondence;
  std::vector<modelCDMLibrary*> libraries = ((modelCDMLib*)this->parent)->GetLibraries();
  for (int i = 0; i < libraries.size(); ++i)
    {
      if(libraries[i]->GetNameLibrary() == this->nameLibrary)
        continue;
      correspondence[libraries[i]->GetNameLibrary()] = libraries[i]->GetName();
    }

  if (correspondence.find(library_name) != correspondence.end())
    {
      if (this->HasCMakeLists())
        {
          std::string resCMfile = "";
          std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());
          bool found = false;

          //find included libraries
          boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
          std::string::const_iterator start, end;
          start = CMfile.begin();
          end = CMfile.end();
          boost::match_results<std::string::const_iterator> what;
          boost::match_flag_type flags = boost::match_default;
          if(boost::regex_search(start, end, what, expression, flags))
            {
              resCMfile += what.prefix().str();
              expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{LIBRARY_NAME\\}_LINK_LIBRARIES");
              std::string::const_iterator start1, end1;
              start1 = what[0].first;
              end1 = what[0].second;
              boost::match_results<std::string::const_iterator> what1;
              if(boost::regex_search(start1, end1, what1, expression, flags))
                {
                  resCMfile += what1.prefix().str() + what1.str();
                  //check if already exists
                  expression = boost::regex("^\\h*"+library_name);
                  std::string::const_iterator start2, end2;
                  start2 = what1[0].second;
                  end2 = what[0].second;
                  boost::match_results<std::string::const_iterator> what2, temp2;
                  while(boost::regex_search(start2, end2, what2, expression, flags))
                    {
                      resCMfile += what2.prefix().str();
                      found = true;
                      if (!toInclude)
                        {
                          resCMfile += "#";
                        }
                      resCMfile += what2.str();
                      temp2 = what2;
                      start2 = what2[0].second;
                    }
                  if(found)
                    resCMfile += temp2.suffix().str();
                  //check if is commented
                  else
                    {
                      expression = boost::regex("^\\h*#+\\h*"+library_name);
                      start2 = what1[0].second;
                      end2 = what[0].second;
                      while(boost::regex_search(start2, end2, what2, expression, flags))
                        {
                          found = true;
                          resCMfile += what2.prefix().str();
                          if(toInclude)
                            {
                              std::string dete = what2[0].str();
                              for (int i = 0; i < dete.size(); ++i) {
                                if (dete[i] != '#')
                                  resCMfile.push_back(dete[i]);
                              }
                            }
                          temp2 = what2;
                          start2 = what2[0].second;
                        }
                      if(found)
                        resCMfile += temp2.suffix().str();
                      //add at the beggining of instruction
                      else
                        {
                          if(toInclude)
                            resCMfile += "\n" + library_name;
                          resCMfile += what1.suffix().str();
                        }
                    }
                }
              resCMfile += what.suffix().str();
            }
          else
            return false;

          //find included folders
          CMfile = resCMfile;
          resCMfile = "";


          found = false;
          std::cout << "searching..." << CMfile << std::endl;
          expression = boost::regex("^\\h*INCLUDE_DIRECTORIES([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*([\\.\\/\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"){0,1}?(([\\s]|#[^\\n]*\\n)+([\\.\\/\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
          start = CMfile.begin();
          end = CMfile.end();
          if(boost::regex_search(start, end, what, expression, flags))
            {
              resCMfile += what.prefix().str();
              std::cout << what.str() << std::endl;
              expression = boost::regex("^\\h*INCLUDE_DIRECTORIES([\\s]|#[^\\n]*\\n)*\\(");
              std::string::const_iterator start1, end1;
              start1 = what[0].first;
              end1 = what[0].second;
              boost::match_results<std::string::const_iterator> what1;
              if(boost::regex_search(start1, end1, what1, expression, flags))
                {
                  resCMfile += what1.prefix().str() + what1.str();
                  std::cout << what1.str() << std::endl;
                  //search if dir is already included
                  expression = boost::regex("^\\h*\\.\\.\\/"+correspondence[library_name]);
                  std::string::const_iterator start2, end2;
                  start2 = what1[0].second;
                  end2 = what[0].second;
                  boost::match_results<std::string::const_iterator> what2, temp2;
                  while(boost::regex_search(start2, end2, what2, expression, flags))
                    {
                      found = true;
                      resCMfile += what2.prefix().str();
                      if(!toInclude)
                        resCMfile += "#";
                      resCMfile += what2.str();
                      temp2 = what2;
                      start2 = what2[0].second;
                    }
                  if(found)
                    resCMfile += temp2.suffix().str();
                  //search if dir is commented
                  else
                    {
                      expression = boost::regex("^\\h*#+\\h*\\.\\.\\/"+correspondence[library_name]);
                      start2 = what1[0].second;
                      end2 = what[0].second;
                      while(boost::regex_search(start2, end2, what2, expression, flags))
                        {
                          found = true;
                          resCMfile += what2.prefix().str();
                          if(toInclude)
                            {
                              std::string dete = what2[0].str();
                              for (int i = 0; i < dete.size(); ++i) {
                                if (dete[i] != '#')
                                  resCMfile.push_back(dete[i]);
                              }
                            }
                          temp2 = what2;
                          start2 = what2[0].second;
                        }
                      if(found)
                        resCMfile += temp2.suffix().str();
                      //add at the beggining of instruction
                      else
                        {
                          if(toInclude)
                            resCMfile += "\n../" + correspondence[library_name];
                          resCMfile += what1.suffix().str();
                        }
                    }
                }
              resCMfile += what.suffix().str();
            }
          else
            return false;

          return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
        }
    }

  return false;
}
