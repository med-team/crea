/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCreaDevManagerTreeCtrl.h
 *
 *  Created on: 19/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPROJECTSTREECTRL_H_
#define WXCDMPROJECTSTREECTRL_H_

#include <creaWx.h>
#include <wx/treectrl.h>
#include "modelCDMProject.h"
#include "modelCDMIProjectTreeNode.h"

#include <vector>
#include <map>

/**
 * Project Tree Control. Shows a project's content in a hierarchy tree control.
 */
class wxCDMProjectsTreeCtrl: public wxTreeCtrl
{
public:
  /**
   * Project Tree Control Constructor.
   * @param parent Parent window reference.
   * @param id Control ID, by default wxID_ANY.
   * @param pos Control position. By default wxDefaultPosition.
   * @param size Control size. By default wxDefaultSize.
   * @param style Control style. By default wxTR_DEFAULT_STYLE.
   * @param validator Validator. By default wxDefaultValidator.
   * @param name Control Name. By default "Projects tree".
   */
  wxCDMProjectsTreeCtrl(
      wxWindow *parent,
      wxWindowID id=wxID_ANY,
      const wxPoint &pos=wxDefaultPosition,
      const wxSize &size=wxDefaultSize,
      long style=wxTR_DEFAULT_STYLE,
      const wxValidator &validator=wxDefaultValidator,
      const wxString &name=_("Projects tree")
  );
  /**
   * Destructor.
   */
  ~wxCDMProjectsTreeCtrl();
  /**
   * Project Tree Control Creator.
   * @param parent Parent window reference.
   * @param id Control ID, by default wxID_ANY.
   * @param pos Control position. By default wxDefaultPosition.
   * @param size Control size. By default wxDefaultSize.
   * @param style Control style. By default wxTR_DEFAULT_STYLE.
   * @param validator Validator. By default wxDefaultValidator.
   * @param name Control Name. By default "Projects tree".
   * @return True if the creation was successful.
   */
  bool Create(
      wxWindow *parent,
      wxWindowID id=wxID_ANY,
      const wxPoint &pos=wxDefaultPosition,
      const wxSize &size=wxDefaultSize,
      long style=wxTR_DEFAULT_STYLE,
      const wxValidator &validator=wxDefaultValidator,
      const wxString &name=_("Projects tree")
  );

  /**
   * Builds the tree hierarchy with the given model elements.
   */
  void BuildTree(std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* >& modelElements, modelCDMProject* tree = NULL);

private:

  /**
   * A Icon ID.
   */
  int ID_AIcon;
  /**
   * Ap Icon ID.
   */
  int ID_ApIcon;
  /**
   * BB Icon ID.
   */
  int ID_BBIcon;
  /**
   * BBG Icon ID.
   */
  int ID_BBGIcon;
  /**
   * BBS Icon ID.
   */
  int ID_BBSIcon;
  /**
   * C Icon ID.
   */
  int ID_Cicon;
  /**
   * CF Icon ID.
   */
  int ID_CFIcon;
  /**
   * CM Icon ID.
   */
  int ID_CMIcon;
  /**
   * Fd Icon ID.
   */
  int ID_FdIcon;
  /**
   * Fl Icon ID.
   */
  int ID_FlIcon;
  /**
   * Lb Icon ID.
   */
  int ID_LbIcon;
  /**
   * L Icon ID.
   */
  int ID_LIcon;
  /**
   * Pr Icon ID.
   */
  int ID_PrIcon;
  /**
   * Pk Icon ID.
   */
  int ID_PkIcon;

  /**
   * Builds the tree with the hierarchy given and fills the modelElements map.
   * @param tree Root node of the tree.
   * @param modelElements Id->node map.
   * @param parent ID of the root node ID.
   */
  void BuildTree(const std::vector<modelCDMIProjectTreeNode*>& tree, std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* >& modelElements, const wxCDMTreeItemId& parent);
  /**
   * Retrieves the icon ID of the given node by its type.
   * @param node Node to search its icon.
   * @return Icon ID
   */
  int GetIconId(modelCDMIProjectTreeNode* node);
};

#endif /* WXCDMPROJECTSTREECTRL_H_ */
