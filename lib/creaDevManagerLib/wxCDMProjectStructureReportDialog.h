/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * wxCDMProjectStructureReportDialog.h
 *
 *  Created on: 3/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef WXCDMPROJECTSTRUCTUREREPORTDIALOG_H_
#define WXCDMPROJECTSTRUCTUREREPORTDIALOG_H_

#include <creaWx.h>
#include <wx/dialog.h>

#include <map>

/**
 * Project Structure Report Dialog. Allows the user to inspect what is compiling and what's not.
 */
class wxCDMProjectStructureReportDialog : public wxDialog
{
  DECLARE_EVENT_TABLE()
public:
  /**
   * Project Structure Report Dialog Constructor.
   * @param parent Parent window.
   * @param properties Project precompilation check map.
   * @param id Window ID. By default wxID_ANY.
   * @param caption Window label. By default "Project Structure Report".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 700, 500.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   */
  wxCDMProjectStructureReportDialog(
      wxWindow* parent,
      const std::map<std::string, bool>& properties,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Project Structure Report"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(700,500),
      long style = wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER
  );
  /**
   * Destructor.
   */
  ~wxCDMProjectStructureReportDialog();
  /**
   * Project Structure Report Dialog Creator.
   * @param parent Parent window.
   * @param id Window ID. By default wxID_ANY.
   * @param caption Window label. By default "Project Structure Report".
   * @param position Dialog position. By default wxDefaultPosition.
   * @param size Dialog size. By default 700, 500.
   * @param style Dialog style. By default wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER.
   * @return if the creation was successful.
   */
  bool Create(
      wxWindow* parent,
      wxWindowID id = wxID_ANY,
      const wxString& caption = wxT("Project Structure Report"),
      const wxPoint& position = wxDefaultPosition,
      const wxSize& size = wxSize(700,500),
      long style = wxDEFAULT_DIALOG_STYLE
  );

protected:
  /**
   * Creates the Dialog controls (text and button).
   */
  void CreateControls();

//attributes
private:
  /**
   * Project check results.
   */
  std::map<std::string, bool> properties;

//handlers
protected:
  /**
   * Handles when the close button is pressed.
   * @param event Unused.
   */
  void OnFinish(wxCommandEvent& event);
  /**
   * Creates the project structure check results processed for showing them. In HTML
   * @return Report in HTML format.
   */
  wxString GetContent();
};

#endif /* WXCDMPROJECTSTRUCTUREREPORTDIALOG_H_ */
