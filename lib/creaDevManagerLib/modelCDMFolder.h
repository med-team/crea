/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMFolder.h
 *
 *  Created on: Nov 28, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMFOLDER_H_
#define MODELCDMFOLDER_H_

#include<iostream>
#include<vector>

#include "modelCDMIProjectTreeNode.h"
#include "modelCDMCMakeListsFile.h"
#include "modelCDMCodeFile.h"
#include "modelCDMBBSFile.h"
#include "modelCDMBBGFile.h"

/**
 * Class representing a folder in the project hierarchy.
 */
class modelCDMFolder : public modelCDMIProjectTreeNode
{
public:
  /**
   * Default constructor.
   */
  modelCDMFolder();
  /**
   * Constructor of the folder node.
   * @param parent Parent node.
   * @param path Full path of the folder node.
   * @param name Folder name of the folder node.
   * @param level Hierarchy level of the folder node in the project.
   */
  modelCDMFolder(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level = 3);
  /**
   * Destructor.
   */
  ~modelCDMFolder();

  /**
   * Returns the reference to the folder node's CMakeLists file if it exists.
   * @return Reference to the CMakeLists file or NULL.
   */
  modelCDMCMakeListsFile* GetCMakeLists() const;
  /**
   * Returns an array with the containing folder children of the actual folder node.
   * @return Array with references to children folder nodes.
   */
  std::vector<modelCDMFolder*> GetFolders() const;

  /**
   * Creates a class (.h and .cpp files) in the folder node. It creates the files in the model as well as in the system.
   * @param name Name of the class to create.
   * @return True if the operation was successful.
   */
  bool CreateClass(const std::string& name);

  /**
   * Creates a folder in the folder node. It creates the folder in the model as well as in the system.
   * @param name Name of the folder to create.
   * @param result Result message of the operation.
   * @return True if the operation was successful.
   */
  modelCDMFolder* CreateFolder(
      const std::string& name,
      std::string*& result
  );
  /**
   * Opens the CMakeLists file in the default code editor.
   * @param result Result message of the operation.
   * @return True if the operation was successful.
   */
  bool OpenCMakeListsFile(std::string* & result);
  /**
   * Refreshes the folder node structure. deletes deleted files and folders, and adds created files and folders.
   * @param result Result message of the operation
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Returns wether the CMakeLists attribute is different from NULL or not.
   * @return True if the MakeLists attribute is different from NULL.
   */
  bool HasCMakeLists();

protected:
  /**
   * Reference to the folder node's CMakeLists.
   */
  modelCDMCMakeListsFile* CMakeLists;
private:
  /**
   * Reference array to the children folders.
   */
  std::vector<modelCDMFolder*> folders;

};

#endif /* MODELCDMFOLDER_H_ */
