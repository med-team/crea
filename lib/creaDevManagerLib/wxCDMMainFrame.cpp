/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


#include "wxCDMMainFrame.h"

#include <iostream>
#include <sstream>

#include <creaWx.h>
#include "creaSystem.h"
#include "wx/treectrl.h"
#include "wx/treebase.h"
#include "wx/tooltip.h"
#include "wx/wxhtml.h"
#include "wx/statline.h"
#include "wx/config.h"
#include "CDMUtilities.h"
#include "images/CIcon64.xpm"

#include "creaDevManagerIds.h"
#include "wxCDMMainDescriptionPanel.h"
#include "wxCDMProjectDescriptionPanel.h"
#include "wxCDMAppliDescriptionPanel.h"
#include "wxCDMApplicationDescriptionPanel.h"
#include "wxCDMLibDescriptionPanel.h"
#include "wxCDMLibraryDescriptionPanel.h"
#include "wxCDMPackageDescriptionPanel.h"
#include "wxCDMBlackBoxDescriptionPanel.h"
#include "wxCDMBBSFileDescriptionPanel.h"
#include "wxCDMBBGFileDescriptionPanel.h"
#include "wxCDMCodeFileDescriptionPanel.h"
#include "wxCDMCMakeListsDescriptionPanel.h"
#include "wxCDMFolderDescriptionPanel.h"
#include "wxCDMFileDescriptionPanel.h"
#include "wxCDMPackageManagerPanel.h"
#include "wxCDMProjectMapDialog.h"

#include "wxCDMSettingsDialog.h"

#include "wxCDMProjectActionsPanel.h"
#include "wxCDMNewProjectDialog.h"



BEGIN_EVENT_TABLE(wxCDMMainFrame, wxFrame)
EVT_MENU_OPEN(wxCDMMainFrame::OnMenuBarOpen)
EVT_MENU(ID_MENU_NEW_PROJECT, wxCDMMainFrame::OnMenuNewProject)
EVT_MENU(ID_MENU_OPEN_PROJECT, wxCDMMainFrame::OnMenuOpenProject)
EVT_MENU(ID_MENU_OPEN_RECENT1, wxCDMMainFrame::OnMenuOpenRecent)
EVT_MENU(ID_MENU_OPEN_RECENT2, wxCDMMainFrame::OnMenuOpenRecent)
EVT_MENU(ID_MENU_OPEN_RECENT3, wxCDMMainFrame::OnMenuOpenRecent)
EVT_MENU(ID_MENU_OPEN_RECENT4, wxCDMMainFrame::OnMenuOpenRecent)
EVT_MENU(ID_MENU_OPEN_RECENT5, wxCDMMainFrame::OnMenuOpenRecent)
EVT_MENU(ID_MENU_CLOSE_PROJECT, wxCDMMainFrame::OnMenuCloseProject)
EVT_MENU(ID_MENU_EXPORT_HIERARCHY, wxCDMMainFrame::OnMenuExportHierarchy)
EVT_MENU(ID_MENU_EXIT, wxCDMMainFrame::OnMenuExit)
EVT_MENU(ID_MENU_REFRESH_PROJECT, wxCDMMainFrame::OnMenuRefreshProject)
EVT_MENU(ID_MENU_SETTINGS, wxCDMMainFrame::OnMenuSettings)
EVT_MENU(ID_MENU_BBTK_GRAPHICAL_EDITOR, wxCDMMainFrame::OnMenuBBTKGraphicalEditor)
EVT_MENU(ID_MENU_MINITOOLS, wxCDMMainFrame::OnMenuMiniTools)
EVT_MENU(ID_MENU_CODE_EDITOR, wxCDMMainFrame::OnMenuCodeEditor)
EVT_MENU(ID_MENU_COMMAND_LINE, wxCDMMainFrame::OnMenuCommandLine)
EVT_MENU(ID_MENU_TOGGLE_HELP, wxCDMMainFrame::OnMenuToggleHelp)
EVT_MENU(ID_MENU_HELP, wxCDMMainFrame::OnMenuHelp)
EVT_MENU(ID_MENU_SHOW_PROJECT_MAP, wxCDMMainFrame::OnMenuShowProjectMap)
EVT_MENU(ID_MENU_REPORT_BUG, wxCDMMainFrame::OnMenuReportBug)
EVT_MENU(ID_MENU_ABOUT_CREADEVMANAGER, wxCDMMainFrame::OnMenuAboutCreaDevManager)
EVT_MENU(ID_MENU_ABOUT_CREATIS, wxCDMMainFrame::OnMenuAboutCreatis)

EVT_BUTTON(ID_BUTTON_NEWPROJECT, wxCDMMainFrame::OnMenuNewProject)
EVT_BUTTON(ID_BUTTON_OPENPROJECT, wxCDMMainFrame::OnMenuOpenProject)

EVT_TREE_SEL_CHANGED(ID_TREE_PROJECTS, wxCDMMainFrame::OnTreeSelectionChanged)

EVT_COMMAND(wxID_ANY, wxEVT_DISPLAY_CHANGED, wxCDMMainFrame::OnChangeView)
EVT_COMMAND(wxID_ANY, wxEVT_COMMAND_LISTBOX_DOUBLECLICKED, wxCDMMainFrame::OnElementSelected)
EVT_COMMAND(wxID_ANY, wxEVT_COMMAND_LISTBOX_SELECTED, wxCDMMainFrame::OnElementDeselected)


EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMMainFrame::OnDisableHelp)
END_EVENT_TABLE()

wxCDMMainFrame::wxCDMMainFrame(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  this->menu_File = NULL;
  this->menu_Edit = NULL;
  this->menu_Tools = NULL;
  this->menu_Help = NULL;
  this->panel_Properties = NULL;
  this->panel_ProjectActions = NULL;
  this->tree_Projects = NULL;
  Create(parent, id, caption, pos, size, style);
}

wxCDMMainFrame::~wxCDMMainFrame()
{
  wxConfigBase* pConfig = wxConfigBase::Get();
  pConfig->Write(wxT("HELP"), this->help);

  auiManager.UnInit();
}

bool wxCDMMainFrame::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxFrame::Create(parent, id, caption, pos, size, style);
  this->help = true;

  wxConfigBase* pConfig = wxConfigBase::Get();
  this->help = pConfig->Read(wxT("HELP"), this->help) != 0;

  this->model = new modelCDMMain();

  CreateMenus();
  CreateControls();
  this->SetIcon(wxIcon(CIcon64));
  return TRUE;
}

modelCDMMain* wxCDMMainFrame::GetModel() const
{
  return this->model;
}

wxPanel* wxCDMMainFrame::GetPropertiesPanel() const
{
  return this->panel_Properties;
}

bool wxCDMMainFrame::isHelp() const
{
  return this->help;
}

void wxCDMMainFrame::RefreshProject()
{
  std::string* result;
  std::cout << "refreshing project" << std::endl;
  this->model->RefreshProject(result);
  std::cout << "rebuilding project tree" << std::endl;
  this->tree_Projects->BuildTree(this->model->GetModelElements(), this->model->GetProject());
  this->tree_Projects->Unselect();
  this->actualTreeItem.Unset();
}

void wxCDMMainFrame::CreateMenus()
{
  wxMenuBar* menuBar = new wxMenuBar;

  //Recently opened projects
  menu_Recent = new wxMenu();
  wxConfigBase* pConfig = wxConfigBase::Get();
  std::string rp = crea::wx2std(pConfig->Read(wxT("RECENT1"), wxT("")));
  if(rp != "")
    {
      menu_Recent->Append(ID_MENU_OPEN_RECENT1, crea::std2wx(rp));
      rp = crea::wx2std(pConfig->Read(wxT("RECENT2"), wxT("")));
      if(rp != "")
        {
          menu_Recent->Append(ID_MENU_OPEN_RECENT2, crea::std2wx(rp));
          rp = crea::wx2std(pConfig->Read(wxT("RECENT3"), wxT("")));
          if(rp != "")
            {
              menu_Recent->Append(ID_MENU_OPEN_RECENT3, crea::std2wx(rp));
              rp = crea::wx2std(pConfig->Read(wxT("RECENT4"), wxT("")));
              if(rp != "")
                {
                  menu_Recent->Append(ID_MENU_OPEN_RECENT4, crea::std2wx(rp));
                  rp = crea::wx2std(pConfig->Read(wxT("RECENT5"), wxT("")));
                  if(rp != "")
                    {
                      menu_Recent->Append(ID_MENU_OPEN_RECENT5, crea::std2wx(rp));
                    }
                }
            }
        }
    }

  //FileMenu
  menu_File = new wxMenu();
  menu_File->Append(ID_MENU_NEW_PROJECT, wxT("&New Project..."));
  menu_File->Append(ID_MENU_OPEN_PROJECT, wxT("&Open Project..."));
  menu_File->AppendSubMenu(menu_Recent,wxT("Open &Recent"),wxT("Open a recently opened project."));
  menu_File->AppendSeparator();
  menu_File->Append(ID_MENU_CLOSE_PROJECT, wxT("&Close Project"));
  menu_File->AppendSeparator();
  menu_File->Append(ID_MENU_EXPORT_HIERARCHY, wxT("&Export Project Hierarchy..."));
  menu_File->AppendSeparator();
  menu_File->Append(ID_MENU_EXIT, wxT("E&xit"));

  menuBar->Append(menu_File, wxT("&File"));

  //EditMenu
  menu_Edit = new wxMenu();
  menu_Edit->Append(ID_MENU_REFRESH_PROJECT, wxT("&Refresh Project"));
  menu_Edit->Append(ID_MENU_SETTINGS, wxT("&Settings"));

  menuBar->Append(menu_Edit, wxT("&Edit"));

  //ToolsMenu
  menu_Tools = new wxMenu();
  menu_Tools->Append(ID_MENU_BBTK_GRAPHICAL_EDITOR, wxT("Open BBEditor (BBTK &Graphical Editor)"));
  menu_Tools->Append(ID_MENU_MINITOOLS, wxT("Open &CreaTools"));
  menu_Tools->Append(ID_MENU_CODE_EDITOR, wxT("Open Code &Editor"));
  menu_Tools->Append(ID_MENU_COMMAND_LINE, wxT("Open Command &Line"));

  menuBar->Append(menu_Tools, wxT("&Tools"));

  //HelpMenu
  menu_Help = new wxMenu();
  menu_Help->AppendCheckItem(ID_MENU_TOGGLE_HELP, wxT("He&lp Dialogs"));
  menu_Help->Check(ID_MENU_TOGGLE_HELP, this->help);
  menu_Help->Append(ID_MENU_SHOW_PROJECT_MAP, wxT("&Show Project Map"));
  menu_Help->Append(ID_MENU_HELP, wxT("&Help"));
  menu_Help->Append(ID_MENU_REPORT_BUG, wxT("Report &Bug"));
  menu_Help->Append(ID_MENU_ABOUT_CREADEVMANAGER, wxT("&About CreaDevManager"));
  menu_Help->Append(ID_MENU_ABOUT_CREATIS, wxT("A&bout CREATIS"));

  menuBar->Append(menu_Help, wxT("&Help"));

  //Set Bar
  SetMenuBar(menuBar);

  wxStatusBar* statusBar = new wxStatusBar(this, ID_STATUS_BAR, wxST_SIZEGRIP);
  statusBar->SetFieldsCount(1);
  SetStatusBar(statusBar);

}

void wxCDMMainFrame::CreateControls()
{

  auiManager.SetManagedWindow(this);


  tree_Projects = new wxCDMProjectsTreeCtrl(
      this,
      ID_TREE_PROJECTS,
      wxDefaultPosition,
      wxSize(200,400),
	  wxTR_HAS_BUTTONS | wxTR_AQUA_BUTTONS
  );
  this->actualTreeItem.Unset();

  panel_Properties = new wxCDMMainDescriptionPanel(
      this,
      ID_WINDOW_PROPERTIES,
      wxT("Description Panel"),
      wxDefaultPosition,
      wxSize(400, 600),
      0
  );

  auiManager.AddPane(panel_Properties, wxAuiPaneInfo().BestSize(600,400).CenterPane().Name(wxT("panel_Properties")).Caption(wxT("")).CloseButton(false));
  auiManager.AddPane(tree_Projects, wxAuiPaneInfo().Left().MinSize(250,300).BestSize(250,400).CloseButton(false).Name(wxT("tree_Projects")).Caption(wxT("Project Tree")).CloseButton(false));
  auiManager.Update();
  //auiManager.LoadPerspective(pers,true);
  wxToolTip::Enable(true);
  wxToolTip::SetDelay(0);
}

//Event Handlers

void wxCDMMainFrame::OnMenuBarOpen(wxMenuEvent& event)
{
  //clean recent menu
  int tam = menu_Recent->GetMenuItemCount();
  for (int i = 0; i < tam; ++i) {
    menu_Recent->Delete(menu_Recent->FindItemByPosition(0));
  }
  //populate recent menu
  wxConfigBase* pConfig = wxConfigBase::Get();
  std::string rp = crea::wx2std(pConfig->Read(wxT("RECENT1"), wxT("")));
  if(rp != "")
    {
      menu_Recent->Append(ID_MENU_OPEN_RECENT1, crea::std2wx(rp));
      rp = crea::wx2std(pConfig->Read(wxT("RECENT2"), wxT("")));
      if(rp != "")
        {
          menu_Recent->Append(ID_MENU_OPEN_RECENT2, crea::std2wx(rp));
          rp = crea::wx2std(pConfig->Read(wxT("RECENT3"), wxT("")));
          if(rp != "")
            {
              menu_Recent->Append(ID_MENU_OPEN_RECENT3, crea::std2wx(rp));
              rp = crea::wx2std(pConfig->Read(wxT("RECENT4"), wxT("")));
              if(rp != "")
                {
                  menu_Recent->Append(ID_MENU_OPEN_RECENT4, crea::std2wx(rp));
                  rp = crea::wx2std(pConfig->Read(wxT("RECENT5"), wxT("")));
                  if(rp != "")
                    {
                      menu_Recent->Append(ID_MENU_OPEN_RECENT5, crea::std2wx(rp));
                    }
                }
            }
        }
    }
}

//File menu
void wxCDMMainFrame::OnMenuNewProject(wxCommandEvent& event)
{
  std::string* result;

  wxCDMNewProjectDialog* dialog = new wxCDMNewProjectDialog(this);
  long userResponse;
  userResponse = dialog->ShowModal();

  if(userResponse == wxID_FORWARD)
    {
      //close open project
      if(this->model->GetProject() != NULL)
        {
          if(!this->model->CloseProject(result))
            {
              std::cout << "error closing project: " << *result << std::endl;
              wxMessageBox(crea::std2wx(*result),_T("New Project - Error!"),wxOK | wxICON_ERROR);
              event.Skip();
              return;
            }

          if(this->panel_Properties != NULL)
            {
              auiManager.DetachPane(this->panel_Properties);
              this->panel_Properties->Hide();
            }
          if(this->panel_ProjectActions != NULL)
            {
              auiManager.DetachPane(this->panel_ProjectActions);
              this->panel_ProjectActions->Hide();    
            }

        }

      //create project
      if(!this->model->CreateProject(
          crea::wx2std(dialog->GetProjectName()),
          crea::wx2std(dialog->GetProjectLocation()),
          result,
          crea::wx2std(dialog->GetPackageAuthor()),
          crea::wx2std(dialog->GetPackageDescription())
      ))
        {
          std::cout << "error opening project: " << *result << std::endl;
          wxMessageBox(crea::std2wx(*result),_T("New Project - Error!"),wxOK | wxICON_ERROR);
          event.Skip();
          return;
        }
      
      //update recently open projects
      wxConfigBase* pConfig = wxConfigBase::Get();
      if(pConfig->Read(wxT("RECENT1"),wxT("")) != crea::std2wx(this->model->GetProject()->GetPath()))
        {
          pConfig->Write(wxT("RECENT5"), pConfig->Read(wxT("RECENT4"),wxT("")));
          pConfig->Write(wxT("RECENT4"), pConfig->Read(wxT("RECENT3"),wxT("")));
          pConfig->Write(wxT("RECENT3"), pConfig->Read(wxT("RECENT2"),wxT("")));
          pConfig->Write(wxT("RECENT2"), pConfig->Read(wxT("RECENT1"),wxT("")));
          pConfig->Write(wxT("RECENT1"), crea::std2wx(this->model->GetProject()->GetPath()));
        }


      //show project actions panel
      if(this->panel_ProjectActions != NULL)
        {
          auiManager.DetachPane(this->panel_Properties);
          this->panel_ProjectActions->Destroy();
          this->panel_ProjectActions = NULL;
        }
	  
      panel_ProjectActions = new wxCDMProjectActionsPanel(
          this,
          this->model->GetProject(),
          ID_WINDOW_PROJ_ACTIONS,
          wxT("Project Actions Panel"),
          wxDefaultPosition,
          wxSize(800,200),
          0
      );
      
      auiManager.AddPane(panel_ProjectActions, wxAuiPaneInfo().Bottom().MinSize(800,50).Name(wxT("panel_ProjectActions")).Caption(wxT("General Project Actions")).BestSize(800,70).CloseButton(false));
      auiManager.Update();

      //populate tree control
	  tree_Projects->BuildTree(this->model->GetModelElements(),this->model->GetProject());
      tree_Projects->Unselect();
	  this->actualTreeItem.Unset();
	  tree_Projects->SelectItem(this->model->GetProject()->GetId().GetWxId(), true);
	  //wxMessageBox(wxT("ProjectSelected") ,_T("New Project - Success!"),wxOK | wxICON_ERROR);
    }
}
void wxCDMMainFrame::OnMenuOpenRecent(wxCommandEvent& event)
{
  std::string* result;

  //((wxMenuItem*)(event.GetEventObject()))->GetItemLabel();

  std::string path = "";
  wxConfigBase* pConfig = wxConfigBase::Get();
  if(event.GetId() == ID_MENU_OPEN_RECENT1)
    path = crea::wx2std (pConfig->Read(wxT("RECENT1"),wxT("")));
  else if(event.GetId() == ID_MENU_OPEN_RECENT2)
    path = crea::wx2std (pConfig->Read(wxT("RECENT2"),wxT("")));
  else if(event.GetId() == ID_MENU_OPEN_RECENT3)
    path = crea::wx2std (pConfig->Read(wxT("RECENT3"),wxT("")));
  else if(event.GetId() == ID_MENU_OPEN_RECENT4)
    path = crea::wx2std (pConfig->Read(wxT("RECENT4"),wxT("")));
  else if(event.GetId() == ID_MENU_OPEN_RECENT5)
    path = crea::wx2std (pConfig->Read(wxT("RECENT5"),wxT("")));

  std::cout << "Selection to open: " << path << std::endl;
  std::cout.flush();


  //populate model
  if(this->model->GetProject() != NULL)
    {
      std::cout << "Project not null, closing it" << std::endl;
      if(!this->model->CloseProject(result))
        {
          std::cout << "error closing project: " << *result << std::endl;
          wxMessageBox(crea::std2wx(result->c_str()),_T("New Project - Error!"),wxOK | wxICON_ERROR);
          event.Skip();
          return;
        }

      if(this->panel_Properties != NULL)
        {
          auiManager.DetachPane(this->panel_Properties);
          this->panel_Properties->Hide();
        }
      if(this->panel_ProjectActions != NULL)
        {
          auiManager.DetachPane(this->panel_ProjectActions);
          this->panel_ProjectActions->Hide();
        }
    }

  if (!this->model->OpenProject(path, result))
    {
      std::cout << "error opening project: " << *result << std::endl;
      wxMessageBox( crea::std2wx(result->c_str()), wxT("Open Project - Error"), wxICON_ERROR);
      event.Skip();
      return;
    };

  //update recently open projects
  if(pConfig->Read(wxT("RECENT1"),wxT("")) != crea::std2wx(this->model->GetProject()->GetPath()))
    {
      pConfig->Write(wxT("RECENT5"), pConfig->Read(wxT("RECENT4"),wxT("")));
      pConfig->Write(wxT("RECENT4"), pConfig->Read(wxT("RECENT3"),wxT("")));
      pConfig->Write(wxT("RECENT3"), pConfig->Read(wxT("RECENT2"),wxT("")));
      pConfig->Write(wxT("RECENT2"), pConfig->Read(wxT("RECENT1"),wxT("")));
      pConfig->Write(wxT("RECENT1"), crea::std2wx(this->model->GetProject()->GetPath()));
    }

  std::cout << "building ui" << std::endl;

  //populate tree control
  tree_Projects->BuildTree(this->model->GetModelElements(), this->model->GetProject());
  tree_Projects->Unselect();
  this->actualTreeItem.Unset();
      tree_Projects->SelectItem(this->model->GetProject()->GetId().GetWxId(), true);


  //change project's actions panel
  if(this->panel_ProjectActions!= NULL)
    {
      auiManager.DetachPane(this->panel_ProjectActions);
      this->panel_ProjectActions->Destroy();
      this->panel_ProjectActions = NULL;
    }
  panel_ProjectActions = new wxCDMProjectActionsPanel(
      this,
      this->model->GetProject(),
      ID_WINDOW_PROJ_ACTIONS,
      wxT("Project Actions Panel"),
      wxDefaultPosition,
      wxSize(800,200),
      0
  );
  panel_ProjectActions->SetMinSize(wxSize(500, 100));


  auiManager.AddPane(panel_ProjectActions, wxAuiPaneInfo().Bottom().MinSize(800,50).Name(wxT("panel_ProjectActions")).Caption(wxT("General Project Actions")).BestSize(800,70).CloseButton(false));

  auiManager.Update();

}
void wxCDMMainFrame::OnMenuOpenProject(wxCommandEvent& event)
{
  std::string* result;

  long style = wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST;
  wxDirDialog* FD = new wxDirDialog(this, wxT("Select the project directory"), wxT(""), style);
  long userResponse = FD->ShowModal();
  if(userResponse == wxID_OK)
    {
      std::string path = crea::wx2std (FD->GetPath());
      FD -> Destroy();
      FD = NULL;

      std::cout << "Selection to open: " << path << std::endl;


      //populate model
      if(this->model->GetProject() != NULL)
        {
          std::cout << "Project not null, closing it" << std::endl;
          if(!this->model->CloseProject(result))
            {
              std::cout << "error closing project: " << *result << std::endl;
              wxMessageBox(crea::std2wx(result->c_str()),_T("New Project - Error!"),wxOK | wxICON_ERROR);
              event.Skip();
              return;
            }

          if(this->panel_Properties != NULL)
            {
              auiManager.DetachPane(this->panel_Properties);
              this->panel_Properties->Hide();
            }
          if(this->panel_ProjectActions != NULL)
            {
              auiManager.DetachPane(this->panel_ProjectActions);
              this->panel_ProjectActions->Hide();    
            }
        }

      if (!this->model->OpenProject(path, result))
        {
          std::cout << "error opening project: " << *result << std::endl;
          wxMessageBox( crea::std2wx(result->c_str()), wxT("Open Project - Error"), wxICON_ERROR);
          event.Skip();
          return;
        };

      //update recently open projects
      wxConfigBase* pConfig = wxConfigBase::Get();
      if(pConfig->Read(wxT("RECENT1"),wxT("")) != crea::std2wx(this->model->GetProject()->GetPath()))
        {
          pConfig->Write(wxT("RECENT5"), pConfig->Read(wxT("RECENT4"),wxT("")));
          pConfig->Write(wxT("RECENT4"), pConfig->Read(wxT("RECENT3"),wxT("")));
          pConfig->Write(wxT("RECENT3"), pConfig->Read(wxT("RECENT2"),wxT("")));
          pConfig->Write(wxT("RECENT2"), pConfig->Read(wxT("RECENT1"),wxT("")));
          pConfig->Write(wxT("RECENT1"), crea::std2wx(this->model->GetProject()->GetPath()));
        }

      std::cout << "building ui" << std::endl;

      //populate tree control
      tree_Projects->BuildTree(this->model->GetModelElements(), this->model->GetProject());
      tree_Projects->Unselect();
      this->actualTreeItem.Unset();
	  tree_Projects->SelectItem(this->model->GetProject()->GetId().GetWxId(), true);


      //change project's actions panel
      if(this->panel_ProjectActions!= NULL)
        {
          auiManager.DetachPane(this->panel_ProjectActions);
          this->panel_ProjectActions->Destroy();
          this->panel_ProjectActions = NULL;
        }
      panel_ProjectActions = new wxCDMProjectActionsPanel(
          this,
          this->model->GetProject(),
          ID_WINDOW_PROJ_ACTIONS,
          wxT("Project Actions Panel"),
          wxDefaultPosition,
          wxSize(800,200),
          0
      );
      panel_ProjectActions->SetMinSize(wxSize(500, 100));


      auiManager.AddPane(panel_ProjectActions, wxAuiPaneInfo().Bottom().MinSize(800,50).Name(wxT("panel_ProjectActions")).Caption(wxT("General Project Actions")).BestSize(800,70).CloseButton(false));

      auiManager.Update();

    }
}

void wxCDMMainFrame::OnMenuCloseProject(wxCommandEvent& event)
{
  std::cout << "closing project" << std::endl;
  std::string* result;
  if(!this->model->CloseProject(result))
    {
      std::cout << "error closing project: " << *result << std::endl;
      wxMessageBox( crea::std2wx(result->c_str()), wxT("Close Project - Error"), wxICON_ERROR);
    }

  tree_Projects->BuildTree(this->model->GetModelElements(), this->model->GetProject());
  this->actualTreeItem.Unset();
  if(this->panel_Properties != NULL)
    {
      auiManager.DetachPane(this->panel_Properties);
	  this->panel_Properties->Hide();
    }
  if(this->panel_ProjectActions != NULL)
    {
      auiManager.DetachPane(this->panel_ProjectActions);
      this->panel_ProjectActions->Hide();    
    }

  this->panel_Properties = new wxCDMMainDescriptionPanel(
      this,
      ID_WINDOW_PROPERTIES,
      wxT("Description Panel"),
      wxDefaultPosition,
      wxSize(600, 400),
      0
  );

  auiManager.AddPane(panel_Properties, wxAuiPaneInfo().Center().Name(wxT("panel_Properties")).Caption(wxT("")).BestSize(600,400).CloseButton(false));

  auiManager.Update();
}

void wxCDMMainFrame::OnMenuExportHierarchy(wxCommandEvent& event)
{
  std::cerr << "Event OnMenuExportHierarchy not implemented" << std::endl;
  event.Skip();
}
void wxCDMMainFrame::OnMenuExit(wxCommandEvent& event)
{
  std::cout << "Closing CreaDevManager..." << std::endl;
  std::string* result;
  if(this->model->GetProject() != NULL && !this->model->CloseProject(result))
    {
      std::cout << "error closing project: " << *result << std::endl;
    }

  if(this->panel_Properties != NULL)
    {
      auiManager.DetachPane(this->panel_Properties);
      this->panel_Properties->Destroy();
      this->panel_Properties = NULL;
    }
  
  if(this->tree_Projects != NULL)
    {
      auiManager.DetachPane(this->tree_Projects);
      this->tree_Projects->Destroy();
      this->tree_Projects = NULL;
    }

  if(this->panel_ProjectActions != NULL)
    {
      auiManager.DetachPane(this->panel_ProjectActions);
      this->panel_ProjectActions->Destroy();
      this->panel_ProjectActions = NULL;
    }
  
  Close();
}

//Edit Menu
void wxCDMMainFrame::OnMenuRefreshProject(wxCommandEvent& event)
{
  std::string* result;
  if(!model->RefreshProject(result))
    {
      wxMessageBox( crea::std2wx(result->c_str()), wxT("Refresh Project - Error"), wxICON_ERROR);
    }
  if(this->model->GetProject() != NULL)
    {
      this->tree_Projects->BuildTree(this->model->GetModelElements(), this->model->GetProject());
      this->auiManager.Update();

      this->tree_Projects->Unselect();
	  this->actualTreeItem.Unset();
      this->tree_Projects->SelectItem(this->model->GetProject()->GetId().GetWxId(), true);
    }
  event.Skip();
}

void wxCDMMainFrame::OnMenuSettings(wxCommandEvent& event)
{
  wxCDMSettingsDialog* settingsDialog = new wxCDMSettingsDialog(this, -1);
  settingsDialog->SetHelpEnabled(this->help);

  int res = settingsDialog->ShowModal();
  if(res == wxID_OK)
    {
      this->help = settingsDialog->IsHelpEnabled();
      this->menu_Help->Check(ID_MENU_TOGGLE_HELP, this->help);
    }
}

void wxCDMMainFrame::OnMenuBBTKGraphicalEditor(wxCommandEvent& event)
{
  std::cerr << "Event OnMenuBBTKGraphicalEditor not implemented" << std::endl;
  event.Skip();
  if(CDMUtilities::openBBEditor())
    {
      wxMessageBox( wxT("Can't open the BB Graphical Editor. Please check your Crea Tools installation."), wxT("Refresh Project - Error"), wxICON_ERROR);
    }
}
void wxCDMMainFrame::OnMenuMiniTools(wxCommandEvent& event)
{
  if(CDMUtilities::openCreaToolsTools())
    {
      wxMessageBox( wxT("Can't open the Minitools. Please check your Crea Tools installation."), wxT("Refresh Project - Error"), wxICON_ERROR);
    }
}
void wxCDMMainFrame::OnMenuCodeEditor(wxCommandEvent& event)
{
  if(CDMUtilities::openTextEditor())
    {
      wxMessageBox( wxT("Can't open the Text Editor. Please check the default text editor command in the Crea Development Manager settings (Edit -> Settings)."), wxT("Refresh Project - Error"), wxICON_ERROR);
    }
}
void wxCDMMainFrame::OnMenuCommandLine(wxCommandEvent& event)
{
  if(CDMUtilities::openTerminal())
    {
      wxMessageBox( wxT("Can't open Terminal. Please check the default terminal command in the Crea Development Manager settings (Edit -> Settings)."), wxT("Refresh Project - Error"), wxICON_ERROR);
    }
}

//Help Menu
void wxCDMMainFrame::OnMenuShowProjectMap(wxCommandEvent& event)
{
  wxCDMProjectMapDialog* dialog = new wxCDMProjectMapDialog(this);

  dialog->Show(true);
}

//Help Menu
void wxCDMMainFrame::OnMenuHelp(wxCommandEvent& event)
{
  wxLaunchDefaultBrowser(_T("http://www.creatis.insa-lyon.fr/~gonzalez/documentationSWDoc.html"), 0);
}
void wxCDMMainFrame::OnMenuReportBug(wxCommandEvent& event)
{
  wxLaunchDefaultBrowser(_T("http://vip.creatis.insa-lyon.fr:9002/projects/crea"), 0);
}
void wxCDMMainFrame::OnMenuAboutCreaDevManager(wxCommandEvent& event)
{
  wxBoxSizer *topsizer;
  wxHtmlWindow *html;
  wxDialog dlg(this, wxID_ANY, wxString(_("About")));

  topsizer = new wxBoxSizer(wxVERTICAL);

  html = new wxHtmlWindow(&dlg, wxID_ANY, wxDefaultPosition, wxSize(380, 160), wxHW_SCROLLBAR_NEVER);
  html -> SetBorders(0);

  std::string content = ""
      "<html>"
      "<body bgcolor=\"#3333CC\">"
      "<table cellspacing=3 cellpadding=4 width=\"100%\">"
      "  <tr>"
      "    <td bgcolor=\"#3333CC\">"
      "    <center>"
      "    <font size=+2 color=\"#FFFFFF\"><b>CREA Development Manager</b>"
      "    </font>"
      "    </center>"
      "    </td>"
      "  </tr>"
      "  <tr>"
      "    <td bgcolor=\"#FFFFFF\">"
      "    <p><b><font size=+1>Creatis 2012 - Lyon, France</font></b></p>"
      "    <font size=-1>"
      "      <table cellpadding=0 cellspacing=0 width=\"100%\">"
      "        <tr>"
      "          <td width=\"65%\">"
      "            <p>Created by Daniel Gonz&aacute;lez - daniel.gonzalez@creatis.insa-lyon.fr</p>"
      "          </td>"
      "        </tr>"
      "      </table>"
      "      <font size=1>"
      "        <p>This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.</p>"
      "      </font>"
      "    </font>"
      "    </td>"
      "  </tr>"
      "</table>"
      "</body>"
      "</html>"
      ;

  html -> SetPage(crea::std2wx(content));
  html -> SetSize(html -> GetInternalRepresentation() -> GetWidth(),
      html -> GetInternalRepresentation() -> GetHeight());

  topsizer -> Add(html, 1, wxALL, 10);

#if wxUSE_STATLINE
  topsizer -> Add(new wxStaticLine(&dlg, wxID_ANY), 0, wxEXPAND | wxLEFT | wxRIGHT, 10);
#endif // wxUSE_STATLINE

  wxButton *bu1 = new wxButton(&dlg, wxID_OK, _("OK"));
  bu1 -> SetDefault();

  topsizer -> Add(bu1, 0, wxALL | wxALIGN_RIGHT, 15);

  dlg.SetSizer(topsizer);
  topsizer -> Fit(&dlg);

  dlg.ShowModal();
}
void wxCDMMainFrame::OnMenuAboutCreatis(wxCommandEvent& event)
{
  wxLaunchDefaultBrowser(_T("http://www.creatis.insa-lyon.fr/site/en"), 0);
}

void wxCDMMainFrame::OnTreeSelectionChanged(wxTreeEvent& event)
{

  //get selected element
  wxTreeItemId elementId = event.GetItem();
  //std::cout << "Tree Selection id: " << elementId.m_pItem << this->actualTreeItem.m_pItem << std::endl;
  //elementId.IsOk() && this->tree_Projects->IsSelected(elementId)
  if(elementId.IsOk() && this->actualTreeItem != elementId)
    {
      
      //std::cout << "Valid tree selection id: " << elementId.m_pItem << std::endl;
      //get element from model
      modelCDMIProjectTreeNode* element = this->model->GetModelElements()[elementId];

	  if (element == NULL)
		  return;
      std::cout << "Tree Selection: " << element->GetName() << std::endl;

	  //std::stringstream ss;
	  //ss << this->actualTreeItem.m_pItem << ":" << event.GetOldItem().m_pItem << " --> " << elementId.m_pItem;
	  //wxMessageBox( wxT("Tree Selection id: " + ss.str() + " by " + element->GetName()), wxT("Refresh Project - Error"), wxICON_ERROR);	
      this->actualTreeItem = elementId;

	  //get element type
      //project
      modelCDMProject* elementProject = dynamic_cast<modelCDMProject*>(element);
      wxPanel* description;
      if(elementProject != NULL)
        {
          //create element description
          description = new wxCDMProjectDescriptionPanel(
              this,
              elementProject,
              ID_WINDOW_PROPERTIES,
              wxT("Description Panel"),
              wxDefaultPosition,
              wxSize(600, 400),
              0
          );

        }
      else
        {
          //appli
          modelCDMAppli* elementAppli = dynamic_cast<modelCDMAppli*>(element);
          if(elementAppli != NULL)
            {
              //create element description
              description = new wxCDMAppliDescriptionPanel(
                  this,
                  elementAppli,
                  ID_WINDOW_PROPERTIES,
                  wxT("Description Panel"),
                  wxDefaultPosition,
                  wxSize(600, 400),
                  0
              );

            }
          else
            {
              //application
              modelCDMApplication* elementApplication = dynamic_cast<modelCDMApplication*>(element);
			  if(elementApplication != NULL)
                {
                  //create element description
                  description = new wxCDMApplicationDescriptionPanel(
                      this,
                      elementApplication,
                      ID_WINDOW_PROPERTIES,
                      wxT("Description Panel"),
                      wxDefaultPosition,
                      wxSize(600, 400),
                      0
                  );
                }
              else
                {
                  //lib
                  modelCDMLib* elementLib = dynamic_cast<modelCDMLib*>(element);
                  if(elementLib != NULL)
                    {
                      //create element description
                      description = new wxCDMLibDescriptionPanel(
                          this,
                          elementLib,
                          ID_WINDOW_PROPERTIES,
                          wxT("Description Panel"),
                          wxDefaultPosition,
                          wxSize(600, 400),
                          0
                      );
                    }
                  else
                    {
                      //library
                      modelCDMLibrary* elementLibrary = dynamic_cast<modelCDMLibrary*>(element);
                      if(elementLibrary != NULL)
                        {
                          //create element description
                          description = new wxCDMLibraryDescriptionPanel(
                              this,
                              elementLibrary,
                              ID_WINDOW_PROPERTIES,
                              wxT("Description Panel"),
                              wxDefaultPosition,
                              wxSize(600, 400),
                              0
                          );
                        }
                      else
                        {
                          //package
                          modelCDMPackage* elementPackage = dynamic_cast<modelCDMPackage*>(element);
                          if(elementPackage != NULL)
                            {
                              //create element description
                              description = new wxCDMPackageDescriptionPanel(
                                  this,
                                  elementPackage,
                                  ID_WINDOW_PROPERTIES,
                                  wxT("Description Panel"),
                                  wxDefaultPosition,
                                  wxSize(600, 400),
                                  0
                              );
                            }
                          else
                            {
                              //black box
                              modelCDMBlackBox* elementBlackBox = dynamic_cast<modelCDMBlackBox*>(element);
                              if(elementBlackBox != NULL)
                                {
                                  //create element description
                                  description = new wxCDMBlackBoxDescriptionPanel(
                                      this,
                                      elementBlackBox,
                                      ID_WINDOW_PROPERTIES,
                                      wxT("Description Panel"),
                                      wxDefaultPosition,
                                      wxSize(600, 400),
                                      0
                                  );
                                }
                              else
                                {
                                  //CMakeLists
                                  modelCDMCMakeListsFile* elementCMakeLists = dynamic_cast<modelCDMCMakeListsFile*>(element);
                                  if(elementCMakeLists != NULL)
                                    {
                                      //create element description
                                      description = new wxCDMCMakeListsDescriptionPanel(
                                          this,
                                          elementCMakeLists,
                                          ID_WINDOW_PROPERTIES,
                                          wxT("Description Panel"),
                                          wxDefaultPosition,
                                          wxSize(600, 400),
                                          0
                                      );
                                    }
                                  else
                                    {
                                      //CodeFile
                                      modelCDMCodeFile* elementCodeFile = dynamic_cast<modelCDMCodeFile*>(element);
                                      if(elementCodeFile != NULL)
                                        {
                                          //create element description
                                          description = new wxCDMCodeFileDescriptionPanel(
                                              this,
                                              elementCodeFile,
                                              ID_WINDOW_PROPERTIES,
                                              wxT("Description Panel"),
                                              wxDefaultPosition,
                                              wxSize(600, 400),
                                              0
                                          );
                                        }
                                      else
                                        {
                                          //BBSFile
                                          modelCDMBBSFile* elementBBSFile = dynamic_cast<modelCDMBBSFile*>(element);
                                          if(elementBBSFile != NULL)
                                            {
                                              //create element description
                                              description = new wxCDMBBSFileDescriptionPanel(
                                                  this,
                                                  elementBBSFile,
                                                  ID_WINDOW_PROPERTIES,
                                                  wxT("Description Panel"),
                                                  wxDefaultPosition,
                                                  wxSize(600, 400),
                                                  0
                                              );
                                            }
                                          else
                                            {
                                              //BBSFile
                                              modelCDMBBGFile* elementBBGFile = dynamic_cast<modelCDMBBGFile*>(element);
                                              if(elementBBGFile != NULL)
                                                {
                                                  //create element description
                                                  description = new wxCDMBBGFileDescriptionPanel(
                                                      this,
                                                      elementBBGFile,
                                                      ID_WINDOW_PROPERTIES,
                                                      wxT("Description Panel"),
                                                      wxDefaultPosition,
                                                      wxSize(600, 400),
                                                      0
                                                  );
                                                }
                                              else
                                                {
                                                  //folder
                                                  modelCDMFolder* elementFolder = dynamic_cast<modelCDMFolder*>(element);
                                                  if(elementFolder != NULL)
                                                    {
                                                      //create element description
                                                      description = new wxCDMFolderDescriptionPanel(
                                                          this,
                                                          elementFolder,
                                                          ID_WINDOW_PROPERTIES,
                                                          wxT("Description Panel"),
                                                          wxDefaultPosition,
                                                          wxSize(600, 400),
                                                          0
                                                      );
                                                    }
                                                  else
                                                    {
                                                      //file
                                                      modelCDMFile* elementFile = dynamic_cast<modelCDMFile*>(element);
                                                      if(elementFile != NULL)
                                                        {
                                                          //create element description
                                                          description = new wxCDMFileDescriptionPanel(
                                                              this,
                                                              elementFile,
                                                              ID_WINDOW_PROPERTIES,
                                                              wxT("Description Panel"),
                                                              wxDefaultPosition,
                                                              wxSize(600, 400),
                                                              0
                                                          );
                                                        }
                                                      else
                                                        {

                                                          //main if not any
                                                          //create element description
                                                          description = new wxCDMMainDescriptionPanel(
                                                              this,
                                                              ID_WINDOW_PROPERTIES,
                                                              wxT("Description Panel"),
                                                              wxDefaultPosition,
                                                              wxSize(600, 400),
                                                              0
                                                          );
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

      //delete old view
      if(this->panel_Properties!= NULL)
        {
          this->panel_Properties->Hide();
          auiManager.DetachPane(this->panel_Properties);
          //this->panel_Properties->Destroy();
          //this->panel_Properties = NULL;
        }
      //set new view
      auiManager.AddPane(description, wxAuiPaneInfo().Center().Name(wxT("panel_Properties")).Caption(wxT("")).BestSize(600,400).CloseButton(false));
      this->panel_Properties = description;

      auiManager.Update();
    }
  else
    {
      event.Skip();
	}

  return;

}

void wxCDMMainFrame::OnChangeView(wxCommandEvent& event)
{
  modelCDMIProjectTreeNode* myItem = NULL;
  wxPanel* description = NULL;
  switch(event.GetId())
  {
  case 0:
    myItem = ((modelCDMIProjectTreeNode*)event.GetClientData());
    //select out old one to generate selection event
    this->tree_Projects->Unselect();
    this->tree_Projects->SelectItem(myItem->GetId().GetWxId());
    this->tree_Projects->Expand(myItem->GetId().GetWxId());
    break;
  case 1:
    
    if(event.GetString() == wxT("manage_packages"))
      {
        //this->tree_Projects->Expand(this->model->GetProject()->GetId());
        //this->tree_Projects->Unselect();
        this->actualTreeItem.Unset();
	description = new wxCDMPackageManagerPanel(
            this,
            this->model->GetProject(),
            ID_WINDOW_PROPERTIES,
            wxT("Description Panel"),
            wxDefaultPosition,
            wxSize(600, 400),
            0
        );
      }
    else if(event.GetString() == wxT("manage_libraries"))
      {
        this->tree_Projects->SelectItem(this->model->GetProject()->GetLib()->GetId().GetWxId());
        this->tree_Projects->Expand(this->model->GetProject()->GetLib()->GetId().GetWxId());
        break;
      }
    else if(event.GetString() == wxT("manage_applications"))
      {
        this->tree_Projects->SelectItem(this->model->GetProject()->GetAppli()->GetId().GetWxId());
        this->tree_Projects->Expand(this->model->GetProject()->GetAppli()->GetId().GetWxId());
        break;
      }
    else if(event.GetString() == wxT("blackbox"))
      {
        modelCDMBlackBox* bb = (modelCDMBlackBox*)event.GetClientData();
        this->actualTreeItem.Unset();
        this->tree_Projects->SelectItem(bb->GetHeaderFile()->GetId().GetWxId());
        description = new wxCDMBlackBoxDescriptionPanel(
            this,
            bb,
            ID_WINDOW_PROPERTIES,
            wxT("Description Panel"),
            wxDefaultPosition,
            wxSize(600, 400),
            0
        );

		
      }

    //delete old view
    if(this->panel_Properties!= NULL)
      {
        this->panel_Properties->Hide();
        auiManager.DetachPane(this->panel_Properties);
        this->panel_Properties->Destroy();
        this->panel_Properties = NULL;
      }
    //set new view
    auiManager.AddPane(description, wxAuiPaneInfo().Center().Name(wxT("panel_Properties")).Caption(wxT("")).BestSize(600,400).CloseButton(false));
    this->panel_Properties = description;
    auiManager.Update();
    break;
  default:
    event.Skip();
    break;
  }
  
}

void wxCDMMainFrame::OnElementSelected(wxCommandEvent& event)
{
  //std::cout << "element " << event.GetInt() << std::endl;
  modelCDMIProjectTreeNode* item = (modelCDMIProjectTreeNode*)event.GetClientData();
  this->tree_Projects->EnsureVisible(item->GetId().GetWxId());
  this->tree_Projects->SetItemBold(item->GetId().GetWxId(), true);
  this->tree_Projects->SetItemTextColour(item->GetId().GetWxId(), wxColour(0,0,255));
  this->tree_Projects->SetItemBackgroundColour(item->GetId().GetWxId(), wxColour(230,230,255));
  this->tree_Projects->UpdateWindowUI(wxUPDATE_UI_RECURSE);
  auiManager.Update();
}

void wxCDMMainFrame::OnElementDeselected(wxCommandEvent& event)
{
  modelCDMIProjectTreeNode* item = (modelCDMIProjectTreeNode*)event.GetClientData();
  this->tree_Projects->SetItemBold(item->GetId().GetWxId(), false);
  this->tree_Projects->SetItemTextColour(item->GetId().GetWxId(), wxColour(0,0,0));
  this->tree_Projects->SetItemBackgroundColour(item->GetId().GetWxId(), wxColour(255,255,255));
  this->tree_Projects->UpdateWindowUI(wxUPDATE_UI_RECURSE);
  auiManager.Update();
}

void wxCDMMainFrame::OnMenuToggleHelp(wxCommandEvent& event)
{
  this->help = !this->help;
  this->menu_Help->Check(ID_MENU_TOGGLE_HELP, this->help);
}

void wxCDMMainFrame::OnDisableHelp(wxCommandEvent& event)
{
  if (event.GetInt())
    this->help = false;
  else
    this->help = true;

  this->menu_Help->Check(ID_MENU_TOGGLE_HELP, this->help);
}
