/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMIProjectTreeNode.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */
#include "modelCDMIProjectTreeNode.h"
#include <algorithm>

#include "CDMUtilities.h"

#include "wx/dir.h"

bool modelCDMIProjectTreeNode::CompareNodeItem(const modelCDMIProjectTreeNode* x, const modelCDMIProjectTreeNode* y)
{
  bool returnValue;
  bool noWinner = true;
  unsigned int i = 0;
  std::string xName = x->GetName();
  std::string yName = y->GetName();
  unsigned char xType = x->GetType();
  unsigned char yType = y->GetType();

  while ((i < xName.length()) && (i < yName.length()))
    {
      if (tolower (xName[i]) < tolower (yName[i]))
        {
          noWinner = false;
          returnValue = true;
          break;
        }
      else if (tolower (xName[i]) > tolower (yName[i]))
        {
          noWinner = false;
          returnValue = false;
          break;
        }
      i++;
    }

  if(noWinner)
    {
      if (xName.length() < yName.length())
        returnValue = true;
      else
        returnValue = false;
    }

  if(xType != yType)
    {
      if(xType == wxDIR_DIRS)
        returnValue = true;
      else
        returnValue = false;
    }

  return returnValue;
}

const wxCDMTreeItemId& modelCDMIProjectTreeNode::GetId() const
{
  return this->id;
}

const std::string& modelCDMIProjectTreeNode::GetPath() const
{
  return this->path;
}

const std::string& modelCDMIProjectTreeNode::GetName() const
{
  return this->name;
}

const unsigned char& modelCDMIProjectTreeNode::GetType() const
{
  return this->type;
}

const int& modelCDMIProjectTreeNode::GetLevel() const
{
  return this->level;
}

modelCDMIProjectTreeNode* modelCDMIProjectTreeNode::GetParent() const
{
  return this->parent;
}

std::vector<modelCDMIProjectTreeNode*> modelCDMIProjectTreeNode::GetParents() const
{
  std::vector<modelCDMIProjectTreeNode*> family;
  modelCDMIProjectTreeNode* someFamily = this->parent;
  while(someFamily != NULL)
    {
      family.push_back(someFamily);
      someFamily = someFamily->GetParent();
    }
  return family;

}

const std::vector<modelCDMIProjectTreeNode*>& modelCDMIProjectTreeNode::GetChildren() const
{
  return this->children;
}

void modelCDMIProjectTreeNode::SetId(const wxCDMTreeItemId& id)
{
  this->id = id;
}

void modelCDMIProjectTreeNode::SortChildren()
{
  std::sort(this->children.begin(), this->children.end(), CompareNodeItem);
}

void modelCDMIProjectTreeNode::SetChildren(
    const std::vector<modelCDMIProjectTreeNode*>& children)
{
  this->children.clear();
  this->children = children;
}

const bool modelCDMIProjectTreeNode::Refresh(std::string*& result)
{
  //TODO: implement method
  return false;
}

const int& modelCDMIProjectTreeNode::GetLength()
{
  return this->length;
}

const bool modelCDMIProjectTreeNode::OpenInFileExplorer(std::string*& result) const
{
  if (!CDMUtilities::openFileExplorer(this->GetPath()))
    return true;
  else
    {
      result = new std::string("Couldn't open file.");
      return false;
    }
}
