/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMLibHelpDialog.cpp
 *
 *  Created on: 11/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMLibHelpDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMLibHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMLibHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMLibHelpDialog::OnEditCMake)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMLibHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMLibHelpDialog::wxCDMLibHelpDialog(
    wxWindow* parent,
    modelCDMLib* lib,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMLibHelpDialog::Create(parent, id, caption, position, size, style);
  this->lib = lib;
}

wxCDMLibHelpDialog::~wxCDMLibHelpDialog()
{
}

bool wxCDMLibHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMLibHelpDialog::CreateControls()
{
  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Managing your libraries"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Libraries contain the core classes of your project, these libraries should be used by black boxes and applications "
          "from this project to perform the main functions of the project. Functions implementing important tasks such as image "
          "processing algorithms or point cloud processing algorithms should be developed in the libraries of the project, while "
          "the black boxes and applications serve as interfaces between the user or other platforms and the libraries' functions.\n"
          "\n"
          "In the library manager you can view a list of the available libraries in the current project, as well as create "
          "new libraries. Remember that any library you create must be included in the lib's folder CMakeLists file by using the "
          "\"ADD_SUBDIRECTORY([libraryName])\" command. You can open this file by clicking on the \"Edit Lib's CMakeLists File\" "
          "button below or in the Library Manager the \"Edit CMakeLists file\" button and include the desired libraries at the end "
          "of the file."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

  wxButton* editCMakeLibBtn = new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Edit Lib's CMakeLists File"));
  editCMakeLibBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibHelpDialog::OnEditCMakeMouseEnter,NULL,this);
  editCMakeLibBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibHelpDialog::OnEditCMakeMouseExit,NULL,this);
  v_sizer1->Add(editCMakeLibBtn, 0, wxEXPAND | wxLEFT | wxRIGHT, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);

}

void wxCDMLibHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMLibHelpDialog::OnEditCMake(wxCommandEvent& event)
{
  std::string* result;
  if(!this->lib->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}


void wxCDMLibHelpDialog::OnEditCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMLibHelpDialog::OnEditCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->lib->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->lib->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}


void wxCDMLibHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
