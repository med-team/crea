/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMLibrary.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMLIBRARY_H_
#define MODELCDMLIBRARY_H_

#include<iostream>
#include<vector>
#include<map>

#include "modelCDMFolder.h"

/**
 * Class that represents a library in a Crea project.
 */
class modelCDMLibrary : public modelCDMFolder
{
public:
  /**
   * Default Constructor
   */
  modelCDMLibrary();
  /**
   * Constructor of the Library node.
   * @param parent Parent node of the library node.
   * @param path Full path to the library node.
   * @param name Name of the library folder node.
   * @param level Project hierarchy level of the library folder node.
   */
  modelCDMLibrary(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level = 2);
  /**
   * Destructor.
   */
  ~modelCDMLibrary();

  /**
   * Retrieves the name of the Library node. The name of a library can be different than the name of the folder that contains it
   * @return Name of the library node.
   */
  const std::string& GetNameLibrary() const;
  /**
   * Renames the library with the given name.
   * @param fileName New name of the library node.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool SetNameLibrary(const std::string& fileName, std::string*& result);

  /**
   * Creates a new folder inside the library folder node. This method not only modifies the project model, but also the system.
   * @param name Name of the new folder.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  modelCDMFolder* CreateFolder(const std::string& name, std::string*& result);

  /**
   * Refreshes the structure of the library folder node. Deletes files and folders deleted since last refresh, adds files and folders created since last refresh.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Checks the library structure and CMakeLists file to find project structure definition problems before compiling the project.
   * @param properties Project properties.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks the library CMakeLists file to check which third party libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> Get3rdPartyLibraries();

  /**
   * Sets the 3rd party library inclusion in the CMakeLists file.
   * @return if the operation was successful.
   */
  bool Set3rdPartyLibrary(const std::string& library_name, const bool& toInclude);

  /**
   * Checks the library CMakeLists file to check which custom libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> GetCustomLibraries();

  /**
   * Sets the custom library inclusion in the CMakeLists file.
   * @return if the operation was successful.
   */
  bool SetCustomLibrary(const std::string& library_name, const bool& toInclude);

private:
  /**
   * Name of the library node. The name of a library can be different than the library folder name.
   */
  std::string nameLibrary;
  /**
   * Folder reference array of folder node inside the library node.
   */
  std::vector<modelCDMFolder*> folders;
};

#endif /* MODELCDMLIBRARY_H_ */
