/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * creaDevManagerIds.h
 *
 *  Created on: 19/10/2012
 *      Author: Daniel Felipe Gonzalez Obando Gonzalez
 */

#ifndef CREADEVMANAGERIDS_H_
#define CREADEVMANAGERIDS_H_

#define ID_STATUS_BAR                   10200

#define ID_MENU_NEW_PROJECT             10201
#define ID_MENU_OPEN_PROJECT            10202
#define ID_MENU_OPEN_RECENT             10203
#define ID_MENU_CLOSE_PROJECT           10204
#define ID_MENU_CLOSE_ALL_PROJECTS      10205
#define ID_MENU_EXPORT_HIERARCHY        10206
#define ID_MENU_EXIT                    10207

#define ID_MENU_REFRESH_PROJECT         10208
#define ID_MENU_SETTINGS                10209
#define ID_MENU_COPY                    10210
#define ID_MENU_PASTE                   10211
#define ID_MENU_DELETE                  10212
#define ID_MENU_SELECT_ALL              10213
#define ID_MENU_SELECT_NONE             10214

#define ID_MENU_EVENT_LOG               10215
#define ID_MENU_BBTK_GRAPHICAL_EDITOR   10216
#define ID_MENU_MINITOOLS               10217
#define ID_MENU_CODE_EDITOR             10218
#define ID_MENU_COMMAND_LINE            10219

#define ID_MENU_TOGGLE_HELP             10220
#define ID_MENU_HELP                    10221
#define ID_MENU_REPORT_BUG              10222
#define ID_MENU_ABOUT_CREADEVMANAGER    10223
#define ID_MENU_ABOUT_CREATIS           10224
#define ID_MENU_SHOW_PROJECT_MAP        10225

#define ID_TREE_PROJECTS                10226
#define ID_WINDOW_PROPERTIES            10227
#define ID_WINDOW_PROJ_ACTIONS          10228

#define ID_BUTTON_NEXT                  10300
#define ID_BUTTON_PREV                  10301
#define ID_BUTTON_CANCEL                10302
#define ID_BUTTON_CHOOSE                10303

#define ID_BUTTON_NEWPROJECT            10304
#define ID_BUTTON_OPENPROJECT           10305

#define ID_BUTTON_CREATE_PACKAGE        10306
#define ID_BUTTON_CREATE_BLACKBOX       10307
#define ID_BUTTON_CREATE_LIBRARY        10308
#define ID_BUTTON_CREATE_APPLICATION    10309
#define ID_BUTTON_EDIT_CMAKELISTSFILE   10310
#define ID_BUTTON_CREATE_FOLDER         10311
#define ID_BUTTON_CREATE_CLASS          10312
#define ID_BUTTON_OPEN_CXX              10313
#define ID_BUTTON_OPEN_HXX              10314
#define ID_BUTTON_OPEN_FOLDER           10315
#define ID_BUTTON_OPEN_FILE             10316
#define ID_BUTTON_OPEN_COMMAND          10317
#define ID_BUTTON_SET_VERSION           10318
#define ID_BUTTON_SET_BUILD_PATH        10319
#define ID_BUTTON_OPEN_BUILD_PATH       10320
#define ID_BUTTON_SET_AUTHOR            10321
#define ID_BUTTON_SET_DESCRIPTION       10322
#define ID_BUTTON_SET_NAME              10323
#define ID_BUTTON_SET_CATEGORY          10324


#define ID_BUTTON_BUILD_PROJECT         10325
#define ID_BUTTON_CONFIGURE_BUILD       10326
#define ID_BUTTON_CONNECT_PROJECT       10327

#define ID_BUTTON_GOTO_PACKAGE_MANAGER  10328
#define ID_BUTTON_GOTO_APPLI_MANAGER    10329
#define ID_BUTTON_GOTO_LIB_MANAGER      10330

#define ID_LINK_SELECT_PACKAGE          10331
#define ID_LINK_SELECT_LIBRARY          10332
#define ID_LINK_SELECT_APPLICATION      10333
#define ID_LINK_SELECT_BLACKBOX         10334

#define ID_CHECK_INCLUDE_LIBRARY        10335
#define ID_CHECK_INCLUDE_3RDLIBRARY     10336
#define ID_CHECK_INCLUDE_PACKAGE        10337
#define ID_CHECK_INCLUDE_APPLICATION    10338

#define ID_CHECKBOX_ENABLE_HELP         10339
#define ID_CHECKBOX_DISABLE_HELP        10340
#define ID_CHECKBOX_TOGGLE_HELP         10341

#define ID_BUTTON_CHECK_PROJECT         10342

#define ID_MENU_OPEN_RECENT1            10343
#define ID_MENU_OPEN_RECENT2            10344
#define ID_MENU_OPEN_RECENT3            10345
#define ID_MENU_OPEN_RECENT4            10346
#define ID_MENU_OPEN_RECENT5            10347

#endif /* CREADEVMANAGERIDS_H_ */
