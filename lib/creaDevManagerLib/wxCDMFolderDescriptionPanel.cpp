/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMFolderDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMFolderDescriptionPanel.h"

#include "wxCDMMainFrame.h"
#include "CDMUtilities.h"

#include "creaDevManagerIds.h"
#include "images/FdIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMFolderDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMFolderDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMFolderDescriptionPanel::OnBtnOpenInExplorer)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMFolderDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_CREATE_CLASS, wxCDMFolderDescriptionPanel::OnBtnCreateClass)
EVT_BUTTON(ID_BUTTON_CREATE_FOLDER, wxCDMFolderDescriptionPanel::OnBtnCreateFolder)
END_EVENT_TABLE()

wxCDMFolderDescriptionPanel::wxCDMFolderDescriptionPanel(
    wxWindow* parent,
    modelCDMFolder* folder,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMFolderDescriptionPanel::Create(parent, folder, id, caption, pos, size, style);
}

wxCDMFolderDescriptionPanel::~wxCDMFolderDescriptionPanel()
{
}

bool wxCDMFolderDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMFolder* folder,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->folder = folder;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMFolderDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->folder->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
      returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->folder->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(FdIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Folder")),0, wxALIGN_LEFT, 0);
    //Folder Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->folder->GetName())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);
  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("Open in File Explorer"));
  openFolderbt->SetToolTip(wxT("Open this folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);

  if(this->folder->HasCMakeLists())
    {
      wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
      editCMakebt->SetToolTip(wxT("Open the CMakeLists.txt file in this folder with the default text editor."));
      editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMFolderDescriptionPanel::OnCMakeMouseEnter,NULL,this);
      editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMFolderDescriptionPanel::OnCMakeMouseExit,NULL,this);
      actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
    }

  wxButton* createClassbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_CLASS, _T("Create Class"));
  createClassbt->SetToolTip(wxT("Create a new class (.h and .cpp) inside this folder."));
  actionsGridSizer->Add(createClassbt, 1, wxALL | wxEXPAND, 5);

  wxButton* createFolderbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_FOLDER, _T("Create Folder"));
  createFolderbt->SetToolTip(wxT("Create a new folder inside this folder."));
  actionsGridSizer->Add(createFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxEXPAND | wxALL, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);
}

void wxCDMFolderDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->folder->GetParents();
  std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
  //std::cout << parentURL << std::endl;
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      if (parents[i]->GetPath() == parentURL)
        {
          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetClientData(parents[i]);
          newEvent->SetId(0);
          wxPostEvent(this->GetParent(), *newEvent);
        }
    }
}

void wxCDMFolderDescriptionPanel::OnBtnOpenInExplorer(wxCommandEvent& event)
{
  std::string* result;
  if(!this->folder->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMFolderDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->folder->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->folder->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->folder->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
}

void wxCDMFolderDescriptionPanel::OnBtnCreateClass(wxCommandEvent& event)
{
  //get class name from user
  wxTextEntryDialog* newClassDlg = new wxTextEntryDialog(
      this,
      wxT("Please enter the new class name."),
      wxT("New Class - creaDevManager"),
      wxT(""),
      wxOK | wxCANCEL
  );

  if (newClassDlg->ShowModal() == wxID_OK)
    {
      std::string className = crea::wx2std(newClassDlg->GetValue());
      //check class name
      if(className.size() > 0)
        {
          if(!this->folder->CreateClass(className))
            wxMessageBox(crea::std2wx("Something has gone wrong with the creation of the class."),_T("New Class - Error!"),wxOK | wxICON_ERROR);

          ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetId(0);
          newEvent->SetClientData(folder);
          wxPostEvent(this->GetParent(), *newEvent);

          wxMessageBox(crea::std2wx("The class has been created successfully."),_T("New Class - Success"),wxOK | wxICON_INFORMATION);
        }
      else
        {
          wxMessageBox(crea::std2wx("The new class name cannot be empty."),_T("New Class - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMFolderDescriptionPanel::OnBtnCreateFolder(wxCommandEvent& event)
{
  //get name
  wxString folderName = wxGetTextFromUser(
      wxT("Enter the name of the new folder:"),
      wxT("Create Folder - creaDevManager")
  );
  //check name
  std::vector<std::string> parts;
  CDMUtilities::splitter::split(parts, crea::wx2std(folderName), " /\\\"", CDMUtilities::splitter::no_empties);
  if(parts.size() > 0)
    {
      std::string* result;
      modelCDMFolder* folderC = this->folder->CreateFolder(crea::wx2std(folderName), result);
      if(folderC == NULL)
        {
          wxMessageBox(crea::std2wx(*result),_T("Create Folder - Error!"),wxOK | wxICON_ERROR);
          return;
        }
      ((wxCDMMainFrame*)this->GetParent())->RefreshProject();
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
      newEvent->SetClientData(folderC);
      wxPostEvent(this->GetParent(), *newEvent);
      wxMessageBox(crea::std2wx("The folder was successfully created"),_T("Create Folder - Success"),wxOK | wxICON_INFORMATION);
    }
  else
    {
      wxMessageBox(crea::std2wx("No name specified"),_T("Create Folder - Error!"),wxOK | wxICON_ERROR);
    }
}

void wxCDMFolderDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->folder->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->folder->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMFolderDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->folder->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->folder->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
