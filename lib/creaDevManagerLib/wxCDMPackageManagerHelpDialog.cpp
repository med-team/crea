/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMPackageManagerHelpDialog.cpp
 *
 *  Created on: 7/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMPackageManagerHelpDialog.h"

#include "creaDevManagerIds.h"

BEGIN_EVENT_TABLE(wxCDMPackageManagerHelpDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMPackageManagerHelpDialog::OnFinish)
EVT_BUTTON(ID_BUTTON_CREATE_PACKAGE, wxCDMPackageManagerHelpDialog::OnCreatePackage)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMPackageManagerHelpDialog::OnEditCMake)
EVT_CHECKBOX(ID_CHECKBOX_DISABLE_HELP, wxCDMPackageManagerHelpDialog::OnDisableHelp)
END_EVENT_TABLE()

wxCDMPackageManagerHelpDialog::wxCDMPackageManagerHelpDialog(
    wxWindow* parent,
    wxCDMPackageManagerPanel* packageManager,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  wxCDMPackageManagerHelpDialog::Create(parent, id, caption, position, size, style);
  this->packageManager = packageManager;
}

wxCDMPackageManagerHelpDialog::~wxCDMPackageManagerHelpDialog()
{
}

bool wxCDMPackageManagerHelpDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMPackageManagerHelpDialog::CreateControls()
{
  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);


  wxStaticText* title = new wxStaticText(this, wxID_ANY, wxT("Managing your packages"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);//new wxRichTextCtrl(this,wxID_ANY, wxString("Create a new project"), wxDefaultPosition, wxDefaultSize, wxRE_READONLY);
  v_sizer1->Add(title, 0, wxALIGN_LEFT | wxALL | wxALIGN_CENTER_VERTICAL, 5);

  wxStaticText* instruction = new wxStaticText(
      this,
      wxID_ANY,
      crea::std2wx(
          "Packages contain black boxes, which allow to work modularly with other boxes. This boxes can use the functions "
          "present in your libraries and expose them to work in a BBTK-fashion.\n"
          "\n"
          "In the package manager you can view a list of the available packages in the current project, as well as create "
          "new packages. Remember that any package you make must be included in the CMakeLists file. You can do that by "
          "clicking on the \"Edit CMakeLists File\" button in the package manager section and include the desired packages "
          "at the end of the file using \"ADD_SUBDIRECTORY([packageName])\"."),
          wxDefaultPosition,
          wxDefaultSize,
          wxALIGN_LEFT
  );
  v_sizer1->Add(instruction, 0,wxEXPAND | wxALL, 5);

//  wxFlexGridSizer* formItems = new wxFlexGridSizer(1,2,9,15);
//
//  wxButton* createPackageBtn = new wxButton(this, ID_BUTTON_CREATE_PACKAGE, wxT("Create a Package"));
//  wxButton* editCMakeBtn= new wxButton(this, ID_BUTTON_EDIT_CMAKELISTSFILE, wxT("Edit the CMakeLists File"));
//  editCMakeBtn->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerHelpDialog::OnEditCMakeMouseEnter,NULL,this);
//  editCMakeBtn->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMPackageManagerHelpDialog::OnEditCMakeMouseExit,NULL,this);
//
//  formItems->Add(createPackageBtn, 1, wxALIGN_CENTER);
//  formItems->Add(editCMakeBtn, 1, wxALIGN_CENTER);
//
//  formItems->AddGrowableCol(0,1);
//  formItems->AddGrowableCol(1,1);
//
//  v_sizer1->Add(formItems, 1, wxEXPAND | wxALL, 15);

  v_sizer1->Add(new wxCheckBox(this, ID_CHECKBOX_DISABLE_HELP, wxT("&Disable help")), 0, wxALIGN_RIGHT | wxRIGHT, 10);

  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")), 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM | wxALIGN_CENTER_VERTICAL, 30);

  SetSizer(v_sizer1);
  //v_sizer1->RecalcSizes();
}

void wxCDMPackageManagerHelpDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

void wxCDMPackageManagerHelpDialog::OnCreatePackage(wxCommandEvent& event)
{
  if(this->packageManager != NULL)
    {
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_TOOL_CLICKED);
      wxPostEvent(this->packageManager, *newEvent);
    }
  event.Skip();

  this->EndDialog(wxID_OK);
}

void wxCDMPackageManagerHelpDialog::OnEditCMake(wxCommandEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_TOOL_ENTER);
  if(this->packageManager != NULL)
    wxPostEvent(this->packageManager, *newEvent);
  event.Skip();

  this->EndDialog(wxID_OK);
}


void wxCDMPackageManagerHelpDialog::OnEditCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(packageManager->GetProject()->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(packageManager->GetProject()->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMPackageManagerHelpDialog::OnEditCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(packageManager->GetProject()->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(packageManager->GetProject()->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}


void wxCDMPackageManagerHelpDialog::OnDisableHelp(wxCommandEvent& event)
{
  wxPostEvent(this->GetParent(), event);
}
