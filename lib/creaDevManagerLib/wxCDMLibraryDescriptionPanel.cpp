/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * wxCDMLibraryDescriptionPanel.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMLibraryDescriptionPanel.h"

#include "CDMUtilities.h"
#include "wxCDMMainFrame.h"

#include "wxCDMLibraryHelpDialog.h"

#include "creaDevManagerIds.h"
#include "images/LIcon64.xpm"

BEGIN_EVENT_TABLE(wxCDMLibraryDescriptionPanel, wxPanel)
EVT_HYPERLINK(ID_BUTTON_PREV, wxCDMLibraryDescriptionPanel::OnBtnReturn)
EVT_BUTTON(ID_BUTTON_SET_NAME, wxCDMLibraryDescriptionPanel::OnBtnSetExeName)
EVT_BUTTON(ID_BUTTON_CREATE_CLASS, wxCDMLibraryDescriptionPanel::OnBtnCreateClass)
EVT_BUTTON(ID_BUTTON_CREATE_FOLDER, wxCDMLibraryDescriptionPanel::OnBtnCreateFolder)
EVT_BUTTON(ID_BUTTON_EDIT_CMAKELISTSFILE, wxCDMLibraryDescriptionPanel::OnBtnEditCMakeLists)
EVT_BUTTON(ID_BUTTON_OPEN_FOLDER, wxCDMLibraryDescriptionPanel::OnBtnOpenFolder)
EVT_CHECKBOX(ID_CHECK_INCLUDE_3RDLIBRARY, wxCDMLibraryDescriptionPanel::On3rdLibraryChBChange)
EVT_CHECKBOX(ID_CHECK_INCLUDE_LIBRARY, wxCDMLibraryDescriptionPanel::OnLibraryChBChange)
END_EVENT_TABLE()

wxCDMLibraryDescriptionPanel::wxCDMLibraryDescriptionPanel(
    wxWindow* parent,
    modelCDMLibrary* library,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxCDMLibraryDescriptionPanel::Create(parent, library, id, caption, pos, size, style);
}

wxCDMLibraryDescriptionPanel::~wxCDMLibraryDescriptionPanel()
{
}

bool wxCDMLibraryDescriptionPanel::Create(
    wxWindow* parent,
    modelCDMLibrary* library,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& pos,
    const wxSize& size,
    long style
)
{
  wxPanel::Create(parent, id, pos, size, style);
  this->library = library;
  CreateControls();
  // this part makes the scrollbars show up
  this->FitInside(); // ask the sizer about the needed size
  this->SetScrollRate(5, 5);
  return TRUE;
}

void wxCDMLibraryDescriptionPanel::CreateControls()
{
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

  //Links to return
  wxBoxSizer *linksSizer = new wxBoxSizer(wxHORIZONTAL);
  std::vector<modelCDMIProjectTreeNode*> parents = this->library->GetParents();
  for (int i = 0; i < (int)(parents.size()); i++)
    {
      wxHyperlinkCtrl* returnLnk = new wxHyperlinkCtrl(this, ID_BUTTON_PREV, crea::std2wx(parents[parents.size()-1-i]->GetName()), crea::std2wx(parents[parents.size()-1-i]->GetPath()), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
      returnLnk->SetWindowStyle(wxNO_BORDER);
	  returnLnk->SetToolTip(crea::std2wx("Return to " + parents[parents.size()-1-i]->GetName() + "."));
      linksSizer->Add(returnLnk, 0, wxALIGN_CENTER_VERTICAL | wxLEFT | wxRIGHT, 5);
      linksSizer->Add(new wxStaticText(this,wxID_ANY, wxT("/")), 0, wxALIGN_CENTER, 0);
    }

  linksSizer->Add(new wxStaticText(this, wxID_ANY, crea::std2wx(this->library->GetName())), 0, wxALIGN_CENTER, 0);

  sizer->Add(linksSizer, 0, wxALIGN_CENTER | wxALL, 5);

  //Header
  wxBoxSizer* headerSizer = new wxBoxSizer(wxHORIZONTAL);
  {
    //Image
    headerSizer->Add(new wxStaticBitmap(this, -1, wxBitmap(LIcon64)),0, wxALIGN_CENTER, 0);
    wxBoxSizer* textSizer = new wxBoxSizer(wxVERTICAL);
    //Title
    textSizer->Add(new wxStaticText(this, -1, _("Library")),0, wxALIGN_LEFT, 0);
    //Library Name
    textSizer->Add(new wxStaticText(this, -1, crea::std2wx(this->library->GetName())),0, wxALIGN_LEFT, 0);
    headerSizer->Add(textSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  }
  sizer->Add(headerSizer, 0, wxALIGN_CENTER);

  /*//Properties
  wxStaticBoxSizer* propertiesBox = new wxStaticBoxSizer(wxVERTICAL, this, wxT("&Properties"));
  wxPanel* propertiesPanel = new wxPanel(this);
  wxBoxSizer* propertiesPanelSizer = new wxBoxSizer(wxVERTICAL);

  wxFlexGridSizer* propertiesGridSizer = new wxFlexGridSizer(4, 2, 9, 15);

  wxStaticText *pMainFile = new wxStaticText(propertiesPanel, -1, wxT("Library Name"));
  wxBoxSizer* pMainFilesz = new wxBoxSizer(wxHORIZONTAL);
  this->libraryNametc = new wxStaticText(propertiesPanel, wxID_ANY, crea::std2wx(this->library->GetNameLibrary()));
  wxButton* pMainFilebt = new wxButton(propertiesPanel, ID_BUTTON_SET_NAME, wxT("Set"));
  pMainFilebt->SetToolTip(wxT("Set the name of the library for the project."));
  pMainFilesz->Add(this->libraryNametc, 0, wxALIGN_CENTER_VERTICAL, 0);
  pMainFilesz->Add(pMainFilebt, 0, wxALIGN_CENTER | wxLEFT, 10);

  propertiesGridSizer->Add(pMainFile, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
  propertiesGridSizer->Add(pMainFilesz, 1, wxEXPAND);

  propertiesGridSizer->AddGrowableCol(1,1);

  propertiesPanelSizer->Add(propertiesGridSizer, 0, wxEXPAND);
  propertiesPanel->SetSizer(propertiesPanelSizer);
  propertiesPanelSizer->Fit(propertiesPanel);
  propertiesBox->Add(propertiesPanel, 0, wxEXPAND);
  sizer->Add(propertiesBox, 0, wxEXPAND | wxALL, 10);
   */

  //Includes
  wxStaticBoxSizer* includesBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Used Libraries"));
  includesBox->SetMinSize(200,250);
  wxScrolledWindow* includesPanel = new wxScrolledWindow(this);
  wxBoxSizer* includesPanelSizer = new wxBoxSizer(wxVERTICAL);

  //Third Party Libraries
  wxStaticText* Title1 = new wxStaticText(includesPanel, wxID_ANY, wxT("Third Party Libraries:"));
  wxFont font = Title1->GetFont();
  font.SetWeight(wxFONTWEIGHT_BOLD);
  Title1->SetFont(font);
  includesPanelSizer->Add(Title1, 0, wxEXPAND);

  //inclusion data
  std::map<std::string, bool> inclusions = this->library->Get3rdPartyLibraries();
  //includesGrid Sizer
  wxFlexGridSizer* includesGridSizer = new wxFlexGridSizer(inclusions.size()+1, 2, 0, 5);

  wxStaticText* ChBTitle = new wxStaticText(
      includesPanel,
      wxID_ANY,
      wxT("Included"),
      wxDefaultPosition,
      wxDefaultSize,
      wxALIGN_CENTER
    );
  wxStaticText* LNmTitle = new wxStaticText(
      includesPanel,
      wxID_ANY,
      wxT("Library Name"),
      wxDefaultPosition,
      wxDefaultSize,
      wxALIGN_LEFT
    );

  includesGridSizer->Add(ChBTitle, 1, wxEXPAND);
  includesGridSizer->Add(LNmTitle, 1, wxEXPAND);

  for (std::map<std::string, bool>::iterator it = inclusions.begin(); it != inclusions.end(); ++it) {
    wxCheckBox* ChBIncl = new wxCheckBox(
        includesPanel, ID_CHECK_INCLUDE_3RDLIBRARY, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT
      );
    ChBIncl->SetToolTip(crea::std2wx(
        "When this box is checked the " + it->first + " library\n"
        "is included in the project configuration for\n"
        "this library including the following instruction\n"
        "in the library's folder CMakeLists.txt file:\n"
        "SET ( ${LIBRARY_NAME}_LINK_LIBRARIES\n"
        "  ${" + it->first+ "_LIBRARIES}\n"
        ")"));
    ChBIncl->SetName(crea::std2wx(it->first));
    ChBIncl->SetValue(it->second);
    includesGridSizer->Add(ChBIncl, 1, wxEXPAND);

    wxStaticText* LNmIncl = new wxStaticText(includesPanel, wxID_ANY, crea::std2wx(it->first));
    includesGridSizer->Add(LNmIncl, 1, wxEXPAND);
  }

  includesGridSizer->AddGrowableCol(1,1);

  includesPanelSizer->Add(includesGridSizer, 0, wxEXPAND | wxLEFT, 5);

  //Custom Libraries
  wxStaticText* Title2 = new wxStaticText(includesPanel, wxID_ANY, wxT("Custom Libraries:"));
  font = Title2->GetFont();
  font.SetWeight(wxFONTWEIGHT_BOLD);
  Title2->SetFont(font);
  includesPanelSizer->Add(Title2, 0, wxEXPAND);

  //inclusion data
  std::map<std::string, bool> inclusionsLibs = this->library->GetCustomLibraries();
  //includesGrid Sizer
  wxFlexGridSizer* includesLibGridSizer = new wxFlexGridSizer(inclusionsLibs.size()+1, 2, 0, 5);

  wxStaticText* ChBTitle1 = new wxStaticText(
      includesPanel,
      wxID_ANY,
      wxT("Included"),
      wxDefaultPosition,
      wxDefaultSize,
      wxALIGN_CENTER
    );
  wxStaticText* LNmTitle1 = new wxStaticText(
      includesPanel,
      wxID_ANY,
      wxT("Library Name"),
      wxDefaultPosition,
      wxDefaultSize,
      wxALIGN_LEFT
    );

  includesLibGridSizer->Add(ChBTitle1, 1, wxEXPAND);
  includesLibGridSizer->Add(LNmTitle1, 1, wxEXPAND);

  for (std::map<std::string, bool>::iterator it = inclusionsLibs.begin(); it != inclusionsLibs.end(); ++it) {
    wxCheckBox* ChBIncl = new wxCheckBox(
        includesPanel, ID_CHECK_INCLUDE_LIBRARY, wxT(""), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT
      );
    ChBIncl->SetToolTip(crea::std2wx(
        "When this box is checked the " + it->first + " custom\n"
        "library is included in the project configuration for\n"
        "this library including the following code in the\n"
        "library's CMakeLists.txt file:\n"
        "INCLUDE_DIRECTORIES (\n"
        "  ../"+ it->first + "\n"
        ")\n"
        "SET ( ${LIBRARY_NAME}_LINK_LIBRARIES\n"
        "  " + it->first+ "\n"
        ")"));
    ChBIncl->SetName(crea::std2wx(it->first));
    ChBIncl->SetValue(it->second);
    includesLibGridSizer->Add(ChBIncl, 1, wxEXPAND);

    wxStaticText* LNmIncl = new wxStaticText(includesPanel, wxID_ANY, crea::std2wx(it->first));
    includesLibGridSizer->Add(LNmIncl, 1, wxEXPAND);
  }

  includesLibGridSizer->AddGrowableCol(1,1);

  includesPanelSizer->Add(includesLibGridSizer, 0, wxEXPAND | wxLEFT, 5);

  includesPanel->SetSizer(includesPanelSizer);
  includesPanelSizer->Fit(includesPanel);

  includesPanel->FitInside();
  includesPanel->SetScrollRate(5,5);

  includesBox->Add(includesPanel, 1, wxEXPAND);
  sizer -> Add(includesBox, 0, wxALL | wxEXPAND, 10);



  //Actions
  wxStaticBoxSizer* actionsBox = new wxStaticBoxSizer(wxHORIZONTAL, this, wxT("&Actions"));
  wxPanel* actionsPanel = new wxPanel(this);
  wxBoxSizer* actionsPanelSizer = new wxBoxSizer(wxHORIZONTAL);

  //actionsGrid Sizer
  wxFlexGridSizer* actionsGridSizer = new wxFlexGridSizer(2, 2, 9, 15);

  wxButton* createClassbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_CLASS, _T("A. Create Class"));
  createClassbt->SetToolTip(wxT("Create a new class for this library."));
  actionsGridSizer->Add(createClassbt, 1, wxALL | wxEXPAND, 5);
  wxButton* openFolderbt = new wxButton(actionsPanel, ID_BUTTON_OPEN_FOLDER, _T("B. Open Library Folder"));
  openFolderbt->SetToolTip(wxT("Open the library folder in the file explorer."));
  actionsGridSizer->Add(openFolderbt, 1, wxALL | wxEXPAND, 5);
  wxButton* editCMakebt = new wxButton(actionsPanel, ID_BUTTON_EDIT_CMAKELISTSFILE, _T("Edit CMakeLists File"));
  editCMakebt->SetToolTip(wxT("Edit the CMakeLists.txt of this library in the default text editor."));
  editCMakebt->Connect(wxEVT_ENTER_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryDescriptionPanel::OnCMakeMouseEnter,NULL,this);
  editCMakebt->Connect(wxEVT_LEAVE_WINDOW, (wxObjectEventFunction)(wxEventFunction)(wxMouseEventFunction)&wxCDMLibraryDescriptionPanel::OnCMakeMouseExit,NULL,this);
  actionsGridSizer->Add(editCMakebt, 1, wxALL | wxEXPAND, 5);
  wxButton* createFolderbt = new wxButton(actionsPanel, ID_BUTTON_CREATE_FOLDER, _T("Create Folder"));
  createFolderbt->SetToolTip(wxT("Create a new folder for this library."));
  actionsGridSizer->Add(createFolderbt, 1, wxALL | wxEXPAND, 5);

  actionsGridSizer->AddGrowableCol(0,1);
  actionsGridSizer->AddGrowableCol(1,1);

  actionsPanelSizer->Add(actionsGridSizer, 1, wxEXPAND, 0);
  actionsPanel->SetSizer(actionsPanelSizer);
  actionsPanelSizer->Fit(actionsPanel);
  actionsBox->Add(actionsPanel, 1, wxEXPAND);
  sizer -> Add(actionsBox, 0, wxALL | wxEXPAND, 10);

  //Assign sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);

  if (((wxCDMMainFrame*)this->GetParent())->isHelp())
    {
      wxCDMLibraryHelpDialog* helpDialog = new wxCDMLibraryHelpDialog(this->GetParent(), this->library, wxID_ANY);
      helpDialog->Show(true);
    }
}

void wxCDMLibraryDescriptionPanel::OnBtnReturn(wxHyperlinkEvent& event)
{
  std::vector<modelCDMIProjectTreeNode*> parents = this->library->GetParents();
    std::string parentURL = crea::wx2std(((wxHyperlinkCtrl*)event.GetEventObject())->GetURL());
    //std::cout << parentURL << std::endl;
    for (int i = 0; i < (int)(parents.size()); i++)
      {
        if (parents[i]->GetPath() == parentURL)
          {
            wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
            newEvent->SetClientData(parents[i]);
            newEvent->SetId(0);
            wxPostEvent(this->GetParent(), *newEvent);
          }
      }
}

void wxCDMLibraryDescriptionPanel::OnBtnSetExeName(wxCommandEvent& event)
{
  //get name
  wxString versionWx = wxGetTextFromUser(
      wxT("Enter the new executable name"),
      wxT("Change Library Name - creaDevManager"),
      crea::std2wx(this->library->GetNameLibrary())
  );
  //check name
  std::vector<std::string> parts;
  CDMUtilities::splitter::split(parts, crea::wx2std(versionWx), " .", CDMUtilities::splitter::no_empties);
  if(parts.size() > 0)
    {
      std::string* result;
      if(!this->library->SetNameLibrary(crea::wx2std(versionWx), result))
        wxMessageBox(crea::std2wx(*result),_T("Change Library Name - Error!"),wxOK | wxICON_ERROR);
    }
  else
    {
      wxMessageBox(crea::std2wx("No name specified"),_T("Set Library Name - Error!"),wxOK | wxICON_ERROR);
    }
  this->libraryNametc->SetLabel(crea::std2wx(this->library->GetNameLibrary()));
}

void wxCDMLibraryDescriptionPanel::On3rdLibraryChBChange(wxCommandEvent& event)
{
  if(this->library->Set3rdPartyLibrary(crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()), ((wxCheckBox*)event.GetEventObject())->GetValue()))
    ((wxCheckBox*)event.GetEventObject())->SetValue(((wxCheckBox*)event.GetEventObject())->GetValue());
  else
    ((wxCheckBox*)event.GetEventObject())->SetValue(!((wxCheckBox*)event.GetEventObject())->GetValue());
}

void wxCDMLibraryDescriptionPanel::OnLibraryChBChange(wxCommandEvent& event)
{
  if(this->library->SetCustomLibrary(crea::wx2std(((wxCheckBox*)event.GetEventObject())->GetName()), ((wxCheckBox*)event.GetEventObject())->GetValue()))
      ((wxCheckBox*)event.GetEventObject())->SetValue(((wxCheckBox*)event.GetEventObject())->GetValue());
    else
      ((wxCheckBox*)event.GetEventObject())->SetValue(!((wxCheckBox*)event.GetEventObject())->GetValue());
}

void wxCDMLibraryDescriptionPanel::OnBtnCreateClass(wxCommandEvent& event)
{
  //get class name from user
  wxTextEntryDialog* newClassDlg = new wxTextEntryDialog(
      this,
      wxT("Please enter the new class name."),
      wxT("New Class - creaDevManager"),
      wxT(""),
      wxOK | wxCANCEL
  );

  if (newClassDlg->ShowModal() == wxID_OK)
    {
      std::string className = crea::wx2std(newClassDlg->GetValue());
      //check class name
      if(className.size() > 0)
        {
          if(!this->library->CreateClass(className))
            wxMessageBox(crea::std2wx("Something has gone wrong with the creation of the class."),_T("New Class - Error!"),wxOK | wxICON_ERROR);

          ((wxCDMMainFrame*)this->GetParent())->RefreshProject();

          wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
          newEvent->SetId(0);
          newEvent->SetClientData(this->library);
          wxPostEvent(this->GetParent(), *newEvent);

          wxMessageBox(crea::std2wx("The class has been created successfully."),_T("New Class - Success"),wxOK | wxICON_INFORMATION);
        }
      else
        {
          wxMessageBox(crea::std2wx("The new class name cannot be empty."),_T("New Class - Error!"),wxOK | wxICON_ERROR);
        }
    }
}

void wxCDMLibraryDescriptionPanel::OnBtnCreateFolder(wxCommandEvent& event)
{
  //get name
  wxString folderName = wxGetTextFromUser(
      wxT("Enter the name of the new folder:"),
      wxT("Create Folder - creaDevManager")
  );
  //check name
  std::vector<std::string> parts;
  CDMUtilities::splitter::split(parts, crea::wx2std(folderName), " /\\\"", CDMUtilities::splitter::no_empties);
  if(parts.size() > 0)
    {
      std::string* result;
      modelCDMFolder* folderC = this->library->CreateFolder(crea::wx2std(folderName), result);
      if(folderC == NULL)
        {
          wxMessageBox(crea::std2wx(*result),_T("Create Folder - Error!"),wxOK | wxICON_ERROR);
          return;
        }
      ((wxCDMMainFrame*)this->GetParent())->RefreshProject();
      wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_DISPLAY_CHANGED);
      newEvent->SetClientData(folderC);
      wxPostEvent(this->GetParent(), *newEvent);
      wxMessageBox(crea::std2wx("The folder was successfully created"),_T("Create Folder - Success"),wxOK | wxICON_INFORMATION);
    }
  else
    {
      wxMessageBox(crea::std2wx("No name specified"),_T("Create Folder - Error!"),wxOK | wxICON_ERROR);
    }
}

void wxCDMLibraryDescriptionPanel::OnBtnEditCMakeLists(wxCommandEvent& event)
{
  std::string* result;
  if(!this->library->OpenCMakeListsFile(result))
    wxMessageBox(crea::std2wx(*result),_T("Open CMakeLists File - Error!"),wxOK | wxICON_ERROR);

  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->library->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->library->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }

  event.Skip();
}

void wxCDMLibraryDescriptionPanel::OnBtnOpenFolder(wxCommandEvent& event)
{
  std::string* result;
  if(!this->library->OpenInFileExplorer(result))
    wxMessageBox(crea::std2wx(*result),_T("Open Folder - Error!"),wxOK | wxICON_ERROR);
}

void wxCDMLibraryDescriptionPanel::OnCMakeMouseEnter(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED);

  if(this->library->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->library->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}

void wxCDMLibraryDescriptionPanel::OnCMakeMouseExit(wxMouseEvent& event)
{
  wxCommandEvent* newEvent = new wxCommandEvent(wxEVT_COMMAND_LISTBOX_SELECTED);

  if(this->library->GetCMakeLists() != NULL)
    {
      newEvent->SetClientData(this->library->GetCMakeLists());
      newEvent->SetId(0);
      wxPostEvent(this->GetParent(), *newEvent);
    }
  event.Skip();
}
