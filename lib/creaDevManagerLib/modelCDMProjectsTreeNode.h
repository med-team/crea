/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 


/*
 * modelCDMProjectsTreeNode.h
 *
 *  Created on: 22/10/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMPROJECTSTREENODE_H_
#define MODELCDMPROJECTSTREENODE_H_

#include <iostream>
#include <vector>

class modelCDMProjectsTreeNode
{
public:
  modelCDMProjectsTreeNode() { }
  modelCDMProjectsTreeNode(std::string path, std::string name, unsigned char type, int level);
  ~modelCDMProjectsTreeNode();

  const std::string& GetPath() const;
  const std::string& GetName() const;
  const unsigned char& GetType() const;
  const int& GetLevel() const;
  const std::vector<modelCDMProjectsTreeNode>& GetChildren() const;

  void SetChildren(std::vector<modelCDMProjectsTreeNode>& children);


private:
  std::string _path;
  std::string _name;
  unsigned char _type;
  int _level;
  std::vector<modelCDMProjectsTreeNode> _children;

};

#endif /* MODELCDMPROJECTSTREENODE_H_ */
