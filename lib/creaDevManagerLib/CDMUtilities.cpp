/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * CDMUtilities.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "CDMUtilities.h"

#include<vector>
#include<string>
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstdlib>

#include<creaWx.h>
#include<wx/config.h>

namespace CDMUtilities
{
  const std::string fixPath(const std::string& path)
  {
    std::string pathFixed = "";

#ifdef _WIN32
    // ------ Windows
    std::vector<std::string> pathSplit;

    splitter::split(pathSplit, path, CDMUtilities::SLASH, splitter::no_empties);

	if(0 < pathSplit.size())
		pathFixed = pathSplit[0];

    for (int i = 1; i < (int)(pathSplit.size()); i++)
      {
        pathFixed += CDMUtilities::SLASH + pathSplit[i];
      }
#else
    // ------ LINUX / MacOS
    //break path into folders
    std::vector<std::string> pathSplit;

    splitter::split(pathSplit, path, CDMUtilities::SLASH, splitter::no_empties);

    for (int i = 0; i < pathSplit.size(); i++)
      {
        pathFixed += CDMUtilities::SLASH + pathSplit[i];
      }
#endif
    return pathFixed;

  }

  int openTextEditor(const std::string& file)
  {
#ifdef _WIN32
    wxConfigBase* pConfig =  wxConfigBase::Get();
    std::string command = "start " + crea::wx2std(pConfig->Read(wxT("TEXT_EDITOR"), crea::std2wx(CDMUtilities::TEXT_EDITOR)));

    if(file != "")
      command += " \"" + file + "\"";
#else
    wxConfigBase* pConfig =  wxConfigBase::Get();
    std::string command = crea::wx2std(pConfig->Read(wxT("TEXT_EDITOR"), crea::std2wx(CDMUtilities::TEXT_EDITOR)));

    if(file != "")
      command += " \"" + file + "\"";
    command += " &";
#endif
    return system(command.c_str());
  }

  int openFileExplorer(const std::string& file)
  {
#ifdef _WIN32
    wxConfigBase* pConfig =  wxConfigBase::Get();
    std::string command = "start " + crea::wx2std(pConfig->Read(wxT("FILE_EXPLORER"), crea::std2wx(CDMUtilities::FILE_EXPLORER)));

    if(file != "")
      command += " \"" + file + "\"";
#else
    wxConfigBase* pConfig =  wxConfigBase::Get();
    std::string command = crea::wx2std(pConfig->Read(wxT("FILE_EXPLORER"), crea::std2wx(CDMUtilities::FILE_EXPLORER)));

    if(file != "")
      command += " \"" + file + "\"";
    command += " &";
#endif
    return system(command.c_str());
  }

  int openFileWithCommand(const std::string& file, const std::string& command, const std::string& parameters)
  {
#ifdef _WIN32
    std::string comm = "start " + command;
    if(file != "")
      comm += " \"" + file + "\" " + parameters;
#else
    std::string comm = command;
    if(file != "")
      comm += " \"" + file + "\" " + parameters;
    comm += " &";
#endif
    std::cout << "executing: " << comm << std::endl;
    return system(comm.c_str());
  }

  int openBBEditor()
  {
#ifdef _WIN32
    std::string comm = "start bbEditor";
#else
	std::string comm = "bbEditor &";
#endif
    return system(comm.c_str());
  }

  int openCreaToolsTools()
  {
#ifdef _WIN32
    std::string comm = "start creaTools";
#else
    std::string comm = "creaTools.sh &";
#endif
    
    return system(comm.c_str());
  }

  int openTerminal(const std::string& command)
  {
    wxConfigBase* pConfig =  wxConfigBase::Get();
    std::string comm = crea::wx2std(pConfig->Read(wxT("TERMINAl"), crea::std2wx(CDMUtilities::TERMINAL)));
    if (command != "")
      comm += + " " + command;
    comm += " &";
    return system(comm.c_str());
  }

  bool createEmptyClass(const std::string& name, const std::string& path)
  {
    std::vector<std::string> words;
    splitter::split(words,name," \\/\",.'`",splitter::no_empties);
    std::string fixedName = "";
    for (int i = 0; i < (int)(words.size()); i++)
      {
        fixedName += words[i];
      }

    if(fixedName == "" || path == "")
      {
        return false;
      }

    std::string nameupper = fixedName;
    std::transform(nameupper.begin(), nameupper.end(),nameupper.begin(),::toupper);

    std::ofstream out((path + SLASH + fixedName + ".h").c_str());
    if( !out.is_open())
      {
        return false;
      }

    out << "/*" << std::endl;
    out << "# ---------------------------------------------------------------------" << std::endl;
    out << "#" << std::endl;
    out << "# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image" << std::endl;
    out << "#                        pour la Sante)" << std::endl;
    out << "# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton" << std::endl;
    out << "# Previous Authors : Laurent Guigues, Jean-Pierre Roux" << std::endl;
    out << "# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil" << std::endl;
    out << "#" << std::endl;
    out << "#  This software is governed by the CeCILL-B license under French law and" << std::endl;
    out << "#  abiding by the rules of distribution of free software. You can  use," << std::endl;
    out << "#  modify and/ or redistribute the software under the terms of the CeCILL-B" << std::endl;
    out << "#  license as circulated by CEA, CNRS and INRIA at the following URL" << std::endl;
    out << "#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html" << std::endl;
    out << "#  or in the file LICENSE.txt." << std::endl;
    out << "#" << std::endl;
    out << "#  As a counterpart to the access to the source code and  rights to copy," << std::endl;
    out << "#  modify and redistribute granted by the license, users are provided only" << std::endl;
    out << "#  with a limited warranty  and the software's author,  the holder of the" << std::endl;
    out << "#  economic rights,  and the successive licensors  have only  limited" << std::endl;
    out << "#  liability." << std::endl;
    out << "#" << std::endl;
    out << "#  The fact that you are presently reading this means that you have had" << std::endl;
    out << "#  knowledge of the CeCILL-B license and that you accept its terms." << std::endl;
    out << "# ------------------------------------------------------------------------" << std::endl;
    out << "*/" << std::endl;
    out << "" << std::endl;
    out << "#ifndef _" << nameupper << "_H_" << std::endl;
    out << "#define _" << nameupper << "_H_" << std::endl;
    out << "" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "// Class Name: " << fixedName << "" << std::endl;
    out << "// [classdescription]" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "" << std::endl;
    out << "class " << fixedName << "" << std::endl;
    out << "{" << std::endl;
    out << "" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "//Methods and attributes exposed to other classes" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "public :" << std::endl;
    out << "  " << fixedName << "();" << std::endl;
    out << "  ~" << fixedName << "();" << std::endl;
    out << "" << std::endl;
    out << "//--Method template----------------------------" << std::endl;
    out << "//  void FunctionName(int& parameterA);" << std::endl;
    out << "" << std::endl;
    out << "" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "//Methods and attributes exposed only to classes" << std::endl;
    out << "//that are derived from this class" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "protected:" << std::endl;
    out << "" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "//Methods and attributes only visible by this class" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "private:" << std::endl;
    out << "" << std::endl;
    out << "};" << std::endl;
    out << "" << std::endl;
    out << "//-end of _" << nameupper << "_H_------------------------------------------------------" << std::endl;
    out << "#endif" << std::endl;

    out.close();

    out.open((path + CDMUtilities::SLASH + fixedName + ".cpp").c_str());
    if( !out.is_open())
      {
        return false;
      }

    out << "/*" << std::endl;
    out << "# ---------------------------------------------------------------------" << std::endl;
    out << "#" << std::endl;
    out << "# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image" << std::endl;
    out << "#                        pour la Sante)" << std::endl;
    out << "# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton" << std::endl;
    out << "# Previous Authors : Laurent Guigues, Jean-Pierre Roux" << std::endl;
    out << "# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil" << std::endl;
    out << "#" << std::endl;
    out << "#  This software is governed by the CeCILL-B license under French law and" << std::endl;
    out << "#  abiding by the rules of distribution of free software. You can  use," << std::endl;
    out << "#  modify and/ or redistribute the software under the terms of the CeCILL-B" << std::endl;
    out << "#  license as circulated by CEA, CNRS and INRIA at the following URL" << std::endl;
    out << "#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html" << std::endl;
    out << "#  or in the file LICENSE.txt." << std::endl;
    out << "#" << std::endl;
    out << "#  As a counterpart to the access to the source code and  rights to copy," << std::endl;
    out << "#  modify and redistribute granted by the license, users are provided only" << std::endl;
    out << "#  with a limited warranty  and the software's author,  the holder of the" << std::endl;
    out << "#  economic rights,  and the successive licensors  have only  limited" << std::endl;
    out << "#  liability." << std::endl;
    out << "#" << std::endl;
    out << "#  The fact that you are presently reading this means that you have had" << std::endl;
    out << "#  knowledge of the CeCILL-B license and that you accept its terms." << std::endl;
    out << "# ------------------------------------------------------------------------" << std::endl;
    out << "*/" << std::endl;
    out << "" << std::endl;
    out << "#include \"" << fixedName << ".h\"" << std::endl;
    out << "" << std::endl;
    out << "" << fixedName << "::" << fixedName << "()" << std::endl;
    out << "{" << std::endl;
    out << "}" << std::endl;
    out << "" << std::endl;
    out << "" << fixedName << "::~" << fixedName << "()" << std::endl;
    out << "{" << std::endl;
    out << "}" << std::endl;
    out << "" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "//Method template" << std::endl;
    out << "//---------------------------------------------" << std::endl;
    out << "/*" << std::endl;
    out << "void " << fixedName << "::FunctionName(int& parameterA)" << std::endl;
    out << "{" << std::endl;
    out << "  parameterA = 2 * parameterA;" << std::endl;
    out << "  return;" << std::endl;
    out << "}" << std::endl;
    out << "*/" << std::endl;

    return true;
  }

  std::string stringify(const std::string& line)
  {
	std::string res;
	for (int i = 0; i < (int)(line.size()); i++)
	{
	  if(line[i] == '\\')
	    res.push_back('\\');
	  if(line[i] == '\"')
	    res.push_back('\\');
	  res.push_back(line[i]);
	}
	return res;
  }

  std::string readFile(const std::string& file_path)
  {
    std::string res;
    std::ifstream file(file_path.c_str());
    if (file.is_open())
      {
        char ch = file.get();
        while (!file.eof())
          {
            res.push_back(ch);
            ch = file.get();
          }
        file.close();
      }
    return res;
  }

  bool writeFile(const std::string& file_path, const std::string& st)
  {
    std::ofstream file(file_path.c_str());
    if (file.is_open())
      {
        file << st;
        file.close();
        return true;
      }
    return false;

  }

  CMLFile readCMLFile(const std::string& file_path)
  {
    CMLFile res;

    std::ifstream file(file_path.c_str());
    if (file.is_open())
      {
        char ch = file.get();
        while (!file.eof())
          {
            syntaxElement element;
            if (isspace(ch))
              {
                //std::cout << "space" << std::endl;
                element.first = "space";
                element.second.push_back(std::string(1,ch));
              }
            else if (ch == '#')
              {
                //std::cout << "comment" << std::endl;
                element.first = "comment";
                std::string commentValue;
                while (ch != '\n')
                  {
                    commentValue.push_back(ch);

                    ch = file.get();
                    if (file.eof())
                      break;
                  };
                if (!file.eof())
                  commentValue.push_back('\n');
                element.second.push_back(commentValue);
              }
            else
              {
                //std::cout << "command" << std::endl;
                element.first = "command";
                std::string commandValue;
                while (true)
                  {
                    //std::cout << ch;
                    //std::cout.flush();
                    while(!isspace(ch) && ch != '(' && ch != ')' && ch != '#')
                      {
                        if(ch == '"')
                          {
                            if (commandValue.size()) {
                              element.second.push_back(commandValue);
                              commandValue.clear();
                            }
                            commandValue.push_back(ch);
                            ch = file.get();
                            while(!file.eof() && ch != '"')
                              {
                                if(ch == '\\')
                                  {
                                    commandValue.push_back(ch);
                                    ch = file.get();
                                    if(!file.eof())
                                      commandValue.push_back(ch);
                                  }
                                else
                                  {
                                    commandValue.push_back(ch);
                                  }
                                ch = file.get();
                              }
                            if(!file.eof())
                              commandValue.push_back(ch);
                            element.second.push_back(commandValue);
                            commandValue.clear();
                          }
                        else
                          commandValue.push_back(ch);

                        ch = file.get();
                      }

                    if (!file.eof() && (isspace(ch) || ch == '(' || ch == ')' || ch == '#'))
                      {
                        if (commandValue.size()) {
                          element.second.push_back(commandValue);
                          commandValue.clear();
                        }
                        commandValue.push_back(ch);
                        if (ch == '#') {
                          while (ch != '\n') {
                            ch = file.get();
                            if (file.eof())
                              break;
                            commandValue.push_back(ch);
                          };
                        }
                        element.second.push_back(commandValue);
                        if (commandValue == ")")
                          {
                            commandValue.clear();
                            break;
                          }
                        commandValue.clear();
                      }

                    ch = file.get();
                    if (file.eof())
                      break;
                  }
              }
            res.push_back(element);

            ch = file.get();
            if (file.eof())
              break;
          }

        file.close();
      }

/*
    std::cout << "CMakeLists: " << file_path << std::endl;
    for (int i = 0; i < res.size(); ++i) {
      if (res[i].first == "command")
        std::cout << "@";
      for (int j = 0; j < res[i].second.size(); ++j) {
        std::cout << "~" << res[i].second[j];
      }
      if (res[i].first == "command")
        std::cout << "@";
    }
    std::cout << "End of file" << std::endl;
*/
    return res;
  }

  bool writeCMLFile(const std::string& file_path, const CMLFile& data)
  {
    std::ofstream file(file_path.c_str());
    if(file.is_open())
      {
        for (int i = 0; i < data.size(); ++i) {
          for (int j = 0; j < data[i].second.size(); ++j) {
            file << data[i].second[j];
          }
        }
        file.close();
        return true;
      }
    return false;
  }



  void
  normalizeStr(std::string& st)
  {
    while(st.size() && isspace(st[0]))
      st.erase(0,1);
    while(st.size() && isspace(st[st.size()-1]))
      st.erase(st.size()-1,1);
    return;
  }

}
