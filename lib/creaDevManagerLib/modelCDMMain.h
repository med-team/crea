/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */

/*
 * modelCDMMain.h
 *
 *  Created on: 13/11/2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMMAIN_H_
#define MODELCDMMAIN_H_

#include<iostream>
#include<map>

#include<creaWx.h>
#include<wx/treectrl.h>

#include "modelCDMIProjectTreeNode.h"
#include "modelCDMProject.h"

/**
 * Class representing the model of the project manager.
 */
class modelCDMMain
{
public:
  /**
   * Default constructor.
   */
  modelCDMMain();
  /**
   * Destructor.
   */
  ~modelCDMMain();

  /**
   * Retrieves the current active project.
   * @return Reference to the current active project, if there's no active project NULL is returned.
   */
  modelCDMProject* GetProject() const;
  /**
   * Retrieves the map of all the nodes inside the current active project.
   * @return Map with ids and node reference of the project.
   */
  std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* >& GetModelElements();


  /**
   * Creates a new project and sets it as the current active project. This method creates a new project model and also creates a project in the system.
   * @param name Name of the new project.
   * @param location Path where the project is to be created.
   * @param result Result message.
   * @param author Default Package Authors' names.
   * @param description Description of the default package.
   * @return True if the operation was successful.
   */
  bool CreateProject(
      const std::string& name,
      const std::string& location,
      std::string*& result,
      const std::string& author = "unknown",
      const std::string& description = "no description"
  );
  /**
   * Opens an existing project given the source or the binaries folder.
   * @param path Path to the project source or binaries folder.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool OpenProject(
      const std::string& path,
      std::string*& result
  );
  /**
   * Refreshes the currently active project structure.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool RefreshProject(
      std::string*& result
  );
  /**
   * Closes the currently active project. This method deletes the project model but doesn't erase the project from the system.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool CloseProject(
      std::string*& result
  );


private:
  /**
   * Currently active project reference.
   */
  modelCDMProject* project;
  /**
   * Map of all the elements of the currently active project and their IDs.
   */
  std::map< wxCDMTreeItemId, modelCDMIProjectTreeNode* > modelElements;
};


#endif /* MODELCDMMAIN_H_ */
