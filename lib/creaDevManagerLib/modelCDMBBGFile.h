/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMBBGFile.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#ifndef MODELCDMBBGFILE_H_
#define MODELCDMBBGFILE_H_

#include "modelCDMFile.h"

/**
 * Class representing a bbg file in a folder of a Crea project.
 */
class modelCDMBBGFile : public modelCDMFile
{
public:
  /**
   * Default Constructor.
   */
  modelCDMBBGFile();
  /**
   * BBG file Constructor.
   * @param parent Parent node of the BBG file node.
   * @param path Full path to the BBG file node.
   * @param name File name of the BBG file node.
   * @param level Project hierarchy level of the BBG file node.
   */
  modelCDMBBGFile(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name = "CMakeLists.txt", const int& level = 1);
  /**
   * Destructor
   */
  ~modelCDMBBGFile();

  /**
   * Opens the file in the system default code editor.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool OpenFile(std::string*& result);

  /**
   * Opens the file using bbEditor.
   * @param parameters Execution parameters.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  bool EditFile(std::string*& result, const std::string& parameters = "");

  /**
   * Refreshes the state of the BBG file.
   * @param result Result message.
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);
};

#endif /* MODELCDMBBGFILE_H_ */
