/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
 */


/*
 * wxCDMProjectStructureReportDialog.cpp
 *
 *  Created on: 17/1/2013
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "wxCDMProjectStructureReportDialog.h"

#include "creaDevManagerIds.h"

#include <wx/html/htmlwin.h>
#include <vector>
#include "CDMUtilities.h"

BEGIN_EVENT_TABLE(wxCDMProjectStructureReportDialog, wxDialog)
EVT_BUTTON(ID_BUTTON_CANCEL, wxCDMProjectStructureReportDialog::OnFinish)
END_EVENT_TABLE()

wxCDMProjectStructureReportDialog::wxCDMProjectStructureReportDialog(
    wxWindow* parent,
    const std::map<std::string, bool>& properties,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long style
)
{
  this->properties = properties;
  wxCDMProjectStructureReportDialog::Create(parent, id, caption, position, size, style);
}

wxCDMProjectStructureReportDialog::~wxCDMProjectStructureReportDialog()
{
}

bool wxCDMProjectStructureReportDialog::Create(
    wxWindow* parent,
    wxWindowID id,
    const wxString& caption,
    const wxPoint& position,
    const wxSize& size,
    long int style
)
{
  wxDialog::Create(parent, id, caption, position, size, style);

  this->CreateControls();

  return TRUE;
}

void wxCDMProjectStructureReportDialog::CreateControls()
{

  wxBoxSizer* v_sizer1 = new wxBoxSizer(wxVERTICAL);

  wxHtmlWindow* htmlWindow = new wxHtmlWindow(this, wxID_ANY);
  htmlWindow->SetPage(this->GetContent());
  v_sizer1->Add(htmlWindow, 1, wxEXPAND);
  v_sizer1->Add(new wxButton(this, ID_BUTTON_CANCEL, wxT("Close")),0,wxEXPAND);

  SetSizer(v_sizer1);
}

void wxCDMProjectStructureReportDialog::OnFinish(wxCommandEvent& event)
{
  this->EndDialog(wxID_CANCEL);
}

wxString wxCDMProjectStructureReportDialog::GetContent()
{
  std::map<std::string, bool>::iterator it, it2;
  std::map<std::string, bool> pkgs;
  std::map<std::string, bool> libs;
  std::map<std::string, bool> applis;
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      std::cout << it->first << " " << it->second << std::endl;
      if(it->first.substr(0,7) == "package")
        {
          std::vector<std::string> words;
          CDMUtilities::splitter::split(words, it->first, " ", CDMUtilities::splitter::no_empties);
          pkgs[words[1]] = true;
        }
      else if(it->first.substr(0,7) == "library")
        {
          std::vector<std::string> words;
          CDMUtilities::splitter::split(words, it->first, " ", CDMUtilities::splitter::no_empties);
          libs[words[1]] = true;
        }
      else if(it->first.substr(0,11) == "application")
        {
          std::vector<std::string> words;
          CDMUtilities::splitter::split(words, it->first, " ", CDMUtilities::splitter::no_empties);
          applis[words[1]] = true;
        }
    }
  std::string report = "";
  report +=
      "<html>"
      "<head>"
      "  <title>Project Structure Check Report</title>"
      "</head>"
      "<body>"
      "  <h1>Project Structure Check Report</h1>"
      "  <div id=\"projectChk\">"
      "    <h3>Project CMakeLists.txt</h3>"
      "    <p>"
      "      Includes the following built-in libraries:"
      "      <font color=\"#00CC00\"><ul>";
  if(this->properties["project set USE_CREA"])
    report +=
        "        <li>Crea</li>";
  if(this->properties["project set USE_GDCM"])
    report +=
        "        <li>GDCM</li>";
  if(this->properties["project set USE_GDCM_VTK"])
    report +=
        "        <li>GDCM_VTK</li>";
  if(this->properties["project set USE_GDCM2"])
    report +=
        "        <li>GDCM2</li>";
  if(this->properties["project set USE_WXWIDGETS"])
    report +=
        "        <li>wxWidgets</li>";
  if(this->properties["project set USE_KWWIDGETS"])
    report +=
        "        <li>kwWidgets</li>";
  if(this->properties["project set USE_VTK"])
    report +=
        "        <li>VTK</li>";
  if(this->properties["project set USE_ITK"])
    report +=
        "        <li>ITK</li>";
  if(this->properties["project set USE_BOOST"])
    report +=
        "        <li>Boost</li>";
  report+=
      "      </ul></font>"
      "    </p>"
      "    <p>"
      "      Doesn't include the following built-in libraries:"
      "      <font color=\"#AAAA00\"><ul>";
  if(!this->properties["project set USE_CREA"])
    report +=
        "        <li>Crea</li>";
  if(!this->properties["project set USE_GDCM"])
    report +=
        "        <li>GDCM</li>";
  if(!this->properties["project set USE_GDCM_VTK"])
    report +=
        "        <li>GDCM_VTK</li>";
  if(!this->properties["project set USE_GDCM2"])
    report +=
        "        <li>GDCM2</li>";
  if(!this->properties["project set USE_WXWIDGETS"])
    report +=
        "        <li>wxWidgets</li>";
  if(!this->properties["project set USE_KWWIDGETS"])
    report +=
        "        <li>kwWidgets</li>";
  if(!this->properties["project set USE_VTK"])
    report +=
        "        <li>VTK</li>";
  if(!this->properties["project set USE_ITK"])
    report +=
        "        <li>ITK</li>";
  if(!this->properties["project set USE_BOOST"])
    report +=
        "        <li>Boost</li>";
  report+=
      "      </ul></font>"
      "    </p>"
      "    <p>"
      "      Includes:"
      "      <font color=\"#00CC00\"><ul>";
  if(this->properties["project add lib"])
    report +=
        "        <li>Lib</li>";
  if(this->properties["project add appli"])
    report +=
        "        <li>Appli</li>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == true && it->first.substr(0,11) == "project add")
        {
          std::string namePkg = it->first.substr(12);
          if (namePkg.substr(0,4) == "bbtk")
            report +=
                "        <li>Package " + namePkg + "</li>";
        }
    }
  report+=
      "      </ul></font>"
      "    </p>"
      "    <p>Doesn't include:"
      "      <font color=\"#FF0000\"><ul>";
  if(!this->properties["project add lib"])
    report +=
        "        <li >"
        "          Lib: use"
        "          <blockquote>ADD_SUBDIRECTORY(lib)</blockquote>"
        "          in the project CMakeLists file."
        "        </li>";
  if(!this->properties["project add appli"])
    report +=
        "        <li>"
        "          Appli: use"
        "          <blockquote>ADD_SUBDIRECTORY(appli)</blockquote>"
        "          in the project CMakeLists file."
        "        </li>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == false && it->first.substr(0,11) == "project add")
        {
          std::string namePkg = it->first.substr(12);
          report +=
              "        <li>"
              "          Package \"" + namePkg + "\": use"
              "          <blockquote>ADD_SUBDIRECTORY(" + namePkg + ")</blockquote>"
              "          in the project CMakeLists file."
              "        </li>";
        }
    }
  report+=
      "      </ul></font>"
      "    </p>"
      "  </div>";

  for (it = pkgs.begin(); it != pkgs.end(); it++)
    {
      report +=
          "  <div id=\"package" + it->first + "Chk\">"
          "    <h3>Package " + it->first + " CMakeLists.txt</h3>"
          "    <p>"
          "      Includes the following built-in libraries:"
          "      <font color=\"#00CC00\"><ul>";
      if(this->properties["package " + it->first + " set USE_VTK"])
        report +=
            "        <li>VTK</li>";
      if(this->properties["package " + it->first + " set USE_ITK"])
        report +=
            "        <li>ITK</li>";
      if(this->properties["package " + it->first + " set USE_GDCM"])
        report +=
            "        <li>GDCM</li>";
      if(this->properties["package " + it->first + " set USE_GDC_VTK"])
        report +=
            "        <li>GDCM_VTK</li>";
      if(this->properties["package " + it->first + " set USE_GSMIS"])
        report +=
            "        <li>GSMIS</li>";
      if(this->properties["package " + it->first + " set USE_WXWIDGETS"])
        report +=
            "        <li>wxWidgets</li>";
      if(this->properties["package " + it->first + " set USE_KWWIDGETS"])
        report +=
            "        <li>kwWidgets</li>";
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Doesn't include the following built-in libraries:"
          "      <font color=\"#AAAA00\"><ul>";
      if(!this->properties["package " + it->first + " set USE_VTK"])
        report +=
            "        <li>VTK</li>";
      if(!this->properties["package " + it->first + " set USE_ITK"])
        report +=
            "        <li>ITK</li>";
      if(!this->properties["package " + it->first + " set USE_GDCM"])
        report +=
            "        <li>GDCM</li>";
      if(!this->properties["package " + it->first + " set USE_GDC_VTK"])
        report +=
            "        <li>GDCM_VTK</li>";
      if(!this->properties["package " + it->first + " set USE_GSMIS"])
        report +=
            "        <li>GSMIS</li>";
      if(!this->properties["package " + it->first + " set USE_WXWIDGETS"])
        report +=
            "        <li>wxWidgets</li>";
      if(!this->properties["package " + it->first + " set USE_KWWIDGETS"])
        report +=
            "        <li>kwWidgets</li>";
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Includes the following additional directories:"
          "      <font color=\"#00CC00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string pkgname = "package " + it->first + " dir";
          if(it2->first.substr(0,pkgname.size()) == pkgname)
            {
              std::string directoryName = it2->first.substr(pkgname.size()+1);
              report +=
                  "        <li>" + directoryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Includes the following libraries:"
          "      <font color=\"#00CC00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string pkgname = "package " + it->first + " lib";
          if(it2->first.substr(0,pkgname.size()) == pkgname)
            {
              std::string libraryName = it2->first.substr(pkgname.size()+1);
              report +=
                  "        <li>" + libraryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      <strong>Note:</strong><br>"
          "      Please include the following lines in the package CMakeLists file in order to add a library:<br>"
          "          - In section <strong>SET(${BBTK_PACKAGE_NAME}_INCLUDE_DIRS</strong> use:"
          "          <blockquote>"
          "            ../lib/[libraryFolderName]"
          "          </blockquote>"
          "          - In section <strong>SET(${BBTK_PACKAGE_NAME}_LIBS</strong> use:"
          "          <blockquote>"
          "            [libraryName]"
          "          </blockquote>"
          "    </p>"
          "  </div>";
    }
  report +=
      "  <div id=\"libChk\">"
      "    <h3>Lib CMakeLists.txt</h3>"
      "    <p>"
      "      Includes libraries:"
      "      <font color=\"#00CC00\"><ul>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == true && it->first.substr(0, 7) == "lib add")
        {
          std::string nameLib = it->first.substr(8);
          report +=
              "        <li>" + nameLib + "</li>";
        }
    }
  report +=
      "      </ul></font>"
      "    </p>"
      "    <p>Doesn't include libraries:"
      "      <font color=\"#FF0000\"><ul>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == false && it->first.substr(0, 7) == "lib add")
        {
          std::string nameLib = it->first.substr(8);
          report +=
              "        <li>" + nameLib + "</li>";
        }
    }
  report +=
      "      </ul></font>"
      "    </p>"
      "    <p>"
      "      <strong>Note:</strong><br>"
      "      Please include the following lines in the lib CMakeLists file in order to add a library:"
      "          <blockquote>ADD_SUBDIRECTORY([libraryName])</blockquote>"
      "    </p>"
      "  </div>";
  for (it = libs.begin(); it != libs.end(); it++)
    {
      report +=
          "  <div id=\"library" + it->first + "Chk\">"
          "    <h3>Library " + it->first + " CMakeLists.txt</h3>"
          "    <p>"
          "      Includes the following libraries:"
          "      <font color=\"#00CC00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string libname = "library " + it->first + " lib";
          if(it2->second == true && it2->first.substr(0,libname.size()) == libname)
            {
              std::string libraryName = it2->first.substr(libname.size()+1);
              report +=
                  "        <li>" + libraryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Doesn't include the following libraries:"
          "      <font color=\"#AAAA00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string libname = "library " + it->first + " lib";
          if(it2->second == false && it2->first.substr(0,libname.size()) == libname)
            {
              std::string libraryName = it2->first.substr(libname.size()+1);
              report +=
                  "        <li>" + libraryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      <strong>Note:</strong><br>"
          "      Please include the following line in the library CMakeLists file in order to add a library:"
          "          <blockquote>[libraryName]</blockquote>"
          "      on <strong>SET ( ${LIBRARY_NAME}_LINK_LIBRARIES</strong> section if needed."
          "    </p>"
          "  </div>";
    }
  report +=
      "  <div id=\"appliChk\">"
      "    <h3>Appli CMakeLists.txt</h3>"
      "    <p>"
      "      Includes applications:"
      "      <font color=\"#00CC00\"><ul>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == true && it->first.substr(0, 9) == "appli add")
        {
          std::string nameAppli = it->first.substr(10);
          report +=
              "        <li>" + nameAppli + "</li>";
        }
    }
  report +=
      "      </ul></font>"
      "    </p>"
      "    <p>Doesn't include applications:"
      "      <font color=\"#FF0000\"><ul>";
  for (it = this->properties.begin(); it != this->properties.end(); it++)
    {
      if(it->second == false && it->first.substr(0, 9) == "appli add")
        {
          std::string nameAppli = it->first.substr(10);
          report +=
              "        <li>" + nameAppli + "</li>";
        }
    }
  report +=
      "      </ul></font>"
      "    </p>"
      "    <p>"
      "      <strong>Note:</strong><br>"
      "      Please include the following lines in the appli CMakeLists file in order to add an application:"
      "          <blockquote>ADD_SUBDIRECTORY([applicationName])</blockquote>"
      "    </p>"
      "  </div>";
  for (it = applis.begin(); it != applis.end(); it++)
    {
      report +=
          "  <div id=\"application" + it->first + "Chk\">"
          "    <h3>Application " + it->first + " CMakeLists.txt</h3>"
          "    <p>"
          "      Includes the following libraries:"
          "      <font color=\"#00CC00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string appliname = "application " + it->first + " dir";
          if(it2->second && it2->first.substr(0,appliname.size()) == appliname)
            {
              std::string directoryName = it2->first.substr(appliname.size()+1);
              report +=
                  "        <li>" + directoryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Includes the following libraries:"
          "      <font color=\"#00CC00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string appliname = "application " + it->first + " lib";
          if(it2->second && it2->first.substr(0,appliname.size()) == appliname)
            {
              std::string libraryName = it2->first.substr(appliname.size()+1);
              report +=
                  "        <li>" + libraryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      Doesn't include the following libraries:"
          "      <font color=\"#AAAA00\"><ul>";
      for (it2 = this->properties.begin(); it2 != this->properties.end(); it2++)
        {
          std::string appliname = "application " + it->first + " lib";
          if(it2->second == false && it2->first.substr(0,appliname.size()) == appliname)
            {
              std::string libraryName = it2->first.substr(appliname.size()+1);
              report +=
                  "        <li>" + libraryName + "</li>";
            }
        }
      report +=
          "      </ul></font>"
          "    </p>"
          "    <p>"
          "      <strong>Note:</strong><br>"
          "      Please include the following lines in the application CMakeLists file in order to add a library:<br>"
          "          - In section <strong>INCLUDE_DIRECTORIES(</strong> use:"
          "          <blockquote>"
          "            ../lib/[libraryFolderName]"
          "          </blockquote>"
          "          - In section <strong>SET(${EXE_NAME}_LINK_LIBRARIES</strong> use:"
          "          <blockquote>"
          "            [libraryName]"
          "          </blockquote>"
          "    </p>"
          "  </div>";
    }
  report +=
      "</body>"
      "</html>"
      ;
  return crea::std2wx(report);
}
