/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMPackage.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez
 */

#ifndef MODELCDMPACKAGE_H_
#define MODELCDMPACKAGE_H_

#include<iostream>
#include<vector>
#include<map>

#include"modelCDMFolder.h"
#include"modelCDMPackageSrc.h"

/**
 * Class representing a package of a Crea project.
 */
class modelCDMPackage : public modelCDMFolder
{
public:
  /**
   * Default constructor.
   */
  modelCDMPackage();
  /**
   * Package node constructor.
   * @param parent Parent node of the package folder node.
   * @param path Full path to the package.
   * @param name Name of the package folder.
   * @param level Project hierarchy level of the package node.
   */
  modelCDMPackage(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level = 1);
  /**
   * Destructor.
   */
  ~modelCDMPackage();

  /**
   * Retrieves the name of the package. the name of the package can be different from the package folder name.
   * @return Package name.
   */
  const std::string& GetNamePackage() const;
  /**
   * Retrieves the authors of the package.
   * @return Package authors.
   */
  const std::string& GetAuthors() const;
  /**
   * Retrieves the Author e-mails of the package.
   * @return Author e-mails.
   */
  const std::string& GetAuthorsEmail() const;
  /**
   * Retrieves the version of the package.
   * @return Package version.
   */
  const std::string& GetVersion() const;
  /**
   * Retrieves the description of the package.
   * @return Package description
   */
  const std::string& GetDescription() const;
  /**
   * Retrieves the src folder node of the package node.
   * @return Reference to the package src file node.
   */
  modelCDMPackageSrc* GetSrc() const;

  /**
   * Sets the name of the package authors. This operation affects the project model as well as the system files.
   * @param authors Name of the package authors.
   * @param result Result message
   * @return True if the operation was successful.
   */
  bool SetAuthors(const std::string& authors, std::string*& result);
  /**
   * Sets the email of the package authors. This operation affects the project model as well as the system files.
   * @param email
   * @param result Result message
   * @return True if the operation was successful.
   */
  bool SetAuthorsEmail(const std::string& email, std::string*& result);
  /**
   * Sets the version of the package. This operation affects the project model as well as the system files.
   * @param version
   * @param result Result message
   * @return True if the operation was successful.
   */
  bool SetVersion(const std::string& version, std::string*& result);
  /**
   * Sets the description of the package. This operation affects the project model as well as the system files.
   * @param description
   * @param result Result message
   * @return True if the operation was successful.
   */
  bool SetDescription(const std::string& description, std::string*& result);


  /**
   * Creates a new black box and returns a reference to it if the creation is successful. This operation affects the project model as well as the system files.
   * @param result Result message
   * @param name New black box name.
   * @param type Black box type.
   * @param format Black box format.
   * @param categories Categories associated to this black box.
   * @param authors Black box authors' name.
   * @param authorsEmail Black box authors' email.
   * @param description Black box description.
   * @return True if the operation was successful.
   */
  modelCDMBlackBox* CreateBlackBox(
      std::string*& result,
      const std::string& name,
      const std::string& type = "std",
      const std::string& format = "C++",
      const std::string& categories = "empty",
      const std::string& authors = "unknown",
      const std::string& authorsEmail = "",
      const std::string& description = "no description"
  );
  /**
   * Refreshes the structure of the package folder node. This method updates the properties of the package as well as it refreshes its children.
   * @param result Result message
   * @return True if the operation was successful.
   */
  virtual const bool Refresh(std::string*& result);

  /**
   * Checks the package structure with the CMakeLists file to look for project structure definition problems before compiling the project.
   * @param properties Project properties.
   */
  void CheckStructure(std::map<std::string, bool>& properties);

  /**
   * Checks the package's CMakeLists file to check which third party libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> Get3rdPartyLibraries();

  /**
   * Sets the 3rd party library inclusion in the CMakeLists file.
   * @return if the operation was successful.
   */
  bool Set3rdPartyLibrary(const std::string& library_name, const bool& toInclude);

  /**
   * Checks the package CMakeLists file to check which custom libraries are enabled.
   * @return A map with the name of the library and if it's included in the CMakeLists file.
   */
  std::map<std::string, bool> GetCustomLibraries();

  /**
   * Sets the custom library inclusion in the CMakeLists file.
   * @return if the operation was successful.
   */
  bool SetCustomLibrary(const std::string& library_name, const bool& toInclude);

private:
  /**
   * Package name.
   */
  std::string namePackage;
  /**
   * Package authors' name.
   */
  std::string authors;
  /**
   * Package authors' e-mails.
   */
  std::string authorsEmail;
  /**
   * Package version.
   */
  std::string version;
  /**
   * Package description.
   */
  std::string description;
  /**
   * Reference to the package source folder.
   */
  modelCDMPackageSrc* src;

};

#endif /* MODELCDMPACKAGE_H_ */
