/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and
#  abiding by the rules of distribution of free software. You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL-B
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------
 */

/*
 * modelCDMPackage.cpp
 *
 *  Created on: Nov 23, 2012
 *      Author: Daniel Felipe Gonzalez Obando
 */

#include "modelCDMPackage.h"

#include "modelCDMProject.h"
#include "modelCDMLib.h"
#include "modelCDMLibrary.h"


#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/regex.hpp>

#include "creaWx.h"
#include "wx/dir.h"
#include "CDMUtilities.h"

modelCDMPackage::modelCDMPackage()
{
  this->src = NULL;
}

modelCDMPackage::modelCDMPackage(modelCDMIProjectTreeNode* parent, const std::string& path, const std::string& name, const int& level)
{
  std::cout << "creating package: " + path + "\n";
  this->parent = parent;
  this->type = wxDIR_DIRS;
  this->name = name;
  //Get Package Name

  std::string pathMakeLists = path + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //get sets
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get package name
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "BBTK_PACKAGE_NAME")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->namePackage = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_AUTHOR")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->authors = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_DESCRIPTION")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->description = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_MAJOR_VERSION")
            {
              this->version = wordBits[1];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_MINOR_VERSION")
            {
              this->version += "." + wordBits[1];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_BUILD_VERSION")
            {
              this->version += "." + wordBits[1];
            }
        }
    }

  this->level = level;
  this->path = path;

  //check all folders and files
  wxDir dir(crea::std2wx((path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;

      //folders
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          //if src, check for black boxes
          if(stdfileName == "src")
            {
              this->src = new modelCDMPackageSrc(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->src);
            }
          else
            {
              this->children.push_back(new modelCDMFolder(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }

      //files
      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              this->CMakeLists = new modelCDMCMakeListsFile(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(this->CMakeLists);
            }
          //if is a code file, create code file
          else if(fileType == ".c" ||
              fileType == ".cxx" ||
              fileType == ".h" ||
              fileType == ".cpp" ||
              fileType == ".txx" ||
              fileType == ".cmake" )
            {
              modelCDMCodeFile* file = new modelCDMCodeFile(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
              this->children.push_back(file);
            }
          //if is an unknown file, create file
          else
            {
              this->children.push_back(new modelCDMFile(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
            }

          cont = dir.GetNext(&fileName);
        }
    }
  this->SortChildren();
}

modelCDMPackage::~modelCDMPackage()
{
}

const std::string& modelCDMPackage::GetNamePackage() const
{
  return this->namePackage;
}

const std::string& modelCDMPackage::GetAuthors() const
{
  return this->authors;
}

const std::string& modelCDMPackage::GetAuthorsEmail() const
{
  return this->authorsEmail;
}

const std::string& modelCDMPackage::GetVersion() const
{
  return this->version;
}

const std::string& modelCDMPackage::GetDescription() const
{
  return this->description;
}

modelCDMPackageSrc* modelCDMPackage::GetSrc() const
{
  return this->src;
}

bool modelCDMPackage::SetAuthors(const std::string& authors, std::string*& result)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, authors, ",\n", CDMUtilities::splitter::no_empties);
  std::string authorsReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      authorsReal += "/" + words[i];
    }

  std::string line;
  //opening original cmakelists
  std::ifstream in((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
  if( !in.is_open())
    {
      result = new std::string("CMakeLists.txt file failed to open.");
      return false;
    }
  //opening temporal cmakelists
  std::ofstream out((this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string("CMakeLists.txt.tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  while (getline(in, line))
    {
      if(line.find("SET(${BBTK_PACKAGE_NAME}_AUTHOR") != std::string::npos)
        line = "SET(${BBTK_PACKAGE_NAME}_AUTHOR \"" + authorsReal + "\")";
      out << line << std::endl;
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
  std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif
  
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->authors = authorsReal;
  return true;
}

bool modelCDMPackage::SetAuthorsEmail(const std::string& email, std::string*& result)
{
  //TODO: implement method
  return true;
}

bool modelCDMPackage::SetVersion(const std::string& version, std::string*& result)
{
  std::vector<std::string> vers;
  CDMUtilities::splitter::split(vers, version, " .", CDMUtilities::splitter::no_empties);


  std::string line;
  //opening original cmakelists
  std::ifstream in((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
  if( !in.is_open())
    {
      result = new std::string("CMakeLists.txt file failed to open.");
      return false;
    }
  //opening temporal cmakelists
  std::ofstream out((this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string("CMakeLists.txt.tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  while (getline(in, line))
    {
      if(line.find("SET(${BBTK_PACKAGE_NAME}_MAJOR_VERSION") != std::string::npos)
        line = "SET(${BBTK_PACKAGE_NAME}_MAJOR_VERSION " + vers[0] + ")";
      else if(line.find("SET(${BBTK_PACKAGE_NAME}_VERSION") != std::string::npos)
        line = "SET(${BBTK_PACKAGE_NAME}_MINOR_VERSION " + vers[1] + ")";
      else if(line.find("SET(${BBTK_PACKAGE_NAME}_BUILD_VERSION") != std::string::npos)
        line = "SET(${BBTK_PACKAGE_NAME}_BUILD_VERSION " + vers[2] + ")";
      out << line << std::endl;
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
  std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif
  
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->version = vers[0] + "." + vers[1] + "." + vers[2];
  return true;
}

bool modelCDMPackage::SetDescription(const std::string& description, std::string*& result)
{
  std::vector<std::string> words;
  CDMUtilities::splitter::split(words, description, " \n", CDMUtilities::splitter::no_empties);
  std::string descriptionReal = words[0];
  for (int i = 1; i < (int)(words.size()); i++)
    {
      descriptionReal += " " + words[i];
    }

  std::string line;
  //opening original cmakelists
  std::ifstream in((this->path + CDMUtilities::SLASH + "CMakeLists.txt").c_str());
  if( !in.is_open())
    {
      result = new std::string("CMakeLists.txt file failed to open.");
      return false;
    }
  //opening temporal cmakelists
  std::ofstream out((this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp").c_str());
  if( !out.is_open())
    {
      result = new std::string("CMakeLists.txt.tmp file failed to open.");
      return false;
    }
  //copying contents from original to temporal and making changes
  while (getline(in, line))
    {
      if(line.find("SET(${BBTK_PACKAGE_NAME}_DESCRIPTION") != std::string::npos)
        line = "SET(${BBTK_PACKAGE_NAME}_DESCRIPTION \"" + descriptionReal + "\")";
      out << line << std::endl;
    }
  in.close();
  out.close();
  //delete old file and rename new file
#ifdef _WIN32
  std::string renameCommand = "move /Y \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#else
  std::string renameCommand = "mv \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt.tmp\" \"" + this->path + CDMUtilities::SLASH + "CMakeLists.txt\"";
#endif
  
  if(system(renameCommand.c_str()))
    {
      result = new std::string("An error occurred while running '" + renameCommand + "'.");
      return false;
    }

  this->description = descriptionReal;
  return true;
}

modelCDMBlackBox* modelCDMPackage::CreateBlackBox(
    std::string*& result,
    const std::string& name,
    const std::string& type,
    const std::string& format,
    const std::string& categories,
    const std::string& authors,
    const std::string& authorsEmail,
    const std::string& description
)
{
  return this->src->CreateBlackBox(result,name, this->namePackage, type,format,categories,authors,authorsEmail,description);
}

const bool modelCDMPackage::Refresh(std::string*& result)
{
  std::cout << "refreshing package " << this->namePackage << std::endl;
  this->type = wxDIR_DIRS;

  //Get Package Name

  std::string pathMakeLists = path + CDMUtilities::SLASH + "CMakeLists.txt";

  std::ifstream confFile;
  confFile.open((pathMakeLists).c_str());

  std::string word;
  while(confFile.is_open() && !confFile.eof())
    {
      //get sets
      std::getline(confFile,word,'(');
      std::vector<std::string> wordBits;
      CDMUtilities::splitter::split(wordBits,word," (\n",CDMUtilities::splitter::no_empties);

      if(wordBits[wordBits.size()-1] == "SET")
        {
          //get package name
          std::getline(confFile,word,')');
          CDMUtilities::splitter::split(wordBits, word, " ", CDMUtilities::splitter::no_empties);
          if(wordBits[0] == "BBTK_PACKAGE_NAME")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->namePackage = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_AUTHOR")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->authors = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_DESCRIPTION")
            {
              word = wordBits[1];
              for (int i = 2; i < (int)(wordBits.size()); i++)
                {
                  word += " " + wordBits[i];
                }
              wordBits.clear();
              CDMUtilities::splitter::split(wordBits, word, "\"", CDMUtilities::splitter::no_empties);

              this->description = wordBits[0];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_MAJOR_VERSION")
            {
              this->version = wordBits[1];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_MINOR_VERSION")
            {
              this->version += "." + wordBits[1];
            }
          else if(wordBits[0] == "${BBTK_PACKAGE_NAME}_BUILD_VERSION")
            {
              this->version += "." + wordBits[1];
            }
        }
    }



  std::vector<bool> checked(this->children.size(), false);
  bool checkedSrc = false;

  //check all folders
  wxDir dir(crea::std2wx((this->path).c_str()));
  if (dir.IsOpened())
    {
      wxString fileName;
      bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS);
      while (cont)
        {

          std::string stdfileName = crea::wx2std(fileName);

          //detect black boxes in src
          if(stdfileName == "src")
            {
              //check if box already exist
              bool found = false;
              if (this->src != NULL)
                {
                  found = true;
                  int pos = std::find(this->children.begin(), this->children.end(), this->src) - this->children.begin();
                  checked[pos] = true;
                  checkedSrc = true;
                  if(!this->src->Refresh(result))
                    return false;
                }
              else
                {
                  this->src = new modelCDMPackageSrc(this, path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level +1);
                  this->children.push_back(this->src);
                }
            }
          else
            {

              //check if folder already exist
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }
              if(!found)
                {
                  modelCDMFolder* folder = new modelCDMFolder(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(folder);
                }
            }
          cont = dir.GetNext(&fileName);

        }

      cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_FILES);
      while (cont)
        {
          std::string stdfileName = crea::wx2std(fileName);
          std::size_t fileTypePos = stdfileName.find_last_of(".");
          std::string fileType;
          if(fileTypePos != std::string::npos)
            fileType = stdfileName.substr(fileTypePos);
          else
            fileType = "";

          //if CMakeLists, create CMakeLists
          if(stdfileName == "CMakeLists.txt")
            {
              if (this->CMakeLists == NULL)
                {
                  this->CMakeLists = new modelCDMCMakeListsFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                  this->children.push_back(this->CMakeLists);
                }
              else
                {
                  int pos = std::find(this->children.begin(), this->children.end(), this->CMakeLists) - this->children.begin();
                  checked[pos] = true;
                  if(!this->CMakeLists->Refresh(result))
                    return false;
                }
            }
          //if is an unknown file, create file
          else
            {
              bool found = false;
              for (int i = 0; !found && i < (int)(this->children.size()); i++)
                {
                  if (this->children[i]->GetName() == stdfileName)
                    {
                      found = true;
                      checked[i] = true;
                      if(!this->children[i]->Refresh(result))
                        return false;
                    }
                }

              if(!found)
                {
                  //if is a code file, create modelCDMCodeFile
                  if(
                      fileType == ".c" ||
                      fileType == ".cxx" ||
                      fileType == ".h" ||
                      fileType == ".cpp" ||
                      fileType == ".txx" ||
                      fileType == ".cmake" )
                    {
                      this->children.push_back(new modelCDMCodeFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1));
                    }
                  else
                    {
                      modelCDMFile* file = new modelCDMFile(this, this->path + CDMUtilities::SLASH + stdfileName, stdfileName, this->level + 1);
                      this->children.push_back(file);
                    }
                }
            }

          cont = dir.GetNext(&fileName);
        }
    }

  if(!checkedSrc)
    {
      this->src = NULL;
    }

  for (int i = 0; i < (int)(checked.size()); i++)
    {
      if(!checked[i])
        {
          delete this->children[i];
          this->children.erase(this->children.begin()+i);
          checked.erase(checked.begin()+i);
          i--;
        }
    }
  this->SortChildren();
  return true;
}

void modelCDMPackage::CheckStructure(std::map<std::string, bool>& properties)
{
  //check cmake exist
  if(this->CMakeLists != NULL)
    {
      //open cmakelists
      std::ifstream confFile;
      confFile.open((this->CMakeLists->GetPath()).c_str());

      //take everything that is not commented
      std::string fileContent;

      std::string word;
      std::vector<std::string> words;
      while(confFile.is_open() && !confFile.eof())
        {
          std::getline(confFile,word, '\n');
          if(word[0] != '#')
            {
              CDMUtilities::splitter::split(words, word, "#", CDMUtilities::splitter::empties_ok);
              if (words.size() > 0)
                {
                  word = words[0];
                  CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::empties_ok);
                  for (int i = 0; i < (int)(words.size()); i++)
                    {
                      if(words[i].substr(0,2) == "//")
                        break;
                      fileContent += words[i] + " ";
                    }
                }
            }
        }

      //check every instruction
      std::stringstream ss(fileContent);
      while(!ss.eof())
        {
          std::getline(ss,word, '(');

          //check instruction name
          CDMUtilities::splitter::split(words, word, " ", CDMUtilities::splitter::no_empties);

          //set instructions
          if (words.size() > 0 && words[words.size()-1] == "SET")
            {
              std::getline(ss,word, ')');

              CDMUtilities::splitter::split(words, word, " \t", CDMUtilities::splitter::no_empties);
              if (words.size() > 1)
                {
                  if (words[0] == "${BBTK_PACKAGE_NAME}_USE_VTK" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_VTK"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_ITK" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_ITK"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_GDCM" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_GDCM"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_GDCM_VTK" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_GDCM_VTK"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_GSMIS" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_GSMIS"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_WXWIDGETS" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_WXWIDGETS"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_USE_KWWIDGETS" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_KWWIDGETS"] = true;
                    }
                  else if (words[0] == "USE_BOOST" && words[1] == "ON")
                    {
                      properties["package " + this->name + " set USE_BOOST"] = true;
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_INCLUDE_DIRS")
                    {
                      for (int i = 1; i < (int)(words.size()); i++)
                        {
                          if(words[i].substr(0,2) == "${" || words[i].substr(0,2) == "..")
                          properties["package " + this->name + " dir " + words[i]] = true;
                        }
                    }
                  else if (words[0] == "${BBTK_PACKAGE_NAME}_LIBS")
                    {
                      for (int i = 1; i < (int)(words.size()); i++)
                        {
                          properties["package " + this->name + " lib " + words[i]] = true;
                        }
                    }
                }
            }
        }

    }
}

std::map<std::string, bool> modelCDMPackage::Get3rdPartyLibraries()
{
  std::map<std::string, std::string> correspondence;
  correspondence["${BBTK_PACKAGE_NAME}_USE_VTK"] = "VTK";
  correspondence["${BBTK_PACKAGE_NAME}_USE_ITK"] = "ITK";
  correspondence["${BBTK_PACKAGE_NAME}_USE_GDCM"] = "GDCM";
  correspondence["${BBTK_PACKAGE_NAME}_USE_GDCM_VTK"] = "GDCM_VTK";
  correspondence["${BBTK_PACKAGE_NAME}_USE_GSMIS"] = "GSMIS";
  correspondence["${BBTK_PACKAGE_NAME}_USE_WXWIDGETS"] = "WxWidgets";
  correspondence["${BBTK_PACKAGE_NAME}_USE_KWWIDGETS"] = "KWWidgets";
  std::map<std::string, bool> res;
  res["VTK"] = false;
  res["ITK"] = false;
  res["GDCM"] = false;
  res["GDCM_VTK"] = false;
  res["GSMIS"] = false;
  res["WxWidgets"] = false;
  res["KWWidgets"] = false;

  if (this->HasCMakeLists())
    {
      std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());

      boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_USE_\\w+\\s+ON");
      std::string::const_iterator start, end;
      start = CMfile.begin();
      end = CMfile.end();
      boost::match_results<std::string::const_iterator> what;
      boost::match_flag_type flags = boost::match_default;
      while(boost::regex_search(start, end, what, expression, flags))
        {
          //std::cout << what[0].str() << std::endl;
          boost::regex expression1 = boost::regex("\\$\\{BBTK_PACKAGE_NAME\\}_USE_\\w+");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression1, flags))
            {
              std::string dete = what1.str();
              CDMUtilities::normalizeStr(dete);
              //std::cout << dete << std::endl;
              if(correspondence.find(dete) != correspondence.end())
                res[correspondence[dete]] = true;
            }
          start = what[0].second;
        }
    }
  return res;
}

bool modelCDMPackage::Set3rdPartyLibrary(const std::string& library_name,
    const bool& toInclude)
{
  std::map<std::string, std::string> correspondence;

  correspondence["VTK"] = "_USE_VTK";
  correspondence["ITK"] = "_USE_ITK";
  correspondence["GDCM"] = "_USE_GDCM";
  correspondence["GDCM_VTK"] = "_USE_GDCM_VTK";
  correspondence["GSMIS"] = "_USE_GSMIS";
  correspondence["WxWidgets"] = "_USE_WXWIDGETS";
  correspondence["KWWidgets"] = "_USE_KWWIDGETS";

  if (correspondence.find(library_name) != correspondence.end())
    {
      std::string library_command = correspondence[library_name];
      if (this->HasCMakeLists())
        {
          std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());
          std::string resCMfile = "";
          bool found = false;

          try {
            boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}"+library_command+"([\\s]|#[^\\n]*\\n)+ON([\\s]|#[^\\n]*\\n)*\\)");

            std::string::const_iterator start, end;
            start = CMfile.begin();
            end = CMfile.end();
            boost::match_results<std::string::const_iterator> what;
            boost::match_flag_type flags = boost::match_default;
            if(boost::regex_search(start, end, what, expression, flags))
              {
                found = true;
                resCMfile += what.prefix().str();
                if (toInclude)
                  resCMfile += what.str();
                else
                  resCMfile += "#" + what.str();
                resCMfile += what.suffix().str();

                return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
              }
            else
              {
                boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}"+library_command+"([\\s]|#[^\\n]*\\n)+OFF([\\s]|#[^\\n]*\\n)*\\)");

                start = CMfile.begin();
                end = CMfile.end();
                if(boost::regex_search(start, end, what, expression, flags))
                  {
                    found = true;
                    resCMfile += what.prefix().str();
                    if (toInclude)
                      {
                        std::string dete = what.str();
                        int pos = dete.rfind("OFF");
                        dete.replace(pos, 3, "ON");
                        resCMfile += dete;
                      }
                    else
                      resCMfile += what.str();
                    resCMfile += what.suffix().str();

                    return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
                  }
                else
                  {
                    boost::regex expression("^\\h*#\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}"+library_command+"([\\s]|#[^\\n]*\\n)+ON([\\s]|#[^\\n]*\\n)*\\)");
                    if(boost::regex_search(start, end, what, expression, flags))
                      {
                        found = true;
                        resCMfile += what.prefix().str();
                        if(toInclude)
                          {
                            std::string dete = what[0].str();
                            for (int i = 0; i < dete.size(); ++i) {
                              if (dete[i] != '#')
                                resCMfile.push_back(dete[i]);
                              if (dete[i] == 'S')
                                {
                                  resCMfile += dete.substr(i+1);
                                  break;
                                }
                            }
                          }
                        else
                          resCMfile += what.str();

                        resCMfile += what.suffix().str();
                        return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
                      }
                    else
                      {
                        boost::regex expression("^\\h*#\\h*UNCOMMENT EACH LIBRARY NEEDED \\(WILL BE FOUND AND USED AUTOMATICALLY\\)[^\\n]*\\n");
                        if(boost::regex_search(start, end, what, expression, flags))
                          {
                            found = true;
                            resCMfile += what.prefix().str();
                            resCMfile += what.str();
                            if(toInclude)
                              {
                                resCMfile += "SET(${BBTK_PACKAGE_NAME}"+ library_command +"  ON)\n";
                              }
                            resCMfile += what.suffix().str();
                            return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
                          }
                      }
                  }
              }
          } catch (boost::bad_expression& e) {
            std::cout << "bad regex: " << e.what() << std::endl;
            std::cout.flush();
          }
        }
    }
  return false;
}

std::map<std::string, bool> modelCDMPackage::GetCustomLibraries()
{
  std::map<std::string, bool> res;
  std::map<std::string, bool> res1;

  std::map<std::string, std::string> correspondence;
  std::vector<modelCDMLibrary*> libraries;
  modelCDMIProjectTreeNode* p = this;
  while(p != NULL && dynamic_cast<modelCDMProject*>(p) == NULL)
    p = p->GetParent();

  if(p != NULL && dynamic_cast<modelCDMProject*>(p)->GetLib() != NULL)
    libraries = dynamic_cast<modelCDMProject*>(p)->GetLib()->GetLibraries();

  for (int i = 0; i < libraries.size(); ++i)
    {
      correspondence[libraries[i]->GetName()] = libraries[i]->GetNameLibrary();
      res[libraries[i]->GetNameLibrary()] = false;
      res1[libraries[i]->GetNameLibrary()] = false;
    }

  if (this->HasCMakeLists())
    {
      std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());

      //find included libraries
      boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_LIBS(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
      std::string::const_iterator start, end;
      start = CMfile.begin();
      end = CMfile.end();
      boost::match_results<std::string::const_iterator> what;
      boost::match_flag_type flags = boost::match_default;
      if(boost::regex_search(start, end, what, expression, flags))
        {

          expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_LIBS");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression, flags))
            {
              expression = boost::regex("^\\h*[\\w\\d]+");
              std::string::const_iterator start2, end2;
              start2 = what1[0].second;
              end2 = what[0].second;
              boost::match_results<std::string::const_iterator> what2;
              while(boost::regex_search(start2, end2, what2, expression, flags))
                {
                  std::string dete = what2.str();
                  CDMUtilities::normalizeStr(dete);
                  //std::cout << "detectado lib: " << dete << std::endl;
                  if(res1.find(dete) != res1.end())
                    res1[dete] = true;

                  start2 = what2[0].second;
                }
            }
        }

      //find included folders
      //std::cout << "searching..." << std::endl;
      expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_INCLUDE_DIRS(([\\s]|#[^\\n]*\\n|////[^\\n]*\\n)+([\\.\\/\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
      start = CMfile.begin();
      end = CMfile.end();
      if(boost::regex_search(start, end, what, expression, flags))
        {
          //std::cout << what.str() << std::endl;
          expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_INCLUDE_DIRS");
          std::string::const_iterator start1, end1;
          start1 = what[0].first;
          end1 = what[0].second;
          boost::match_results<std::string::const_iterator> what1;
          if(boost::regex_search(start1, end1, what1, expression, flags))
            {
              //std::cout << what1.str() << std::endl;
              expression = boost::regex("^\\h*\\.\\.\\/lib\\/([\\w\\d])+");
              std::string::const_iterator start2, end2;
              start2 = what1[0].second;
              end2 = what[0].second;
              boost::match_results<std::string::const_iterator> what2;
              while(boost::regex_search(start2, end2, what2, expression, flags))
                {
                  std::string dete = what2.str();
                  CDMUtilities::normalizeStr(dete);
                  //std::cout << "detectado dir: " << dete.substr(7) << std::endl;
                  if(correspondence.find(dete.substr(7)) != correspondence.end())
                    res[correspondence[dete.substr(7)]] = res1[correspondence[dete.substr(7)]];

                  start2 = what2[0].second;
                }
            }
        }
    }

  return res;
}

bool modelCDMPackage::SetCustomLibrary(const std::string& library_name,
    const bool& toInclude)
{
  std::map<std::string, std::string> correspondence;

    std::vector<modelCDMLibrary*> libraries;
    modelCDMIProjectTreeNode* p = this;
    while(p != NULL && dynamic_cast<modelCDMProject*>(p) == NULL)
      p = p->GetParent();

    if(p != NULL && dynamic_cast<modelCDMProject*>(p)->GetLib() != NULL)
      libraries = dynamic_cast<modelCDMProject*>(p)->GetLib()->GetLibraries();

    for (int i = 0; i < libraries.size(); ++i)
      {
        correspondence[libraries[i]->GetNameLibrary()] = libraries[i]->GetName();
      }

    if (correspondence.find(library_name) != correspondence.end())
      {
        if (this->HasCMakeLists())
          {
            std::string resCMfile = "";
            std::string CMfile = CDMUtilities::readFile(this->CMakeLists->GetPath().c_str());
            bool found = false;

            //find included libraries
            //std::cout << "searching..." << CMfile << std::endl;
            boost::regex expression("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_LIBS(([\\s]|#[^\\n]*\\n)+([\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
            std::string::const_iterator start, end;
            start = CMfile.begin();
            end = CMfile.end();
            boost::match_results<std::string::const_iterator> what;
            boost::match_flag_type flags = boost::match_default;
            if(boost::regex_search(start, end, what, expression, flags))
              {
                //std::cout << what.str() << std::endl;
                resCMfile += what.prefix().str();
                expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_LIBS");
                std::string::const_iterator start1, end1;
                start1 = what[0].first;
                end1 = what[0].second;
                boost::match_results<std::string::const_iterator> what1;
                if(boost::regex_search(start1, end1, what1, expression, flags))
                  {
                    resCMfile += what1.prefix().str() + what1.str();
                    //check if already exists
                    expression = boost::regex("^\\h*"+library_name);
                    std::string::const_iterator start2, end2;
                    start2 = what1[0].second;
                    end2 = what[0].second;
                    boost::match_results<std::string::const_iterator> what2, temp2;
                    while(boost::regex_search(start2, end2, what2, expression, flags))
                      {
                        resCMfile += what2.prefix().str();
                        found = true;
                        if (!toInclude)
                          {
                            resCMfile += "#";
                          }
                        resCMfile += what2.str();
                        temp2 = what2;
                        start2 = what2[0].second;
                      }
                    if(found)
                      resCMfile += temp2.suffix().str();
                    //check if is commented
                    else
                      {
                        expression = boost::regex("^\\h*#+\\h*"+library_name);
                        start2 = what1[0].second;
                        end2 = what[0].second;
                        while(boost::regex_search(start2, end2, what2, expression, flags))
                          {
                            found = true;
                            resCMfile += what2.prefix().str();
                            if(toInclude)
                              {
                                std::string dete = what2[0].str();
                                for (int i = 0; i < dete.size(); ++i) {
                                  if (dete[i] != '#')
                                    resCMfile.push_back(dete[i]);
                                }
                              }
                            temp2 = what2;
                            start2 = what2[0].second;
                          }
                        if(found)
                          resCMfile += temp2.suffix().str();
                        //add at the beggining of instruction
                        else
                          {
                            if(toInclude)
                              resCMfile += "\n" + library_name;
                            resCMfile += what1.suffix().str();
                          }
                      }
                  }
                resCMfile += what.suffix().str();
              }
            else
              return false;

            //find included folders
            CMfile = resCMfile;
            resCMfile = "";


            found = false;
            //std::cout << "searching..." << CMfile << std::endl;
            expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_INCLUDE_DIRS(([\\s]|#[^\\n]*\\n)+([\\.\\/\\$\\{\\}\\w\\d]+|\"(?:[^\"\\\\]|\\\\.)*\"))*([\\s]|#[^\\n]*\\n)*\\)");
            start = CMfile.begin();
            end = CMfile.end();
            if(boost::regex_search(start, end, what, expression, flags))
              {
                resCMfile += what.prefix().str();
                //std::cout << what.str() << std::endl;
                expression = boost::regex("^\\h*SET([\\s]|#[^\\n]*\\n)*\\(([\\s]|#[^\\n]*\\n)*\\$\\{BBTK_PACKAGE_NAME\\}_INCLUDE_DIRS");
                std::string::const_iterator start1, end1;
                start1 = what[0].first;
                end1 = what[0].second;
                boost::match_results<std::string::const_iterator> what1;
                if(boost::regex_search(start1, end1, what1, expression, flags))
                  {
                    resCMfile += what1.prefix().str() + what1.str();
                    //std::cout << what1.str() << std::endl;
                    //search if dir is already included
                    expression = boost::regex("^\\h*\\.\\.\\/lib\\/"+correspondence[library_name]);
                    std::string::const_iterator start2, end2;
                    start2 = what1[0].second;
                    end2 = what[0].second;
                    boost::match_results<std::string::const_iterator> what2, temp2;
                    while(boost::regex_search(start2, end2, what2, expression, flags))
                      {
                        found = true;
                        resCMfile += what2.prefix().str();
                        if(!toInclude)
                          resCMfile += "#";
                        resCMfile += what2.str();
                        temp2 = what2;
                        start2 = what2[0].second;
                      }
                    if(found)
                      resCMfile += temp2.suffix().str();
                    //search if dir is commented
                    else
                      {
                        expression = boost::regex("^\\h*#+\\h*\\.\\.\\/lib\\/"+correspondence[library_name]);
                        start2 = what1[0].second;
                        end2 = what[0].second;
                        while(boost::regex_search(start2, end2, what2, expression, flags))
                          {
                            found = true;
                            resCMfile += what2.prefix().str();
                            if(toInclude)
                              {
                                std::string dete = what2[0].str();
                                for (int i = 0; i < dete.size(); ++i) {
                                  if (dete[i] != '#')
                                    resCMfile.push_back(dete[i]);
                                }
                              }
                            temp2 = what2;
                            start2 = what2[0].second;
                          }
                        if(found)
                          resCMfile += temp2.suffix().str();
                        //add at the beggining of instruction
                        else
                          {
                            if(toInclude)
                              resCMfile += "\n../lib/" + correspondence[library_name];
                            resCMfile += what1.suffix().str();
                          }
                      }
                  }
                resCMfile += what.suffix().str();
              }
            else
              return false;

            return CDMUtilities::writeFile(this->CMakeLists->GetPath().c_str(), resCMfile);
          }
      }

    return false;
}
