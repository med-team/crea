
#ifndef _$PROJECT_NAME$SYSTEM_H_
#define _$PROJECT_NAME$SYSTEM_H_


// Windoze related troubles (as usual)

//-----------------------------------------------------------------------------

#if defined(_WIN32)
  #ifdef $PROJECT_NAME$_EXPORT_SYMBOLS
    #define $PROJECT_NAME$_EXPORT __declspec( dllexport )
#else
    #define $PROJECT_NAME$_EXPORT __declspec( dllimport )
  #endif
  #define $PROJECT_NAME$_CDECL __cdecl
#else
  #define $PROJECT_NAME$_EXPORT
  #define $PROJECT_NAME$_CDECL
#endif // defined(_WIN32)

#ifdef __BORLANDC__
  #include <mem.h>
#endif

#endif
