# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#========================================================================
# Creates a directory if it does not exist
MACRO(CREA_MKDIR DIR)
  IF(NOT IS_DIRECTORY ${DIR})
    MESSAGE(STATUS "* Creating directory '${DIR}'")
    FILE(MAKE_DIRECTORY ${DIR})
  ENDIF(NOT IS_DIRECTORY ${DIR})
ENDMACRO(CREA_MKDIR)
#========================================================================

#========================================================================
# COPIES A DIRECTORY EXCLUDING CVS ENTRIES
MACRO(CREA_CPDIR SOURCE DEST)
  FILE(GLOB_RECURSE NAMES RELATIVE ${SOURCE} ${SOURCE}/*)
  FOREACH( NAME  ${NAMES})
    STRING(SUBSTRING ${NAME} 0 3 subStrTmp )
    IF( NOT subStrTmp STREQUAL "../" )
      STRING(REGEX MATCH "CVS" CVSSUBS_FOUND ${NAME} )
      IF( NOT CVSSUBS_FOUND STREQUAL "CVS" )
	CONFIGURE_FILE(
	  ${SOURCE}/${NAME} 
	  ${DEST}/${NAME}
	  COPYONLY )
      ENDIF( NOT CVSSUBS_FOUND STREQUAL "CVS" )
    ENDIF( NOT subStrTmp STREQUAL "../" )
  ENDFOREACH(NAME)
ENDMACRO(CREA_CPDIR)
#========================================================================
