# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


# Manages the shared library creation/use 
# * Creates an option ${LIBRARY_NAME}_BUILD_SHARED 
# * Generates the file ${LIBRARY_NAME}_EXPORT.h which 
#   defines the symbols ${LIBRARY_NAME}_EXPORT and ${LIBRARY_NAME}_CDECL
#   to be used in exported classes/functions declarations
MACRO(CREA_DYNAMIC_LIBRARY_EXPORT_OPTION LIBRARY_NAME)

  # STATIC OR DYNAMIC (SHARED) ? 
  OPTION( ${LIBRARY_NAME}_BUILD_SHARED 
  #JPR 8 Avr 2011 ON-> OFF, to avoid troubles with unaware users
    "Build ${LIBRARY_NAME} as a shared library ( i.e. dynamic) ?" OFF)
  IF (${LIBRARY_NAME}_BUILD_SHARED)
    SET(${LIBRARY_NAME}_SHARED SHARED)
    CREA_DEFINE(${LIBRARY_NAME}_BUILD_SHARED)
  ENDIF(${LIBRARY_NAME}_BUILD_SHARED)
  
  CREA_DEFINE(${LIBRARY_NAME}_EXPORT_SYMBOLS)

  # ADDS CURRENT BINARY DIR TO INCLUDE DIRS
  INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

  # CONFIGURES ${LIBRARY_NAME}_EXPORT.h
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/LIBRARY_NAME_EXPORT.h.in
    ${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_NAME}_EXPORT.h
    @ONLY IMMEDIATE
    )
  # ADDS IT TO THE LIST OF HEADERS
  SET(${LIBRARY_NAME}_HEADERS
    ${${LIBRARY_NAME}_HEADERS}
    ${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_NAME}_EXPORT.h
    )
  

ENDMACRO(CREA_DYNAMIC_LIBRARY_EXPORT_OPTION LIBRARY_NAME)


# Manages the shared library creation/use 
# * Generates the file ${LIBRARY_NAME}_EXPORT.h which 
#   defines the symbols ${LIBRARY_NAME}_EXPORT and ${LIBRARY_NAME}_CDECL
#   to be used in exported classes/functions declarations
MACRO(CREA_DYNAMIC_LIBRARY_EXPORT LIBRARY_NAME)

  CREA_DEFINE(${LIBRARY_NAME}_BUILD_SHARED)
  CREA_DEFINE(${LIBRARY_NAME}_EXPORT_SYMBOLS)

  # ADDS CURRENT BINARY DIR TO INCLUDE DIRS
  INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

  # CONFIGURES ${LIBRARY_NAME}_EXPORT.h
  SET(LIBRARY_NAME ${LIBRARY_NAME})
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/LIBRARY_NAME_EXPORT.h.in
    ${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_NAME}_EXPORT.h
    @ONLY IMMEDIATE
    )
  # ADDS IT TO THE LIST OF HEADERS
  SET(${LIBRARY_NAME}_HEADERS
    ${${LIBRARY_NAME}_HEADERS}
    ${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY_NAME}_EXPORT.h
    )

ENDMACRO(CREA_DYNAMIC_LIBRARY_EXPORT LIBRARY_NAME)
