# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#-----------------------------------------------------------------------------
# MACRO TO ADD A COMPILER DEFINITION WHICH ADDS ALSO THE DEF 
# INTO DOXYGEN PREDEFINED VAR 
# (OTHERWISE DOXYGEN WOULD NOT PARSE THE FILES BLOCKED)
MACRO(CREA_DEFINE DEF)
  MESSAGE(STATUS "* Added definition -D${DEF}")
  ADD_DEFINITIONS( -D${DEF} )
  SET(CREA_DEFINITIONS
    ${CREA_DEFINITIONS};${DEF}
    CACHE 
    STRING 
    ""
    FORCE)
  MARK_AS_ADVANCED(CREA_DEFINITIONS)
ENDMACRO(CREA_DEFINE)
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# MACRO TO ADD A COMPILER DEFINITION WITH VALUE WHICH ADDS ALSO THE DEF 
# INTO DOXYGEN PREDEFINED VAR 
# (OTHERWISE DOXYGEN WOULD NOT PARSE THE FILES BLOCKED)
MACRO(CREA_DEFINE_WITH_VAL DEF VAL)
  MESSAGE(STATUS "* Added definition -D${DEF}=${VAL}")
  ADD_DEFINITIONS( -D${DEF}=${VAL} )
  SET(CREA_DEFINITIONS
    ${CREA_DEFINITIONS};${DEF}=${VAL}
    CACHE 
    STRING 
    ""
    FORCE)
  MARK_AS_ADVANCED(CREA_DEFINITIONS)
ENDMACRO(CREA_DEFINE_WITH_VAL)
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
MACRO(CREA_DEFINE_OPERATING_SYSTEM )
  IF(APPLE)
   CREA_DEFINE(MACOSX)
  ELSE(APPLE)
    IF("${CMAKE_SYSTEM}" MATCHES "Linux")
      CREA_DEFINE(LINUX)
    ELSE("${CMAKE_SYSTEM}" MATCHES "Linux")
      IF(WIN32)
        CREA_DEFINE(WIN32)
      ELSE(WIN32)
        MESSAGE(FATAL_ERROR "${CMAKE_SYSTEM} unsupported ! Build at your own risks ! (remove this line from kernel/cmake/BBTKAddDefinitions.cmake)")
      ENDIF(WIN32)
    ENDIF("${CMAKE_SYSTEM}" MATCHES "Linux")
  ENDIF(APPLE) 


  IF (WIN32)
    SET(CMAKE_CREA_LIB_PATH bin)
  ELSE (WIN32)
    if( NOT APPLE )
      # check 64 bit
      if( ${CMAKE_SIZEOF_VOID_P} EQUAL 4 )
	 set( HAVE_64_BIT 0 )
	 SET(CMAKE_CREA_LIB_PATH lib/creatools)
      else( ${CMAKE_SIZEOF_VOID_P}EQUAL 4 )
	 set( HAVE_64_BIT 1 )
	 SET(CMAKE_CREA_LIB_PATH lib64/creatools)
      endif( ${CMAKE_SIZEOF_VOID_P} EQUAL 4 )
    else ( NOT APPLE )
	 SET(CMAKE_CREA_LIB_PATH lib/creatools)

      if( ${CMAKE_SIZEOF_VOID_P} EQUAL 4 )
	 message("EED crea definitions  --------APPLE------------ 64 0")
      else( ${CMAKE_SIZEOF_VOID_P}EQUAL 4 )
	 message("EED crea definitions  --------APPLE------------ 64 1")
      endif( ${CMAKE_SIZEOF_VOID_P} EQUAL 4 )


    endif( NOT APPLE )
  ENDIF(WIN32)


ENDMACRO(CREA_DEFINE_OPERATING_SYSTEM)
#-----------------------------------------------------------------------------

