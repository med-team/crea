

MACRO(CREA_FIND_AND_USE_LIBRARIES)

IF(INCLUDE_CTEST)
	include(CTest) 
ENDIF(INCLUDE_CTEST)


set(USE_DEBUG_LIB "" CACHE BOOL "Use Debug Libraries" )

#-----------------------------------------------------------------------------
# If one can invoke this macro then crea has already been found !!
# However if one whishes to link against crea and crea was built 
# with wx or vtk then we have to find them.
# This USE_CREA var is made to resolve this dependency.
IF(USE_CREA)
  IF(CREA_BUILT_WITH_WX)
    SET(USE_WXWIDGETS ON)
  ENDIF(CREA_BUILT_WITH_WX)
  IF(CREA_BUILT_WITH_VTK)
    SET(USE_VTK ON)
  ENDIF(CREA_BUILT_WITH_VTK)
ENDIF(USE_CREA)
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
IF(USE_QT4)
  IF(NOT QT_FOUND)  	
  
	INCLUDE(${CREA_CMAKE_DIR}/CREAMacro_FindAndUseQt.cmake)
	#FIND_PACKAGE(Qt4 REQUIRED)	
	
    IF(QT_FOUND)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for QT4... found")
      MESSAGE ( STATUS "* Inc Dir = ${QT_INCLUDE_DIR}")
      MESSAGE ( STATUS "* Version = ${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}")
      INCLUDE(${QT_USE_FILE})
	 
      CREA_DEFINE( USE_QT )
    #    SET(QT_LIBRARIES
    #         ${QT_LIBRARIES}
    #      ) 
    #  MARK_AS_ADVANCED(Qt4_DIR)
   ENDIF(QT_FOUND)
  ENDIF(NOT QT_FOUND)
ENDIF(USE_QT4)
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
IF(USE_VTK)
  INCLUDE(${CREA_CMAKE_DIR}/CREAMacro_FindAndUseVTK.cmake)
  CREA_FIND_AND_USE_VTK()  
ENDIF(USE_VTK)
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
IF(USE_KWWIDGETS)
  IF(NOT KWWidgets_FOUND)
    # Search KWWidgets
    FIND_PACKAGE(KWWidgets)
    # If found
    IF(KWWidgets_FOUND)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for KWWidgets... found")
      MESSAGE ( STATUS "* Dir     = ${KWWidgets_DIR}")
      MESSAGE ( STATUS "* Version = ${KWWidgets_MAJOR_VERSION}.${KWWidgets_MINOR_VERSION}.${KWWidgets_BUILD_VERSION}")
      IF (USE_VTK)
         # KWWidgets must not search again for vtk !
        SET(KWWidgets_NO_USE_VTK ON)
      ENDIF (USE_VTK)

      INCLUDE(${KWWidgets_USE_FILE})
      CREA_DEFINE( USE_KWWIDGETS )
      SET(KWWIDGETS_LIBRARIES
	${KWWidgets_LIBRARIES}
	) 
      MARK_AS_ADVANCED(KWWidgets_DIR)
    ELSE(KWWidgets_FOUND)
      message(FATAL_ERROR "KWWidgets_DIR not found")
    ENDIF(KWWidgets_FOUND)
  ENDIF(NOT KWWidgets_FOUND)
ENDIF(USE_KWWIDGETS)
#-----------------------------------------------------------------------------

#  --- W A R N I N G :
#  Always let ITK *BEFORE* GDCM
#  It allows to use 'system installed gdcm' instead of 'itk embedded gdcm'
#

#-----------------------------------------------------------------------------
IF(USE_ITK)
  IF(NOT ITK_FOUND)
    # Search ITK
    FIND_PACKAGE(ITK)
    # If itk found
    IF(ITK_FOUND)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for ITK... found")
      MESSAGE ( STATUS "* Dir     = ${ITK_DIR}")
      MESSAGE ( STATUS "* Version = ${ITK_VERSION_MAJOR}.${ITK_VERSION_MINOR}.${ITK_VERSION_PATCH}")
      SET(SKIP_ITK_BUILD_SETTINGS_FILE ON)
      INCLUDE(${ITK_USE_FILE})
      CREA_DEFINE( USE_ITK )
      CREA_DEFINE( _USE_ITK_ )
	  If (${ITK_VERSION_MAJOR} MATCHES "4")
	   MESSAGE ( STATUS "* Version = ${ITK_VERSION_MAJOR}.${ITK_VERSION_MINOR}.${ITK_VERSION_PATCH}")
		SET(ITK_LIBRARIES
		${ITK_LIBRARIES}) 
		 MESSAGE ( STATUS "${ITK_LIBRARIES}")
		ELSEIF()
		SET(ITK_LIBRARIES
			ITKCommon ITKIO ITKBasicFilters ITKAlgorithms)
		ENDIF()
      MARK_AS_ADVANCED(ITK_DIR)
    ELSE(ITK_FOUND)
      message(FATAL_ERROR "ITK_DIR not found")
    ENDIF(ITK_FOUND)
  ENDIF(NOT ITK_FOUND)
ENDIF(USE_ITK)
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------

IF(USE_GDCM)
  IF (USE_GDCM2)
    MESSAGE(ERROR "CANNOT USE BOTH gdcm AND gdcm2 !")
  ENDIF (USE_GDCM2)

  IF(NOT GDCM_FOUND)
    # Search GDCM
    FIND_PACKAGE(GDCM)
    # If gdcm found
    IF(GDCM_FOUND)
      INCLUDE(${GDCM_USE_FILE})
      CREA_DEFINE( USE_GDCM )

      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for GDCM... found")
      MESSAGE ( STATUS "* Dir       = ${GDCM_DIR}")
      MESSAGE ( STATUS "* Version   = ${GDCM_MAJOR_VERSION}.${GDCM_MINOR_VERSION}.${GDCM_BUILD_VERSION}")
      MESSAGE ( STATUS "* Namespace = ${GDCM_NAME_SPACE}")

      # Test that GDCM_NAME_SPACE is **NOT** "gdcm" or will conflict with 
      # embedded gdcm in ITK and will result in hard seg faults !
      IF(USE_ITK)
	IF (GDCM_NAME_SPACE STREQUAL "gdcm")
	  MESSAGE(FATAL_ERROR "GDCM_NAME_SPACE value is 'gdcm' which conflicts with ITK embededd gdcm namespace : rebuild gdcm with another GDCM_NAME_SPACE value")
	ENDIF (GDCM_NAME_SPACE STREQUAL "gdcm")
      ENDIF(USE_ITK)

      MARK_AS_ADVANCED(GDCM_DIR)

      SET(GDCM_LIBRARIES
        ${GDCM_LIBRARIES} 
	)

      # TODO : Test that GDCM was built with option "vtkgdcm"
      IF(USE_GDCM_VTK)
	IF(USE_VTK)
	  SET(GDCM_LIBRARIES
  	    ${GDCM_LIBRARIES}
            vtkgdcm  
            )
	ENDIF(USE_VTK)
      ENDIF(USE_GDCM_VTK)

    ELSE(GDCM_FOUND)     
	  MESSAGE(FATAL_ERROR "GDCM not found....")
    ENDIF(GDCM_FOUND)

  ENDIF(NOT GDCM_FOUND)
ENDIF(USE_GDCM)

#-----------------------------------------------------------------------------

IF(USE_GDCM2)
  IF (USE_GDCM)
    MESSAGE(ERROR "CANNOT USE BOTH gdcm AND gdcm2 !")
  ENDIF (USE_GDCM)

#  IF(NOT USE_ITK) # JPR : ITK vs GDCM (avoid cmake break)
    # Search GDCM2

    FIND_PACKAGE(GDCM REQUIRED)
    INCLUDE(${GDCM_USE_FILE})

      IF(NOT ${GDCM_MAJOR_VERSION} EQUAL 2)
	MESSAGE(FATAL_ERROR "gdcm version 2 needed. found ${GDCM_MAJOR_VERSION}.${GDCM_MINOR_VERSION}.${GDCM_BUILD_VERSION}. Set GDCM_DIR to the gdcm version 2 directory")
      ENDIF(NOT ${GDCM_MAJOR_VERSION} EQUAL 2)
      
      CREA_DEFINE( USE_GDCM2 )
	  MARK_AS_ADVANCED(GDCM_DIR)
      CREA_DEFINE_WITH_VAL( GDCM_NAME_SPACE gdcm )
      
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for GDCM2... found")
      MESSAGE ( STATUS "* Dir       = ${GDCM_DIR}")
      MESSAGE ( STATUS "* Version   = ${GDCM_MAJOR_VERSION}.${GDCM_MINOR_VERSION}.${GDCM_BUILD_VERSION}")
      MESSAGE ( STATUS "* Namespace = ${GDCM_NAME_SPACE}")
      
      # Test that GDCM_NAME_SPACE is **NOT** "gdcm" or will conflict with 
      # embedded gdcm in ITK and will result in hard seg faults !
		   INCLUDE_DIRECTORIES(${GDCM_INCLUDE_DIRS})	 
	  		LINK_DIRECTORIES( ${GDCM_LIBRARY_DIRS}) 
			
      SET(GDCM2_LIBRARIES
        ${GDCM_LIBRARIES} 
	)

      IF(USE_GDCM_VTK)
	IF(USE_VTK)
		IF(WIN32)
		  SET(GDCM_LIBRARIES
	  	        ${GDCM_LIBRARIES}
			vtkgdcm
			gdcmIOD
			gdcmMSFF
			gdcmexpat
			gdcmCommon
			gdcmMEXD
			gdcmexpat
			gdcmDSED
			gdcmDICT
			gdcmjpeg8
			gdcmjpeg12
			gdcmjpeg16
			socketxx
			gdcmopenjpeg
			gdcmcharls
            	     )
		ELSE(WIN32)
		    SET(GDCM_LIBRARIES
	  	        ${GDCM_LIBRARIES}
			vtkgdcm
			gdcmIOD
			)
		ENDIF(WIN32)
		
	ENDIF(USE_VTK)
      ENDIF(USE_GDCM_VTK)
 # ENDIF(NOT USE_ITK) # JPR : ITK vs GDCM (avoid cmake break)

ENDIF(USE_GDCM2)

#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
IF(USE_WXWIDGETS)
  IF(NOT wxWidgets_FOUND)
    INCLUDE(${CREA_CMAKE_DIR}/CREAMacro_FindAndUseWxWidgets.cmake)
    CREA_FIND_WXWIDGETS(${USE_DEBUG_LIB})
    CREA_USE_WXWIDGETS()
  ENDIF(NOT wxWidgets_FOUND)
ENDIF(USE_WXWIDGETS)
#-----------------------------------------------------------------------------



#-----------------------------------------------------------------------------
IF(USE_DOXYGEN) 
  IF(NOT DOXYGEN)
    MESSAGE ( STATUS "=======================================")
    FIND_PACKAGE(Doxygen REQUIRED)
    #  MESSAGE(STATUS "Looking for doxygen... - found ${DOXYGEN}")
    #  IF(DOXYGEN)
    #    
    #  ELSE(DOXYGEN)
    #    MESSAGE(FATAL_ERROR "Doxygen not found. Required to build the doxygen documentation")
  ENDIF(NOT DOXYGEN)
ENDIF(USE_DOXYGEN)
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
IF(USE_LATEX)
	IF (NOT LATEX_COMPILER)
		FIND_PACKAGE(LATEX REQUIRED)
		MESSAGE ( STATUS "=======================================")
		MESSAGE(STATUS "Looking for latex... - found ${LATEX_COMPILER}")
		IF (NOT LATEX_FOUND)
                   MESSAGE(FATAL_ERROR "LATEX_COMPILER NOT FOUND. Example:latex  (MacOS:/opt/local/bin/latex)")
                ENDIF (NOT LATEX_FOUND)
	ENDIF (NOT LATEX_COMPILER)
  
ENDIF(USE_LATEX) 
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
IF(USE_TTH) 
  IF (NOT TTH_EXECUTABLE)
    FIND_FILE( TTH_EXECUTABLE NAME tth tth.exe PATHS . $ENV{CREATIS}/tth_exe/ ~/Creatis/tth ~/Creatis/tth_linux $ENV{TTH}) 

    IF (NOT TTH_EXECUTABLE)
      MESSAGE(FATAL_ERROR "TTH not found (LATEX_tth_CONVERTER). Required to build the HTML documentation from tex source")
    ELSE (NOT  TTH_EXECUTABLE)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for tth... - found ${TTH_EXECUTABLE}")
    ENDIF(NOT TTH_EXECUTABLE)
    MARK_AS_ADVANCED( TTH_EXECUTABLE )
  ENDIF(NOT TTH_EXECUTABLE)
ENDIF(USE_TTH)
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
IF(USE_BOOST)
  IF(NOT Boost_FOUND)
	add_definitions( -DBOOST_ALL_NO_LIB )
	add_definitions( -DBOOST_ALL_DYN_LINK )
    FIND_PACKAGE(Boost COMPONENTS filesystem system signals date_time regex REQUIRED)
    IF(Boost_FOUND) 
	  ADD_DEFINITIONS(-DBOOST_FILESYSTEM_VERSION=3)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for boost C++ library... - found in ${Boost_INCLUDE_DIR}")
      INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})	 
	  LINK_DIRECTORIES( ${Boost_LIBRARY_DIRS})
	  IF(WIN32)
		CREA_DEFINE( USE_BOOST )
		ADD_DEFINITIONS(-DBOOST_TT_HAS_OPERATOR_HPP_INCLUDED)
		SET(BOOST_LIBRARIES
			${Boost_LIBRARIES}
			)
			
# FCY with cmake boost, it's useless. 
# But if it is not working, uncomment	
#
#	IF(USE_DEBUG_LIB)
#			SET(BOOST_LIBRARIES
#				boost_signals-vc90-mt-gd-1_40
#				boost_filesystem-vc90-mt-gd-1_40
#				boost_date_time-vc90-mt-gd-1_40			   
#			   #what about boost_thread? JPR
#			)
#		ELSE(NOT USE_DEBUG_LIB)
#			SET(BOOST_LIBRARIES
#				boost_signals-vc90-mt-1_40
#				boost_filesystem-vc90-mt-1_40
#				boost_date_time-vc90-mt-1_40
#			)
#		ENDIF(USE_DEBUG_LIB)	
	  ENDIF(WIN32)
      IF(NOT WIN32)

		  SET(BOOST_LIBRARIES

                        ${Boost_LIBRARIES}
			boost_system-mt
			boost_thread-mt	    
			boost_program_options-mt

#EED 03 mars 2011
#EED			boost_signals-mt
#EED			boost_filesystem-mt
#EED			boost_date_time-mt
#EED			boost_system-mt
#EED			boost_program_options-mt
#EED			boost_regex-mt
#EED			boost_thread-mt	    

			)
      ENDIF(NOT WIN32)

	  MARK_AS_ADVANCED(Boost_INCLUDE_DIR)
	  MARK_AS_ADVANCED(Boost_LIBRARY_DIRS)
     
    ELSE(Boost_FOUND)
      MESSAGE( STATUS "boost C++ library not found - Set Boost_INCLUDE_DIR to the directory containing boost headers")
    ENDIF(Boost_FOUND)
  ENDIF(NOT Boost_FOUND)
 

ENDIF(USE_BOOST)
#-----------------------------------------------------------

IF(USE_XERCES)

	# FIND xerces
	IF(WIN32)
	
		SET(XERCES_LIB_DIR $ENV{XERCES_DIR})
	
		IF(NOT XERCES_LIB_DIR)  
		  MESSAGE(FATAL_ERROR "XERCES_DIR not found")  
		ELSE(NOT XERCES_LIB_DIR)
		  
		INCLUDE_DIRECTORIES( ${XERCES_LIB_DIR}/include )		
		LINK_DIRECTORIES(  ${XERCES_LIB_DIR}/lib )
		  
		ENDIF(NOT XERCES_LIB_DIR)   	
		
		IF(USE_DEBUG_LIB)
			set(XERCES_LIBRARIES  xerces-c_3D)
		ELSE(USE_DEBUG_LIB)
			set(XERCES_LIBRARIES  xerces-c_3)
		ENDIF(USE_DEBUG_LIB)
		
		MARK_AS_ADVANCED(XERCES_LIB_DIR)
	ENDIF(WIN32)


	IF(UNIX)
		IF(APPLE)
			SET(XERCES_LIBRARIES xerces-c )
			 INCLUDE_DIRECTORIES( /opt/local/include )
			 LINK_DIRECTORIES(  /opt/local/lib ) 
#JCP 10/01/11			 
		ELSE(NOT APPLE) 
			SET(XERCES_LIBRARIES xerces-c )
			 INCLUDE_DIRECTORIES( /usr/local/include )
			 LINK_DIRECTORIES(  /usr/local/lib /usr/lib) 
			#FIND_PACKAGE(Xerces)
			# Use if found
			#IF(Xerces_FOUND)
			 #  INCLUDE_DIRECTORIES( ${XERCES_INCLUDE_DIR} ) 
			#ENDIF(Xerces_FOUND)    	
		ENDIF(APPLE)
	ENDIF(UNIX)	
  
ENDIF(USE_XERCES)


ENDMACRO(CREA_FIND_AND_USE_LIBRARIES)
