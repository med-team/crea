# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


MACRO(CREA_FIND_AND_USE_VTK)
  #---------------------------------------------------------------------------
  IF(NOT VTK_FOUND)
    # Search VTK
	message("DFCH--VTK: CREAMacro_FindAndUseVTK.cmake: "${CMAKE_CURRENT_LIST_FILE})
	message("DFCH--VTK: CREAMacro_FindAndUseVTK.cmake: "${_IMPORT_PREFIX})
    FIND_PACKAGE(VTK)
    # If vtk found
    IF(VTK_FOUND)

      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for VTK... found")
      MESSAGE ( STATUS "* Dir     = ${VTK_DIR}")
      MESSAGE ( STATUS "* Version = ${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}.${VTK_BUILD_VERSION}")
      SET(VTK_BUILD_SETTINGS_FILE OFF)      
	INCLUDE(${VTK_USE_FILE})
	LINK_DIRECTORIES(${VTK_DIR}) #DFCH -- 16/7/2012

	IF(VTK_USE_GL2PS)
           	add_definitions(-DUSE_WXGLCANVAS)
	  	# CMake 2.6:
		# technically those packages are not required since one can still use the Motif/X11 version and not the gtk one:
    		FIND_PACKAGE(PkgConfig)
    		pkg_check_modules (GTK2 gtk+-2.0)
    		#MESSAGE("${GTK2_INCLUDE_DIRS}")
    		INCLUDE_DIRECTORIES(${GTK2_INCLUDE_DIRS})
    		LINK_LIBRARIES(${GTK2_LIBRARIES})
    		# Can I require all my user to have the gl lib on linux, even if they do not really need it...
    		SET(WXGLCANVASLIBS "gl")
	ENDIF(VTK_USE_GL2PS)  

      CREA_DEFINE( USE_VTK )
      CREA_DEFINE( _USE_VTK_ )
      SET(VTK_LIBRARIES
	vtkCommon     # Mandatory
	vtkFiltering  # vtkImageData
	vtkRendering  # vtkProp3D
	vtkWidgets    # vtkImagePlaneWidget
	vtkIO         # vtkMetaImageReader/Writer
	vtkImaging    # filters such as vtkImageAnisotropicDiffusion3D
	vtkGraphics   # vtkContourFilter, sources
	vtkVolumeRendering # Mappers 
	vtkGenericFiltering
	vtkHybrid
#	vtkInfovis
#	vtkViews
	)
      IF(NOT USE_ITK)
	SET(VTK_LIBRARIES ${VTK_LIBRARIES} vtkIO)
      ENDIF(NOT USE_ITK)
      MARK_AS_ADVANCED(VTK_DIR)
    ELSE(VTK_FOUND)
      message(FATAL_ERROR "VTK_DIR not found")
    ENDIF(VTK_FOUND)
  ENDIF(NOT VTK_FOUND)  
  #---------------------------------------------------------------------------
ENDMACRO(CREA_FIND_AND_USE_VTK)
