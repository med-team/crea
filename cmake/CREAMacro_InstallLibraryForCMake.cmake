# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#-----------------------------------------------------------------------------
# Macro CREA_ADVANCED_INSTALL_LIBRARY_FOR_CMAKE
# Creates and install the cmake files which allow 
# to find the library by the cmake command FIND_PACKAGE.
#
# Inputs :
# --------
#  LIBRARY_NAME : name of the library to find 
#
# The following variables **MUST** have been set previously :
#
# * ${LIBRARY_NAME}_BUILD_TREE_RELATIVE_INCLUDE_PATHS
#    The list of include paths 
#    when someone uses a *BUILD TREE* version of ${LIBRARY_NAME}.
#    NB : 
#     THE PATHS *MUST BE RELATIVE* TO THE ROOT DIR OF THE PROJECT **SOURCES** !
#    Assume your project architecture is :
#     install/ : directory in which the macro is invoked
#     src/part1/include/ : first include dir
#     src/part2/include/ : second include dir
#    Then you should set the var with :
#    SET(${LIBRARY_NAME}_BUILD_TREE_RELATIVE_INCLUDE_PATHS
#     src/part1/include
#     src/part2/include )
#    Which will result in actual include paths :
#     ${PROJECT_SOURCE_DIR}/src/part1/include;
#     ${PROJECT_SOURCE_DIR}/src/part2/include
# * ${LIBRARY_NAME}_BUILD_TREE_RELATIVE_LIBRARY_PATHS
#    Like the previous var but for the library paths.
#    NB : 
#     THE PATHS *MUST BE RELATIVE* TO THE ROOT DIR OF THE **BUILD TREE** 
#     THAT IS POINT TO THE FOLDERS WHERE THE LIBS WILL BE BUILD
#    Assume that your project architecture is :
#     src/part1/src/ : first source dir, in which the lib 'part1' is built
#     src/part2/src/ : first source dir, in which the lib 'part2' is built
#    Then you should set the var with 
#    SET(${LIBRARY_NAME}_BUILD_TREE_RELATIVE_LIBRARY_PATHS
#     src/part1/src
#     src/part2/src
#     )
#    Which will result in actual library paths :
#     ${PROJECT_BINARY_DIR}/src/part1/src
#     ${PROJECT_BINARY_DIR}/src/part2/src
# * ${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS
#    The list of include paths 
#    when someone uses an *INSTALLED* version of ${LIBRARY_NAME} 
#    The paths *MUST BE RELATIVE* to CMAKE_INSTALL_PREFIX
#  #    A typical example is "include/${LIBRARY_NAME}"
# * ${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS
#    Like the previous var but for library paths.
#    A typical example is "lib"
# * ${LIBRARY_NAME}_LIBRARIES
#    The list of libraries to link against when using ${LIBRARY_NAME}
#
# The following variables can be set optionally :
#
# * ${LIBRARY_NAME}_REQUIRED_C_FLAGS
# * ${LIBRARY_NAME}_REQUIRED_CXX_FLAGS
# * ${LIBRARY_NAME}_REQUIRED_LINK_FLAGS
# * ${LIBRARY_NAME}_MAJOR_VERSION
# * ${LIBRARY_NAME}_MINOR_VERSION
# * ${LIBRARY_NAME}_BUILD_VERSION
# * ${LIBRARY_NAME}_INSTALL_FOLDER : if set then install the generated files 
#   in CMAKE_INSTALL_PREFIX/lib/${LIBRARY_NAME}_INSTALL_FOLDER 
#   instead of CMAKE_INSTALL_PREFIX/lib/${LIBRARY_NAME}
#
# 
# To provide a user defined 
# couple of Config/Use file (in addition to the standard one) use :
#  SET( ${LIBRARY_NAME}_HAS_ADDITIONAL_CONFIG_FILE  TRUE )
# and store the *ABSOLUTE* paths to the additional files in the vars :
#  ${LIBRARY_NAME}_ADDITIONAL_CONFIG_FILE
#  ${LIBRARY_NAME}_ADDITIONAL_USE_FILE
# (e.g. ${CMAKE_CURRENT_SOURCE_DIR}/MyConfig.cmake)
#
# Outputs :
# --------
# At cmake run-time, the macro creates the following files 
# in the current dir of the build tree (where the macro is invoked) :
#  Use${LIBRARY_NAME}.cmake
#  ${LIBRARY_NAME}Config.cmake
#  ${LIBRARY_NAME}BuildSettings.cmake
# And if the vars ${LIBRARY_NAME}_ADDITIONAL_CONFIG_FILE and
# ${LIBRARY_NAME}_ADDITIONAL_USE_FILE are set, it also creates :
#  Additional${LIBRARY_NAME}Config.cmake
#  AdditionalUse${LIBRARY_NAME}.cmake
# 
# At install-time, the same files are installed 
# in CMAKE_INSTALL_PREFIX/lib/${LIBRARY_NAME}_INSTALL_FOLDER 
# and the file :
#  Find${LIBRARY_NAME}.cmake
# is installed in ${CMAKE_ROOT}/Modules/
#
#
#-----------------------------------------------------------------------------

MACRO(CREA_ADVANCED_INSTALL_LIBRARY_FOR_CMAKE LIBRARY_NAME)

  #---------------------------------------------------------------------------
  # Sets the common values to build tree and install tree configs
  SET(CILC_LIB_REQUIRED_C_FLAGS    ${${LIBRARY_NAME}_REQUIRED_C_FLAGS})
  SET(CILC_LIB_REQUIRED_CXX_FLAGS  ${${LIBRARY_NAME}_REQUIRED_CXX_FLAGS})
  SET(CILC_LIB_REQUIRED_LINK_FLAGS ${${LIBRARY_NAME}_REQUIRED_LINK_FLAGS})
  SET(CILC_LIB_MAJOR_VERSION       ${${LIBRARY_NAME}_MAJOR_VERSION})
  SET(CILC_LIB_MINOR_VERSION       ${${LIBRARY_NAME}_MINOR_VERSION})
  SET(CILC_LIB_BUILD_VERSION       ${${LIBRARY_NAME}_BUILD_VERSION})
  SET(CILC_LIB_VERSION             
    ${CILC_LIB_MAJOR_VERSION}.${CILC_LIB_MINOR_VERSION}.${CILC_LIB_BUILD_VERSION})
  SET(CILC_LIB_LIBRARIES ${${LIBRARY_NAME}_LIBRARIES})
  IF (${LIBRARY_NAME}_HAS_ADDITIONAL_CONFIG_FILE)
    SET(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE TRUE)
  ELSE (${LIBRARY_NAME}_HAS_ADDITIONAL_CONFIG_FILE)
    SET(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE FALSE)
  ENDIF (${LIBRARY_NAME}_HAS_ADDITIONAL_CONFIG_FILE)
  #---------------------------------------------------------------------------


  #---------------------------------------------------------------------------
  #-- BUILD TREE configuration 
  SET(CILC_BUILD_TREE_CONFIGURATION TRUE)
  # UseLIBRARY_NAME.cmake 
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/InstallLibraryForCMake_UseLibrary.cmake.in
    ${PROJECT_BINARY_DIR}/Use${LIBRARY_NAME}.cmake
    @ONLY IMMEDIATE
    )
  # LIBRARY_NAMEConfig.cmake 
  SET(CILC_LIB_RELATIVE_INCLUDE_PATHS 
    ${${LIBRARY_NAME}_BUILD_TREE_RELATIVE_INCLUDE_PATHS})
  SET(CILC_LIB_RELATIVE_LIBRARY_PATHS 
    ${${LIBRARY_NAME}_BUILD_TREE_RELATIVE_LIBRARY_PATHS})
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/InstallLibraryForCMake_LibraryConfig.cmake.in
    ${PROJECT_BINARY_DIR}/${LIBRARY_NAME}Config.cmake
    @ONLY IMMEDIATE
    )
  # LIBRARY_NAMEBuildSettings.cmake :
  # Save the compiler settings so another project can import them.
  # FCY : DEPREDCATED FUNCTION FOR CMAKE 2.8
  IF( "${CMAKE_MINIMUM_REQUIRED_VERSION}" VERSION_LESS 2.7)
  INCLUDE(${CMAKE_ROOT}/Modules/CMakeExportBuildSettings.cmake)
  CMAKE_EXPORT_BUILD_SETTINGS(${PROJECT_BINARY_DIR}/${LIBRARY_NAME}BuildSettings.cmake)
  ENDIF()
  # Additional Config and Use files
  IF(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE)
    CONFIGURE_FILE(
      ${${LIBRARY_NAME}_ADDITIONAL_CONFIG_FILE}
      ${PROJECT_BINARY_DIR}/Additional${LIBRARY_NAME}Config.cmake
      @ONLY IMMEDIATE
      )
    CONFIGURE_FILE(
      ${${LIBRARY_NAME}_ADDITIONAL_USE_FILE}
      ${PROJECT_BINARY_DIR}/AdditionalUse${LIBRARY_NAME}.cmake
      @ONLY IMMEDIATE
      )
  ENDIF(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE)
  #---------------------------------------------------------------------------
  

  #---------------------------------------------------------------------------
  #-- INSTALL TREE configuration 


  SET(CILC_BUILD_TREE_CONFIGURATION FALSE)
  # Create work directory to put the configured files because 
  # if the user invoked the macro at the root of the build tree the files 
  # would overwrite those configured for the build tree
  SET(CILC_WORK_DIR ${CMAKE_CURRENT_BINARY_DIR}/InstallLibraryForCMake_tmp)
  IF(NOT IS_DIRECTORY ${CILC_WORK_DIR})
    FILE(MAKE_DIRECTORY ${CILC_WORK_DIR})
  ENDIF(NOT IS_DIRECTORY ${CILC_WORK_DIR})
  
  #----------------------------------------------------------------------------
  #INSTALLATION PATH
  IF(WIN32)
    SET(INSTALL_PATH .)
  ELSE(WIN32)
    IF(${LIBRARY_NAME}_INSTALL_FOLDER)
##EED      SET(INSTALL_PATH lib/${${LIBRARY_NAME}_INSTALL_FOLDER})
      SET(INSTALL_PATH ${${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS}/${${LIBRARY_NAME}_INSTALL_FOLDER})
    ELSE(${LIBRARY_NAME}_INSTALL_FOLDER)
##EED      SET(INSTALL_PATH lib/${LIBRARY_NAME})
	   SET(INSTALL_PATH ${${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS}/${LIBRARY_NAME})
    ENDIF(${LIBRARY_NAME}_INSTALL_FOLDER)
  ENDIF(WIN32)

  message(STATUS "Library Install Path ${INSTALL_PATH}")

  # UseLIBRARY_NAME.cmake 
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/InstallLibraryForCMake_UseLibrary.cmake.in
    ${CILC_WORK_DIR}/Use${LIBRARY_NAME}.cmake
    @ONLY IMMEDIATE
    )
  INSTALL(
    FILES 
    ${CILC_WORK_DIR}/Use${LIBRARY_NAME}.cmake
    DESTINATION ${INSTALL_PATH}
    )
  # LIBRARY_NAMEConfig.cmake 
  SET(CILC_LIB_RELATIVE_INCLUDE_PATHS 
    ${${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS})
  SET(CILC_LIB_RELATIVE_LIBRARY_PATHS 
    ${${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS})
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/InstallLibraryForCMake_LibraryConfig.cmake.in
    ${CILC_WORK_DIR}/${LIBRARY_NAME}Config.cmake
    @ONLY IMMEDIATE
    )
  INSTALL(
    FILES 
    ${CILC_WORK_DIR}/${LIBRARY_NAME}Config.cmake
    DESTINATION ${INSTALL_PATH}
    )
  # LIBRARY_NAMEBuildSettings.cmake :
  # FCY : DEPREDCATED FUNCTION FOR CMAKE 2.8
  IF( "${CMAKE_MINIMUM_REQUIRED_VERSION}" VERSION_LESS 2.7)
  CMAKE_EXPORT_BUILD_SETTINGS(${CILC_WORK_DIR}/${LIBRARY_NAME}BuildSettings.cmake)
  ENDIF()
  INSTALL(
    FILES 
    ${CILC_WORK_DIR}/${LIBRARY_NAME}BuildSettings.cmake
    DESTINATION  ${INSTALL_PATH}
    )
  # Additional Config and Use files
  IF(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE)
    CONFIGURE_FILE(
      ${${LIBRARY_NAME}_ADDITIONAL_CONFIG_FILE}
      ${CILC_WORK_DIR}/Additional${LIBRARY_NAME}Config.cmake
      @ONLY IMMEDIATE
      )
    INSTALL(
      FILES 
      ${CILC_WORK_DIR}/Additional${LIBRARY_NAME}Config.cmake
      DESTINATION ${INSTALL_PATH}
      )
    CONFIGURE_FILE(
      ${${LIBRARY_NAME}_ADDITIONAL_USE_FILE}
      ${CILC_WORK_DIR}/AdditionalUse${LIBRARY_NAME}.cmake
      @ONLY IMMEDIATE
      )
    INSTALL(
      FILES 
      ${CILC_WORK_DIR}/AdditionalUse${LIBRARY_NAME}.cmake
      DESTINATION  ${INSTALL_PATH}
      )
  ENDIF(CILC_LIB_HAS_ADDITIONAL_CONFIG_FILE)
  # Specific to install tree
  # FindLIBRARY_NAME.cmake in CMake/Modules dir
  CONFIGURE_FILE(
    ${CREA_CMAKE_DIR}/InstallLibraryForCMake_FindLibrary.cmake.in
    ${CILC_WORK_DIR}/Find${LIBRARY_NAME}.cmake
    @ONLY IMMEDIATE
    )
#EED 02 mars 2011
#  INSTALL( 
#    FILES ${CILC_WORK_DIR}/Find${LIBRARY_NAME}.cmake
#    DESTINATION ${CMAKE_ROOT}/Modules 
#    )

# JPR 04 Mars 2011
  INSTALL( 
    FILES ${CILC_WORK_DIR}/Find${LIBRARY_NAME}.cmake
##EED12Fev2013    DESTINATION ${CMAKE_INSTALL_PREFIX}/share/cmake/Modules
    DESTINATION share/cmake/Modules
    )
    
 
  #---------------------------------------------------------------------------
  



ENDMACRO(CREA_ADVANCED_INSTALL_LIBRARY_FOR_CMAKE)
#-----------------------------------------------------------------------------




#-----------------------------------------------------------------------------
MACRO(CREA_INSTALL_LIBRARY_FOR_CMAKE LIBRARY_NAME1)
  
  # Sets the settings to default values
  IF(NOT ${LIBRARY_NAME1}_INSTALL_FOLDER)
    SET(${LIBRARY_NAME1}_INSTALL_FOLDER ${LIBRARY_NAME1})
  ENDIF(NOT ${LIBRARY_NAME1}_INSTALL_FOLDER)

  SET(${LIBRARY_NAME1}_LIBRARIES ${LIBRARY_NAME1})
  
  FILE(RELATIVE_PATH 
    ${LIBRARY_NAME1}_BUILD_TREE_RELATIVE_INCLUDE_PATHS 
    ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}
    )
  IF ( ${PROJECT_BINARY_DIR} STREQUAL ${EXECUTABLE_OUTPUT_PATH} )
    SET(CILFC_EXECUTABLE_OUTPUT_REL_PATH ".")
  ELSE ( ${PROJECT_BINARY_DIR} STREQUAL ${EXECUTABLE_OUTPUT_PATH} )

    FILE(RELATIVE_PATH 
      CILFC_EXECUTABLE_OUTPUT_REL_PATH
      ${PROJECT_BINARY_DIR} ${EXECUTABLE_OUTPUT_PATH})
  ENDIF ( ${PROJECT_BINARY_DIR} STREQUAL ${EXECUTABLE_OUTPUT_PATH} )


  IF(UNIX)
    SET(${LIBRARY_NAME1}_BUILD_TREE_RELATIVE_LIBRARY_PATHS 
      ${CILFC_EXECUTABLE_OUTPUT_REL_PATH})
    SET(${LIBRARY_NAME1}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS ${CMAKE_CREA_LIB_PATH} )
  ELSE(UNIX)
    SET(${LIBRARY_NAME1}_BUILD_TREE_RELATIVE_LIBRARY_PATHS 
      ${CILFC_EXECUTABLE_OUTPUT_REL_PATH} )
# LG 15/01/09 : Bug
#      ${CILFC_EXECUTABLE_OUTPUT_REL_PATH}/Debug
#     ${CILFC_EXECUTABLE_OUTPUT_REL_PATH}/Release)
    SET(${LIBRARY_NAME1}_INSTALL_TREE_RELATIVE_LIBRARY_PATHS bin)
  ENDIF(UNIX)

  IF(NOT ${LIBRARY_NAME1}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS)
    SET(${LIBRARY_NAME1}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS include/${LIBRARY_NAME1})
  ENDIF(NOT ${LIBRARY_NAME1}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS)
  
  # Invoke the advanced macro
  CREA_ADVANCED_INSTALL_LIBRARY_FOR_CMAKE(${LIBRARY_NAME1})


ENDMACRO(CREA_INSTALL_LIBRARY_FOR_CMAKE)
#-----------------------------------------------------------------------------
