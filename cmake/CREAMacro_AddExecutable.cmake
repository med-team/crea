# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#Generates the cmake commands to build and install the executable EXE_NAME.
# ${EXE_NAME}_SOURCES        must contain the list of files to compile 
#                            to build the executable.
# ${EXE_NAME}_LINK_LIBRARIES must contain the list of libraries to link with
#
# On Windows:
# if ${EXE_NAME}_HAS_GUI is set to TRUE then a Win32 application is generated
# if ${EXE_NAME}_CONSOLE is set to TRUE then the application will have a console attached.
MACRO(CREA_ADD_EXECUTABLE EXE_NAME)

  IF(CREA_VERBOSE_CMAKE)
    MESSAGE(STATUS "===============================================")
    MESSAGE(STATUS "Configuring executable ${EXE_NAME}")
    MESSAGE(STATUS "   Sources  : ${${EXE_NAME}_SOURCES}")
    MESSAGE(STATUS "   Link libs: ${${EXE_NAME}_LINK_LIBRARIES}")
    IF(WIN32)
      MESSAGE(STATUS "   Win32 app: ${${EXE_NAME}_HAS_GUI}")
      MESSAGE(STATUS "   Console  : ${${EXE_NAME}_CONSOLE}")
    ENDIF(WIN32)
    MESSAGE(STATUS "===============================================")
  ENDIF(CREA_VERBOSE_CMAKE)

  IF(WIN32 AND ${EXE_NAME}_HAS_GUI )
    ADD_EXECUTABLE(${EXE_NAME} WIN32 ${${EXE_NAME}_SOURCES})
    IF( ${${EXE_NAME}_CONSOLE} )
      SET_TARGET_PROPERTIES(${EXE_NAME} PROPERTIES LINK_FLAGS /subsystem:console )
    ENDIF( ${${EXE_NAME}_CONSOLE} )
  ELSE(WIN32 AND ${EXE_NAME}_HAS_GUI )
    IF(${EXE_NAME}_HAS_GUI AND APPLE)
      ADD_EXECUTABLE(${EXE_NAME} MACOSX_BUNDLE ${${EXE_NAME}_SOURCES})
    ELSE(${EXE_NAME}_HAS_GUI AND APPLE)
      ADD_EXECUTABLE(${EXE_NAME}  ${${EXE_NAME}_SOURCES})
    ENDIF(${EXE_NAME}_HAS_GUI AND APPLE)
  ENDIF(WIN32 AND ${EXE_NAME}_HAS_GUI )
  
  TARGET_LINK_LIBRARIES(${EXE_NAME} ${${EXE_NAME}_LINK_LIBRARIES})

  INSTALL_TARGETS(/bin/ ${EXE_NAME})

ENDMACRO(CREA_ADD_EXECUTABLE)


