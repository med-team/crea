# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


# Creates and installs a library
# Uses CREA_DYNAMIC_LIBRARY_EXPORT and CREA_INSTALL_LIBRARY
# hence the variables to be set for these are needed 
# Additionnaly one needs to set :
# ${LIBRARY_NAME}_HEADERS : the list of headers to install
# ${LIBRARY_NAME}_SOURCES : the list of files to compile to build the lib
# ${LIBRARY_NAME}_LINK_LIBRARIES : the list of libs to link with
MACRO(CREA_ADD_LIBRARY LIBRARY_NAME)

  IF(CREA_VERBOSE_CMAKE)
    MESSAGE(STATUS "===============================================")
    MESSAGE(STATUS "Configuring library ${LIBRARY_NAME}")
    MESSAGE(STATUS "   Headers  : ${${LIBRARY_NAME}_HEADERS}")
    MESSAGE(STATUS "   Sources  : ${${LIBRARY_NAME}_SOURCES}")
    MESSAGE(STATUS "   Link libs: ${${LIBRARY_NAME}_LINK_LIBRARIES}")
    MESSAGE(STATUS "===============================================")
  ENDIF(CREA_VERBOSE_CMAKE)


  # MANAGE SHARED LIB
  CREA_DYNAMIC_LIBRARY_EXPORT_OPTION(${LIBRARY_NAME})

  # CREATE THE TARGET
  ADD_LIBRARY(${LIBRARY_NAME} ${${LIBRARY_NAME}_SHARED} ${${LIBRARY_NAME}_SOURCES})

  # LINK
  TARGET_LINK_LIBRARIES(${LIBRARY_NAME} ${${LIBRARY_NAME}_LINK_LIBRARIES})

  # INSTALLS LIBRARY FOR CMAKE
  CREA_INSTALL_LIBRARY_FOR_CMAKE(${LIBRARY_NAME})

  # INSTALLS LIBRARY
  INSTALL(
    FILES ${${LIBRARY_NAME}_HEADERS}
    DESTINATION ${${LIBRARY_NAME}_INSTALL_TREE_RELATIVE_INCLUDE_PATHS}
    )  

IF(WIN32)	
  INSTALL(
    TARGETS ${LIBRARY_NAME}
    DESTINATION bin)
ELSE(WIN32)
  INSTALL(
    TARGETS ${LIBRARY_NAME}
    DESTINATION ${CMAKE_CREA_LIB_PATH} )
ENDIF(WIN32)

#EED 01/July/2011
IF (UNIX)
  add_definitions(-fPIC)
ENDIF(UNIX)


ENDMACRO(CREA_ADD_LIBRARY)
