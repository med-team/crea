
IF (GTK2_LIBRARIES AND GTK2_INCLUDE_DIRS)
  # in cache already
  SET(GTK2_FOUND TRUE)
ELSE (GTK2_LIBRARIES AND GTK2_INCLUDE_DIRS)
  IF(UNIX)
    # use pkg-config to get the directories and then use these values
    # in the FIND_PATH() and FIND_LIBRARY() calls
    INCLUDE(UsePkgConfig)

#    PKGCONFIG
FindPkgConfig(gtk-2.0 _GTK22IncDir _GTK22LinkDir _GTK22LinkFlags _GTK22Cflags)

#   pkg_search_module(<PREFIX> [REQUIRED] <MODULE> [<MODULE>]*)
#     checks for given modules and uses the first working one

    FIND_PATH(GTK2_GTK_INCLUDE_PATH gtk/gtk.h
      $ENV{GTK2_HOME}
      ${_GTK22IncDir}
      /usr/include/gtk-2.0
      /usr/local/include/gtk-2.0
      /opt/include/gtk-2.0
      /opt/gnome/include/gtk-2.0
      /sw/include/gtk-2.0
    )

    # Some Linux distributions (e.g. Red Hat) have glibconfig.h
    # and glib.h in different directories, so we need to look
    # for both.
    #  - Atanas Georgiev <atanas@cs.columbia.edu>
    PKGCONFIG(glib-2.0 _GLIB2IncDir _GLIB2inkDir _GLIB2LinkFlags _GLIB2Cflags)
    PKGCONFIG(gmodule-2.0 _GMODULE2IncDir _GMODULE2inkDir _GMODULE2LinkFlags _GMODULE2Cflags)
    SET(GDIR /opt/gnome/lib/glib-2.0/include)

    FIND_PATH(GTK2_GLIBCONFIG_INCLUDE_PATH glibconfig.h
      ${_GLIB2IncDir}
      /opt/gnome/lib64/glib-2.0/include
      /opt/gnome/lib/glib-2.0/include
      /opt/lib/glib-2.0/include
      /usr/lib64/glib-2.0/include
      /usr/lib/glib-2.0/include
      /sw/lib/glib-2.0/include
    )
    #MESSAGE(STATUS "DEBUG: GTK2_GLIBCONFIG_INCLUDE_PATH = ${GTK2_GLIBCONFIG_INCLUDE_PATH}")

    FIND_PATH(GTK2_GLIB_INCLUDE_PATH glib.h
      ${_GLIB2IncDir}
      /opt/include/glib-2.0
      /opt/gnome/include/glib-2.0
      /usr/include/glib-2.0
      /sw/include/glib-2.0
    )
    #MESSAGE(STATUS "DEBUG: GTK2_GLIBCONFIG_INCLUDE_PATH = ${GTK2_GLIBCONFIG_INCLUDE_PATH}")

    FIND_PATH(GTK2_GTKGL_INCLUDE_PATH gtkgl/gtkglarea.h
      ${_GLIB2IncDir}
      /usr/include
      /usr/local/include
      /usr/openwin/share/include
      /opt/gnome/include
      /opt/include
      /sw/include
    )

    PKGCONFIG(pango _PANGOIncDir _PANGOinkDir _PANGOLinkFlags _PANGOCflags)

    FIND_PATH(GTK2_PANGO_INCLUDE_PATH pango/pango.h
      ${_PANGOIncDir}
      /opt/gnome/include/pango-1.0
      /opt/include/pango-1.0
      /usr/include/pango-1.0
      /sw/include/pango-1.0
    )

    PKGCONFIG(gdk-2.0 _GDK2IncDir _GDK2inkDir _GDK2LinkFlags _GDK2Cflags)

    FIND_PATH(GTK2_GDKCONFIG_INCLUDE_PATH gdkconfig.h
      ${_GDK2IncDir}
      /opt/gnome/lib/gtk-2.0/include
      /opt/gnome/lib64/gtk-2.0/include
      /opt/lib/gtk-2.0/include
      /usr/lib/gtk-2.0/include
      /usr/lib64/gtk-2.0/include
      /sw/lib/gtk-2.0/include
    )

    # Dave:
    FIND_PATH(GTK2_GDK_INCLUDE_PATH gdk.h
      ${_GDK2IncDir}
      /usr/include/gtk-2.0
      /usr/include/gtk-2.0/gdk
    )
    #MESSAGE(STATUS "DEBUG: GTK2_GLIBCONFIG_INCLUDE_PATH = ${GTK2_GLIBCONFIG_INCLUDE_PATH}")

    PKGCONFIG(cairo _CAIROIncDir _CAIROinkDir _CAIROLinkFlags _CAIROCflags)

    FIND_PATH(GTK2_CAIRO_INCLUDE_PATH cairo.h
      ${_CAIROIncDir}
      /opt/gnome/include/cairo
      /usr/include
      /usr/include/cairo
      /opt/include
      /opt/include/cairo
      /sw/include
      /sw/include/cairo
    )
    #MESSAGE(STATUS "DEBUG: GTK2_CAIRO_INCLUDE_PATH = ${GTK2_CAIRO_INCLUDE_PATH}")

    PKGCONFIG(atk _ATKIncDir _ATKinkDir _ATKLinkFlags _ATKCflags)

    FIND_PATH(GTK2_ATK_INCLUDE_PATH atk/atk.h
      ${_ATKIncDir}
      /opt/gnome/include/atk-1.0
      /usr/include/atk-1.0
      /opt/include/atk-1.0
      /sw/include/atk-1.0
    )
    #MESSAGE(STATUS "DEBUG: GTK2_ATK_INCLUDE_PATH = ${GTK2_ATK_INCLUDE_PATH}")

    FIND_LIBRARY(GTK2_GTKGL_LIBRARY
      NAMES
        gtkgl
      PATHS
        ${_GTK22IncDir}
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_GTK_LIBRARY
      NAMES
        gtk-x11-2.0
      PATHS
        ${_GTK22LinkDir}
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_GDK_LIBRARY
      NAMES
        gdk-x11-2.0
      PATHS
        ${_GDK2LinkDir}
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_GMODULE_LIBRARY
      NAMES
        gmodule-2.0
      PATHS
        ${_GMODULE2inkDir}
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_GLIB_LIBRARY
      NAMES
        glib-2.0
      PATHS
        ${_GLIB2inkDir}
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_Xi_LIBRARY 
      NAMES
        Xi
      PATHS 
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    FIND_LIBRARY(GTK2_GTHREAD_LIBRARY
      NAMES
        gthread-2.0
      PATHS
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )


    FIND_LIBRARY(GTK2_GOBJECT_LIBRARY
      NAMES
        gobject-2.0
      PATHS
        /usr/lib
        /usr/local/lib
        /usr/openwin/lib
        /usr/X11R6/lib
        /opt/gnome/lib
        /opt/lib
        /sw/lib
    )

    IF(GTK2_GTK_INCLUDE_PATH)
      IF(GTK2_GLIBCONFIG_INCLUDE_PATH)
        IF(GTK2_GLIB_INCLUDE_PATH)
          IF(GTK2_GTK_LIBRARY)
            IF(GTK2_GLIB_LIBRARY)
              IF(GTK2_PANGO_INCLUDE_PATH)
                IF(GTK2_ATK_INCLUDE_PATH)
                  IF(GTK2_CAIRO_INCLUDE_PATH)
                    # Assume that if gtk and glib were found, the other
                    # supporting libraries have also been found.

                    SET(GTK2_FOUND TRUE)

                    SET(GTK2_INCLUDE_DIRS
                      ${GTK2_GTK_INCLUDE_PATH}
                      ${GTK2_GLIBCONFIG_INCLUDE_PATH}
                      ${GTK2_GLIB_INCLUDE_PATH}
                      ${GTK2_PANGO_INCLUDE_PATH}
                      ${GTK2_GDKCONFIG_INCLUDE_PATH}
                      # Dave:
                      ${GTK2_GDK_INCLUDE_PATH}
                      ${GTK2_ATK_INCLUDE_PATH}
                      ${GTK2_CAIRO_INCLUDE_PATH}
                    )

                    SET(GTK2_LIBRARIES
                      ${GTK2_GTK_LIBRARY}
                      ${GTK2_GDK_LIBRARY}
                      ${GTK2_GLIB_LIBRARY}
                    )
                    #${GTK2_GOBJECT_LIBRARY})

                    IF(GTK2_GMODULE_LIBRARY)
                      SET(GTK2_LIBRARIES
                        ${GTK2_LIBRARIES}
                        ${GTK2_GMODULE_LIBRARY}
                      )
                    ENDIF(GTK2_GMODULE_LIBRARY)

                    IF(GTK2_GTHREAD_LIBRARY)
                      SET(GTK2_LIBRARIES
                        ${GTK2_LIBRARIES}
                        ${GTK2_GTHREAD_LIBRARY}
                      )
                    SET(GTK2_LIBRARIES ${GTK2_LIBRARIES})
                    ENDIF(GTK2_GTHREAD_LIBRARY)
                  ELSE(GTK2_CAIRO_INCLUDE_PATH)
                    MESSAGE(STATUS "Can not find cairo")
                  ENDIF(GTK2_CAIRO_INCLUDE_PATH)
                ELSE(GTK2_ATK_INCLUDE_PATH)
                  MESSAGE(STATUS "Can not find atk")
                ENDIF(GTK2_ATK_INCLUDE_PATH)
              ELSE(GTK2_PANGO_INCLUDE_PATH)
                MESSAGE(STATUS "Can not find pango includes")
              ENDIF(GTK2_PANGO_INCLUDE_PATH)
            ELSE(GTK2_GLIB_LIBRARY)
              MESSAGE(STATUS "Can not find glib lib")
            ENDIF(GTK2_GLIB_LIBRARY)
          ELSE(GTK2_GTK_LIBRARY)
            MESSAGE(STATUS "Can not find gtk lib")
          ENDIF(GTK2_GTK_LIBRARY)
        ELSE(GTK2_GLIB_INCLUDE_PATH)
          MESSAGE(STATUS "Can not find glib includes")
        ENDIF(GTK2_GLIB_INCLUDE_PATH)
      ELSE(GTK2_GLIBCONFIG_INCLUDE_PATH)
        MESSAGE(STATUS "Can not find glibconfig")
      ENDIF(GTK2_GLIBCONFIG_INCLUDE_PATH)
    ELSE (GTK2_GTK_INCLUDE_PATH)
      MESSAGE(STATUS "Can not find gtk includes")
    ENDIF (GTK2_GTK_INCLUDE_PATH)

    IF (GTK2_FOUND)
      IF (NOT GTK2_FIND_QUIETLY)
        MESSAGE(STATUS "Looking for gtk2... - found ${GTK2_LIBRARIES}")
      ENDIF (NOT GTK2_FIND_QUIETLY)
    ELSE (GTK2_FOUND)
      IF (GTK2_FIND_REQUIRED)
        MESSAGE(SEND_ERROR "Could NOT find GTK2")
      ENDIF (GTK2_FIND_REQUIRED)
    ENDIF (GTK2_FOUND)

    MARK_AS_ADVANCED(
      GTK2_GDK_LIBRARY
      GTK2_GLIB_INCLUDE_PATH
      GTK2_GLIB_LIBRARY
      GTK2_GLIBCONFIG_INCLUDE_PATH
      GTK2_GMODULE_LIBRARY
      GTK2_GTHREAD_LIBRARY
      GTK2_Xi_LIBRARY
      GTK2_GTK_INCLUDE_PATH
      GTK2_GTK_LIBRARY
      GTK2_GTKGL_INCLUDE_PATH
      GTK2_GTKGL_LIBRARY
      GTK2_ATK_INCLUDE_PATH
      GTK2_GDKCONFIG_INCLUDE_PATH
      # Dave:
      GTK2_GDK_INCLUDE_PATH
      #GTK2_GOBJECT_LIBRARY
      GTK2_PANGO_INCLUDE_PATH
      # LG :
      GTK2_CAIRO_INCLUDE_PATH
      GTK2_GOBJECT_LIBRARY
    )
  ENDIF(UNIX)
ENDIF (GTK2_LIBRARIES AND GTK2_INCLUDE_DIRS)

