# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ */ 


#=======================================================================
# Looks for WxWidgets
#=======================================================================
MACRO(CREA_FIND_WXWIDGETS)
  IF(WIN32)
    # Search WXWIDGETS
    
    #  WXWINDOWS_LIBRARY         = full path to the wxWindows library and linker flags on unix
    #  CMAKE_WX_CXX_FLAGS        = compiler flags for building wxWindows 
    #  WXWINDOWS_INCLUDE_PATH    = include path of wxWindows
    SET(WXWINDOWS_USE_GL 1)
    
    #wxWidgets build related stuff
    # DO NOT FORCE DEBUG LIBS !!
	SET(WXW_USE_DEBUG OFF)
    SET(WXW_USE_UNICODE OFF)
    SET(WXW_USE_SHARED ON)
    SET(WXW_USE_UNIV OFF)
    SET(WXW_USE_MONO OFF)
    SET(WXW_FILE_VERSION "28")
    SET(WXW_VERSION "2.8")
    
    #CMake Options
    # Why forcing verbosity ?
    # SET(CMAKE_VERBOSE_MAKEFILE TRUE)
    
    INCLUDE (${CREA_CMAKE_DIR}/FindWxWidgetsWin32.cmake)
    #
  
  ELSE(WIN32)
    IF(NOT wxWidgets_FOUND)
    # CMake 2.6:
    # technically those packages are not required 
    # since one can still use the Motif/X11 version and not the gtk one:
    FIND_PACKAGE(PkgConfig)
    pkg_check_modules (GTK2 gtk+-2.0)
    #MESSAGE("${GTK2_INCLUDE_DIRS}")
    # Can I require all my user to have the gl lib on linux, even if they do not really need it...
#EED  SET(WXGLCANVASLIBS "gl")
    # aui ?
    SET(WXAUILIBS "aui")
    # LG : These options should be set by the user at top level of crea
    FIND_PACKAGE(wxWidgets COMPONENTS base core adv html 
      ${WXAUILIBS}
      ${WXGLCANVASLIBS})
    #
    IF(wxWidgets_FOUND)
      MESSAGE ( STATUS "=======================================")
      MESSAGE ( STATUS "Looking for wxWidgets... found ${wxWidgets_CONFIG_EXECUTABLE}")
    ENDIF(wxWidgets_FOUND)
    ENDIF(NOT wxWidgets_FOUND)
  ENDIF(WIN32)
  


ENDMACRO(CREA_FIND_WXWIDGETS)
#=======================================================================

#=======================================================================
MACRO(CREA_USE_WXWIDGETS)
  # Preprocessor settings
  CREA_DEFINE( USE_WXWIDGETS )
  CREA_DEFINE( _USE_WXWIDGETS_ )
  #
  IF(WIN32)
    # Clean ?
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}${WXWIDGETS_CXX_FLAGS}")
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}${WXWIDGETS_EXE_LINKER_FLAGS}")
    #
    ADD_DEFINITIONS( ${WXWIDGETS_DEFINITIONS}  )
    #
    INCLUDE_DIRECTORIES(${WXWIDGETS_INCLUDE_DIR})
    LINK_DIRECTORIES( ${WXWIDGETS_LINK_DIRECTORIES} )
    SET(WXWIDGETS_LIBRARIES
      ${WXWIDGETS_LIBRARIES}  
#DO NOT FORCE DEBUG LIBS !!
      debug wxmsw28d_aui
      optimized wxmsw28_aui
      debug msvcrtd
      debug msvcprtd
      optimized msvcrt
      optimized msvcprt
      )
  ELSE(WIN32)
    # GTK2
    INCLUDE_DIRECTORIES(${GTK2_INCLUDE_DIRS})
    LINK_DIRECTORIES(${GTK2_LIBRARY_DIRS})
    LINK_LIBRARIES(${GTK2_LIBRARIES})
    # WX


#    INCLUDE( ${GTK_USE_FILE} )
    INCLUDE( ${wxWidgets_USE_FILE} )
    MARK_AS_ADVANCED(wxWidgets_CONFIG_EXECUTABLE)
    SET(WXWIDGETS_LIBRARIES
      ${GTK2_LIBRARIES}
      #${GTK_LIBRARIES}
      ${wxWidgets_LIBRARIES}
      )
  ENDIF(WIN32)
ENDMACRO(CREA_USE_WXWIDGETS)
#=======================================================================

#=======================================================================
MACRO(CREA_FIND_AND_USE_WXWIDGETS)
  CREA_FIND_WXWIDGETS()
  CREA_USE_WXWIDGETS()
ENDMACRO(CREA_FIND_AND_USE_WXWIDGETS)
#=======================================================================
