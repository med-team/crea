/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include <creaMessageManager.h>
#include <creaSystem.h>

int main(int argc, char* argv[])
{	
  std::cout << "** creaSample_MessageManager : begin of main "<<std::endl;

  crea::MessageManager::RegisterMessageType("TEST","test messages registered by creaSample_MessageManager main",2);
  crea::MessageManager::SetMessageLevel("TEST",4);

  crea::MessageManager::PrintInfo();

  creaMessage("TEST",1,"hi ! this is a message of category 'TEST', level 1"<<std::endl);
  creaMessage("TEST",5,"hi ! this is a message of category 'TEST', level 5"<<std::endl);
  crea::MessageManager::SetMessageLevel("TEST",5);
  creaMessage("TEST",5,"hi ! this is a message of category 'TEST', level 5"<<std::endl);

  creaDebugMessage("TEST",5,"hi ! this is a debug message of category 'TEST', level 5, you can only see it if you set crea_COMPILE_DEBUG_MESSAGES to ON with cmake"<<std::endl);

  std::cout << "** creaSample_MessageManager : end of main "<<std::endl;
}

