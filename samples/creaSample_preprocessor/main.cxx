/*
# ---------------------------------------------------------------------
#
# Copyright (c) CREATIS (Centre de Recherche en Acquisition et Traitement de l'Image 
#                        pour la Sant�)
# Authors : Eduardo Davila, Frederic Cervenansky, Claire Mouton
# Previous Authors : Laurent Guigues, Jean-Pierre Roux
# CreaTools website : www.creatis.insa-lyon.fr/site/fr/creatools_accueil
#
#  This software is governed by the CeCILL-B license under French law and 
#  abiding by the rules of distribution of free software. You can  use, 
#  modify and/ or redistribute the software under the terms of the CeCILL-B 
#  license as circulated by CEA, CNRS and INRIA at the following URL 
#  http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html 
#  or in the file LICENSE.txt.
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability. 
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL-B license and that you accept its terms.
# ------------------------------------------------------------------------ 
*/ 

#include <creaSystem.h>

int main(int argc, char* argv[])
{	
 std::cout << "** creaSample_preprocessor : begin of main "<<std::endl;

#if defined (DEF1)
 std::cout << "* DEF1 is defined "<<std::endl;
 std::cout << "  Value = [" << CREA_STRINGIFY_SYMBOL(DEF1) << "]" << std::endl;
#endif 

#if defined (DEF2)
  std::cout << "* DEF2 is defined "<<std::endl;
  std::cout << "  Value = [" << CREA_STRINGIFY_SYMBOL(DEF2) << "]" << std::endl;
#endif

#if defined (DEF3)
  std::cout << "* DEF3 is defined "<<std::endl;
  std::cout << "  Value = [" << CREA_STRINGIFY_SYMBOL(DEF3) << "]" << std::endl;
#endif


	
 std::cout << "** creaSample_preprocessor : end of main "<<std::endl;
}
